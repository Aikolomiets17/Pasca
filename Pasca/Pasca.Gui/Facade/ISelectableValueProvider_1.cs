﻿using System.Threading.Tasks;

namespace Pasca.Gui.Facade
{
    public interface ISelectableValueProvider<T> : ISelectableValueProvider
    {
        T TrySelect();
        T GetVisibleData(T data);
    }
}