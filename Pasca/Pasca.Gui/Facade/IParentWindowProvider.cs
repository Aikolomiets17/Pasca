﻿using System;

namespace Pasca.Gui.Facade
{
    public interface IParentWindowProvider
    {
        IntPtr? GetParentWindowHandle();
    }
}