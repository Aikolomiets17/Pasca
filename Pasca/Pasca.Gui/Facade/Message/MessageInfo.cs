﻿namespace Pasca.Gui.Facade.Message
{
    public sealed class MessageInfo
    {
        public string Body { get; set; }
        public string Title { get; set; }
        public MessageButtons Buttons { get; set; }

        public static implicit operator MessageInfo(string body) =>
            new MessageInfo {
                Body = body
            };
    }
}
