﻿namespace Pasca.Gui.Facade.Message
{
    public enum MessageResult
    {
        //
        // Summary:
        //     The result value of the message box is OK.
        Ok = 1,
        //
        // Summary:
        //     The result value of the message box is Cancel.
        Cancel = 2,
        //
        // Summary:
        //     The result value of the message box is Yes.
        Yes = 6,
        //
        // Summary:
        //     The result value of the message box is No.
        No = 7
    }
}
