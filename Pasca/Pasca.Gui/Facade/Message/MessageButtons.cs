﻿namespace Pasca.Gui.Facade.Message
{
    public enum MessageButtons
    {
        //
        // Summary:
        //     The message box displays an OK button.
        Ok = 0,
        //
        // Summary:
        //     The message box displays OK and Cancel buttons.
        OkCancel = 1,
        //
        // Summary:
        //     The message box displays Yes, No, and Cancel buttons.
        YesNoCancel = 3,
        //
        // Summary:
        //     The message box displays Yes and No buttons.
        YesNo = 4
    }
}
