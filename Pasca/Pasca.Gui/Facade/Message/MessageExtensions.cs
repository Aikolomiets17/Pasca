﻿using System.Windows;
using System.Windows.Forms;
using Pasca.Common.Exceptions;

namespace Pasca.Gui.Facade.Message
{
    public static class MessageExtensions
    {
        #region Buttons

        public static MessageBoxButtons ToWinFormsMessageBoxButtons(this MessageButtons buttons)
        {
            switch (buttons)
            {
            case MessageButtons.Ok:
                return MessageBoxButtons.OK;
            case MessageButtons.OkCancel:
                return MessageBoxButtons.OKCancel;
            case MessageButtons.YesNoCancel:
                return MessageBoxButtons.YesNoCancel;
            case MessageButtons.YesNo:
                return MessageBoxButtons.YesNo;
            }

            throw new EnumValueException(typeof(MessageButtons), buttons);
        }

        public static MessageBoxButton ToWpfMessageBoxButton(this MessageButtons buttons)
        {
            switch (buttons)
            {
            case MessageButtons.Ok:
                return MessageBoxButton.OK;
            case MessageButtons.OkCancel:
                return MessageBoxButton.OKCancel;
            case MessageButtons.YesNoCancel:
                return MessageBoxButton.YesNoCancel;
            case MessageButtons.YesNo:
                return MessageBoxButton.YesNo;
            }

            throw new EnumValueException(typeof(MessageButtons), buttons);
        }

        public static MessageButtons ToMessageButtons(this MessageBoxButtons buttons)
        {
            switch (buttons)
            {
            case MessageBoxButtons.OK:
                return MessageButtons.Ok;
            case MessageBoxButtons.OKCancel:
                return MessageButtons.OkCancel;
            case MessageBoxButtons.YesNoCancel:
                return MessageButtons.YesNoCancel;
            case MessageBoxButtons.YesNo:
                return MessageButtons.YesNo;
            }

            throw new EnumValueException(typeof(MessageBoxButtons), buttons);
        }

        public static MessageButtons ToMessageButtons(this MessageBoxButton buttons)
        {
            switch (buttons)
            {
            case MessageBoxButton.OK:
                return MessageButtons.Ok;
            case MessageBoxButton.OKCancel:
                return MessageButtons.OkCancel;
            case MessageBoxButton.YesNoCancel:
                return MessageButtons.YesNoCancel;
            case MessageBoxButton.YesNo:
                return MessageButtons.YesNo;
            }

            throw new EnumValueException(typeof(MessageBoxButtons), buttons);
        }

        #endregion

        #region Result

        public static DialogResult ToWinFormsDialogResult(this MessageResult result)
        {
            switch (result)
            {
            case MessageResult.Ok:
                return DialogResult.OK;
            case MessageResult.Cancel:
                return DialogResult.Cancel;
            case MessageResult.Yes:
                return DialogResult.Yes;
            case MessageResult.No:
                return DialogResult.No;
            }

            throw new EnumValueException(typeof(MessageResult), result);
        }

        public static MessageBoxResult ToWpfMessageBoxResult(this MessageResult result)
        {
            switch (result)
            {
            case MessageResult.Ok:
                return MessageBoxResult.OK;
            case MessageResult.Cancel:
                return MessageBoxResult.Cancel;
            case MessageResult.Yes:
                return MessageBoxResult.Yes;
            case MessageResult.No:
                return MessageBoxResult.No;
            }

            throw new EnumValueException(typeof(MessageResult), result);
        }

        public static MessageResult ToMessageResult(this DialogResult result)
        {
            switch (result)
            {
            case DialogResult.OK:
                return MessageResult.Ok;
            case DialogResult.Cancel:
                return MessageResult.Cancel;
            case DialogResult.Yes:
                return MessageResult.Yes;
            case DialogResult.No:
                return MessageResult.No;
            default:
                return MessageResult.Cancel;
            }
        }

        public static MessageResult ToMessageResult(this MessageBoxResult result)
        {
            switch (result)
            {
            case MessageBoxResult.OK:
                return MessageResult.Ok;
            case MessageBoxResult.Cancel:
                return MessageResult.Cancel;
            case MessageBoxResult.Yes:
                return MessageResult.Yes;
            case MessageBoxResult.No:
                return MessageResult.No;
            default:
                return MessageResult.Cancel;
            }
        }

        #endregion
    }
}
