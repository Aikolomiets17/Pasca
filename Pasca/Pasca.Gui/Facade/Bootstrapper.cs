﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Threading;
using FirstFloor.ModernUI;
using Microsoft.Windows.Shell;
using Pasca.Common.Auth;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Providers;
using Pasca.Common.Settings;
using Pasca.Common.Students;
using Pasca.Common.Trace;
using Pasca.Common.Types;
using Pasca.Gui.Exceptions;
using Pasca.Gui.Facade.Message;
using Pasca.Gui.Trace;
using Pasca.Gui.View.Artifacts;
using Pasca.Gui.View.Auth;
using Pasca.Gui.View.Charts;
using Pasca.Gui.View.Mapping;
using Pasca.Gui.View.Select;
using Pasca.Gui.View.Settings;
using Pasca.Gui.ViewModel;
using Pasca.Gui.ViewModel.Artifacts;
using Pasca.Gui.ViewModel.Auth;
using Pasca.Gui.ViewModel.Charts;
using Pasca.Gui.ViewModel.Mapping;
using Pasca.Gui.ViewModel.Select;
using Pasca.Gui.ViewModel.Settings;
using Xceed.Wpf.Toolkit;
using WinFormsMessageBox = System.Windows.Forms.MessageBox;
using WpfMessageBox = System.Windows.MessageBox;
using TreeView = System.Windows.Controls.TreeView;

namespace Pasca.Gui.Facade
{
    public class Bootstrapper
    {
        public Bootstrapper(Action<DR> registerDependencies, Action<Action> shutdownSubscription)
        {
            SingletonComponentBase.InvokeReset();
            registerDependencies(DR.Instance);

            var @event = new ManualResetEventSlim(false);
            var t = new Thread(
                () =>
                {
                    shutdownSubscription(() => Shutdown());
                    _wpfApp = new App
                    {
                        ShutdownMode = ShutdownMode.OnExplicitShutdown
                    };
                    _wpfApp.Startup += WarmUp;
                    @event.Set();
                    _wpfApp.Run();
                });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();

            if (!@event.Wait(TimeSpan.FromSeconds(200)))
            {
                throw new WpfApplicationInitializationException(
                    "Could not create Wpf Application in 20 seconds." +
                    " Perhaps, More than one instance of the System.Windows.Application class is created per AppDomain.");
            }
        }

        private ITracer Tracer => _tracer.Value;

        public void Shutdown(bool killProcess = true)
        {
            if (_wpfApp != null)
            {
                try
                {
                    _wpfApp.Dispatcher.ShutdownFinished += delegate
                    {
                        // todo: not so dirty hack
                        Process.GetCurrentProcess().Kill();
                    };
                    _wpfApp.Dispatcher.InvokeShutdown();
                    _wpfApp = null;
                }
                catch (Exception e)
                {
                    Tracer.WriteLine(TracerTags.Bootstrapper, "Could not shutdown wpf app dispatcher");
                    Tracer.WriteLine(TracerTags.Bootstrapper, e.Message);
                }
            }
        }

        ~Bootstrapper()
        {
            Shutdown();
        }

        private static string GetResult(Window window)
        {
            var vm = window.DataContext as BaseViewModel;
            return vm == null
                       ? Results.Ok
                       : vm.GetResult();
        }

        public MessageResult ShowMessage(MessageInfo info)
        {
            Assert.NotNull(info);
            Assert.NotNull(info.Body);
            if (info.Title == null)
            {
                info.Title = string.Empty;
            }

            var last = _topWindow.LastOrDefault();
            if (last == null)
            {
                var ownerHandle = DR.GetOrDefault<IParentWindowProvider>()?.GetParentWindowHandle();
                if (ownerHandle.HasValue)
                {
                    return WinFormsMessageBox.Show(
                        new WindowCompat(ownerHandle.Value),
                        info.Body,
                        info.Title,
                        info.Buttons.ToWinFormsMessageBoxButtons()).ToMessageResult();
                }

                return WpfMessageBox.Show(info.Body, info.Title, info.Buttons.ToWpfMessageBoxButton()).ToMessageResult();
            }

            return ExecuteInUiThreadSync(() => WpfMessageBox.Show(last, info.Body, info.Title, info.Buttons.ToWpfMessageBoxButton()).ToMessageResult());
        }

        public string ShowSettingsWindow(ISettingProvider<ArraySetting> provider)
        {
            return ShowWindow(
                       () => new SessionSettingsWindow
                       {
                           DataContext = new ArraySettingsViewModel(provider)
                           {
                               IsEditable = true
                           }
                       });
        }

        public string ShowSessionMappingWindow(ISettingProvider<SessionSettings> provider)
        {
            return ShowWindow(
                       () => new SessionMappingWindow
                       {
                           DataContext = new SessionMappingViewModel(provider)
                           {
                               IsEditable = true
                           }
                       });
        }

        public string ShowNewSessionWindow(ISettingProvider<SessionSettings> provider)
        {
            return ShowWindow(
                       () => new SessionSettingsWindow
                       {
                           DataContext = new ArraySettingsViewModel(provider)
                       });
        }

        public string ShowSignInWindow(IAuth auth)
        {
            return ShowWindow(
                       () => new ApproveAuthWindow
                       {
                           DataContext = new ApproveAuthViewModel(auth)
                       });
        }

        public string ShowTemplateEditWindow(ISettingProvider<ArraySetting> provider)
        {
            return ShowWindow(
                       () => new SessionSettingsWindow
                       {
                           DataContext = new ArraySettingsViewModel(provider)
                           {
                               IsEditable = true
                           }
                       });
        }

        public string ShowArtifactsDoneWindow(Dictionary<Person, string> data, Action<string> onClick)
        {
            return ShowWindow(
                       () => new ArtifactsDoneWindow
                       {
                           DataContext = new ArtifactsDoneViewModel(data, onClick)
                       });
        }

        public ITreeItem<T> ShowSelectWindow<T>(IEnumerable<ITreeItem<T>> input)
        {
            ITreeItem<T> returnValue = null;
            ShowWindow(
                () => new SelectWindow
                {
                    DataContext = new SelectViewModel(input, item => returnValue = item as ITreeItem<T>)
                });
            return returnValue;
        }

        public string ShowFinalGradesSeriesChart(IGradesProvider gradesProvider)
        {
            try
            {
                return ShowWindow(
                           () => new FinalGradesSeriesChartWindow
                           {
                               DataContext = new FinalGradesSeriesChartViewModel(gradesProvider)
                           });
            }
            catch (GradesProviderException)
            {
                return Results.GradesProviderError;
            }
        }

        public string ShowFinalGradesPieChart(IGradesProvider gradesProvider)
        {
            try
            {
                return ShowWindow(
                           () => new FinalGradesPieChartWindow
                           {
                               DataContext = new FinalGradesSeriesChartViewModel(gradesProvider)
                           });
            }
            catch (GradesProviderException)
            {
                return Results.GradesProviderError;
            }
        }

        public string ShowFinalGradesDistributionChart(IGradesProvider gradesProvider)
        {
            try
            {
                return ShowWindow(
                           () => new FinalGradesDistributionChartWindow
                           {
                               DataContext = new FinalGradesDistributionChartViewModel(gradesProvider)
                           });
            }
            catch (GradesProviderException)
            {
                return Results.GradesProviderError;
            }
        }

        private T ExecuteInUiThreadSync<T>(Func<T> func)
        {
            if (_wpfApp.Dispatcher.CheckAccess())
            {
                return func();
            }

            return (T)_wpfApp.Dispatcher.Invoke(func, DispatcherPriority.Send);
        }

        private async Task ExecuteInUiThreadAsync(Action action)
        {
            if (_wpfApp.Dispatcher.CheckAccess())
            {
                action();
                return;
            }

            _wpfApp.Dispatcher.BeginInvoke(action);
        }

        private string ShowWindow(Func<Window> windowBuilder)
        {
            return ExecuteInUiThreadSync(() =>
                {
                    try
                    {
                        var window = windowBuilder();

                        if (_topWindow.Count == 0)
                        {
                            var ownerHandle =
                                    DR.GetOrDefault<IParentWindowProvider>()?.GetParentWindowHandle();
                            if (ownerHandle.HasValue)
                            {
                                GC.KeepAlive(
                                    new WindowInteropHelper(window)
                                    {
                                        Owner = ownerHandle.Value
                                    });
                            }
                        }
                        else
                        {
                            window.Owner = _topWindow[_topWindow.Count - 1];
                        }

                        var frame = new DispatcherFrame();
                        window.Closed += (a, e) =>
                        {
                            frame.Continue = false;
                        };
                        window.Loaded +=
                                delegate { window.Dispatcher.BeginInvoke(new Func<bool>(window.Activate)); };
                        try
                        {
                            _topWindow.Add(window);
                            window.Show();

                            ComponentDispatcher.PushModal();
                            Dispatcher.PushFrame(frame);
                        }
                        finally
                        {
                            ComponentDispatcher.PopModal();

                            _topWindow.Remove(window);
                            window.Owner = null;
                        }
                        return GetResult(window);

                    }
                    catch (AuthException)
                    {
                        return Results.AuthorizeOnly;
                    }
                    catch (Exception e)
                    {
#if DEBUG
                        var agg = e as AggregateException;
                        if (agg != null && agg.InnerExceptions.Any())
                        {
                            ShowMessage(agg.InnerExceptions.First().Message);
                            ShowMessage(agg.InnerExceptions.First().Message);
                        }
                        else
                        {
                            ShowMessage(e.Message);
                            ShowMessage(e.StackTrace);
                        }
#endif
                        return Results.UnknownError;
                    }
                });
        }

        private static void WarmUp(object sender, StartupEventArgs e)
        {
            Task.Factory.StartNew(
                WarmUp,
                new CancellationToken(),
                TaskCreationOptions.None,
                TaskScheduler.FromCurrentSynchronizationContext());
        }

        private static void WarmUp()
        {
            GC.KeepAlive(new Window());
            GC.KeepAlive(Resources.Close);
            GC.KeepAlive(new ChildWindow());
            GC.KeepAlive(new MultiSelectTreeView());
            GC.KeepAlive(new TreeView());
            GC.KeepAlive(new LiveCharts.ChartPoint());
            GC.KeepAlive(new LiveCharts.Wpf.AxesCollection());
            //GC.KeepAlive(new Xceed.Wpf.DataGrid.Cell());
            //GC.KeepAlive(new Xceed.Wpf.AvalonDock.Converters.BoolToVisibilityConverter());
            //GC.KeepAlive(new Xceed.Wpf.AvalonDock.Themes.AeroTheme());
            //GC.KeepAlive(new Xceed.Wpf.AvalonDock.Themes.MetroTheme());
            //GC.KeepAlive(new Xceed.Wpf.AvalonDock.Themes.VS2010Theme());
            GC.KeepAlive(new JumpItemsRejectedEventArgs());
        }

        private readonly List<Window> _topWindow = new List<Window>();

        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();

        private App _wpfApp;
    }
}