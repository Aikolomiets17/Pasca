﻿using System;
using Pasca.Common.Settings;

namespace Pasca.Gui.Facade
{
    public interface ISettingProvider<out T>
        where T : Setting
    {
        void Subscribe(Action<T, T> settingChangedHandler);
        T GetSetting();
        bool IsEditable(Setting setting);
        ISelectableValueProvider GetSelectableValueProvider(Setting setting);
        void Commit(Setting newSettings);
    }
}