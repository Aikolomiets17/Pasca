﻿using System;
using System.Collections.Generic;
using Pasca.Common.Exceptions;
using Pasca.Common.Providers;
using Pasca.Common.Students;

namespace Pasca.Gui.Exceptions
{
    public class GradesProviderException : Exception
    {
        public GradesProviderException(IGradesProvider gradesProvider)
        {
            GradesProvider = gradesProvider;
        }

        public IGradesProvider GradesProvider { get; }

        public static Dictionary<Person, double> GetFinalGradesAndThrowIfEmpty(IGradesProvider gradesProvider)
        {
            Assert.NotNull(gradesProvider);
            var grades = gradesProvider.GetFinalGrades();
            if (grades.Count == 0)
            {
                throw new GradesProviderException(gradesProvider);
            }
            return grades;
        }
    }
}