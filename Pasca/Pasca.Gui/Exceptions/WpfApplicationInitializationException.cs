﻿using System;

namespace Pasca.Gui.Exceptions
{
    public class WpfApplicationInitializationException : ApplicationException
    {
        public WpfApplicationInitializationException(string message)
            : base(message) {}
    }
}