﻿using Pasca.Common.Trace;

namespace Pasca.Gui.Trace
{
    public class TracerTags : CommonTracerTags
    {
        public const string Settings = nameof(Settings);
        public const string Bootstrapper = nameof(Bootstrapper);
        public const string Convert = nameof(Convert);
    }
}