﻿using System;
using System.Windows.Input;

namespace Pasca.Gui.ViewModel
{
    public class ActionCommand<T> : ICommand
    {
        public ActionCommand(Action<T> invokeAction, Func<T, bool> canExecute = null)
        {
            _invokeAction = invokeAction;
            _canExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                _canExecuteChanged += value;
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                _canExecuteChanged -= value;
                CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            //DR.Get<ITracer>().WriteLine(TracerTags.General, parameter == null ? "null " + typeof(T) : parameter.ToString() + parameter.GetType());
            if (parameter is T)
            {
                return _canExecute == null || _canExecute((T)parameter);
            }

            return false;
        }

        public void Execute(object parameter)
        {
            if (CanExecute(parameter))
            {
                _invokeAction?.Invoke((T)parameter);
            }
        }

        // ReSharper disable once InconsistentNaming
        private event EventHandler _canExecuteChanged;

        public void RaiseCanExecuteChanged()
        {
            _canExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        private readonly Func<T, bool> _canExecute;

        private readonly Action<T> _invokeAction;
    }
}