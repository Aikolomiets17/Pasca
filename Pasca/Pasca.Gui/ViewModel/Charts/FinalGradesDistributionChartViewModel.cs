﻿using System;
using System.Linq;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.Common.Providers;
using Pasca.Gui.Exceptions;

namespace Pasca.Gui.ViewModel.Charts
{
    public class FinalGradesDistributionChartViewModel : BaseViewModel
    {
        public FinalGradesDistributionChartViewModel(IGradesProvider gradesProvider)
        {
            Assert.NotNull(gradesProvider);

            var grades = GradesProviderException.GetFinalGradesAndThrowIfEmpty(gradesProvider);
            MinGrade = 0;
            MaxGrade = grades.Max(k => k.Value) + 1;

            Bins = new Bin[Math.Ceiling(MaxGrade - MinGrade).AsInt()];
            foreach (var kv in grades)
            {
                var bin = (int)kv.Value;
                Bins[bin].Count++;
                Bins[bin].Students.Add(kv.Key);
            }

            IndexToGrade = GetGrade;
            CountToPercent = GetPercent;
        }

        public Bin[] Bins { get; }

        public double MinCount => 0;
        public double MaxCount => Bins.Length;

        public double MinGrade { get; }
        public double MaxGrade { get; }

        public Func<double, string> IndexToGrade { get; }
        public Func<double, string> CountToPercent { get; }

        private string GetGrade(double indexDouble)
        {
            var index = indexDouble.AsInt();
            if (index >= 0 && index < MaxGrade)
            {
                return index.ToString();
            }
            return null;
        }

        private string GetPercent(double countDouble)
        {
            var count = countDouble.AsInt();
            if (count >= 0 && count <= MaxCount)
            {
                return (int)(countDouble / (MaxCount - MinCount) * 100) + "%";
            }
            return null;
        }
    }
}