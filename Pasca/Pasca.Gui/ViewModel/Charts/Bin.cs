﻿using System.Collections.Generic;
using Pasca.Common.Students;

namespace Pasca.Gui.ViewModel.Charts
{
    public struct Bin
    {
        public int Count { get; set; }

        public List<Person> Students => _students ?? (_students = new List<Person>());

        private List<Person> _students;
    }
}