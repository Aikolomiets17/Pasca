﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Comparers;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.Common.Providers;
using Pasca.Common.Students;
using Pasca.Gui.Exceptions;

namespace Pasca.Gui.ViewModel.Charts
{
    public class FinalGradesSeriesChartViewModel : BaseViewModel
    {
        public FinalGradesSeriesChartViewModel(IGradesProvider gradesProvider)
        {
            Assert.NotNull(gradesProvider);

            Grades = GradesProviderException
                    .GetFinalGradesAndThrowIfEmpty(gradesProvider)
                    .ToList()
                    .Sorted(PersonValuePairComparer<double>.Instance);
            IndexToPerson = GetPerson;
        }

        public List<KeyValuePair<Person, double>> Grades { get; }

        public double MinGrade => 0;
        public double MaxGrade => Grades.Max(q => q.Value) + 1;

        public Func<double, string> IndexToPerson { get; }

        private string GetPerson(double indexDouble)
        {
            var index = indexDouble.AsInt();
            if (index >= 0 && index < Grades.Count)
            {
                return Grades[index].Key.ToString();
            }
            return null;
        }
    }
}