﻿using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Exceptions;
using Pasca.Common.Providers;
using Pasca.Common.Students;
using Pasca.Gui.Exceptions;

namespace Pasca.Gui.ViewModel.Charts
{
    public class FinalGradesPieChartViewModel : BaseViewModel
    {
        public FinalGradesPieChartViewModel(IGradesProvider gradesProvider)
        {
            Assert.NotNull(gradesProvider);

            Grades = from grade in GradesProviderException.GetFinalGradesAndThrowIfEmpty(gradesProvider)
                     orderby grade.Value
                     select grade;
        }

        public IEnumerable<KeyValuePair<Person, double>> Grades { get; }
    }
}