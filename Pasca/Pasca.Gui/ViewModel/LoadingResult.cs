﻿using System;

namespace Pasca.Gui.ViewModel
{
    public class LoadingResult
    {
        public LoadingResult()
        {
            Done = true;
        }

        public LoadingResult(Exception e)
        {
            Exception = e;
        }

        public bool Done { get; set; }
        public Exception Exception { get; set; }
    }
}
