﻿using System;
using System.ComponentModel;

namespace Pasca.Gui.ViewModel
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public event Action CloseRequested = () => { };

        protected void RequestClose()
        {
            CloseRequested();
        }

        public virtual string GetResult() => Results.Ok;

        public virtual void RaisePropertyChanged(string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}