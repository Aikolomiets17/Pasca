﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Students;

namespace Pasca.Gui.ViewModel.Artifacts
{
    public class ArtifactsDoneViewModel : BaseViewModel
    {
        public ArtifactsDoneViewModel(
            Dictionary<Person, string> data,
            Action<string> onClick)
        {
            Data = data.Select(kv => new Tuple<Person, string>(kv.Key, kv.Value)).ToList();

            PageClick = new ActionCommand<string>(onClick);
        }

        public List<Tuple<Person, string>> Data { get; }

        public ActionCommand<string> PageClick { get; }
    }
}