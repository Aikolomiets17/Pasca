﻿namespace Pasca.Gui.ViewModel
{
    public class Results
    {
        public const string Ok = nameof(Ok);
        public const string UnknownError = nameof(UnknownError);
        public const string TimeOut = nameof(TimeOut);
        public const string GradesProviderError = nameof(GradesProviderError);
        public const string AuthorizeOnly = nameof(AuthorizeOnly);
    }
}