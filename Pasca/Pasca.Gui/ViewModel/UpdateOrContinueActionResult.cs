﻿namespace Pasca.Gui.ViewModel
{
    public enum UpdateOrContinueActionResult
    {
        Update,
        Continue
    }
}