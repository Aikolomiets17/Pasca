﻿using System;

namespace Pasca.Gui.ViewModel
{
    public class InteractionRequest<TData, TResult> : EventArgs
    {
        public Action<TData, TResult> ResultCallback { get; set; }
        public TData Data { get; set; }
    }
}