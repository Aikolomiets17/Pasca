﻿using System;
using System.Windows.Input;

namespace Pasca.Gui.ViewModel
{
    public class ActionCommand : ICommand
    {
        public ActionCommand(Action invokeAction, Func<bool> canExecute = null)
        {
            _invokeAction = invokeAction;
            _canExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            if (_canExecute == null)
            {
                return true;
            }

            return _canExecute();
        }

        public void Execute(object parameter)
        {
            if (CanExecute(parameter))
            {
                _invokeAction?.Invoke();
            }
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        private readonly Func<bool> _canExecute;

        private readonly Action _invokeAction;
    }
}