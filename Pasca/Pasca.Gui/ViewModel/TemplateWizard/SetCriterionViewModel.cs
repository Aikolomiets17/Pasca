﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Pasca.Common.Algorithm.Criteria;

namespace Pasca.Gui.ViewModel.TemplateWizard
{
    public class SetCriterionViewModel : CriterionViewModel
    {
        public SetCriterionViewModel(SetCriterion model, int? number)
            : base(model, number)
        {
            _criterion = model;
            Set = new ObservableCollection<string>(
                _criterion.Set);
            Set.CollectionChanged += SetChanged;
        }

        public ObservableCollection<string> Set { get; }

        public string NewItem
        {
            get { return _newItem; }
            set
            {
                _newItem = value;
                RaisePropertyChanged(nameof(NewItem));
                Add.RaiseCanExecuteChanged();
                Remove.RaiseCanExecuteChanged();
            }
        }

        public ActionCommand<string> Add => new ActionCommand<string>(AddAction, AddCanExecute);
        public ActionCommand<string> Remove => new ActionCommand<string>(RemoveAction, RemoveCanExecute);

        private void SetChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            _criterion.Set = Set.ToList();
        }

        private void AddAction(string newItem)
        {
            Set.Add(newItem);
            NewItem = string.Empty;
        }

        private bool AddCanExecute(string item)
        {
            return !string.IsNullOrWhiteSpace(item) && !Set.Contains(item);
        }

        private void RemoveAction(string removeItem)
        {
            Set.Remove(removeItem);
            NewItem = removeItem;
        }

        private bool RemoveCanExecute(string item)
        {
            return !string.IsNullOrWhiteSpace(item) && Set.Contains(item);
        }

        private readonly SetCriterion _criterion;
        private string _newItem;
    }
}