﻿using System;
using Pasca.Common.Algorithm.Criteria;

namespace Pasca.Gui.ViewModel.TemplateWizard
{
    public abstract class RangeCriterionViewModel<T> : CriterionViewModel
        where T : struct, IComparable<T>
    {
        protected RangeCriterionViewModel(RangeCriterion<T> model, int? number)
            : base(model, number)
        {
            _criterion = model;
        }

        public T Min
        {
            get { return _criterion.Min; }
            set
            {
                _criterion.Min = value;
                RaisePropertyChanged(nameof(Min));
            }
        }

        public T Max
        {
            get { return _criterion.Max; }
            set
            {
                _criterion.Max = value;
                RaisePropertyChanged(nameof(Max));
            }
        }

        private readonly RangeCriterion<T> _criterion;
    }
}