﻿using Pasca.Common.Algorithm.Criteria;

namespace Pasca.Gui.ViewModel.TemplateWizard
{
    public class NumericCriterionViewModel : RangeCriterionViewModel<int>
    {
        public NumericCriterionViewModel(NumericCriterion model, int? number)
            : base(model, number) {}
    }
}