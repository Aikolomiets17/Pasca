﻿using Pasca.Common.Algorithm.Criteria;
using Pasca.Common.Exceptions;

namespace Pasca.Gui.ViewModel.TemplateWizard
{
    public class CriterionViewModel : BaseViewModel
    {
        protected CriterionViewModel(Criterion criterion, int? number)
        {
            Assert.NotNull(criterion);

            _criterion = criterion;
            Number = number + 1;
        }

        public string Description
        {
            get { return _criterion.Description; }
            set
            {
                _criterion.Description = value;
                RaisePropertyChanged(nameof(Description));
            }
        }

        public int? Number { get; }

        public Criterion GetCriterion()
        {
            return _criterion;
        }

        public static CriterionViewModel Create(Criterion criterion, int? number = null)
        {
            Assert.NotNull(criterion);

            if (criterion.GetType() == typeof(SetCriterion))
            {
                return new SetCriterionViewModel((SetCriterion)criterion, number);
            }

            if (criterion.GetType() == typeof(NumericCriterion))
            {
                return new NumericCriterionViewModel((NumericCriterion)criterion, number);
            }

            if (criterion.GetType() == typeof(FloatCriterion))
            {
                return new FloatCriterionViewModel((FloatCriterion)criterion, number);
            }

            throw new ArgumentTypeException(nameof(criterion), typeof(Criterion), criterion.GetType());
        }

        public override string ToString()
        {
            return _criterion.GetType().Name;
        }

        private readonly Criterion _criterion;
    }
}