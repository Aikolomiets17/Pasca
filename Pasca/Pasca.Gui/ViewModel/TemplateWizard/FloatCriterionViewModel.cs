﻿using Pasca.Common.Algorithm.Criteria;

namespace Pasca.Gui.ViewModel.TemplateWizard
{
    public class FloatCriterionViewModel : RangeCriterionViewModel<double>
    {
        public FloatCriterionViewModel(FloatCriterion model, int? number)
            : base(model, number) {}
    }
}