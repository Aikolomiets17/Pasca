﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Pasca.Common.Algorithm.Criteria;

namespace Pasca.Gui.ViewModel.TemplateWizard
{
    public class CriteriaGroupViewModel : BaseViewModel
    {
        public CriteriaGroupViewModel(CriteriaGroup model, int? number = null)
        {
            _criteriaGroup = model;
            Number = number + 1;

            CriteriaViewModels = new ObservableCollection<CriterionViewModel>(
                _criteriaGroup.Criteria.Select((c, i)=>CriterionViewModel.Create(c, i)));
            CriteriaViewModels.CollectionChanged += CriteriaViewModelsChanged;
        }

        public ActionCommand<CriterionViewModel> AddNew => new ActionCommand<CriterionViewModel>(AddNewAction);
        public ActionCommand<int> Remove => new ActionCommand<int>(RemoveAction, RemoveCanExecute);

        public IList<CriterionViewModel> AvailableNewCriteria => new[] {
            CriterionViewModel.Create(new SetCriterion(), CriteriaViewModels.Count),
            CriterionViewModel.Create(new NumericCriterion(), CriteriaViewModels.Count),
            CriterionViewModel.Create(new FloatCriterion(), CriteriaViewModels.Count)
        };

        public string GroupName
        {
            get { return _criteriaGroup.GroupName; }
            set
            {
                _criteriaGroup.GroupName = value;
                RaisePropertyChanged(nameof(GroupName));
            }
        }

        public int? Number { get; }

        public string AggregateExpression
        {
            get { return _criteriaGroup.AggregateExpression; }
            set
            {
                _criteriaGroup.AggregateExpression = value;
                RaisePropertyChanged(nameof(AggregateExpression));
            }
        }

        public ObservableCollection<CriterionViewModel> CriteriaViewModels { get; }

        private void CriteriaViewModelsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            _criteriaGroup.Criteria = CriteriaViewModels.Select(vm => vm.GetCriterion()).ToList();
            Remove.RaiseCanExecuteChanged();
        }

        private void RemoveAction(int idx)
        {
            CriteriaViewModels.RemoveAt(idx);
        }

        private bool RemoveCanExecute(int idx)
        {
            return CriteriaViewModels.Count > idx;
        }

        private void AddNewAction(CriterionViewModel newVm)
        {
            CriteriaViewModels.Add(newVm);
            RaisePropertyChanged(nameof(AvailableNewCriteria));
        }

        public CriteriaGroup GetCriteriaGroup()
        {
            return _criteriaGroup;
        }

        private readonly CriteriaGroup _criteriaGroup;
    }
}