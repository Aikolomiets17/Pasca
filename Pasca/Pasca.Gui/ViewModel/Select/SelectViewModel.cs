﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Pasca.Common.Exceptions;
using Pasca.Common.Types;

namespace Pasca.Gui.ViewModel.Select
{
    public class SelectViewModel : BaseViewModel
    {
        public SelectViewModel(IEnumerable<ITreeItem> heads, Action<ITreeItem> closeAction)
        {
            Assert.NotNull(heads);
            Assert.NotNull(closeAction);

            Heads = new ObservableCollection<ITreeItem>(heads);
            _closeAction = closeAction;
            Commit = new ActionCommand<ITreeItem>(CommitAction, item => item != null && !item.Children.Any());
        }

        public ActionCommand<ITreeItem> Commit { get; }
        public ObservableCollection<ITreeItem> Heads { get; }

        public ITreeItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged(nameof(SelectedItem));
                Commit.RaiseCanExecuteChanged();
            }
        }

        private void CommitAction(ITreeItem selected)
        {
            _closeAction(SelectedItem);
            RequestClose();
        }

        private readonly Action<ITreeItem> _closeAction;
        private ITreeItem _selectedItem;
    }
}