﻿using System;

namespace Pasca.Gui.ViewModel
{
    public interface ILoading
    {
        event Action<LoadingResult> LoadingDone;

        void Load();
        bool IsLoadingDone { get; }
    }
}
