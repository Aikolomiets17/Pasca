﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;
using Pasca.Common.DI;
using Pasca.Common.Extensions;
using Pasca.Common.Providers;
using Pasca.Common.Settings;
using Pasca.Common.Students;

namespace Pasca.Gui.ViewModel.Settings
{
    // todo: clarify usage
    public class TeacherListSettingViewModel : SettingViewModel, ILoading
    {
        public TeacherListSettingViewModel(PersonListSetting setting, bool canEdit, Action<Setting> onCommit = null)
            : base(setting, canEdit, onCommit)
        {
            _setting = setting;
            Selected = new ObservableCollection<Person>(_setting.Value);

            Select = new ActionCommand<IList>(SelectTeachers);
            Deselect = new ActionCommand<IList>(DeselectTeachers);
        }

        public event Action<LoadingResult> LoadingDone = _ => { };

        public async void Load()
        {
            try
            {
                Available = new ObservableCollection<Person>(
                    (await DR.Get<IAvailableTeachersProvider>().GetAvailableTeachers())
                    .Except(Selected));
            }
            catch (Exception e)
            {
                LoadingDone(new LoadingResult(e));
            }
            RaisePropertyChanged(nameof(Available));

            Selected.CollectionChanged += CollectionChanged;

            IsLoadingDone = true;
            RaisePropertyChanged(nameof(IsLoadingDone));

            LoadingDone(new LoadingResult());
        }

        public bool IsLoadingDone { get; private set; }

        public ICommand Select { get; }
        public ICommand Deselect { get; }

        public ObservableCollection<Person> Selected { get; }
        public ObservableCollection<Person> Available { get; private set; }

        private void CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (var old in e.OldItems.Clone<Person>())
                {
                    if (!Available.Contains(old))
                    {
                        Available.Add(old);
                    }
                }
            }

            if (e.NewItems != null)
            {
                foreach (var @new in e.NewItems.Clone<Person>())
                {
                    Available.Remove(@new);
                }
            }

            _setting.Value = new List<Person>(Selected);
        }

        private void SelectTeachers(IList teachers)
        {
            if (teachers == null)
            {
                return;
            }

            foreach (var t in teachers.Clone<Person>())
            {
                if (!Selected.Contains(t))
                {
                    Selected.Add(t);
                }
            }
        }

        private void DeselectTeachers(IList teachers)
        {
            if (teachers == null)
            {
                return;
            }

            foreach (var t in teachers.Clone<Person>())
            {
                Selected.Remove(t);
            }
        }

        private readonly PersonListSetting _setting;
    }
}