﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Pasca.Common.Algorithm.Criteria;
using Pasca.Common.Settings;
using Pasca.Gui.ViewModel.TemplateWizard;

namespace Pasca.Gui.ViewModel.Settings
{
    public class AssessmentCriteriaSettingViewModel : SettingViewModel
    {
        public AssessmentCriteriaSettingViewModel(
            AssessmentCriteriaSetting setting,
            bool canEdit,
            Action<Setting> onCommit = null)
            : base(setting, canEdit, onCommit)
        {
            _setting = setting;

            CriteriaGroupViewModels = new ObservableCollection<CriteriaGroupViewModel>(
                _setting.Value.CriteriaGroups.Select((group, i) => new CriteriaGroupViewModel(group, i)));
            CriteriaGroupViewModels.CollectionChanged += CriteriaGroupViewModelsChanged;
        }

        public ActionCommand AddNew => new ActionCommand(AddNewAction);
        public ActionCommand<int> Remove => new ActionCommand<int>(RemoveAction, AddNewCanExecute);

        public string AggregateExpression
        {
            get { return _setting.Value.AggregateExpression; }
            set
            {
                _setting.Value.AggregateExpression = value;
                RaisePropertyChanged(nameof(AggregateExpression));
            }
        }

        public ObservableCollection<CriteriaGroupViewModel> CriteriaGroupViewModels { get; }

        private void CriteriaGroupViewModelsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            _setting.Value.CriteriaGroups = CriteriaGroupViewModels.Select(vm => vm.GetCriteriaGroup()).ToList();
            Remove.RaiseCanExecuteChanged();
        }

        private void RemoveAction(int idx)
        {
            CriteriaGroupViewModels.RemoveAt(idx);
        }

        private void AddNewAction()
        {
            CriteriaGroupViewModels.Add(new CriteriaGroupViewModel(new CriteriaGroup(), CriteriaGroupViewModels.Count));
        }

        private bool AddNewCanExecute(int arg)
        {
            return arg >= 0 && arg < CriteriaGroupViewModels.Count;
        }

        private readonly AssessmentCriteriaSetting _setting;
    }
}