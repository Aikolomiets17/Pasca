﻿using System;
using System.Windows.Input;
using Pasca.Common.Exceptions;
using Pasca.Common.Settings;
using Pasca.Gui.Facade;

namespace Pasca.Gui.ViewModel.Settings
{
    public abstract class SettingViewModel : BaseViewModel
    {
        protected SettingViewModel(Setting setting, bool canEdit, Action<Setting> onCommit = null)
        {
            Setting = setting;
            Name = setting.Name;
            IsEditable = canEdit;
            OnCommit = new ActionCommand<Setting>(onCommit);
        }

        public Setting Setting { get; }

        public Enum Name { get; }

        public bool IsEditable
        {
            get { return _isEditable; }
            set
            {
                _isEditable = value;
                RaisePropertyChanged(nameof(IsEditable));
            }
        }

        // todo: remove or clarify need
        public ICommand OnCommit { get; }

        public static SettingViewModel Create(
            Setting setting,
            ISelectableValueProvider selector,
            bool canEdit,
            Action<LoadingResult> onLoaded,
            Action<Setting> onCommit = null)
        {
            Assert.NotNull(setting);

            if (setting.GetType() == typeof(BooleanSetting))
            {
                return new BooleanSettingViewModel((BooleanSetting)setting, canEdit, onCommit);
            }

            if (setting.GetType() == typeof(StringSetting))
            {
                var stringSelector = selector as ISelectableValueProvider<string>;
                if (stringSelector == null)
                {
                    return new StringSettingViewModel((StringSetting)setting, canEdit, onCommit);
                }
                return new SelectableStringSettingViewModel((StringSetting)setting, stringSelector, canEdit, onCommit);
            }

            if (setting.GetType() == typeof(NumericSetting))
            {
                return new NumericSettingViewModel((NumericSetting)setting, canEdit, onCommit);
            }

            if (setting.GetType() == typeof(NumericWithRangeSetting))
            {
                return new NumericWithRangeSettingViewModel((NumericWithRangeSetting)setting, canEdit, onCommit);
            }

            if (setting.GetType() == typeof(RangeSetting))
            {
                return new RangeSettingViewModel((RangeSetting)setting, canEdit, onCommit);
            }

            if (setting is PersonListSetting)
            {
                return new StudentListSettingViewModel((PersonListSetting)setting, canEdit, onCommit);
            }

            //if (setting.GetType() == typeof(AnonymizedPersonListSetting))
            //{
            //    return new StudentListSettingViewModel((PersonListSetting)setting, canEdit, onCommit);
            //}

            if (setting.GetType() == typeof(DateTimeSetting))
            {
                return new DateTimeSettingViewModel((DateTimeSetting)setting, canEdit, onCommit);
            }

            //if (setting.GetType() == typeof(StudentMappingSetting))
            //{
            //    return new StudentMappingSettingViewModel((StudentMappingSetting)setting, canEdit, onCommit);
            //}

            if (setting.GetType() == typeof(AssessmentCriteriaSetting))
            {
                return new AssessmentCriteriaSettingViewModel((AssessmentCriteriaSetting)setting, canEdit, onCommit);
            }

            throw new ArgumentTypeException(nameof(setting), typeof(Setting), setting.GetType());
        }

        private bool _isEditable;
    }
}