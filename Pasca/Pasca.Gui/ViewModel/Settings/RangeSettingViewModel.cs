﻿using System;
using Pasca.Common.Settings;

namespace Pasca.Gui.ViewModel.Settings
{
    public class RangeSettingViewModel : SettingViewModel
    {
        public RangeSettingViewModel(RangeSetting setting, bool canEdit, Action<Setting> onCommit = null)
            : base(setting, canEdit, onCommit)
        {
            _setting = setting;
        }

        public long Min
        {
            get { return _setting.Min; }
            set
            {
                _setting.Min = value;
                RaisePropertyChanged(nameof(Min));
            }
        }

        public long Max
        {
            get { return _setting.Max; }
            set
            {
                _setting.Max = value;
                RaisePropertyChanged(nameof(Max));
            }
        }

        private readonly RangeSetting _setting;
    }
}