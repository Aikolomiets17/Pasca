﻿using System;
using Pasca.Common.Settings;

namespace Pasca.Gui.ViewModel.Settings
{
    public class DateTimeSettingViewModel : SettingViewModel
    {
        public DateTimeSettingViewModel(DateTimeSetting setting, bool canEdit, Action<Setting> onCommit = null)
            : base(setting, canEdit, onCommit)
        {
            _setting = setting;
        }

        public DateTime Value
        {
            get { return _setting.Value; }
            set
            {
                _setting.Value = value;
                RaisePropertyChanged(nameof(Value));
            }
        }

        private readonly DateTimeSetting _setting;
    }
}