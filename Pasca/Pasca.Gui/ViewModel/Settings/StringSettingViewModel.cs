﻿using System;
using Pasca.Common.Settings;

namespace Pasca.Gui.ViewModel.Settings
{
    public class StringSettingViewModel : SettingViewModel
    {
        public StringSettingViewModel(StringSetting setting, bool canEdit, Action<Setting> onCommit)
            : base(setting, canEdit, onCommit)
        {
            _setting = setting;
        }

        public virtual string Value
        {
            get { return _setting.Value; }
            set
            {
                _setting.Value = value;
                RaisePropertyChanged(nameof(Value));
            }
        }

        private readonly StringSetting _setting;
    }
}