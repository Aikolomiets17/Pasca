﻿using System;
using Pasca.Common.Settings;

namespace Pasca.Gui.ViewModel.Settings
{
    public class NumericSettingViewModel : SettingViewModel
    {
        public NumericSettingViewModel(NumericSetting setting, bool canEdit, Action<Setting> onCommit = null)
            : base(setting, canEdit, onCommit)
        {
            _setting = setting;
        }

        public long Value
        {
            get { return _setting.Value; }
            set
            {
                _setting.Value = value;
                RaisePropertyChanged(nameof(Value));
            }
        }

        private readonly NumericSetting _setting;
    }
}