﻿using System;
using Pasca.Common.Exceptions;
using Pasca.Common.Settings;
using Pasca.Gui.Facade;

namespace Pasca.Gui.ViewModel.Settings
{
    public class SelectableStringSettingViewModel : StringSettingViewModel
    {
        public SelectableStringSettingViewModel(
            StringSetting setting,
            ISelectableValueProvider<string> selector,
            bool canEdit,
            Action<Setting> onCommit)
            : base(setting, canEdit, onCommit)
        {
            Assert.NotNull(selector);

            _selector = selector;
            Select = new ActionCommand(SelectAction);
            _visibleValue = _selector.GetVisibleData(base.Value);
        }

        public ActionCommand Select { get; }

        public override string Value
        {
            get { return _visibleValue; }
            set
            {
                _visibleValue = _selector.GetVisibleData(value);
                base.Value = value;
            }
        }

        private void SelectAction()
        {
            var newValue = _selector.TrySelect();
            if (newValue != null)
            {
                Value = newValue;
            }
        }

        private readonly ISelectableValueProvider<string> _selector;
        private string _visibleValue;
    }
}