﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Pasca.Common.Exceptions;
using Pasca.Common.Settings;
using Pasca.Gui.Facade;

namespace Pasca.Gui.ViewModel.Settings
{
    // todo: make this class base, derive for ISettingProvider support. setting/settings vm hierarchy sucks.
    public class ArraySettingsViewModel : SettingsViewModel
    {
        public ArraySettingsViewModel(ISettingProvider<ArraySetting> provider)
        {
            Provider = provider;
            Provider.Subscribe(OnSettingChanged);
            LoadSettings();
        }

        public ArraySetting Value => Settings;
        protected ISettingProvider<ArraySetting> Provider { get; }

        protected ArraySetting Settings { get; private set; }
        
        protected override void CommitAction()
        {
            IsEditable = false;
            _commitEvent.Reset();
            Task.Factory.StartNew(() => Provider.Commit(Value),CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default).ContinueWith(OnCommited).ConfigureAwait(false);
            RequestClose();
        }

        private void OnCommited(Task commitTask)
        {
            var exception = commitTask?.Exception?.InnerExceptions?.FirstOrDefault();
            if (exception is TimeoutException)
            {
                _result = Results.TimeOut;
            }
            _commitEvent.Set();
        }

        public override string GetResult()
        {
            if (!_commitEvent.Wait(TimeSpan.FromMinutes(5)))
            {
                return Results.TimeOut;
            }

            return _result ?? base.GetResult();
        }

        protected override void EditAction()
        {
            foreach (var setting in SettingViewModels)
            {
                if (Provider.IsEditable(setting.Setting))
                {
                    setting.IsEditable = true;
                }
            }
            IsEditable = true;
        }

        private void LoadSettings()
        {
            Settings = Provider.GetSetting();
            IsEditable = Provider.IsEditable(Settings);

            BuildViewModels();
        }

        protected virtual void BuildViewModels()
        {
            SettingViewModels.Clear();
            foreach (var setting in Settings.SubSettings)
            {
                SettingViewModel vm;
                SettingViewModels.Add(vm = 
                    SettingViewModel.Create(
                        setting,
                        Provider.GetSelectableValueProvider(setting),
                        Provider.IsEditable(setting) && IsEditable,
                        OnLoad));
                var loadingVm = vm as ILoading;
                if (loadingVm != null)
                {
                    loadingVm.LoadingDone += OnLoad;
                    loadingVm.Load();
                }
            }
        }

        protected override void OnSettingsChangedUserCallback(Setting newSetting, UpdateOrContinueActionResult result)
        {
            if (result == UpdateOrContinueActionResult.Update)
            {
                LoadSettings();
            }
        }

        private void OnLoad(LoadingResult result)
        {
            if (result.Done)
            {
                return;
            }

            if (result.Exception is AuthException)
            {
                _result = Results.AuthorizeOnly;
                RequestClose();
            }
        }

        private readonly ManualResetEventSlim _commitEvent = new ManualResetEventSlim(true);

        private string _result;
    }
}