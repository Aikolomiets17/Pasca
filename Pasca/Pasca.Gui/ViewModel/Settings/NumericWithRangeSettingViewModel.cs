﻿using System;
using Pasca.Common.Settings;

namespace Pasca.Gui.ViewModel.Settings
{
    public class NumericWithRangeSettingViewModel : SettingViewModel
    {
        public NumericWithRangeSettingViewModel(
            NumericWithRangeSetting setting,
            bool canEdit,
            Action<Setting> onCommit = null)
            : base(setting, canEdit, onCommit)
        {
            _setting = setting;
        }

        public long Min
        {
            get { return _setting.Min; }
            set
            {
                _setting.Min = value;
                RaisePropertyChanged(nameof(Min));
            }
        }

        public long Max
        {
            get { return _setting.Max; }
            set
            {
                _setting.Max = value;
                RaisePropertyChanged(nameof(Max));
            }
        }

        public long Value
        {
            get { return _setting.Value; }
            set
            {
                _setting.Value = value;
                RaisePropertyChanged(nameof(Value));
            }
        }

        private readonly NumericWithRangeSetting _setting;
    }
}