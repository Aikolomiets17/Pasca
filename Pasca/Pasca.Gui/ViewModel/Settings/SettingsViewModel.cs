﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Pasca.Common.Settings;

namespace Pasca.Gui.ViewModel.Settings
{
    public abstract class SettingsViewModel : BaseViewModel
    {
        protected SettingsViewModel()
        {
            Commit = new ActionCommand(CommitAction, CommitActionCanExecute);
            Edit = new ActionCommand(EditAction, EditActionCanExecute);
        }

        public bool IsEditable
        {
            get { return _isEditable; }
            set
            {
                _isEditable = value;
                RaisePropertyChanged(nameof(IsEditable));
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged(nameof(Title));
            }
        }

        public ICommand Commit { get; }
        public ICommand Edit { get; }

        public ObservableCollection<SettingViewModel> SettingViewModels { get; } =
            new ObservableCollection<SettingViewModel>();

        protected virtual bool CommitActionCanExecute()
        {
            return IsEditable;
        }
        protected abstract void CommitAction();

        protected virtual bool EditActionCanExecute()
        {
            return IsEditable == false;
        }

        protected virtual void EditAction()
        {
            IsEditable = true;
        }

        // useless in release, for future
        public event EventHandler<InteractionRequest<Setting, UpdateOrContinueActionResult>> TriggerSettingsChanged;

        protected void OnSettingChanged(Setting oldSetting, Setting newSetting)
        {
            var settingsChangedRequest = new InteractionRequest<Setting, UpdateOrContinueActionResult> {
                Data = newSetting,
                ResultCallback = OnSettingsChangedUserCallback
            };
            TriggerSettingsChanged?.Invoke(this, settingsChangedRequest);
        }

        protected abstract void OnSettingsChangedUserCallback(Setting newSetting, UpdateOrContinueActionResult result);

        private bool _isEditable;

        private string _title;
    }
}