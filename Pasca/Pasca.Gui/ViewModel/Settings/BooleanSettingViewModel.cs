﻿using System;
using Pasca.Common.Settings;

namespace Pasca.Gui.ViewModel.Settings
{
    public class BooleanSettingViewModel : SettingViewModel
    {
        public BooleanSettingViewModel(BooleanSetting setting, bool canEdit, Action<Setting> onCommit = null)
            : base(setting, canEdit, onCommit)
        {
            _setting = setting;
        }

        public bool Value
        {
            get { return _setting.Value; }
            set
            {
                _setting.Value = value;
                RaisePropertyChanged(nameof(Value));
            }
        }

        private readonly BooleanSetting _setting;
    }
}