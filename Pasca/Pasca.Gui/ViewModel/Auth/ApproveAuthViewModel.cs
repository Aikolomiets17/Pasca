﻿using System;
using System.Windows.Input;
using Pasca.Common.Auth;

namespace Pasca.Gui.ViewModel.Auth
{
    public class ApproveAuthViewModel : BaseViewModel
    {
        public ApproveAuthViewModel(IAuth auth)
        {
            _auth = auth;
            IsAuth = _auth.IsAuth;
            CurrentUri = auth.SignInUri;

            Navigated = new ActionCommand<Uri>(OnNavigated);
        }

        public dynamic UserInfo { get; private set; }
        public string Token { get; private set; }
        public bool IsAuth { get; private set; }
        public ICommand Navigated { get; }

        public Uri CurrentUri { get; set; }

        private async void OnNavigated(Uri uri)
        {
            try
            {
                await _auth.SubmitCode(uri).ConfigureAwait(false);
                IsAuth = true;
                CurrentUri = uri;
                Token = _auth.AccessToken;
                RaisePropertyChanged(nameof(IsAuth));
                RaisePropertyChanged(nameof(UserInfo));
                RaisePropertyChanged(nameof(Token));

                RequestClose();
            }
            catch
            {
                // bad request
            }
        }

        private readonly IAuth _auth;
    }
}