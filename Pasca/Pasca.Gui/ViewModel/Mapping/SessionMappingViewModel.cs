﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.Common.Providers;
using Pasca.Common.Settings;
using Pasca.Common.Students;
using Pasca.Common.Trace;
using Pasca.Gui.Facade;
using Pasca.Gui.Trace;
using Pasca.Gui.ViewModel.Settings;

namespace Pasca.Gui.ViewModel.Mapping
{
    public class SessionMappingViewModel : ArraySettingsViewModel
    {
        public SessionMappingViewModel(ISettingProvider<SessionSettings> mappingSettingsProvider)
            :base(mappingSettingsProvider)
        {
            Assert.NotNull(mappingSettingsProvider);

            var sessionSetting = Settings as SessionSettings;
            Assert.NotNull(sessionSetting);
            
            Next = new ActionCommand(NextAction, NextCanExecute);
            Previous = new ActionCommand(PreviousAction, PreviousCanExecute);

            Init(sessionSetting);
        }

        private async void Init(SessionSettings sessionSetting)
        {
            try
            {
                AuthorsViewModel = new StudentListSettingViewModel(sessionSetting.Find<PersonListSetting>(CommonSettingName.SessionAuthors), IsEditable);
                AuthorsViewModel.LoadingDone += OnLoad;
                AuthorsViewModel.Load();

                ReviewersViewModel = new StudentListSettingViewModel(sessionSetting.Find<PersonListSetting>(CommonSettingName.SessionReviewers), IsEditable);
                ReviewersViewModel.LoadingDone += OnLoad;
                ReviewersViewModel.Load();

                AuthorsViewModel.SelectedChanged += AuthorsChanged;
                AuthorsViewModel.SelectedChanged += ReviewersChanged;
                Available = (await DR.Get<IAvailableStudentsProvider>().GetAvailableStudents()).ToList();
            }
            catch (AuthException)
            {
                _result = Results.AuthorizeOnly;
                RequestClose();
            }
            RaisePropertyChanged(nameof(Available));

            CreateItemsAndMapping(sessionSetting.SessionMapping());
            IsLoadingDone = true;

            RaisePropertyChanged(nameof(IsLoadingDone));
            RaisePropertyChanged(nameof(IsEmptyAndLoadingDone));
            RaisePropertyChanged(nameof(NotEmptyAndLoadingDone));
            RaiseSelectedChanged();
        }

        private void OnLoad(LoadingResult result)
        {
            if (result.Done)
            {
                RaisePropertyChanged(nameof(AuthorsViewModel));
                RaisePropertyChanged(nameof(ReviewersViewModel));
                Next.RaiseCanExecuteChanged();
                Previous.RaiseCanExecuteChanged();
                return;
            }

            if (result.Exception != null)
            {
                throw result.Exception;
            }
        }

        public bool IsLoadingDone { get; private set; }

        public StudentListSettingViewModel AuthorsViewModel { get; private set; }
        public StudentListSettingViewModel ReviewersViewModel { get; private set; }
        public List<Person> Available { get; private set; }
        public Dictionary<Person, List<Person>> Mapping { get; } = new Dictionary<Person, List<Person>>();

        public ActionCommand Next { get; }
        public ActionCommand Previous { get; }

        public ObservableCollection<StudentMappingItemViewModel> Items { get; } =
            new ObservableCollection<StudentMappingItemViewModel>();

        public StudentMappingItemViewModel Selected => Items[_selectedIndex];

        public bool IsEmptyAndLoadingDone => IsLoadingDone && !Items.Any();
        public bool NotEmptyAndLoadingDone => IsLoadingDone && Items.Any();
        
        private void NextAction()
        {
            _selectedIndex++;
            RaiseSelectedChanged();
        }

        public bool NextCanExecute()
        {
            return _selectedIndex + 1 < Items.Count;
        }

        private void PreviousAction()
        {
            _selectedIndex--;
            RaiseSelectedChanged();
        }

        public bool PreviousCanExecute()
        {
            return _selectedIndex > 0;
        }

        public override string GetResult()
        {
            return _result ?? base.GetResult();
        }

        protected override void CommitAction()
        {
            Value[CommonSettingName.SessionMapping] = new PersonMappingSetting(
                CommonSettingName.SessionMapping, Mapping);
            base.CommitAction();
        }

        protected override void BuildViewModels() { }

        private void RaiseSelectedChanged()
        {
            RaisePropertyChanged(nameof(Selected));
            Next.RaiseCanExecuteChanged();
            Previous.RaiseCanExecuteChanged();
        }

        private void CreateItemsAndMapping(IDictionary<int,List<int>> mapping)
        {
            Items.Clear();
            Mapping.Clear();

            foreach (var kv in mapping)
            {
                var authorNumber = kv.Key;
                var author = AuthorsViewModel.Selected.OfType<AnonymizedPerson>().With(authorNumber);
                if (author == null)
                {
                    throw new MappingException($"Author with number {kv.Key} not found", kv.Key);
                }

                var reviewers = kv.Value.Select(n =>
                                                {
                                                    var r = ReviewersViewModel.Selected.OfType<AnonymizedPerson>().With(n);
                                                    if (r == null)
                                                    {
                                                        throw new MappingException(
                                                            $"Reviewer with number {n} not found", n);
                                                    }
                                                    return r;
                                                }).ToList<Person>();
                Mapping[author] = reviewers;

                var vm = new StudentMappingItemViewModel(author, reviewers,ReviewersViewModel.Selected);
                vm.MappingChanged += MappingChanged;
                Items.Add(vm);
            }
        }

        private void AuthorsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                var oldAuthors = e.OldItems.OfType<Person>();

                foreach (var oldAuthor in oldAuthors)
                {
                    var vm = Items.FirstOrDefault(q => q.Key.Equals(oldAuthor));
                    if (vm == null)
                    {
                        _tracer.WriteLine(
                            TracerTags.Settings,
                            $"Old Author {oldAuthor} not found in SessionMapping vm items");
                    }
                    else
                    {
                        Items.Remove(vm);
                    }
                }
            }
            if (e.NewItems != null)
            {
                var newAuthors = e.NewItems.OfType<Person>();

                foreach (var newAuthor in newAuthors)
                {
                    var vm = Items.FirstOrDefault(q => q.Key.Equals(newAuthors));
                    if (vm != null)
                    {
                        _tracer.WriteLine(
                            TracerTags.Settings,
                            $"New Author {newAuthor} already found in SessionMapping vm items");
                    }
                    else
                    {
                        Items.Add(new StudentMappingItemViewModel(newAuthor, new Person[0], ReviewersViewModel.Selected));
                    }
                }
            }
        }

        private void ReviewersChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                var oldReviewers = e.OldItems.OfType<Person>();

                foreach (var vm in Items)
                {
                    if (vm.Selected.Any(q => oldReviewers.Contains(q)))
                    {
                        vm.Deselect.Execute(vm.Selected.Where(q => oldReviewers.Contains(q)));
                    }
                }
            }
            if (e.NewItems != null)
            {
                var newReviewers = e.NewItems.OfType<Person>();

                foreach (var vm in Items)
                {
                    if (newReviewers.Any(q => !vm.Selected.Contains(q)))
                    {
                        vm.Select.Execute(newReviewers.Where(q => !vm.Selected.Contains(q)));
                    }
                }
            }
        }

        private void MappingChanged(Person key, IEnumerable<Person> value)
        {
            Mapping[key] = value.ToList();
        }
        
        private int _selectedIndex;

        private readonly Action<IList<Person>, IList<Person>, IDictionary<Person, IList<Person>>> _onCommit;
        private string _result;

        private readonly ITracer _tracer = DR.Get<ITracer>();
    }
}