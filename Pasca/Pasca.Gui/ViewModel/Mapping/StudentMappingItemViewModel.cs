﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;
using Pasca.Common.Extensions;
using Pasca.Common.Students;

namespace Pasca.Gui.ViewModel.Mapping
{
    public class StudentMappingItemViewModel : BaseViewModel
    {
        public StudentMappingItemViewModel(Person key, IEnumerable<Person> list, IEnumerable<Person> available)
        {
            Key = key;

            Selected = new ObservableCollection<Person>(list);
            Available = new ObservableCollection<Person>(available.Except(Selected));
            Selected.CollectionChanged += CollectionChanged;

            Select = new ActionCommand<IList>(SelectStudents);
            Deselect = new ActionCommand<IList>(DeselectStudents);
        }

        public ICommand Select { get; }
        public ICommand Deselect { get; }

        public Person Key { get; }

        public ObservableCollection<Person> Available { get; }
        public ObservableCollection<Person> Selected { get; }

        public event Action<Person, IEnumerable<Person>> MappingChanged;


        private void CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (var old in e.OldItems.Clone<AnonymizedPerson>())
                {
                    if (!Available.Contains(old))
                    {
                        Available.Add(old);
                    }
                }
            }

            if (e.NewItems != null)
            {
                foreach (var @new in e.NewItems.Clone<AnonymizedPerson>())
                {
                    Available.Remove(@new);
                }
            }

            MappingChanged?.Invoke(Key, Selected);
        }

        private void SelectStudents(IList students)
        {
            if (students == null)
            {
                return;
            }

            foreach (var t in students.Clone<AnonymizedPerson>())
            {
                if (!Selected.Contains(t))
                {
                    Selected.Add(t);
                }
            }
        }

        private void DeselectStudents(IList students)
        {
            if (students == null)
            {
                return;
            }

            foreach (var t in students.Clone<AnonymizedPerson>())
            {
                Selected.Remove(t);
            }
        }
    }
}