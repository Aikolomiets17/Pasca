﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Pasca.Gui.View
{
    public abstract class DataTemplateSelectorBase : DataTemplateSelector
    {
        protected abstract Dictionary<Type, string> Templates { get; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var element = container as FrameworkElement;

            if (item == null || element == null)
            {
                return base.SelectTemplate(item, container);
            }

            string resourceKey;
            if (Templates.TryGetValue(item.GetType(), out resourceKey))
            {
                return (DataTemplate)element.FindResource(resourceKey);
            }

            return base.SelectTemplate(item, container);
        }
    }
}