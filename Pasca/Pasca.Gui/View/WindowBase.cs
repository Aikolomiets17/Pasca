﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using Pasca.Gui.ViewModel;

namespace Pasca.Gui.View
{
    public class WindowBase : Window
    {
        public WindowBase()
        {
            MinWidth = 800;
            MinHeight = 600;
            using (var ms = new MemoryStream())
            {
                Properties.Resources.Icon_png.Save(ms, ImageFormat.Png);
                ms.Position = 0;
                var bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = ms;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                Icon = bitmapimage;
            }
            DataContextChanged += OnDataContextChanged;
        }

        protected virtual void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var baseVm = e.NewValue as BaseViewModel;
            if (baseVm != null)
            {
                baseVm.CloseRequested += delegate { Dispatcher.Invoke(new Action(Close)); };
            }
        }

        //protected void OnPropertyChanged(string propertyName)
        //{
        //    var baseVm = DataContext as BaseViewModel;
        //    if (baseVm != null)
        //    {
        //        baseVm.RaisePropertyChanged(propertyName);
        //    }
        //}

        protected void CloseClick(object sender, object e)
        {
            Close();
        }
    }
}