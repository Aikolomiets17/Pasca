﻿using System;
using System.Collections.Generic;
using Pasca.Gui.ViewModel.TemplateWizard;

namespace Pasca.Gui.View.TemplateWizard
{
    public class CriterionDataTemplateSelector : DataTemplateSelectorBase
    {
        protected override Dictionary<Type, string> Templates => new Dictionary<Type, string> {
            {
                typeof(NumericCriterionViewModel), "NumericCriterionDataTemplate"
            }, {
                typeof(FloatCriterionViewModel), "FloatCriterionDataTemplate"
            }, {
                typeof(SetCriterionViewModel), "SetCriterionDataTemplate"
            }
        };
    }
}