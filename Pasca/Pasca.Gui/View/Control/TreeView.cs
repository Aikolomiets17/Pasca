﻿using System.Windows;
using System.Windows.Controls;

namespace Pasca.Gui.View.Control
{
    public static class AttachedSelectedItem
    {
        public static void SetEnabled(DependencyObject element, bool value)
        {
            element.SetValue(EnabledProperty, value);
        }

        public static bool GetEnabled(DependencyObject element)
        {
            return (bool)element.GetValue(EnabledProperty);
        }

        private static void OnEnabledChanged(
            DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var treeView = dependencyObject as TreeView;
            if (treeView != null)
            {
                if (dependencyPropertyChangedEventArgs.NewValue.Equals(true))
                {
                    SetSelectedItem(treeView, treeView.SelectedItem);
                    treeView.SelectedItemChanged += TreeViewOnSelectedItemChanged;
                }
                else
                {
                    treeView.SelectedItemChanged -= TreeViewOnSelectedItemChanged;
                }
            }
        }

        private static void TreeViewOnSelectedItemChanged(
            object sender,
            RoutedPropertyChangedEventArgs<object> routedPropertyChangedEventArgs)
        {
            var treeView = sender as TreeView;
            if (treeView != null)
            {
                SetSelectedItem(treeView, routedPropertyChangedEventArgs.NewValue);
            }
        }

        public static void SetSelectedItem(DependencyObject element, object value)
        {
            element.SetValue(SelectedItemProperty, value);
        }

        public static bool GetSelectedItem(DependencyObject element)
        {
            return (bool)element.GetValue(SelectedItemProperty);
        }

        public static readonly DependencyProperty EnabledProperty = DependencyProperty.RegisterAttached(
            "Enabled",
            typeof(bool),
            typeof(AttachedSelectedItem),
            new PropertyMetadata(false, OnEnabledChanged));

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.RegisterAttached(
            "SelectedItem",
            typeof(object),
            typeof(AttachedSelectedItem),
            new PropertyMetadata(null));
    }
}