﻿using System;
using System.Globalization;
using System.Windows;

namespace Pasca.Gui.View.Converters
{
    public class InverseBooleanToVisibilityConverter
            : ValueConverterBase<bool, Visibility, InverseBooleanToVisibilityConverter>
    {
        protected override Visibility Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return value ? Visibility.Collapsed : Visibility.Visible;
        }

        protected override bool ConvertBack(Visibility value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != Visibility.Visible;
        }
    }
}