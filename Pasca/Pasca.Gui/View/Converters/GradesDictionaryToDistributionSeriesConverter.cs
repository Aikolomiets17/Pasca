﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using LiveCharts;
using LiveCharts.Wpf;
using Pasca.Common.Extensions;
using Pasca.Gui.ViewModel.Charts;

namespace Pasca.Gui.View.Converters
{
    public sealed class GradesDictionaryToDistributionSeriesConverter
            : ValueConverterBase<IList<Bin>, SeriesCollection, GradesDictionaryToDistributionSeriesConverter>
    {
        protected override SeriesCollection Convert(
            IList<Bin> value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            var series = new SeriesCollection();
            {
                series.Add(
                    new ColumnSeries {
                        Title = string.Empty,
                        LabelPoint = point => CreateLabel(point, value)
                        ,
                        DataLabels = true,
                        Values = new ChartValues<int>(value.Select(b => b.Count))
                    });
            }

            return series;
        }

        private string CreateLabel(ChartPoint point, IList<Bin> bins)
        {
            var binN = point.X.AsInt();
            if (point.Y.AsInt() == 0 || binN < 0 || binN >= bins.Count)
            {
                return string.Empty;
            }

            return string.Format(
                LabelFormat,
                string.Join(
                    "\n",
                    bins[binN]
                            .Students
                            .Select(b => b.ToString())),
                point.X);
        }

        private static string LabelFormat = "{0}\n<=\"{1}\"";
    }
}