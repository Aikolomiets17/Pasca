﻿using System;
using System.Globalization;
using System.Windows.Data;
using Pasca.Common.DI;
using Pasca.Common.Trace;
using Pasca.Common.Types;
using Pasca.Gui.Trace;

// ReSharper disable RedundantIfElseBlock

namespace Pasca.Gui.View.Converters
{
    public abstract class ValueConverterBase<TFrom, TTo, TConverter> : IValueConverter
        where TConverter : class, new()
    {
        public static TConverter Instance => Singleton.Instance;
        protected ITracer Tracer => _tracer.Value;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TFrom)
            {
                return Convert((TFrom)value, targetType, parameter, culture);
            }
            else
            {
                Tracer.WriteLine(
                    TracerTags.Convert,
                    $"{typeof(TConverter).Name}.{nameof(Convert)}: value is not {typeof(TFrom).Name} but:{value ?? "null"}");
                return Binding.DoNothing;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TTo)
            {
                return ConvertBack((TTo)value, targetType, parameter, culture);
            }
            else
            {
                return Binding.DoNothing;
            }
        }

        protected abstract TTo Convert(TFrom value, Type targeType, object parameter, CultureInfo culture);

        protected virtual TFrom ConvertBack(TTo value, Type targeType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();

        private static readonly SingletonComponent<TConverter> Singleton =
                new SingletonComponent<TConverter>(() => new TConverter());
    }
}