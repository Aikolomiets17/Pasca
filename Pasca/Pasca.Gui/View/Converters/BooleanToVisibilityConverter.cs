﻿using System;
using System.Globalization;
using System.Windows;
using Pasca.Gui.Trace;

namespace Pasca.Gui.View.Converters
{
    public class BooleanToVisibilityConverter
            : ValueConverterBase<bool, Visibility, BooleanToVisibilityConverter>
    {
        protected override Visibility Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            var visibilityOnFalse = parameter as Visibility?;
            if (parameter != null && visibilityOnFalse == null)
            {
                Tracer.WriteLine(
                    TracerTags.Convert,
                    $"{nameof(BooleanToVisibilityConverter)}.{nameof(Convert)}: PARAMETER is not {nameof(Boolean)} but:{parameter}");
                return value ? Visibility.Visible : Visibility.Collapsed;
            }
            if (visibilityOnFalse != null)
            {
                return value ? Visibility.Visible : visibilityOnFalse.Value;
            }

            return value ? Visibility.Visible : DefaultVisibilityOnFalse;
        }

        protected override bool ConvertBack(Visibility value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == Visibility.Visible;
        }

        private const Visibility DefaultVisibilityOnFalse = Visibility.Collapsed;
    }
}