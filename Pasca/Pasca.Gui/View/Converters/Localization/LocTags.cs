﻿namespace Pasca.Gui.View.Converters.Localization
{
    public class LocTags : Common.Localization.LocTags
    {
        #region Inherited Tas

        public new const string Settings = nameof(Settings);

        #endregion

        #region GuiArtifacts

        public const string ArtifactPage = nameof(ArtifactPage);

        #endregion

        #region Gui

        public const string Gui = nameof(Gui); // Tag - must be unique
        public const string Cancel = nameof(Cancel);
        public const string Ok = nameof(Ok);
        public const string Warning = nameof(Warning);
        public const string SettingsChanged = nameof(SettingsChanged);
        public const string Loading = nameof(Loading);

        public const string Student = nameof(Student);

        #endregion

        #region GuiSettings

        public const string GuiSettings = nameof(GuiSettings);
        public const string AggregateExpression = nameof(AggregateExpression);
        public const string CriteriaGroupName = nameof(CriteriaGroupName);
        public const string AddNewCriterion = nameof(AddNewCriterion);
        public const string CriterionDescription = nameof(CriterionDescription);


        public const string CriteriaGroupAggregateExpressionPlaceholder = nameof(CriteriaGroupAggregateExpressionPlaceholder);
        public const string CriteriaAggregateExpressionPlaceholder = nameof(CriteriaAggregateExpressionPlaceholder);

        public const string NoAuthors = nameof(NoAuthors);

        public const string FloatCriterionViewModel = nameof(FloatCriterionViewModel);
        public const string NumericCriterionViewModel = nameof(NumericCriterionViewModel);
        public const string SetCriterionViewModel = nameof(SetCriterionViewModel);

        public const string Max = nameof(Max);
        public const string Min = nameof(Min);
        public const string Set = nameof(Set);
        public const string NewSetItem = nameof(NewSetItem);
        public const string Add = nameof(Add);
        public const string Remove = nameof(Remove);
        public const string Edit = nameof(Edit);

        public const string AddButton = nameof(AddButton);
        public const string RemoveButton = nameof(RemoveButton);
        public const string NextButton = nameof(NextButton);
        public const string PreviousButton = nameof(PreviousButton);
        public const string SelectButton = nameof(SelectButton);

        #endregion
    }
}