﻿using System;
using System.Globalization;
using System.Windows.Data;
using Pasca.Common.DI;
using Pasca.Common.Localization;
using Pasca.Common.Trace;
using Pasca.Common.Types;

namespace Pasca.Gui.View.Converters.Localization
{
    public class LocConverter : IValueConverter
    {
        public LocConverter(ILoc loc)
        {
            _loc = loc;
        }

        public static LocConverter Instance => Singleton.Instance;

        private ITracer Tracer => _tracer.Value;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Convert(value, parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }


        public object Convert(object value, object scope)
        {
            if (value == null)
            {
                Tracer.WriteLine(CommonTracerTags.Loc, "Value is null");
                return Binding.DoNothing;
            }

            if (value is Enum)
            {
                value = Enum.GetName(value.GetType(), value);

                if (value == null)
                {
                    Tracer.WriteLine(CommonTracerTags.Loc, "Value is null after Enum.GetName");
                    return Binding.DoNothing;
                }
            }

            if (scope != null)
            {
                return _loc.GetLocalizedString(scope.ToString(), value.ToString());
            }

            return _loc.GetLocalizedString(value.ToString());
        }

        private readonly ILoc _loc;
        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();

        private static readonly SingletonComponent<LocConverter> Singleton
                = new SingletonComponent<LocConverter>(
                    () => new LocConverter(DR.Get<ILoc>()));
    }
}