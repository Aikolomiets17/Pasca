﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Pasca.Gui.View.Converters.Localization
{
    public class Loc : MarkupExtension
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public Loc(string Scope)
        {
            this.Scope = Scope;
        }

        [ConstructorArgument(nameof(Scope))]
        public string Scope { get; set; }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            var ivp = serviceProvider.GetService(typeof(IProvideValueTarget)) as IProvideValueTarget;
            var target = ivp?.TargetObject as DependencyObject;

            if (target == null)
            {
                return this;
            }
            target.InvalidateProperty(KeyProperty);
            var binding = new Binding {
                Source = target,
                Path = new PropertyPath(KeyProperty),
                Converter = LocConverter.Instance,
                ConverterParameter = Scope
            };

            return binding.ProvideValue(serviceProvider);
        }

        public static void SetKey(DependencyObject element, object value)
        {
            element.SetValue(KeyProperty, value);
        }

        public static object GetKey(DependencyObject element)
        {
            return element.GetValue(KeyProperty);
        }

        public static readonly DependencyProperty KeyProperty = DependencyProperty.RegisterAttached(
            "Key",
            typeof(object),
            typeof(Loc));
    }
}