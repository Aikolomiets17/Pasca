﻿using System;
using System.Globalization;

namespace Pasca.Gui.View.Converters
{
    internal class InverseBooleanConverter
            : ValueConverterBase<bool, bool, InverseBooleanConverter>
    {
        protected override bool Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return !value;
        }

        protected override bool ConvertBack(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return !value;
        }
    }
}