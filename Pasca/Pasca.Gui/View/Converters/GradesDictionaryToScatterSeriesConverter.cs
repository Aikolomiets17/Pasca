﻿using System;
using System.Collections.Generic;
using System.Globalization;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using Pasca.Common.Students;

namespace Pasca.Gui.View.Converters
{
    public sealed class GradesDictionaryToScatterSeriesConverter
            :
                    ValueConverterBase
                    <IEnumerable<KeyValuePair<Person, double>>, SeriesCollection,
                        GradesDictionaryToScatterSeriesConverter>
    {
        protected override SeriesCollection Convert(
            IEnumerable<KeyValuePair<Person, double>> value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            var series = new SeriesCollection();
            var x = 0;
            foreach (var kv in value)
            {
                var label = kv.Value.ToString();
                var title = kv.Key.ToString();
                series.Add(
                    new ScatterSeries {
                        Title = title,
                        LabelPoint = _ => label,
                        Values = new ChartValues<ObservablePoint> {
                            new ObservablePoint(x++, kv.Value)
                        }
                    });
            }

            return series;
        }
    }
}