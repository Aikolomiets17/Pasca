﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using Pasca.Common.Students;

namespace Pasca.Gui.View.Converters
{
    public sealed class GradesDictionaryToPieSeriesConverter
            :
                    ValueConverterBase
                    <IEnumerable<KeyValuePair<Person, double>>, SeriesCollection, GradesDictionaryToPieSeriesConverter>
    {
        protected override SeriesCollection Convert(
            IEnumerable<KeyValuePair<Person, double>> value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            var bins = from grade in value
                       group grade by grade.Value
                       into g
                       select g;
            var series = new SeriesCollection();
            foreach (var bin in bins)
            {
                var grade = bin.Key;
                var count = bin.Count();
                series.Add(
                    new PieSeries {
                        Title = string.Join(Environment.NewLine, bin.Select(kv => kv.Key.ToString())),
                        Values = new ChartValues<ObservableValue> {
                            new ObservableValue(bin.Count())
                        },
                        LabelPoint = point => string.Format(LabelFormat, grade, count),
                        DataLabels = true,
                    });
            }

            return series;
        }

        private static string LabelFormat = "\"{0}\": {1}";
    }
}