﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Pasca.Gui.ViewModel.Auth;

namespace Pasca.Gui.View.Auth
{
    public partial class ApproveAuthWindow
    {
        public ApproveAuthWindow()
        {
            InitializeComponent();
        }

        protected override void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            base.OnDataContextChanged(sender, e);
            var vm = e.NewValue as ApproveAuthViewModel;

            var wb = FindName("WebBrowser") as WebBrowser;
            if (wb != null)
            {
                wb.Source = vm?.CurrentUri;
            }

            _navigatedCommand = vm?.Navigated;
        }

        private void WebBrowser_OnNavigated(object sender, NavigationEventArgs e)
        {
            _navigatedCommand?.Execute(e.Uri);
        }

        private ICommand _navigatedCommand;
    }
}