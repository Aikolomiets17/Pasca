﻿using System.Windows;
using System.Windows.Controls;

namespace Pasca.Gui.View.Artifacts
{
    public class ArtifactPageCellTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var element = container as FrameworkElement;

            if (element == null)
            {
                return base.SelectTemplate(item, container);
            }

            if (item == null)
            {
                return (DataTemplate)element.FindResource("NullArtifactPageCellTemplate");
            }
            return (DataTemplate)element.FindResource("ArtifactPageCellTemplate");
        }
    }
}