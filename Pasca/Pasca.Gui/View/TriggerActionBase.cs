﻿using System.Windows;
using System.Windows.Interactivity;
using Pasca.Common.Exceptions;
using Pasca.Gui.ViewModel;

namespace Pasca.Gui.View
{
    public abstract class TriggerActionBase<TData, TResult> : TriggerAction<FrameworkElement>
    {
        /*
        public InteractionRequest<TData, TResult> Argument
        {
            get
            {
                return (InteractionRequest<TData, TResult>)GetValue(ArgumentProperty);
            }
            set
            {
                SetValue(ArgumentProperty, value);
            }
        }
        public static readonly DependencyProperty ArgumentProperty = DependencyProperty.RegisterAttached(
            nameof(Argument),typeof(InteractionRequest<TData,TResult>),typeof(TriggerActionBase<TData,TResult>));
            */

        protected override void Invoke(object parameter)
        {
            var param = parameter as InteractionRequest<TData, TResult>;
            if (param == null)
            {
                throw new ArgumentTypeException(
                    nameof(parameter),
                    typeof(InteractionRequest<TData, TResult>),
                    parameter.GetType());
            }

            param.ResultCallback?.Invoke(param.Data, Execute(param.Data));
        }

        protected abstract TResult Execute(TData param);
    }
}