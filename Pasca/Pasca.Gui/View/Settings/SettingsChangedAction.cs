﻿using Pasca.Gui.View.Converters.Localization;

namespace Pasca.Gui.View.Settings
{
    public class SettingsChangedAction : UpdateOrContinueAction
    {
        protected override string Caption { get; } = LocTags.Warning;

        protected override string Message { get; } = LocTags.SettingsChanged;
    }
}