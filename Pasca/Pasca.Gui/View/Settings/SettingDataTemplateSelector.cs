﻿using System;
using System.Collections.Generic;
using Pasca.Gui.ViewModel.Settings;

namespace Pasca.Gui.View.Settings
{
    public class SettingDataTemplateSelector : DataTemplateSelectorBase
    {
        protected override Dictionary<Type, string> Templates => new Dictionary<Type, string> {
            {
                typeof(BooleanSettingViewModel), "BooleanSettingDataTemplate"
            }, {
                typeof(StringSettingViewModel), "StringSettingDataTemplate"
            }, {
                typeof(NumericWithRangeSettingViewModel), "NumericWithRangeSettingDataTemplate"
            }, {
                typeof(NumericSettingViewModel), "NumericSettingDataTemplate"
            }, {
                typeof(RangeSettingViewModel), "RangeSettingDataTemplate"
            }, {
                typeof(StudentListSettingViewModel), "StudentListSettingDataTemplate"
            }, {
                typeof(TeacherListSettingViewModel), "TeacherListSettingDataTemplate"
            }, {
                typeof(DateTimeSettingViewModel), "DateTimeSettingDataTemplate"
            }, {
                typeof(AssessmentCriteriaSettingViewModel), "AssessmentCriteriaSettingDataTemplate"
            }, {
                typeof(SelectableStringSettingViewModel), "SelectableStringSettingDataTemplate"
            }
        };
    }
}