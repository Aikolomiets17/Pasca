﻿using System.Windows;
using Pasca.Gui.View.Converters.Localization;
using Pasca.Gui.ViewModel;

namespace Pasca.Gui.View
{
    public abstract class UpdateOrContinueAction : TriggerActionBase<object, UpdateOrContinueActionResult>
    {
        protected abstract string Message { get; }
        protected abstract string Caption { get; }

        protected override UpdateOrContinueActionResult Execute(object param)
        {
            // TODO: Custom Message Box

            var result = MessageBox.Show(
                (string)LocConverter.Instance.Convert(Message, LocTags.Gui),
                (string)LocConverter.Instance.Convert(Caption, LocTags.Gui),
                MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                return UpdateOrContinueActionResult.Update;
            }

            return UpdateOrContinueActionResult.Continue;
        }
    }
}