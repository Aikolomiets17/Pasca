﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Pasca.Gui.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static object GetValue(this Binding binding, IServiceProvider provider)
        {
            var provideValueTarget = (IProvideValueTarget)provider.GetService(typeof(IProvideValueTarget));
            var targetObject = provideValueTarget.TargetObject as DependencyObject;
            var targetProperty = provideValueTarget.TargetProperty as DependencyProperty;
            if (targetObject == null || targetProperty == null)
            {
                return BindingOperations.GetBindingExpression(targetObject, targetProperty)?.DataItem;
            }

            return (binding.ProvideValue(provider) as BindingExpression)?.DataItem;
        }
    }
}