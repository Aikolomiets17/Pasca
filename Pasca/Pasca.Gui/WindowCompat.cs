﻿using System;
using System.Windows.Forms;

namespace Pasca.Gui
{
    internal class WindowCompat : IWin32Window
    {
        public WindowCompat(IntPtr handle)
        {
            Handle = handle;
        }

        public IntPtr Handle { get; }
    }
}