﻿namespace Pasca.Common.Types
{
    public class KeyItemPair<TKey, TItem>
    {
        public KeyItemPair() {}

        public KeyItemPair(TKey key, TItem item)
        {
            Key = key;
            Item = item;
        }

        public TKey Key { get; set; }
        public TItem Item { get; set; }
    }
}