﻿namespace Pasca.Common.Types
{
    public interface ITreeItem<T> : ITreeItem
    {
        T Value { get; }
    }
}