﻿using System;
using System.Collections.Generic;

namespace Pasca.Common.Types
{
    public abstract class SingletonComponentBase
    {
        public static void InvokeReset()
        {
            foreach (var component in _components)
            {
                component.OnReset();
            }
        }

        protected static void Add(SingletonComponentBase component)
        {
            _components.Add(component);
        }

        protected abstract void OnReset();
        private static readonly IList<SingletonComponentBase> _components = new List<SingletonComponentBase>();
    }

    public class SingletonComponent<T> : SingletonComponentBase
        where T : class
    {
        public SingletonComponent(Func<T> factory)
        {
            _factory = factory;
            Add(this);
        }

        public T Instance
        {
            get
            {
                lock (_lock)
                {
                    return _instance ?? (_instance = _factory());
                }
            }
            private set
            {
                lock (_lock)
                {
                    _instance = value;
                }
            }
        }

        public bool IsCreated => _instance != null;

        protected override void OnReset()
        {
            Instance = null;
        }

        private readonly Func<T> _factory;
        private readonly object _lock = new object();
        private T _instance;
    }
}