﻿using System;
using System.Collections.Generic;

namespace Pasca.Common.Types
{
    public interface ITreeItem : IDisposable
    {
        IList<ITreeItem> Children { get; }
    }
}