﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pasca.Common.Types
{
    public sealed class TaskThrottler
    {
        public TaskThrottler(TimeSpan timeout)
        {
            _timeout = timeout;
        }

        public void Add(Action action)
        {
            lock (_lock)
            {
                if (_current == null)
                {
                    if (_next == null)
                    {
                        _current = new Task(() => DoAction(action));
                        _current.Start();
                    }
                    else
                    {
                        // never happens
                    }
                }
                else
                {
                    _next = new Task(() => DoAction(action));
                }
            }
        }

        private void DoAction(Action action)
        {
            action();
            Thread.Sleep(_timeout);
            lock (_lock)
            {
                _current = null;
                (_current = _next)?.Start();
                _next = null;
            }
        }
        
        private readonly object _lock = new object();
        private Task _current;
        private Task _next;
        
        private readonly TimeSpan _timeout;
    }
}
