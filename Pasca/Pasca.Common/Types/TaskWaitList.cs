﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pasca.Common.Types
{
    public sealed class TaskWaitList : List<Task>
    {
        public void AddAndStart(Task item)
        {
            Add(item);
            item.Start();
        }

        public void Wait()
        {
            Task.WaitAll(ToArray());
        }
    }
}