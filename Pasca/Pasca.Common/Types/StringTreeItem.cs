﻿using System.Collections.Generic;
using Pasca.Common.Exceptions;

namespace Pasca.Common.Types
{
    public class StringTreeItem : ITreeItem<string>
    {
        public StringTreeItem(string value, string header, ITreeItem parent)
        {
            Assert.NotNull(value);

            Value = value;
            _header = header;
            Parent = parent;
        }

        public ITreeItem Parent { get; }

        public IList<ITreeItem> Children { get; } = new List<ITreeItem>();
        public string Value { get; }

        public void Dispose()
        {
            Parent?.Children.Remove(this);
        }

        public override string ToString()
        {
            return _header;
        }

        private readonly string _header;
    }
}