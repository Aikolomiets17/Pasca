﻿using System.Collections.Generic;
using System.Linq;

namespace Pasca.Common.Types
{
    public class SerializableDictionary<TKey, TValue>
    {
        protected SerializableDictionary()
        {
            _dictionaryImplementation = new Dictionary<TKey, TValue>();
        }

        public SerializableDictionary(Dictionary<TKey, TValue> dictionary)
        {
            _dictionaryImplementation = dictionary;
        }

        public KeyItemPair<TKey, TValue>[] Items
        {
            get
            {
                return
                        _dictionaryImplementation
                                .Keys
                                .Select(key => new KeyItemPair<TKey, TValue>(key, _dictionaryImplementation[key]))
                                .ToArray();
            }
            set
            {
                _dictionaryImplementation.Clear();
                if (value == null) {}
                else
                {
                    foreach (var pair in value)
                    {
                        _dictionaryImplementation.Add(pair.Key, pair.Item);
                    }
                }
            }
        }

        public static explicit operator Dictionary<TKey, TValue>(SerializableDictionary<TKey, TValue> dict)
        {
            return dict._dictionaryImplementation;
        }

        private readonly Dictionary<TKey, TValue> _dictionaryImplementation;
    }
}