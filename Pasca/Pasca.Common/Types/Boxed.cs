﻿namespace Pasca.Common.Types
{
    public class Boxed<T>
    {
        public T Value { get; set; }
    }
}