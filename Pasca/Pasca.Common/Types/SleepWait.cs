﻿using System;
using System.Threading;

namespace Pasca.Common.Types
{
    public class SleepWait
    {
        public SleepWait(TimeSpan interval)
            : this(interval, interval) {}

        public SleepWait(TimeSpan minInterval, TimeSpan maxInterval)
            : this(minInterval, maxInterval, TimeSpan.MaxValue) {}

        public SleepWait(TimeSpan minInterval, TimeSpan maxInterval, TimeSpan timeout)
        {
            _minInterval = minInterval;
            _maxInterval = maxInterval;
            _timeout = timeout;
        }

        public bool Wait(Func<bool> predicate)
        {
            var deadline = DateTime.Now + _timeout;
            var interval = _minInterval;
            while (DateTime.Now < deadline)
            {
                if (predicate())
                {
                    return true;
                }
                Thread.Sleep(interval);
                var doubled = interval + interval;
                if (doubled < _maxInterval)
                {
                    interval = doubled;
                }
            }
            return false;
        }

        private readonly TimeSpan _maxInterval;
        private readonly TimeSpan _minInterval;
        private readonly TimeSpan _timeout;
    }
}