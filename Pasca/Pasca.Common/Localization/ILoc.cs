﻿namespace Pasca.Common.Localization
{
    public interface ILoc
    {
        string GetLocalizedString(string tag, string value, params object[] parameters);
        string GetLocalizedString(string value);
    }
}