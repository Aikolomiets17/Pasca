﻿namespace Pasca.Common.Localization
{
    public class LocTags
    {
        public class Common
        {
            public const string Tag = nameof(Common);


            public const string Yes = nameof(Yes);

            public const string No = nameof(No);
        }

        public class Settings
        {
            public const string Tag = nameof(Settings);
        }
    }
}