﻿using System;
using System.Threading.Tasks;

namespace Pasca.Common.Auth
{
    public interface IAuth
    {
        Uri SignInUri { get; }
        Uri SignOutUri { get; }
        bool IsAuth { get; }
        string AccessToken { get; }

        Task SubmitCode(Uri redirectUriWithCode);
    }
}