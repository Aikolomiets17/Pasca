﻿using System;

namespace Pasca.Common.Exceptions
{
    public sealed class MappingException: Exception
    {
        public MappingException(string message, int number) 
            : base(message)
        {
            Number = number;
        }

        public int Number { get; }
    }
}
