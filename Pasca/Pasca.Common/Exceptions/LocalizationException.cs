﻿using System.Collections.Generic;

namespace Pasca.Common.Exceptions
{
    public class LocalizationException : KeyNotFoundException
    {
        public LocalizationException(string locString)
            : base("Could not find loc for string:" + locString + ".")
        {
            LocString = locString;
        }

        public string LocString { get; }
    }
}