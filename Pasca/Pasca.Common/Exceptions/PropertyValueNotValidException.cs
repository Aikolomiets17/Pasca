﻿using System;

namespace Pasca.Common.Exceptions
{
    public class PropertyValueNotValidException : ArgumentException
    {
        public PropertyValueNotValidException(string value, string propertyName)
            : base($"{value} is not valid {propertyName}", propertyName)
        {
            PropertyName = propertyName;
        }

        public string PropertyName { get; }
    }
}