﻿using System;

namespace Pasca.Common.Exceptions
{
    public class EqualityException : Exception
    {
        public EqualityException(object first, object second)
            : base($"Expected object {first} not equals object {second}") {}
    }
}