﻿using System;

namespace Pasca.Common.Exceptions
{
    public class ArgumentTypeException : ArgumentException
    {
        public ArgumentTypeException(string argumentName, Type expected, Type given)
            : base(argumentName + " is of type " + given + ". Expected: " + expected + ".") {}
    }
}