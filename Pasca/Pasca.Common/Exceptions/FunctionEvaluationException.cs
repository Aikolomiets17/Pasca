﻿using System;
using NCalc;

namespace Pasca.Common.Exceptions
{
    public sealed class FunctionEvaluationException : ArgumentException
    {
        public FunctionEvaluationException(string message, FunctionArgs args)
            : base(message)
        {
            FunctionArgs = args;
        }

        public FunctionArgs FunctionArgs { get; }
    }
}