﻿using System;

namespace Pasca.Common.Exceptions
{
    public class EnumValueException : NotSupportedException
    {
        public EnumValueException(Type type, Enum value)
            : base($"Value {value} is not supported for type {type}.") {}
    }
}