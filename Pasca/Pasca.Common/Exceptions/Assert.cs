﻿using System;
using Pasca.Common.Settings;

namespace Pasca.Common.Exceptions
{
    public static class Assert
    {
        public static void Type(Type expected, Type given, string argumentName = "")
        {
            if (expected != given)
            {
                throw new ArgumentTypeException(argumentName, expected, given);
            }
        }

        public static void Inherits(Type childType, Type baseType, string argumentName = "")
        {
            if (!(baseType == childType || childType.IsSubclassOf(baseType)))
            {
                throw new ArgumentTypeException(argumentName, childType, baseType);
            }
        }

        public static void IsInstanceOfType(Type type, object o, string argumentName = "")
        {
            if (!type.IsInstanceOfType(o))
            {
                throw new ArgumentTypeException(argumentName, type, o?.GetType());
            }
        }

        public static void SettingName(Setting setting, Enum expected)
        {
            NotNull(setting);
            NotNull(expected);

            if (!Equals(setting.Name, expected))
            {
                throw new EnumValueException(setting.GetType(), expected);
            }
        }

        public static void NotNull(object obj)
        {
            if (ReferenceEquals(obj, null))
            {
                throw new ArgumentNullException();
            }
        }

        public static void AreEqual(object first, object second)
        {
            if (!Equals(first, second))
            {
                throw new EqualityException(first, second);
            }
        }

        public static void True(bool expression)
        {
            if (!expression)
            {
                // todo: better exception
                throw new EqualityException(false, true);
            }
        }
    }
}