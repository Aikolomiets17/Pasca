﻿using System;

namespace Pasca.Common.Exceptions
{
    public class TypeNotRegisteredException : Exception
    {
        public TypeNotRegisteredException(Type type)
            : base($"Type {type} is not registered.") {}
    }
}