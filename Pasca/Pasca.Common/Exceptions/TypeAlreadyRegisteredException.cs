﻿using System;

namespace Pasca.Common.Exceptions
{
    public class TypeAlreadyRegisteredException : Exception
    {
        public TypeAlreadyRegisteredException(Type type, object instance)
            : base($"Type {type} is already registered with instance of type {instance.GetType()}.") {}
    }
}