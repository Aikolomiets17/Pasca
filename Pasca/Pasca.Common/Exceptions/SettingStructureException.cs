﻿using System;

namespace Pasca.Common.Exceptions
{
    public class SettingStructureException : InvalidOperationException
    {
        public SettingStructureException(string expectedName, string argumentName)
            : base($"Setting structure in {argumentName} not supported. Expected setting named: {expectedName}.") {}
    }
}