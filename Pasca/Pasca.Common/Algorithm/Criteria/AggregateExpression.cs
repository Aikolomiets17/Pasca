﻿using System;
using System.Collections.Generic;
using System.Linq;
using NCalc;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Trace;

namespace Pasca.Common.Algorithm.Criteria
{
    public class AggregateExpression : Expression
    {
        public AggregateExpression(string expression, IEnumerable<object> parameters)
            : base(expression, EvaluateOptions.IgnoreCase)
        {
            var i = 1;
            foreach (var parameter in parameters)
            {
                Parameters.Add($"{i++}", parameter);
            }
            EvaluateFunction += ExtensionFunctions;
        }

        public AggregateExpression(string expression, IEnumerable<double> parameters)
            : this(expression, parameters.Cast<object>()) { }

        protected ITracer Tracer => _tracer.Value;

        public double EvaluateDouble()
        {
            try
            {
                return Convert.ToDouble(Evaluate());
            }
            catch (Exception e)
            {
                Tracer.WriteLine(CommonTracerTags.Algo, "Could not convert Evaluate() result ToDouble");
                return 0;
            }
        }

        private static void ExtensionFunctions(string name, FunctionArgs functionArgs)
        {
            if (functionArgs.HasResult)
            {
                return;
            }

            name = name.ToLower();
            switch (name)
            {
            case Case:
                CaseFunction(functionArgs);
                return;
            default:
                return;
            }
        }

        private static void CaseFunction(FunctionArgs functionArgs)
        {
            var args = functionArgs.EvaluateParameters();
            Assert.True(args.Length % 2 == 1);

            var value = args[0];
            for (var i = 1; i < args.Length; i += 2)
            {
                if (value.Equals(args[i]) || ("\'" + value + "\'").Equals(args[i]) || ("\'" + args[i] + "\'").Equals(value))
                {
                    functionArgs.Result = args[i + 1];
                    return;
                }
            }

            throw new FunctionEvaluationException("Case function did not find an equal value", functionArgs);
        }

        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();

        private const string Case = "case";
    }
}