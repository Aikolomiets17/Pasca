﻿namespace Pasca.Common.Algorithm.Criteria
{
    public class NumericCriterion : RangeCriterion<int>
    {
        public NumericCriterion()
        {
            _min = int.MinValue;
            _max = int.MaxValue;
        }

        protected override int Parse(string data)
        {
            return int.Parse(data);
        }

        protected override bool TryParse(string data, out int val)
        {
            return int.TryParse(data, out val);
        }

        public override object Clone()
        {
            return new NumericCriterion {
                _min = _min,
                _max = _max,
                Description = Description
            };
        }
    }
}