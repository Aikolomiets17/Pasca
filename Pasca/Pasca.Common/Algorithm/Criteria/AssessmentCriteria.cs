﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;

namespace Pasca.Common.Algorithm.Criteria
{
    public class AssessmentCriteria : ICloneable
    {
        public AssessmentCriteria()
        {
            CriteriaGroups = new List<CriteriaGroup>();
            AggregateExpression = string.Empty;
        }

        public AssessmentCriteria(
            List<CriteriaGroup> criteriaGroups,
            string aggregateExpression)
        {
            CriteriaGroups = criteriaGroups;
            AggregateExpression = aggregateExpression;
        }

        public List<CriteriaGroup> CriteriaGroups { get; set; }

        public string AggregateExpression { get; set; }

        public object Clone()
        {
            return new AssessmentCriteria(CriteriaGroups, AggregateExpression);
        }

        public double Aggregate(double[] criteriaValues)
        {
            Assert.NotNull(criteriaValues);
            Assert.AreEqual(criteriaValues.Length, CriteriaGroups.Count);

            var expression = new AggregateExpression(AggregateExpression, criteriaValues);
            return expression.EvaluateDouble();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as AssessmentCriteria;
            if (other == null)
            {
                return false;
            }

            return AggregateExpression.OrdinalEquals(other.AggregateExpression)
                   && CriteriaGroups.SequenceEqual(other.CriteriaGroups);
        }

        public override int GetHashCode() =>
                AggregateExpression.GetHashCode() +
                CriteriaGroups.SumWithOverflow(group => group.GetHashCode());
    }
}