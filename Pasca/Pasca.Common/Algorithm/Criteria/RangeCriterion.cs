﻿using System;

namespace Pasca.Common.Algorithm.Criteria
{
    public abstract class RangeCriterion<T> : Criterion
        where T : struct, IComparable<T>
    {
        protected RangeCriterion()
            : base(string.Empty) {}

        protected RangeCriterion(string description)
            : base(description) {}

        public T Min
        {
            get { return _min; }
            set
            {
                if (value.CompareTo(_max) <= 0)
                {
                    _min = value;
                }
            }
        }

        public T Max
        {
            get { return _max; }
            set
            {
                if (value.CompareTo(_min) >= 0)
                {
                    _max = value;
                }
            }
        }

        public override string RangeDescription =>
                $"[{Min};{Max}]";

        public override bool Validate(string data, out object value)
        {
            T val;
            var parsed = TryParse(data, out val);
            value = val;
            return
                    parsed
                    && val.CompareTo(Min) >= 0
                    && val.CompareTo(Max) <= 0;
        }

        protected abstract T Parse(string data);
        protected abstract bool TryParse(string data, out T val);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            var other = obj as RangeCriterion<T>;
            if (other == null)
            {
                return false;
            }

            return Min.Equals(other.Min)
                   && Max.Equals(other.Max);
        }

        public override int GetHashCode() =>
                Min.GetHashCode() +
                Max.GetHashCode();

        protected T _max;

        protected T _min;
    }
}