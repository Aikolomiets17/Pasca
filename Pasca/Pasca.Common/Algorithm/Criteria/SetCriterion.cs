﻿using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Extensions;

namespace Pasca.Common.Algorithm.Criteria
{
    public class SetCriterion : Criterion
    {
        public SetCriterion()
        {
            Set = new List<string>();
        }

        public SetCriterion(List<string> set, string description)
            : base(description)
        {
            Set = set;
        }

        public List<string> Set { get; set; }

        public override string RangeDescription =>
                $"({string.Join(", ", Set)})";


        public override bool Validate(string data, out object val)
        {
            val = string.Format(Format, data);
            return Set.Contains(data);
        }

        public override object Clone()
        {
            return new SetCriterion(new List<string>(Set), Description);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            var other = obj as SetCriterion;
            if (other == null)
            {
                return false;
            }

            return
                    Description.OrdinalEquals(other.Description)
                    && Set.SequenceEqual(other.Set);
        }

        public override int GetHashCode() =>
                Description.GetHashCode() +
                Set.SumWithOverflow(c => c.GetHashCode());

        public static string Format = "\'{0}\'";
    }
}