﻿using System;

namespace Pasca.Common.Algorithm.Criteria
{
    public abstract class Criterion : ICloneable
    {
        public Criterion()
            : this(string.Empty) {}

        public Criterion(string description)
        {
            Description = description;
        }

        public string Description { get; set; }
        public abstract string RangeDescription { get; }

        public abstract object Clone();

        public abstract bool Validate(string data, out object value);
    }
}