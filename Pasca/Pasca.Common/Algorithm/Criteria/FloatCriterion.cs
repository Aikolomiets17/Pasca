﻿namespace Pasca.Common.Algorithm.Criteria
{
    public class FloatCriterion : RangeCriterion<double>
    {
        public FloatCriterion()
        {
            _min = double.NegativeInfinity;
            _max = double.PositiveInfinity;
        }

        protected override double Parse(string data)
        {
            return double.Parse(data);
        }

        protected override bool TryParse(string data, out double val)
        {
            if (!double.TryParse(data.Replace(",", "."), out val))
            {
                return double.TryParse(data.Replace(".", ","), out val);
            }

            return true;
        }

        public override object Clone()
        {
            return new FloatCriterion {
                _min = _min,
                _max = _max
            };
        }
    }
}