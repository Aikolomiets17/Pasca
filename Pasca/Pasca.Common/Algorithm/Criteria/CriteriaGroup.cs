﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;

namespace Pasca.Common.Algorithm.Criteria
{
    public class CriteriaGroup : ICloneable
    {
        public CriteriaGroup()
            : this(string.Empty, string.Empty, new List<Criterion>()) {}

        public CriteriaGroup(string groupName, string aggregateExpression, List<Criterion> criteria)
        {
            GroupName = groupName;
            AggregateExpression = aggregateExpression;
            Criteria = criteria;
        }

        public string GroupName { get; set; }
        public string AggregateExpression { get; set; }

        public List<Criterion> Criteria { get; set; }

        public object Clone()
        {
            return new CriteriaGroup(GroupName, AggregateExpression, Criteria.Clone());
        }

        public double Aggregate(object[] criteriaValues)
        {
            Assert.NotNull(criteriaValues);
            Assert.AreEqual(criteriaValues.Length, Criteria.Count);

            var expression = new AggregateExpression(AggregateExpression, criteriaValues);
            return expression.EvaluateDouble();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            var other = obj as CriteriaGroup;
            if (other == null)
            {
                return false;
            }

            return GroupName.OrdinalEquals(other.GroupName)
                   && AggregateExpression.OrdinalEquals(other.AggregateExpression)
                   && Criteria.SequenceEqual(other.Criteria);
        }

        public override int GetHashCode() =>
                GroupName.GetHashCode() +
                AggregateExpression.GetHashCode() +
                Criteria.SumWithOverflow(c => c.GetHashCode());
    }
}