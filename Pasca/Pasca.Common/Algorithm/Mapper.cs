﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Extensions;
using Pasca.Common.Students;

namespace Pasca.Common.Algorithm
{
    public class Mapper
    {
        public Mapper(IList<Person> students)
            : this(students, students) { }

        public Mapper(IList<Person> authors, IList<Person> reviewers)
            : this(authors, reviewers, 1) { }

        public Mapper(IList<Person> authors, IList<Person> reviewers, int desiredRatio, bool exceptAuthors = ExceptAuthorsParam)
        {
            ExceptAuthors = exceptAuthors;
            var reviewersCount = reviewers.Count;
            Ratio = Math.Min(exceptAuthors ? reviewersCount - 1 : reviewersCount, desiredRatio);

            Authors = new List<AnonymizedPerson>(authors.OfType<AnonymizedPerson>());
            Reviewers = new List<AnonymizedPerson>(reviewers.OfType<AnonymizedPerson>());

            if (Authors.Count != authors.Count || Reviewers.Count != reviewers.Count)
            {
                var students = authors.Union(reviewers).ToList();
                var anonymized = CreateAnonymized(students);

                Authors = authors.Select(a => anonymized.First(a.Equals)).ToList();
                Reviewers = reviewers.Select(a => anonymized.First(a.Equals)).ToList();
            }

            Mapping = CreateMapping();
        }

        public Mapper(
            IList<Person> authors,
            IList<Person> reviewers,
            IDictionary<Person, List<Person>> mapping,
            int desiredRatio,
            bool exceptAuthors = ExceptAuthorsParam)
            : this(authors, reviewers, desiredRatio, exceptAuthors)
        {
            Mapping = mapping.Select(
                    kv => new KeyValuePair<int, List<int>>(Authors.Who(kv.Key).Number, new List<int>(
                        kv.Value.Select(r => Reviewers.Who(r).Number)))
                ).ToDictionary(kv => kv.Key, kv => kv.Value);
        }

        public List<AnonymizedPerson> Authors { get; }
        public List<AnonymizedPerson> Reviewers { get; }
        public Dictionary<int, List<int>> Mapping { get; }

        public bool ExceptAuthors { get; }
        public int Ratio { get; }

        private Dictionary<int, List<int>> CreateMapping()
        {
            var dict = new Dictionary<int, List<int>>();
            var prio = Reviewers;
            var second = new List<AnonymizedPerson>();
            foreach (var author in Authors)
            {
                var prioShuffled = Shuffle(prio);
                if (ExceptAuthors)
                {
                    prioShuffled.Remove(author);
                }
                var secondShuffled = Shuffle(second);

                List<AnonymizedPerson> reviewers;
                if (Ratio <= prioShuffled.Count)
                {
                    reviewers = prioShuffled.Take(Ratio).ToList();
                    prio = prio.Except(reviewers).ToList();
                    second.AddRange(reviewers);
                }
                else
                {
                    reviewers = prioShuffled.Concat(
                        secondShuffled).Take(Ratio).ToList();
                    second = reviewers.Except(prio).ToList();
                    prio = Reviewers.Except(second).ToList();
                }

                dict.Add(author.Number, reviewers.Select(r => r.Number).ToList());
                if (!prio.Any())
                {
                    prio = Reviewers;
                    second = new List<AnonymizedPerson>();
                }
            }

            return dict;
        }

        private List<AnonymizedPerson> CreateAnonymized(IList<Person> students)
        {
            var numbers = GenerateUniqueList(GetMax(students.Count), students.Count);
            var anonymized = new List<AnonymizedPerson>();
            for (int i = 0; i < students.Count; i++)
            {
                anonymized.Add(new AnonymizedPerson(students[i], numbers[i]));
            }
            return anonymized;
        }

        private int GetMax(int length)
        {
            for (var i = 10; i <= 10000; i *= 10)
            {
                if (length < i)
                {
                    return i;
                }
            }

            throw new OverflowException($"Mapper.GetMax: length is too big (length={length})");
        }

        private List<int> GenerateUniqueList(int max, int length)
        {
            var set = new HashSet<int>();
            for (var i = 0; i < length; i++)
            {
                while (!set.Add(_random.Next(max)))
                {
                    // do nothing
                }
            }
            return set.ToList();
        }

        private List<T> Shuffle<T>(IEnumerable<T> source)
        {
            var list = new List<T>(source);

            // Note i > 0 to avoid final pointless iteration
            for (var i = list.Count - 1; i > 0; i--)
            {
                // Swap element "i" with a random earlier element it (or itself)
                var swapIndex = _random.Next(i + 1);
                var tmp = list[i];
                list[i] = list[swapIndex];
                list[swapIndex] = tmp;
            }

            return list;
        }

        private const bool ExceptAuthorsParam = true;
        private static readonly Random _random = new Random();
    }
}