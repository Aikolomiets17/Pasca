﻿using System;
using System.Collections.Generic;
using Pasca.Common.Algorithm.Criteria;
using Pasca.Common.Exceptions;
using Pasca.Common.Students;

namespace Pasca.Common.Settings
{
    public class SessionSettings : ArraySetting
    {
        public SessionSettings(Enum name)
            : base(name)
        {
            Assert.AreEqual(name, CommonSettingName.Session);
        }

        public SessionSettings(Enum name, List<Setting> values)
            : base(name, values)
        {
            Assert.AreEqual(name, CommonSettingName.Session);
        }

        public SessionSettings(ArraySetting @base)
            : base(@base.Name, @base.SubSettings)
        {
            Assert.AreEqual(@base.Name, CommonSettingName.Session);
        }

        protected SessionSettings()
            : base(CommonSettingName.Session)
        {
        }

        public string SessionName()
        {
            return FindOrDefault<StringSetting>(CommonSettingName.SessionName).Value;
        }

        public List<Person> SessionAuthors()
        {
            return FindOrDefault<PersonListSetting>(CommonSettingName.SessionAuthors).Value;
        }

        public List<AnonymizedPerson> SessionAnonymizedAuthors()
        {
            return FindOrDefault<AnonymizedPersonListSetting>(CommonSettingName.SessionAuthors).AnonymizedValue;
        }

        public List<Person> SessionReviewers()
        {
            return Find<PersonListSetting>(CommonSettingName.SessionReviewers).Value;
        }

        public List<AnonymizedPerson> SessionAnonymizedReviewers()
        {
            return FindOrDefault<AnonymizedPersonListSetting>(CommonSettingName.SessionReviewers).AnonymizedValue;
        }

        public Dictionary<int, List<int>> SessionMapping()
        {
            return FindOrDefault<IntMappingSetting>(CommonSettingName.SessionMapping).Value;
        }

        public DateTime SessionReviewBegin()
        {
            return FindOrDefault<DateTimeSetting>(CommonSettingName.SessionReviewBegin).Value;
        }

        public DateTime SessionReviewEnd()
        {
            return FindOrDefault<DateTimeSetting>(CommonSettingName.SessionReviewEnd).Value;
        }

        public DateTime SessionSubmissionBegin()
        {
            return FindOrDefault<DateTimeSetting>(CommonSettingName.SessionSubmissionBegin).Value;
        }

        public DateTime SessionSubmissionEnd()
        {
            return FindOrDefault<DateTimeSetting>(CommonSettingName.SessionSubmissionEnd).Value;
        }

        public int SessionReviewersRatio()
        {
            return (int)FindOrDefault<NumericWithRangeSetting>(CommonSettingName.SessionReviewersRatio).Value;
        }

        public string SessionAssignment()
        {
            return FindOrDefault<StringSetting>(CommonSettingName.SessionAssignment).Value;
        }

        public AssessmentCriteria SessionAssessmentCriteria()
        {
            return FindOrDefault<AssessmentCriteriaSetting>(CommonSettingName.SessionAssessmentCriteria).Value;
        }

        public override Setting Clone()
        {
            return new SessionSettings(Name, Clone(SubSettings));
        }
    }
}
