﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Exceptions;

namespace Pasca.Common.Settings
{
    public sealed class IntMappingSetting : MappingSetting<int>
    {
        private IntMappingSetting()
        {
            Value = new Dictionary<int, List<int>>();
        }

        public IntMappingSetting(Enum name, Dictionary<int, List<int>> mapping)
            : base(name)
        {
            Assert.NotNull(mapping);

            Value = new Dictionary<int, List<int>>(mapping);
        }

        public IntMappingSetting(Enum name)
            : this(name, new Dictionary<int, List<int>>()) { }
        
        public override Setting Clone()
        {
            return new IntMappingSetting(
                Name,
                Value.Aggregate(
                    new Dictionary<int, List<int>>(),
                    (s, pair) =>
                    {
                        s.Add(pair.Key, pair.Value);
                        return s;
                    }));
        }
    }
}