﻿using System;
using System.Xml.Serialization;
using Pasca.Common.Types;

namespace Pasca.Common.Settings
{
    public abstract class Setting
    {
        protected Setting() {}

        protected Setting(Enum name)
        {
            Name = name;
        }

        [XmlElement(nameof(Name))]
        public EnumProxyType NameValueProxy
        {
            get
            {
                var type = Name.GetType();
                return new EnumProxyType {
                    Value = Enum.GetName(type, Name),
                    Type = type.AssemblyQualifiedName
                };
            }
            set
            {
                var t = Type.GetType(value.Type, true);
                Name = (Enum)Enum.Parse(t, value.Value);
            }
        }

        [XmlIgnore]
        public Enum Name { get; private set; }

        public abstract Setting Clone();
    }
}