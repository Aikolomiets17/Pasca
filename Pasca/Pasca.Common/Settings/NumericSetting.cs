﻿using System;

namespace Pasca.Common.Settings
{
    public class NumericSetting : Setting
    {
        protected NumericSetting() {}

        public NumericSetting(Enum name, long value)
            : base(name)
        {
            _value = value;
        }

        public virtual long Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public override Setting Clone()
        {
            return new NumericSetting(Name, Value);
        }

        private long _value;
    }
}