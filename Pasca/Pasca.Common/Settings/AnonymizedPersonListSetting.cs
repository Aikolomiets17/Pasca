﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.Common.Students;

namespace Pasca.Common.Settings
{
    public sealed class AnonymizedPersonListSetting : PersonListSetting
    {
        private AnonymizedPersonListSetting() { }

        public AnonymizedPersonListSetting(Enum name, List<AnonymizedPerson> list)
            : base(name, CheckList(list)) { }

        public AnonymizedPersonListSetting(Enum name)
            : this(name, new List<AnonymizedPerson>()) { }

        [XmlIgnore]
        public List<AnonymizedPerson> AnonymizedValue => Value.OfType<AnonymizedPerson>().ToList();

        public override Setting Clone()
        {
            if (Value.Any(v => !(v is AnonymizedPerson)))
            {
                return new PersonListSetting(Name, Value.Clone());
            }

            return new AnonymizedPersonListSetting(Name, AnonymizedValue.Clone());
        }

        private static List<Person> CheckList(List<AnonymizedPerson> list)
        {
            Assert.NotNull(list);
            return new List<Person>(list);
        }
    }
}