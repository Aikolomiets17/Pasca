﻿using System;
using System.Collections.Generic;
using Pasca.Common.Extensions;
using Pasca.Common.Students;

namespace Pasca.Common.Settings
{
    public class PersonListSetting : ListSetting<Person>
    {
        protected PersonListSetting() {}

        public PersonListSetting(Enum name, List<Person> list)
            : base(name, list) { }

        public PersonListSetting(Enum name)
            : this(name, new List<Person>()) {}

        public override Setting Clone()
        {
            return new PersonListSetting(Name, Value.Clone());
        }
    }
}