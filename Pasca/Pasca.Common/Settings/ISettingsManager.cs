﻿using System;
using System.Threading.Tasks;

namespace Pasca.Common.Settings
{
    public interface ISettingsManager
    {
        Task<T> GetSettingAsync<T>(Enum settingName, object param = null)
            where T : Setting;
    }
}