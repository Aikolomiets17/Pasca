﻿namespace Pasca.Common.Settings
{
    public enum CommonSettingName
    {
        Invalid = 0,

        #region General

        [Desc(typeof(SessionListSetting))]
        Sessions,
        [Desc(typeof(ArraySetting))]
        Session,

        #endregion

        #region Participants

        [Desc(typeof(PersonListSetting))]
        Students,
        [Desc(typeof(PersonListSetting))]
        Teachers,

        #endregion

        #region Session

        [Desc(typeof(StringSetting))]
        SessionName,

        [Desc(typeof(DateTimeSetting))]
        SessionSubmissionBegin,
        [Desc(typeof(DateTimeSetting))]
        SessionSubmissionEnd,
        [Desc(typeof(DateTimeSetting))]
        SessionReviewBegin,
        [Desc(typeof(DateTimeSetting))]
        SessionReviewEnd,

        [Desc(typeof(StringSetting))]
        SessionAssignment,
        [Desc(typeof(AssessmentCriteriaSetting))]
        SessionAssessmentCriteria,
        [Desc(typeof(AnonymizedPersonListSetting))]
        SessionAuthors,
        [Desc(typeof(AnonymizedPersonListSetting))]
        SessionReviewers,

        [Desc(typeof(NumericWithRangeSetting), 1L, 0L, 100L)] // todo: 0,0,0,
        SessionReviewersRatio,
        [Desc(typeof(IntMappingSetting))]
        SessionMapping,

        #endregion
    }
}
