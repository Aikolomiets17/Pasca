﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Pasca.Common.Exceptions;
using Pasca.Common.Types;

namespace Pasca.Common.Settings
{
    public abstract class MappingSetting<T> : Setting
    {
        protected MappingSetting()
        {
            Value = new Dictionary<T, List<T>>();
        }

        protected MappingSetting(Enum name, Dictionary<T, List<T>> mapping)
            : base(name)
        {
            Assert.NotNull(mapping);

            Value = new Dictionary<T, List<T>>(mapping);
        }

        protected MappingSetting(Enum name)
            : this(name, new Dictionary<T, List<T>>()) { }

        [XmlIgnore]
        public Dictionary<T, List<T>> Value
        {
            get { return (Dictionary<T, List<T>>)Dictionary; }
            set
            {
                Dictionary = new SerializableDictionary<T, List<T>>(value);
            }
        }

        public SerializableDictionary<T, List<T>> Dictionary { get; set; }

        public abstract override Setting Clone();
    }
}
