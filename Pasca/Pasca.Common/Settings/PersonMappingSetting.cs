﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Exceptions;
using Pasca.Common.Students;

namespace Pasca.Common.Settings
{
    public sealed class PersonMappingSetting : MappingSetting<Person>
    {
        private PersonMappingSetting()
        {
            Value = new Dictionary<Person, List<Person>>();
        }

        public PersonMappingSetting(Enum name, Dictionary<Person, List<Person>> mapping)
            : base(name)
        {
            Assert.NotNull(mapping);

            Value = new Dictionary<Person, List<Person>>(mapping);
        }

        public PersonMappingSetting(Enum name)
            : this(name, new Dictionary<Person, List<Person>>()) { }

        public override Setting Clone()
        {
            return new PersonMappingSetting(
                Name,
                Value.Aggregate(
                    new Dictionary<Person, List<Person>>(),
                    (s, pair) =>
                    {
                        s.Add(pair.Key, pair.Value);
                        return s;
                    }));
        }
    }
}