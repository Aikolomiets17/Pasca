﻿using System;
using Pasca.Common.Algorithm.Criteria;
using Pasca.Common.Exceptions;

namespace Pasca.Common.Settings
{
    public sealed class AssessmentCriteriaSetting : Setting
    {
        private AssessmentCriteriaSetting()
        {
            Value = new AssessmentCriteria();
        }

        public AssessmentCriteriaSetting(Enum name, AssessmentCriteria criteria)
            : base(name)
        {
            Assert.NotNull(criteria);

            Value = criteria;
        }

        public AssessmentCriteriaSetting(Enum name)
            : this(name, new AssessmentCriteria()) {}

        public AssessmentCriteria Value { get; set; }

        public override Setting Clone()
        {
            return new AssessmentCriteriaSetting(Name, (AssessmentCriteria)Value.Clone());
        }
    }
}