﻿using System;
using Pasca.Common.Exceptions;

namespace Pasca.Common.Settings
{
    public sealed class NumericWithRangeSetting : NumericSetting
    {
        private NumericWithRangeSetting() {}

        public NumericWithRangeSetting(Enum name, long value, long min, long max)
            : base(name, value)
        {
            Assert.True(min <= max);
            Assert.True(value >= min);
            Assert.True(value <= max);

            Min = min;
            Max = max;
        }

        public NumericWithRangeSetting(Enum name)
            : this(name, 0, 0, 0) {}

        public long Min
        {
            get { return _min; }
            set
            {
                _min = value;
                if (Value < Min)
                {
                    Value = Min;
                }
            }
        }

        public long Max
        {
            get { return _max; }
            set
            {
                _max = value;
                if (Value > Max)
                {
                    Value = Max;
                }
            }
        }

        public override long Value
        {
            get { return base.Value; }

            set
            {
                base.Value = value;
                if (Value < Min)
                {
                    Value = Min;
                }
                if (Value > Max)
                {
                    Value = Max;
                }
            }
        }

        public override Setting Clone()
        {
            return new NumericWithRangeSetting(Name, Value, Min, Max);
        }

        private long _max = long.MaxValue;


        private long _min = long.MinValue;
    }
}