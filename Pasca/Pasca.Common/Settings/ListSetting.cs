﻿using System;
using System.Collections.Generic;
using Pasca.Common.Exceptions;

namespace Pasca.Common.Settings
{
    public abstract class ListSetting<T> : Setting
    {
        protected ListSetting()
        {
            Value = new List<T>();
        }

        protected ListSetting(Enum name, List<T> list)
            : base(name)
        {
            Assert.NotNull(list);
            Value = list;
        }

        public List<T> Value { get; set; }

        public abstract override Setting Clone();
    }
}