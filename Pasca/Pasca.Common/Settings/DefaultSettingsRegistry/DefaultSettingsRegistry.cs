﻿using System;
using System.Linq;
using Pasca.Common.DI;
using Pasca.Common.Trace;

namespace Pasca.Common.Settings.DefaultSettingsRegistry
{
    public class DefaultSettingsRegistry : IDefaultSettingsRegistry
    {
        private ITracer Tracer => _tracer.Value;

        public virtual T GetDefault<T>(Enum settingName)
            where T : Setting
        {
            T commonSettingValue;
            if (!TryGetDefaultValueBasedOnDescAttribute<T, CommonSettingName>(settingName, out commonSettingValue))
            {
                Tracer.WriteLine(
                    CommonTracerTags.SettingManager,
                    $"DefaultSettingsRegistry: could not get default setting value for {settingName}, type={settingName.GetType()}, valueType={typeof(T).Name}");
            };

            
            return commonSettingValue;
        }

        protected bool TryGetDefaultValueBasedOnDescAttribute<TValue, TSettingName>(Enum settingNameEnum, out TValue val)
            where TValue : Setting
            where TSettingName : struct, IComparable, IFormattable, IConvertible
        {
            val = default(TValue);

            var settingName = settingNameEnum as TSettingName?;
            if (settingName == null)
            {
                return false;
            }

            var info = typeof(TSettingName)
                .GetMember(settingName.ToString())
                .FirstOrDefault()
                ?.GetCustomAttributes(typeof(DescAttribute), false)
                .FirstOrDefault();

            var def = (info as DescAttribute)?.CreateDefault(settingNameEnum);

            val = def as TValue;
            if (val != null)
            {
                return true;
            }

            Tracer.WriteLine(
                CommonTracerTags.SettingManager,
                $"DefaultSettingsRegistry: Desc returned {def?.GetType().Name}, expected {typeof(TValue).Name}");
            return false;
        }

        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();
    }
}