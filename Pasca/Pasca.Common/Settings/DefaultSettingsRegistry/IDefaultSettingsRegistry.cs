﻿using System;

namespace Pasca.Common.Settings.DefaultSettingsRegistry
{
    public interface IDefaultSettingsRegistry
    {
        T GetDefault<T>(Enum settingName) where T : Setting;
    }
}
