﻿using System;
using Pasca.Common.Exceptions;

namespace Pasca.Common.Settings
{
    public sealed class RangeSetting : Setting
    {
        private RangeSetting() {}

        public RangeSetting(Enum name, long min, long max)
            : base(name)
        {
            Assert.True(min <= max);

            Min = min;
            Max = max;
        }

        public RangeSetting(Enum name)
            : this(name, 0, 0) {}

        public long Min
        {
            get { return _min; }
            set
            {
                _min = value;

                if (Max < Min)
                {
                    Max = Min;
                }
            }
        }

        public long Max
        {
            get { return _max; }
            set
            {
                _max = value;

                if (Max < Min)
                {
                    Max = Min;
                }
            }
        }

        public override Setting Clone()
        {
            return new RangeSetting(Name, Min, Max);
        }

        private long _max;

        private long _min;
    }
}