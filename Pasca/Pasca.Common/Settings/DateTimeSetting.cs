﻿using System;

namespace Pasca.Common.Settings
{
    public sealed class DateTimeSetting : Setting
    {
        private DateTimeSetting()
        {
            Value = DateTime.Today;
        }

        public DateTimeSetting(Enum name, DateTime value)
            : base(name)
        {
            Value = value;
        }

        public DateTimeSetting(Enum name)
            : this(name, DateTime.Today) {}

        public DateTime Value { get; set; }

        public override Setting Clone()
        {
            return new DateTimeSetting(Name, Value);
        }
    }
}