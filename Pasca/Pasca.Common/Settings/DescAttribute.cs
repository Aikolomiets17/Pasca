﻿using System;
using System.Linq;
using System.Reflection;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Trace;

namespace Pasca.Common.Settings
{
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class DescAttribute : Attribute
    {
        public DescAttribute(Type type, params object[] defaultConstructorArguments)
        {
            Assert.Inherits(type, typeof(Setting), nameof(type));

            Type = type;
            DefaultConstructorArguments = defaultConstructorArguments ?? new object[0];
        }

        public DescAttribute(Type type)
            : this(type, null) {}

        public Type Type { get; }
        public object[] DefaultConstructorArguments { get; }

        private ITracer Tracer => _tracer.Value;

        public Setting CreateDefault(Enum name)
        {
            var arguments = new object[] {
                name
            }.Union(DefaultConstructorArguments).ToArray();

            var constructor = Type
                    .GetConstructors(BindingFlags.Public | BindingFlags.Instance)
                    .FirstOrDefault(c => FilterConstructor(c, arguments));
            if (constructor != null)
            {
                try
                {
                    return constructor.Invoke(arguments) as Setting;
                }
                catch
                {
                    Tracer.WriteLine(
                        CommonTracerTags.SettingManager,
                        $"Desc: Could not invoke constructor for type {Type.Name} given {arguments.Length} arguments");
                }
            }
            return null;
        }

        private bool FilterConstructor(ConstructorInfo constructor, object[] arguments)
        {
            var parameters = constructor.GetParameters();
            if (parameters.Length != arguments.Length)
            {
                return false;
            }
            for (var i = 0; i < parameters.Length; i++)
            {
                if (!parameters[i].ParameterType.IsInstanceOfType(arguments[i]))
                {
                    return false;
                }
            }
            return true;
        }

        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();
    }
}