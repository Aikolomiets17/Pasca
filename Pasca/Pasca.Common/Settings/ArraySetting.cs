﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Settings.DefaultSettingsRegistry;

namespace Pasca.Common.Settings
{
    public class ArraySetting : Setting
    {
        protected ArraySetting()
        {
            SubSettings = new List<Setting>();
        }

        public ArraySetting(Enum name, List<Setting> values)
            : base(name)
        {
            Assert.NotNull(values);

            SubSettings = Clone(values);
        }

        public ArraySetting(Enum name)
            : this(name, new List<Setting>()) {}

        public Setting this[Enum settingName]
        {
            get { return SubSettings.FirstOrDefault(s => s.Name.Equals(settingName)); }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value), "ArraySetting attempt to insert null setting.");
                }
                var idx = SubSettings.FindIndex(s => s.Name.Equals(settingName));
                if (idx == -1)
                {
                    SubSettings.Add(value);
                }
                else
                {
                    SubSettings[idx] = value;
                }
            }
        }

        public List<Setting> SubSettings { get; set; }

        public Setting Find(Enum name)
        {
            ArraySetting _;
            return Find(name, out _);
        }

        public T Find<T>(Enum name)
            where T : Setting
        {
            var found = Find(name);
            if (found == null)
            {
                return null;
            }
            Assert.Inherits(found.GetType(), typeof(T));
            return (T)found;
        }

        public Setting Find(Enum name, out ArraySetting container)
        {
            foreach (var setting in SubSettings)
            {
                if (name.Equals(setting.Name))
                {
                    container = this;
                    return setting;
                }

                var array = setting as ArraySetting;
                var found = array?.Find(name);
                if (found != null)
                {
                    container = array;
                    return found;
                }
            }

            container = null;
            return null;
        }

        public static ArraySetting Update(ArraySetting oldSetting, ArraySetting newSetting)
        {
            if (newSetting == null)
            {
                return oldSetting;
            }
            if (oldSetting == null)
            {
                oldSetting = new ArraySetting(newSetting.Name, new List<Setting>());
            }

            var name = oldSetting.Name;
            Assert.SettingName(newSetting, name);

            var updatedSettings = new ArraySetting(name, oldSetting.SubSettings);
            foreach (var setting in newSetting.SubSettings)
            {
                ArraySetting container;
                var found = updatedSettings.Find(setting.Name, out container);

                if (found == null)
                {
                    updatedSettings.SubSettings.Add(setting);
                }
                else
                {
                    // todo: what if found is not ArraySetting?
                    if (setting is ArraySetting)
                    {
                        container[setting.Name] = Update((ArraySetting)found, (ArraySetting)setting);
                    }
                    else
                    {
                        container[setting.Name] = setting;
                    }
                }
            }

            return updatedSettings;
        }

        public T FindOrDefault<T>(Enum name)
            where T : Setting
        {
            return Find<T>(name) ?? DR.Get<IDefaultSettingsRegistry>().GetDefault<T>(name);
        }

        public override Setting Clone()
        {
            return new ArraySetting(Name, Clone(SubSettings));
        }

        protected static List<Setting> Clone(List<Setting> values)
        {
            return values.Select(value => value.Clone()).ToList();
        }
    }
}