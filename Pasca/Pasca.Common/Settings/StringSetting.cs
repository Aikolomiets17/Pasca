﻿using System;
using Pasca.Common.Exceptions;

namespace Pasca.Common.Settings
{
    public sealed class StringSetting : Setting
    {
        private StringSetting()
        {
            Value = string.Empty;
        }

        public StringSetting(Enum name, string value)
            : base(name)
        {
            Assert.NotNull(value);

            Value = value;
        }

        public StringSetting(Enum name)
            : this(name, string.Empty) {}

        public string Value { get; set; }

        public override Setting Clone()
        {
            return new StringSetting(Name, Value);
        }
    }
}