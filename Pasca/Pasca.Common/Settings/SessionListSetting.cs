﻿using System;
using System.Collections.Generic;
using Pasca.Common.Extensions;
using Pasca.Common.Students;

namespace Pasca.Common.Settings
{
    public sealed class SessionListSetting : ListSetting<Session>
    {
        private SessionListSetting() {}

        public SessionListSetting(Enum name, List<Session> session)
            : base(name, session) {}

        public SessionListSetting(Enum name)
            : this(name, new List<Session>()) {}

        public override Setting Clone()
        {
            return new SessionListSetting(Name, Value.Clone());
        }
    }
}