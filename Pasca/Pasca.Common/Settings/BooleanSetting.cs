﻿using System;

namespace Pasca.Common.Settings
{
    public sealed class BooleanSetting : Setting
    {
        private BooleanSetting() {}

        public BooleanSetting(Enum name, bool value)
            : base(name)
        {
            Value = value;
        }

        public bool Value { get; set; }

        public override Setting Clone()
        {
            return new BooleanSetting(Name, Value);
        }
    }
}