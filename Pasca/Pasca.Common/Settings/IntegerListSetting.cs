﻿using System;
using System.Collections.Generic;

namespace Pasca.Common.Settings
{
    public sealed class IntegerListSetting : ListSetting<int>
    {
        private IntegerListSetting() {}

        public IntegerListSetting(Enum name, List<int> value)
            : base(name, value) {}

        public IntegerListSetting(Enum name)
            : this(name, new List<int>()) {}

        public override Setting Clone()
        {
            return new IntegerListSetting(Name, new List<int>(Value));
        }
    }
}