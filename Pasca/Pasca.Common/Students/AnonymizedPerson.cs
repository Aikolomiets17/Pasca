﻿using System;
namespace Pasca.Common.Students
{
    public class AnonymizedPerson : Person
    {
        private AnonymizedPerson() { }

        public AnonymizedPerson(Person person, int number)
            : base(person.FullName, person.Email, person.Role)
        {
            Number = number;
        }

        public AnonymizedPerson(String fullName, String email, PersonRole role, int number)
            : base(fullName, email, role)
        {
            Number = number;
        }

        public int Number { get; set; }

        public override object Clone()
        {
            return new AnonymizedPerson(this, Number);
        }

        public override string ToString()
        {
            return $"{FullName}({Number})";
        }

        public string ToAnonymizedString()
        {
            return Number.ToString();
        }
    }
}
