﻿namespace Pasca.Common.Students
{
    public class PersonBuilder
    {
        public string FullName { get; set; }

        //public string Surname { get; set; }
        //public string GivenName { get; set; }
        //public string SecondName { get; set; }

        public string Email { get; set; }
        public PersonRole Role { get; set; }

        public Person ToPerson()
        {
            return new Person(FullName, Email, Role);
        }
    }
}