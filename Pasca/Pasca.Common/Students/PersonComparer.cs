﻿using System;
using System.Collections.Generic;
using Pasca.Common.Types;

namespace Pasca.Common.Students
{
    public sealed class PersonComparer : IComparer<Person>
    {
        public static PersonComparer Instance => _singleton.Instance;

        public int Compare(Person x, Person y)
        {
            if (ReferenceEquals(x, y))
            {
                return 0;
            }

            return StringComparer.InvariantCultureIgnoreCase.Compare(x?.FullName, y?.FullName);
        }

        private static readonly SingletonComponent<PersonComparer> _singleton = new SingletonComponent<PersonComparer>(
            () => new PersonComparer());
    }
}