﻿namespace Pasca.Common.Students
{
    public enum PersonRole
    {
        Student,
        Teacher
    }
}