﻿using System;

namespace Pasca.Common.Students
{
    // todo: substitute to string
    public class Session : ICloneable
    {
        public string SessionName { get; set; }

        public object Clone()
        {
            return new Session {
                SessionName = SessionName
            };
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as Session;

            if (ReferenceEquals(other, null))
            {
                return false;
            }

            return Equals(SessionName, other.SessionName);
        }

        public override int GetHashCode()
        {
            return SessionName?.GetHashCode() ?? 0;
        }
    }
}