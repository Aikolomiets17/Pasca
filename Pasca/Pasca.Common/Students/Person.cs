﻿using System;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;

namespace Pasca.Common.Students
{
    public class Person : ICloneable
    {
        protected Person() {}

        public Person(string fullName, string email, PersonRole role)
        {
            Assert.NotNull(fullName);

            FullName = fullName;
            Email = email;
            Role = role;

            var split = fullName.Split(' ');
            Surname = split.Length > 0 ? split[0] : string.Empty;
            GivenName = split.Length > 1 ? split[1] : string.Empty;
            SecondName = split.Length > 2 ? split[2] : string.Empty;
        }

        public string FullName
        {
            get { return _fullName; }
            set
            {
                if (!value.IsValidFullName())
                {
                    throw new PropertyValueNotValidException(value, nameof(FullName));
                }

                _fullName = value;
            }
        }

        public PersonRole Role { get; }

        public string GivenName { get; }

        public string Surname { get; }

        public string SecondName { get; }

        public string Email
        {
            get { return _email; }
            set
            {
                if (!value.IsValidEmail())
                {
                    throw new PropertyValueNotValidException(value, nameof(Email));
                }

                _email = value;
            }
        }

        public bool IsTeacher => Role == PersonRole.Teacher;
        public bool IsStudent => Role == PersonRole.Student;

        public virtual object Clone()
        {
            return new Person(FullName, Email, Role);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as Person;
            if (other != null)
            {
                return FullName.OrdinalIgnoreCaseEquals(other.FullName);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return FullName.GetHashCode();
        }

        public override string ToString()
        {
            return FullName;
        }

        private string _email;

        private string _fullName;
    }
}