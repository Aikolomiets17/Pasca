﻿using System.Collections.Generic;
using Pasca.Common.Students;

namespace Pasca.Common.Providers
{
    public interface IGradesProvider
    {
        Dictionary<Person, double> GetFinalGrades();
    }
}