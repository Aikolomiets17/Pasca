﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Pasca.Common.Students;

namespace Pasca.Common.Providers
{
    public interface IAvailableStudentsProvider
    {
        Task<IEnumerable<Person>> GetAvailableStudents();
    }
}