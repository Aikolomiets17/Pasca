﻿using NCalc;

namespace Pasca.Common.Extensions
{
    public static class FunctionArgsExtensions
    {
        public static void SetResult(this FunctionArgs args, object result)
        {
            args.Result = result;
            args.HasResult = true;
        }
    }
}