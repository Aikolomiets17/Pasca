﻿using System.Threading.Tasks;

namespace Pasca.Common.Extensions
{
    public static class TaskExtensions
    {
        public static T ResultSync<T>(this Task<T> task)
        {
            //var currentContext = SynchronizationContext.Current;
            //SynchronizationContext.SetSynchronizationContext(null);
            //try
            //{
            //    return task.Result;
            //}
            //finally
            //{
            //    SynchronizationContext.SetSynchronizationContext(currentContext);
            //}
            
            //task.RunSynchronously();
            return task.Result;
        }
    }
}
