﻿using System;
using System.Collections.Specialized;
using System.Web;
using Pasca.Common.Exceptions;

namespace Pasca.Common.Extensions
{
    public static class StringExtensions
    {
        // todo: validate

        public static bool IsValidName(this string name)
        {
            return true;
        }

        public static bool IsValidSurname(this string surname)
        {
            return true;
        }

        public static bool IsValidSecondName(this string secondName)
        {
            return true;
        }

        public static bool IsValidFullName(this string fullName)
        {
            return true;
        }

        public static bool IsValidEmail(this string email)
        {
            return true;
        }

        public static bool OrdinalEquals(this string first, string second)
        {
            return string.Equals(first, second, StringComparison.Ordinal);
        }

        public static bool OrdinalIgnoreCaseEquals(this string first, string second)
        {
            return string.Equals(first, second, StringComparison.OrdinalIgnoreCase);
        }

        public static string WithParams(this string url, params string[] args)
        {
            Assert.AreEqual(args.Length % 2, 0);

            var builder = new UriBuilder(url) {
                Port = -1
            };

            var query = HttpUtility.ParseQueryString(string.Empty);
            for (var i = 0; i < args.Length; i += 2)
            {
                query[args[i]] = args[i + 1];
            }

            builder.Query = query.ToString();
            return builder.ToString();
        }

        public static NameValueCollection GetParams(this string url)
        {
            return HttpUtility.ParseQueryString(url);
        }

        public static Uri ToUri(this string uri)
        {
            return new Uri(uri);
        }

        public static string Since(this string s, string substr, int count = 1000)
        {
            var idx = s.IndexOf(substr, StringComparison.Ordinal);
            if (idx < 0 || idx + substr.Length >= s.Length)
            {
                return s;
            }
            var second = s.Substring(idx + substr.Length, Math.Min(count, s.Length - idx - substr.Length));

            idx = second.IndexOf(substr, StringComparison.Ordinal);
            if (idx < 0)
            {
                return s;
            }
            return second.Substring(idx, Math.Min(count, second.Length - idx));
        }
    }
}