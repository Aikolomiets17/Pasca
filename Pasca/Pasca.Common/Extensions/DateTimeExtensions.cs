﻿using System;

namespace Pasca.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToMsTimeString(this DateTime time)
        {
            return time.ToLongTimeString() + ":" + time.Millisecond.ToString("D3");
        }

        public static bool Passed(this DateTime time)
        {
            return time < DateTime.Now;
        }
    }
}