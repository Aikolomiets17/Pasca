﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Students;

namespace Pasca.Common.Extensions
{
    public static class CollectionExtensions
    {
        public static List<T> Clone<T>(this IList<T> list)
            where T : ICloneable
        {
            return list.Select(q => (T)q.Clone()).ToList();
        }

        public static IList<T> Clone<T>(this IList list)
            where T : ICloneable
        {
            return list.OfType<T>().Select(q => (T)q.Clone()).ToList();
        }

        [Obsolete]
        public static IDictionary<TKey, TValue> Clone<TKey, TValue>(this IDictionary<TKey, TValue> dict)
            where TValue : ICloneable
        {
            return dict.Aggregate(
                new Dictionary<TKey, TValue>(),
                (s, pair) =>
                {
                    s.Add(pair.Key, (TValue)pair.Value.Clone());
                    return s;
                });
        }

        [Obsolete]
        public static bool Same<T>(this IList<T> source, IList<T> other)
        {
            if (ReferenceEquals(source, other))
            {
                return true;
            }

            if (source == null || other == null || source.Count != other.Count)
            {
                return false;
            }
            for (var i = 0; i < source.Count; i++)
            {
                if (!source[i].Equals(other[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public static double Average(this IEnumerable<double> source, double @default)
        {
            var s = source.Where(m => !double.IsNaN(m)).ToArray();
            return s.Any() ? s.Average() : @default;
        }

        public static int SumWithOverflow<T>(this IEnumerable<T> source, Func<T, int> selector)
        {
            var sum = 0;
            foreach (var s in source)
            {
                sum += selector(s);
            }
            return sum;
        }

        public static List<T> Sorted<T>(this List<T> source, IComparer<T> comparer = null)
        {
            source.Sort(comparer);
            return source;
        }

        public static AnonymizedPerson With(this IEnumerable<AnonymizedPerson> source, int number)
        {
            foreach (var person in source)
            {
                if (person.Number == number)
                {
                    return person;
                }
            }

            return null;
        }

        public static AnonymizedPerson Who(this IEnumerable<AnonymizedPerson> source, Person identity)
        {
            foreach (var person in source)
            {
                if (person.Equals(identity))
                {
                    return person;
                }
            }

            return null;
        }
    }
}