﻿using System;
using Pasca.Common.Exceptions;

namespace Pasca.Common.Extensions
{
    public static class MathExtensions
    {
        public static int AsInt(this double value, double epsilon = 0.01)
        {
            var integer = (int)Math.Round(value);
            Assert.True(value < integer + epsilon && value > integer - epsilon);

            return integer;
        }
    }
}