﻿namespace Pasca.Common.Trace
{
    public class CommonTracerTags
    {
        public const string General = nameof(General);
        public const string Algo = nameof(Algo);
        public const string SettingManager = nameof(SettingManager);
        public const string Loc = nameof(Loc);
    }
}