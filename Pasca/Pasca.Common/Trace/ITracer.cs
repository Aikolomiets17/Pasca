﻿namespace Pasca.Common.Trace
{
    // todo: parametric dependency resolve and default tag in constructor
    public interface ITracer
    {
        void WriteLine(string tag, string message);
        void Clean();
        void Show();
    }
}