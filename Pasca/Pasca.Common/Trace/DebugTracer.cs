﻿using System;
using System.Diagnostics;
using Pasca.Common.Extensions;

namespace Pasca.Common.Trace
{
    public class DebugTracer : ITracer
    {
        private static string Template { get; } = "{0}\t{1}\t{2}\n";

        public void WriteLine(string tag, string message)
        {
            Debug.WriteLine(Template, tag, DateTime.Now.ToMsTimeString(), message);
        }

        public void Clean() {}
        public void Show() {}
    }
}