﻿using System;
using System.Collections.Generic;
using Pasca.Common.Exceptions;
using Pasca.Common.Trace;
using Pasca.Common.Types;

namespace Pasca.Common.DI
{
    public class DR
    {
        private DR() {}

        public static DR Instance => Singleton.Instance;

        public static bool IsCreated => Singleton.IsCreated;

        public void Register<T>(object value, bool @override = false)
        {
            var t = typeof(T);

            if (!(value is T))
            {
                throw new ArgumentTypeException(nameof(value), value.GetType(), t);
            }

            if (_registeredObjects.ContainsKey(t) && !@override)
            {
                throw new TypeAlreadyRegisteredException(t, value);
            }
            _registeredObjects[typeof(T)] = value;
        }

        public static T Get<T>()
        {
            return Instance.Resolve<T>();
        }

        public static T GetOrDefault<T>()
        {
            return Instance.ResolveOrDefault<T>();
        }

        public static Lazy<T> GetLazy<T>()
        {
            return new Lazy<T>(Get<T>);
        }

        public T Resolve<T>()
        {
            var t = typeof(T);

            object registeredObject;
            if (!_registeredObjects.TryGetValue(t, out registeredObject))
            {
                throw new TypeNotRegisteredException(t);
            }

            return (T)registeredObject;
        }

        public T ResolveOrDefault<T>()
        {
            var t = typeof(T);

            object registeredObject;
            if (!_registeredObjects.TryGetValue(t, out registeredObject))
            {
                return default(T);
            }

            return (T)registeredObject;
        }

        private readonly Dictionary<Type, object> _registeredObjects = new Dictionary<Type, object> {
            {
                typeof(ITracer), new DebugTracer()
            }
        };

        private static readonly SingletonComponent<DR> Singleton = new SingletonComponent
                <DR>(() => new DR());
    }
}