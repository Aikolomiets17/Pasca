﻿using System;
using System.Collections.Generic;
using Pasca.Common.Exceptions;
using Pasca.Common.Students;
using Pasca.Common.Types;

namespace Pasca.Common.Comparers
{
    public class PersonValuePairComparer<T> : IComparer<KeyValuePair<Person, T>>
    {
        public static PersonValuePairComparer<T> Instance => Singleton.Instance;

        public int Compare(KeyValuePair<Person, T> x, KeyValuePair<Person, T> y)
        {
            Assert.NotNull(x.Key);
            Assert.NotNull(y.Key);

            return StringComparer.CurrentCultureIgnoreCase.Compare(x.Key.ToString(), y.Key.ToString());
        }

        private static readonly SingletonComponent<PersonValuePairComparer<T>> Singleton =
                new SingletonComponent<PersonValuePairComparer<T>>(() => new PersonValuePairComparer<T>());
    }
}