﻿using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Students;
using Pasca.Common.Types;

namespace Pasca.Common.Comparers
{
    public class PersonListComparer : IEqualityComparer<IEnumerable<Person>>
    {
        public static PersonListComparer Instance => Singleton.Instance;

        public bool Equals(IEnumerable<Person> x, IEnumerable<Person> y)
        {
            foreach (var xItem in x)
            {
                if (!y.Contains(xItem))
                {
                    return false;
                }
            }

            foreach (var yItem in y)
            {
                if (!x.Contains(yItem))
                {
                    return false;
                }
            }

            return true;
        }

        public int GetHashCode(IEnumerable<Person> obj)
        {
            return obj.Aggregate(0, (seed, item) => seed + item.GetHashCode());
        }

        private static readonly SingletonComponent<PersonListComparer> Singleton =
                new SingletonComponent<PersonListComparer>(() => new PersonListComparer());
    }
}