﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Pasca.Common.Algorithm;
using Pasca.Common.DI;
using Pasca.Common.Extensions;
using Pasca.Common.Localization;
using Pasca.Common.Students;
using Pasca.Common.Trace;
using Pasca.Common.Types;
using Pasca.Gui.Facade;
using Pasca.OneNoteAddIn.Localization;
using Pasca.Test.Fakes;
using Pasca.Test.Fakes.SettingProviders;

namespace Pasca.Test
{
    internal class Program
    {
        private static void RegisterDependencies(DR d)
        {
            d.Register<ITracer>(new TracerFake(), true);
            d.Register<ILoc>(Loc.Instance, true);
        }

        [SuppressMessage("ReSharper", "UnusedParameter.Local")]
        private static void Main(string[] args)
        {
            new Program().Main();
        }

        private void Main()
        {
            {
                var facade = new Bootstrapper(RegisterDependencies, _ => { });
                facade.ShowTemplateEditWindow(new CriteriaSettingProviderFake());

                //var students = new List<Person> {
                //    new Person("st1", "st@st1.com", PersonRole.Student),
                //    new Person("st2", "st@st2.com", PersonRole.Student),
                //    //new Person("st3", "st@st3.com", PersonRole.Student),
                //    //new Person("st4", "st@st4.com", PersonRole.Student),
                //    //new Person("st5", "st@st5.com", PersonRole.Student),
                //    //new Person("st6", "st@st6.com", PersonRole.Student),
                //    //new Person("st7", "st@st7.com", PersonRole.Student),
                //};
                //var mapper = new Mapper(students, students, 3);
                //var anonymized = mapper.Authors;
                //var mapping = mapper.Mapping;
                //foreach (var author in mapping)
                //{
                //    Console.WriteLine("Author " + anonymized.With(author.Key));
                //    foreach (var reviewer in author.Value)
                //    {
                //        Console.Write(anonymized.With(reviewer));
                //    }
                //    Console.WriteLine();
                //}
                //Console.ReadLine();
                //var serializer = new XmlSettingsSerializer<ArraySetting>();
                //var settings = new ArraySetting(CommonSettingName.Session, new List<Setting> {
                //    new StudentMappingSetting(CommonSettingName.SessionMapping, new Dictionary<int, List<int>> {
                //        { 2,new List<int> {5} },
                //        { 7,new List<int> {2,3 } }
                //    })
                //});
                //var sb = new StringBuilder();
                //using (var tw = new StringWriter(sb))
                //{
                //    serializer.Serialize(tw, settings);
                //}
                //Console.WriteLine(sb.ToString());

                //using (var tr = new StringReader(sb.ToString()))
                //{
                //    var d = serializer.Deserialize(tr);
                //    Console.Write(d);
                //}

                //var expression = new AggregateExpression("case(1,0,2,1,3)", new double[] {});
                //var ev = expression.Evaluate();
                //var evd = expression.EvaluateDouble();
                //var facade = new Bootstrapper(RegisterDependencies, delegate { });
                //facade.ShowFinalGradesDistributionChartViewModel(new GradesProviderFake()).Wait();
                //var head = new StringTreeItem("C:", "C:", null);
                //var a = new StringTreeItem("C:\\a", "a", head);
                //var b = new StringTreeItem("C:\\b", "b", head);
                //var c = new StringTreeItem("C:\\c", "c", head);
                //var aa = new StringTreeItem("C:\\a\\aa", "aa", a);
                //var ab = new StringTreeItem("C:\\a\\ab", "ab", a);
                //var bc = new StringTreeItem("C:\\b\\bc", "bc", b);
                //facade.ShowSelectWindow(
                //    new[] {
                //        head
                //    }).Wait();
                //facade.ShowSettingsWindow(
                //    new SessionSettingsProviderFake(
                //        () => facade.ShowSelectWindow(
                //            new[] {
                //                head
                //            })?.Value));
                //facade.Shutdown(false);
                //facade.ShowArtifactsDoneWindow(new Dictionary<Person, string>
                //{
                //    {new Person("Ivanov ivan ivanovich", "ivan@ivan.com", PersonRole.Student),"123-456"  },
                //    {new Person("Petrov petr petrovich", "petr@petr.com", PersonRole.Student),null  }
                //}, _=> {}).Wait();
                //facade.ShowNewSessionWindow(new SessionSettingsProviderFake()).Wait();

                //var auth =
                //        new OneNoteAuth();
                //facade.ShowSignInWindow(auth).Wait();
                //var api = new ApiFacade(new GraphAuth(), auth,new AuthorizationRequestHandler(_=> new Task(()=>
                //{
                //    new Bootstrapper(RegisterDependencies)
                //        .ShowSignInWindow
                //        (auth);
                //})),new ArraySetting(CommonSettings.OneNote,new List<Setting>())  );
                //api.NotebookName = "Биология, 7 класс";
                //var students = api.GetClassStudents().ResultSync();
                //GC.KeepAlive(students);
                //var Facade = new Bootstrapper(_ => { });
                //var auth = new OneNoteAuth(
                //        "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlJyUXF1OXJ5ZEJWUldtY29jdVhVYjIwSEdSTSIsImtpZCI6IlJyUXF1OXJ5ZEJWUldtY29jdVhVYjIwSEdSTSJ9.eyJhdWQiOiJodHRwczovL29uZW5vdGUuY29tLyIsImlzcyI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzIxZjI2YzI0LTA3OTMtNGIwNy1hNzNkLTU2M2NkMmVjMjM1Zi8iLCJpYXQiOjE0ODEzNzc3NzIsIm5iZiI6MTQ4MTM3Nzc3MiwiZXhwIjoxNDgxMzgxNjcyLCJhY3IiOiIxIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6ImRmYjhmNGIxLTE5NjItNGUyYi1iZDgzLThlOGUwOWUwMjRmYiIsImFwcGlkYWNyIjoiMCIsImZhbWlseV9uYW1lIjoi0JrQvtC70L7QvNC40LXRhiIsImdpdmVuX25hbWUiOiLQkNC90LTRgNC10LkiLCJpcGFkZHIiOiIxMjguNzIuMjAuMTQyIiwibmFtZSI6ItCa0L7Qu9C - 0LzQuNC10YYg0JDQvdC00YDQtdC5INCY0LvRjNC40YciLCJvaWQiOiIyNjJkYzNiNC01MDg0LTQzOTktODg1Mi03MDNjMGU1YWEzMzYiLCJwbGF0ZiI6IjMiLCJwdWlkIjoiMTAwM0JGRkQ4QjZCQjUwOCIsInNjcCI6Ik5vdGVzLkNyZWF0ZSBOb3Rlcy5SZWFkV3JpdGUgU2l0ZXMuUmVhZFdyaXRlLkFsbCIsInN1YiI6Im1LaVR4ZE5LOXVNX2h2aDFIT2tSU3hNQ2ZBc3FTcm5YUDJ0SDIxNVRzNEEiLCJ0aWQiOiIyMWYyNmMyNC0wNzkzLTRiMDctYTczZC01NjNjZDJlYzIzNWYiLCJ1bmlxdWVfbmFtZSI6ImFpa29sb21pZXRzQGVkdS5oc2UucnUiLCJ1cG4iOiJhaWtvbG9taWV0c0BlZHUuaHNlLnJ1IiwidmVyIjoiMS4wIn0.VgP5BOL2WShAhZMnvfvYX2n_3pb6T0zMmpGyqCteWdA7Bq3Qm0PeBLRlNGvXZw1F5Xfw5PUWN9ltqvs8eWX5kBNALxtUjGdFGfGdHjaEOPfpBUuPD4wUWNSyvq3ZapH1irazp3zai6e7M8Zv1_9gaY8CIovY9nlX4 - pkTE3bQ2CW4tZHfvYOpIJLPS5n3FovMyzBSkAXK2SBq5Fh9S9FEE2V81aHtCKARpHPY3JADZy7XO4c3cE0zPDDSjcgVkoWxvJfegtc18ypgYVcO4tzf2BVj57AGajmbhEqr8H8SqX - ezAfAZsmko0DdHhI4GVB2Gyt0To8vZEbgHWXclQtqQ");
                ////Facade.ShowSignInWindow(auth).Wait();
                ////var s = auth.Post(
                ////                new Uri(
                ////                    "https://onenote.com/api/beta/me/notes/sectiongroups/1-354f3713-b657-4139-89d4-a82346fa7007/sectionGroups/"),
                ////                "{\"name\":\"New Name!\"}").Result;
                ////var req = new System.Net.HttpWebRequest();
                //var client = new HttpClient();
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlJyUXF1OXJ5ZEJWUldtY29jdVhVYjIwSEdSTSIsImtpZCI6IlJyUXF1OXJ5ZEJWUldtY29jdVhVYjIwSEdSTSJ9.eyJhdWQiOiJodHRwczovL29uZW5vdGUuY29tLyIsImlzcyI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzIxZjI2YzI0LTA3OTMtNGIwNy1hNzNkLTU2M2NkMmVjMjM1Zi8iLCJpYXQiOjE0ODEzNzc3NzIsIm5iZiI6MTQ4MTM3Nzc3MiwiZXhwIjoxNDgxMzgxNjcyLCJhY3IiOiIxIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6ImRmYjhmNGIxLTE5NjItNGUyYi1iZDgzLThlOGUwOWUwMjRmYiIsImFwcGlkYWNyIjoiMCIsImZhbWlseV9uYW1lIjoi0JrQvtC70L7QvNC40LXRhiIsImdpdmVuX25hbWUiOiLQkNC90LTRgNC10LkiLCJpcGFkZHIiOiIxMjguNzIuMjAuMTQyIiwibmFtZSI6ItCa0L7Qu9C - 0LzQuNC10YYg0JDQvdC00YDQtdC5INCY0LvRjNC40YciLCJvaWQiOiIyNjJkYzNiNC01MDg0LTQzOTktODg1Mi03MDNjMGU1YWEzMzYiLCJwbGF0ZiI6IjMiLCJwdWlkIjoiMTAwM0JGRkQ4QjZCQjUwOCIsInNjcCI6Ik5vdGVzLkNyZWF0ZSBOb3Rlcy5SZWFkV3JpdGUgU2l0ZXMuUmVhZFdyaXRlLkFsbCIsInN1YiI6Im1LaVR4ZE5LOXVNX2h2aDFIT2tSU3hNQ2ZBc3FTcm5YUDJ0SDIxNVRzNEEiLCJ0aWQiOiIyMWYyNmMyNC0wNzkzLTRiMDctYTczZC01NjNjZDJlYzIzNWYiLCJ1bmlxdWVfbmFtZSI6ImFpa29sb21pZXRzQGVkdS5oc2UucnUiLCJ1cG4iOiJhaWtvbG9taWV0c0BlZHUuaHNlLnJ1IiwidmVyIjoiMS4wIn0.VgP5BOL2WShAhZMnvfvYX2n_3pb6T0zMmpGyqCteWdA7Bq3Qm0PeBLRlNGvXZw1F5Xfw5PUWN9ltqvs8eWX5kBNALxtUjGdFGfGdHjaEOPfpBUuPD4wUWNSyvq3ZapH1irazp3zai6e7M8Zv1_9gaY8CIovY9nlX4 - pkTE3bQ2CW4tZHfvYOpIJLPS5n3FovMyzBSkAXK2SBq5Fh9S9FEE2V81aHtCKARpHPY3JADZy7XO4c3cE0zPDDSjcgVkoWxvJfegtc18ypgYVcO4tzf2BVj57AGajmbhEqr8H8SqX-ezAfAZsmko0DdHhI4GVB2Gyt0To8vZEbgHWXclQtqQ");
                //var resp = client.GetAsync("https://www.onenote.com/api/beta/me/notes/sectiongroups/1-354f3713-b657-4139-89d4-a82346fa7007/sectionGroups/").Result;//, new StringContent("{\"name\":\"New Name!\"}",Encoding.Default, "application/json")).Result;
                //GC.KeepAlive(resp);
                ////var req = new RestRequest("/",Method.POST);
                ////req.AddHeader("Authorization",
                ////    "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlJyUXF1OXJ5ZEJWUldtY29jdVhVYjIwSEdSTSIsImtpZCI6IlJyUXF1OXJ5ZEJWUldtY29jdVhVYjIwSEdSTSJ9.eyJhdWQiOiJodHRwczovL29uZW5vdGUuY29tLyIsImlzcyI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzIxZjI2YzI0LTA3OTMtNGIwNy1hNzNkLTU2M2NkMmVjMjM1Zi8iLCJpYXQiOjE0ODEzNzc3NzIsIm5iZiI6MTQ4MTM3Nzc3MiwiZXhwIjoxNDgxMzgxNjcyLCJhY3IiOiIxIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6ImRmYjhmNGIxLTE5NjItNGUyYi1iZDgzLThlOGUwOWUwMjRmYiIsImFwcGlkYWNyIjoiMCIsImZhbWlseV9uYW1lIjoi0JrQvtC70L7QvNC40LXRhiIsImdpdmVuX25hbWUiOiLQkNC90LTRgNC10LkiLCJpcGFkZHIiOiIxMjguNzIuMjAuMTQyIiwibmFtZSI6ItCa0L7Qu9C - 0LzQuNC10YYg0JDQvdC00YDQtdC5INCY0LvRjNC40YciLCJvaWQiOiIyNjJkYzNiNC01MDg0LTQzOTktODg1Mi03MDNjMGU1YWEzMzYiLCJwbGF0ZiI6IjMiLCJwdWlkIjoiMTAwM0JGRkQ4QjZCQjUwOCIsInNjcCI6Ik5vdGVzLkNyZWF0ZSBOb3Rlcy5SZWFkV3JpdGUgU2l0ZXMuUmVhZFdyaXRlLkFsbCIsInN1YiI6Im1LaVR4ZE5LOXVNX2h2aDFIT2tSU3hNQ2ZBc3FTcm5YUDJ0SDIxNVRzNEEiLCJ0aWQiOiIyMWYyNmMyNC0wNzkzLTRiMDctYTczZC01NjNjZDJlYzIzNWYiLCJ1bmlxdWVfbmFtZSI6ImFpa29sb21pZXRzQGVkdS5oc2UucnUiLCJ1cG4iOiJhaWtvbG9taWV0c0BlZHUuaHNlLnJ1IiwidmVyIjoiMS4wIn0.VgP5BOL2WShAhZMnvfvYX2n_3pb6T0zMmpGyqCteWdA7Bq3Qm0PeBLRlNGvXZw1F5Xfw5PUWN9ltqvs8eWX5kBNALxtUjGdFGfGdHjaEOPfpBUuPD4wUWNSyvq3ZapH1irazp3zai6e7M8Zv1_9gaY8CIovY9nlX4 - pkTE3bQ2CW4tZHfvYOpIJLPS5n3FovMyzBSkAXK2SBq5Fh9S9FEE2V81aHtCKARpHPY3JADZy7XO4c3cE0zPDDSjcgVkoWxvJfegtc18ypgYVcO4tzf2BVj57AGajmbhEqr8H8SqX-ezAfAZsmko0DdHhI4GVB2Gyt0To8vZEbgHWXclQtqQ");
                ////var cl = new RestClient();
                ////cl.BaseUrl = new Uri(
                ////    "https://www.onenote.com/api/beta/me/notes/sectiongroups/1-354f3713-b657-4139-89d4-a82346fa7007/sectionGroups/");
                ////var ss = cl.Post(req).Content;
                ////cl.BaseUrl =new Uri(
                ////    "https://www.onenote.com/api/beta/me/notes/sectiongroups/1-354f3713-b657-4139-89d4-a82346fa7007/sectionGroups/");
                ////GC.KeepAlive(ss);
            }
        }
    }
}