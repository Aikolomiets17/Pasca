﻿using System.Diagnostics;
using Pasca.Common.Trace;

namespace Pasca.Test.Fakes
{
    public class TracerFake : ITracer
    {
        public void WriteLine(string tag, string message)
        {
            Debug.WriteLine(message, tag);
        }

        public void Clean() {}
        public void Show() { }
    }
}