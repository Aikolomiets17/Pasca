﻿using System.Collections.Generic;
using Pasca.Common.Algorithm.Criteria;
using Pasca.Common.Settings;

namespace Pasca.Test.Fakes.SettingProviders
{
    public class CriteriaSettingProviderFake : SettingProviderFakeBase<ArraySetting>
    {
        public override ArraySetting GetSetting()
        {
            return new ArraySetting(
                CommonSettingName.Session,
                new List<Setting> {
                    new AssessmentCriteriaSetting(CommonSettingName.SessionAssessmentCriteria, new AssessmentCriteria())
                });
        }

        public override bool IsEditable(Setting setting)
        {
            return true;
        }
    }
}