﻿using System.Collections.Generic;
using Pasca.Common.Settings;
using Pasca.Gui.Facade;
using Pasca.OneNoteAddIn.Model;

namespace Pasca.Test.Fakes.SettingProviders
{
    public class SessionSettingsProviderFake : SettingProviderFakeBase<ArraySetting>
    {
        public SessionSettingsProviderFake(SelectableValueProvider<string>.SelectAction selectPageAction = null)
        {
            _selectPageAction = selectPageAction;
        }

        public override ArraySetting GetSetting()
        {
            return new ArraySetting(
                CommonSettingName.Session,
                new List<Setting> {
                    new StringSetting(CommonSettingName.SessionName, "SessionName"),
                    new StringSetting(CommonSettingName.SessionAssignment, "")
                });
        }

        public override ISelectableValueProvider GetSelectableValueProvider(Setting setting)
        {
            switch (setting.Name as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.SessionAssignment:
                if (_selectPageAction != null)
                {
                    return new SelectableValueProvider<string>(_selectPageAction);
                }
                goto default;
            default:
                return base.GetSelectableValueProvider(setting);
            }
        }

        public override bool IsEditable(Setting setting)
        {
            return true;
        }

        private SelectableValueProvider<string>.SelectAction _selectPageAction;
    }
}