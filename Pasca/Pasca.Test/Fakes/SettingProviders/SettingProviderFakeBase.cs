﻿using System;
using Pasca.Common.Settings;
using Pasca.Gui.Facade;

namespace Pasca.Test.Fakes.SettingProviders
{
    public abstract class SettingProviderFakeBase<T> : ISettingProvider<T>
        where T : Setting
    {
        public void Subscribe(Action<T, T> settingChangedHandler) {}

        public void Commit(Setting newSettings) {}

        public abstract T GetSetting();

        public abstract bool IsEditable(Setting setting);

        public virtual ISelectableValueProvider GetSelectableValueProvider(Setting setting)
        {
            return null;
        }
    }
}