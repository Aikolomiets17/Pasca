﻿using System;
using System.Collections.Generic;
using Pasca.Common.Settings;

namespace Pasca.Test.Fakes.SettingProviders
{
    internal class DateTimeSettingProviderFake : SettingProviderFakeBase<ArraySetting>
    {
        public override ArraySetting GetSetting()
        {
            return new ArraySetting(
                CommonSettingName.Session,
                new List<Setting> {
                    new DateTimeSetting(CommonSettingName.SessionSubmissionBegin, DateTime.Today),
                    new DateTimeSetting(CommonSettingName.SessionSubmissionEnd, DateTime.Today.AddDays(7)),
                    new DateTimeSetting(CommonSettingName.SessionReviewBegin, DateTime.Today.AddDays(7)),
                    new DateTimeSetting(CommonSettingName.SessionReviewEnd, DateTime.Today.AddDays(14))
                });
        }

        public override bool IsEditable(Setting setting)
        {
            return true;
        }
    }
}