﻿using System.Collections.Generic;
using Pasca.Common.Providers;
using Pasca.Common.Students;

namespace Pasca.Test.Fakes
{
    public sealed class GradesProviderFake : IGradesProvider
    {
        public Dictionary<Person, double> GetFinalGrades()
        {
            return new Dictionary<Person, double> {
                {
                    new Person("Ivanov2", "iv@iv.ru", PersonRole.Student), 4.0001
                }, {
                    new Person("Ivanov3", "iv@iv.ru", PersonRole.Student), 2.0001
                }, {
                    new Person("Ivanov4", "iv@iv.ru", PersonRole.Student), 4.0001
                }, {
                    new Person("Ivanov", "iv@iv.ru", PersonRole.Student), 3.0001
                }, {
                    new Person("Ivanov1", "iv@iv.ru", PersonRole.Student), 4.0001
                }, {
                    new Person("Ivanov5", "iv@iv.ru", PersonRole.Student), 3.0001
                },
            };
        }
    }
}