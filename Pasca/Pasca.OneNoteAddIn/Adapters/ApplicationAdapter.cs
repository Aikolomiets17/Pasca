﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.Office.Interop.OneNote;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Trace;
using Pasca.OneNoteAddIn.Trace;
using Application = Microsoft.Office.Interop.OneNote.Application;

namespace Pasca.OneNoteAddIn.Adapters
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ApplicationAdapter : Application
    {
        public ApplicationAdapter(Application instance)
        {
            Assert.NotNull(instance);

            _instance = instance;
        }

        private ITracer Tracer => _lazyTracer.Value;

        public event IOneNoteEvents_OnNavigateEventHandler OnNavigate
        {
            add { _instance.OnNavigate += value; }
            remove { _instance.OnNavigate -= value; }
        }

        public event IOneNoteEvents_OnHierarchyChangeEventHandler OnHierarchyChange
        {
            add { _instance.OnHierarchyChange += value; }
            remove { _instance.OnHierarchyChange -= value; }
        }

        ~ApplicationAdapter()
        {
            Marshal.ReleaseComObject(_instance);
        }

        private readonly Application _instance;
        private readonly Lazy<ITracer> _lazyTracer = DR.GetLazy<ITracer>();

        #region Interface implementation

        public void GetHierarchy(
            string bstrStartNodeID,
            HierarchyScope hsScope,
            out string pbstrHierarchyXmlOut,
            XMLSchema xsSchema = XMLSchema.xs2013)
        {
            pbstrHierarchyXmlOut = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.GetHierarchy(bstrStartNodeID, hsScope, out tmp, xsSchema);
                    return tmp;
                });
        }

        public void UpdateHierarchy(string bstrChangesXmlIn, XMLSchema xsSchema = XMLSchema.xs2013)
        {
            RepeatOnFail(
                () =>
                    _instance.UpdateHierarchy(bstrChangesXmlIn, xsSchema));
        }

        public void OpenHierarchy(
            string bstrPath,
            string bstrRelativeToObjectID,
            out string pbstrObjectID,
            CreateFileType cftIfNotExist = CreateFileType.cftNone)
        {
            pbstrObjectID = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.OpenHierarchy(bstrPath, bstrRelativeToObjectID, out tmp, cftIfNotExist);
                    return tmp;
                });
        }

        public void DeleteHierarchy(
            string bstrObjectID,
            DateTime dateExpectedLastModified,
            bool deletePermanently = false)
        {
            RepeatOnFail(
                () =>
                    _instance.DeleteHierarchy(bstrObjectID, dateExpectedLastModified, deletePermanently));
        }

        public void CreateNewPage(
            string bstrSectionID,
            out string pbstrPageID,
            NewPageStyle npsNewPageStyle = NewPageStyle.npsDefault)
        {
            pbstrPageID = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.CreateNewPage(bstrSectionID, out tmp, npsNewPageStyle);
                    return tmp;
                });
        }

        public void CloseNotebook(string bstrNotebookID, bool force = false)
        {
            RepeatOnFail(
                () =>
                    _instance.CloseNotebook(bstrNotebookID, force));
        }

        public void GetHierarchyParent(string bstrObjectID, out string pbstrParentID)
        {
            pbstrParentID = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.GetHierarchyParent(bstrObjectID, out tmp);
                    return tmp;
                });
        }

        public void GetPageContent(
            string bstrPageID,
            out string pbstrPageXmlOut,
            PageInfo pageInfoToExport = PageInfo.piBasic,
            XMLSchema xsSchema = XMLSchema.xs2013)
        {
            pbstrPageXmlOut = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.GetPageContent(
                        bstrPageID,
                        out tmp,
                        pageInfoToExport,
                        xsSchema);
                    return tmp;
                });
        }

        public void UpdatePageContent(
            string bstrPageChangesXmlIn,
            DateTime dateExpectedLastModified,
            XMLSchema xsSchema = XMLSchema.xs2013,
            bool force = false)
        {
            var regex = new Regex(@"[0-9]\.[0-9]+E\+[0-9]*");
            bstrPageChangesXmlIn = regex.Replace(bstrPageChangesXmlIn, "0.0");
            RepeatOnFail(
                () =>
                    _instance.UpdatePageContent(bstrPageChangesXmlIn, dateExpectedLastModified, xsSchema, force));
        }

        public void GetBinaryPageContent(string bstrPageID, string bstrCallbackID, out string pbstrBinaryObjectB64Out)
        {
            pbstrBinaryObjectB64Out = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.GetBinaryPageContent(bstrPageID, bstrCallbackID, out tmp);
                    return tmp;
                });
        }

        public void DeletePageContent(
            string bstrPageID,
            string bstrObjectID,
            DateTime dateExpectedLastModified,
            bool force = false)
        {
            RepeatOnFail(
                () =>
                    _instance.DeletePageContent(bstrPageID, bstrObjectID, dateExpectedLastModified, force));
        }

        public void NavigateTo(string bstrHierarchyObjectID, string bstrObjectID = "", bool fNewWindow = false)
        {
            Tracer.WriteLine(TracerTags.OneNote, "NavigateTo(" + bstrHierarchyObjectID + ", " + bstrObjectID + ")");
            RepeatOnFail(
                () =>
                    _instance.NavigateTo(bstrHierarchyObjectID, bstrObjectID, fNewWindow));
        }

        public void NavigateToUrl(string bstrUrl, bool fNewWindow = false)
        {
            Tracer.WriteLine(TracerTags.OneNote, "NavigateToUrl(" + bstrUrl + ")");
            RepeatOnFail(
                () =>
                    _instance.NavigateToUrl(bstrUrl, fNewWindow));
        }

        public void Publish(
            string bstrHierarchyID,
            string bstrTargetFilePath,
            PublishFormat pfPublishFormat = PublishFormat.pfOneNote,
            string bstrCLSIDofExporter = "0")
        {
            RepeatOnFail(
                () =>
                    _instance.Publish(bstrHierarchyID, bstrTargetFilePath, pfPublishFormat, bstrCLSIDofExporter));
        }

        public void OpenPackage(string bstrPathPackage, string bstrPathDest, out string pbstrPathOut)
        {
            pbstrPathOut = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.OpenPackage(bstrPathPackage, bstrPathDest, out tmp);
                    return tmp;
                });
        }

        public void GetHyperlinkToObject(
            string bstrHierarchyID,
            string bstrPageContentObjectID,
            out string pbstrHyperlinkOut)
        {
            pbstrHyperlinkOut = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.GetHyperlinkToObject(
                        bstrHierarchyID,
                        bstrPageContentObjectID,
                        out tmp);
                    return tmp;
                });
        }

        public void FindPages(
            string bstrStartNodeID,
            string bstrSearchString,
            out string pbstrHierarchyXmlOut,
            bool fIncludeUnindexedPages = false,
            bool fDisplay = false,
            XMLSchema xsSchema = XMLSchema.xs2013)
        {
            pbstrHierarchyXmlOut = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.FindPages(
                        bstrStartNodeID,
                        bstrSearchString,
                        out tmp,
                        fIncludeUnindexedPages,
                        fDisplay,
                        xsSchema);
                    return tmp;
                });
        }

        public void FindMeta(
            string bstrStartNodeID,
            string bstrSearchStringName,
            out string pbstrHierarchyXmlOut,
            bool fIncludeUnindexedPages = false,
            XMLSchema xsSchema = XMLSchema.xs2013)
        {
            pbstrHierarchyXmlOut = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.FindMeta(
                        bstrStartNodeID,
                        bstrSearchStringName,
                        out tmp,
                        fIncludeUnindexedPages,
                        xsSchema);
                    return tmp;
                });
        }

        public void GetSpecialLocation(SpecialLocation slToGet, out string pbstrSpecialLocationPath)
        {
            pbstrSpecialLocationPath = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.GetSpecialLocation(
                        slToGet,
                        out tmp);
                    return tmp;
                });
        }

        public void MergeFiles(string bstrBaseFile, string bstrClientFile, string bstrServerFile, string bstrTargetFile)
        {
            RepeatOnFail(
                () =>
                    _instance.MergeFiles(bstrBaseFile, bstrClientFile, bstrServerFile, bstrTargetFile));
        }

        public IQuickFilingDialog QuickFiling()
        {
            return RepeatOnFail(
                () =>
                    _instance.QuickFiling());
        }

        public void SyncHierarchy(string bstrHierarchyID)
        {
            RepeatOnFail(
                () =>
                    _instance.SyncHierarchy(bstrHierarchyID));
        }

        public void SetFilingLocation(FilingLocation flToSet, FilingLocationType fltToSet, string bstrFilingSectionID)
        {
            RepeatOnFail(
                () =>
                    _instance.SetFilingLocation(flToSet, fltToSet, bstrFilingSectionID));
        }

        public void MergeSections(string bstrSectionSourceId, string bstrSectionDestinationId)
        {
            RepeatOnFail(
                () =>
                    _instance.MergeSections(bstrSectionSourceId, bstrSectionDestinationId));
        }

        public void GetWebHyperlinkToObject(
            string bstrHierarchyID,
            string bstrPageContentObjectID,
            out string pbstrHyperlinkOut)
        {
            pbstrHyperlinkOut = RepeatOnFail(
                () =>
                {
                    string tmp;
                    _instance.GetWebHyperlinkToObject(
                        bstrHierarchyID,
                        bstrPageContentObjectID,
                        out tmp);
                    return tmp;
                });
        }

        public Windows Windows
        {
            get { return RepeatOnFail(() => _instance.Windows); }
        }

        public bool Dummy1
        {
            get { return RepeatOnFail(() => _instance.Dummy1); }
        }

        public object COMAddIns
        {
            get { return RepeatOnFail(() => _instance.COMAddIns); }
        }

        public object LanguageSettings
        {
            get { return RepeatOnFail(() => _instance.LanguageSettings); }
        }

        #endregion

        #region Exception handler

        private void RepeatOnFail(
            Action action,
            int times = 3,
            int delay = 500,
            [CallerMemberName] string actionName = "")
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{actionName} Start");
            for (var i = 0; i < times; i++)
            {
                try
                {
                    action();
                    Tracer.WriteLine(TracerTags.OneNote, $"{actionName} End");
                    return;
                }
                catch (Exception e)
                {
                    if (!Filter(e) || i == times - 1)
                    {
                        Tracer.WriteLine(TracerTags.OneNote, $"{e} encountered. rethrow.");
                        throw;
                    }
                    Tracer.WriteLine(TracerTags.OneNote, $"{e} encountered. skip.");
                    Thread.Sleep(delay);
                }
            }

            throw new Exception($"{nameof(RepeatOnFail)} tried ${times} and neither threw exception, nor returned");
        }

        private T RepeatOnFail<T>(
            Func<T> action,
            int times = 3,
            int delay = 500,
            [CallerMemberName] string actionName = "")
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{actionName} Start");
            for (var i = 0; i < times; i++)
            {
                try
                {
                    var returnValue = action();
                    Tracer.WriteLine(TracerTags.OneNote, $"{actionName} returned");
                    return returnValue;
                }
                catch (Exception e)
                {
                    if (!Filter(e) || i == times)
                    {
                        Tracer.WriteLine(TracerTags.OneNote, $"{e} encountered. rethrow.");
                        throw;
                    }
                    Tracer.WriteLine(TracerTags.OneNote, $"{e} encountered. skip. ");
                    Thread.Sleep(delay);
                }
            }

            throw new Exception($"{nameof(RepeatOnFail)} tried ${times} and neither threw exception, nor returned");
        }

        private static bool Filter(Exception e)
        {
            var comException = e as COMException;
            if (comException != null)
            {
                var errorCode = (uint)comException.ErrorCode;
                switch (errorCode)
                {
                case 0x80010001:
                    return true;
                    /*case 0x80042014:
                        MessageBox.Show("Active page is going to be deleted. Please, navigate away from current page.");
                        Thread.Sleep(5000);
                        return true;*/
                }
            }

            return false;
        }

        #endregion
    }
}