﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using Pasca.OneNoteAddIn.EqualityComparers;
using Pasca.OneNoteAddIn.Extensions;

namespace Pasca.OneNoteAddIn.Adapters
{
    public class NotebookAdapter
    {
        public NotebookAdapter(string pageContent)
        {
            PageSerializer = new XmlSerializer(typeof(Notebook));
            using (var sr = new StringReader(pageContent))
            {
                using (var reader = XmlReader.Create(sr))
                {
                    if (PageSerializer.CanDeserialize(reader))
                    {
                        Notebook = PageSerializer.Deserialize(reader) as Notebook;
                        if (Notebook == null)
                        {
                            throw new SerializationException("Deserialized page content is not a page");
                        }
                    }
                    else
                    {
                        throw new SerializationException("Can't deserialize page content");
                    }
                }
            }
            if (Notebook.Section == null)
            {
                Notebook.Section = new Section[0];
            }
            var sectionsCount = Notebook.Section.Length;
            _initialSections = new Section[sectionsCount];
            Array.Copy(Notebook.Section, _initialSections, sectionsCount);

            if (Notebook.SectionGroup == null)
            {
                Notebook.SectionGroup = new SectionGroup[0];
            }
            var sectionGroupsCount = Notebook.SectionGroup.Length;
            _initialSectionGroups = new SectionGroup[sectionGroupsCount];
            Array.Copy(Notebook.SectionGroup, _initialSectionGroups, sectionGroupsCount);
        }

        public IEnumerable<string> ObjectsToDelete => _initialSections
                .Except(Notebook.Section, SectionEqualityComparer.Instance)
                .Select(q => q.ID)
                .Concat(
                    _initialSectionGroups
                            .Except(Notebook.SectionGroup, SectionGroupEqualityComparer.Instance)
                            .Select(q => q.ID));

        public Notebook Notebook { get; }

        public XmlSerializer PageSerializer { get; }

        public void CreateSection(string sectionName = null, params string[] sectionGroups)
        {
            var notebookRoot = Notebook;
            SectionGroup sectionGroupRoot = null;
            foreach (var sectionGroupName in sectionGroups)
            {
                if (sectionGroupRoot == null)
                {
                    var sectionGroup = notebookRoot.GetSectionGroup(sectionGroupName);
                    if (sectionGroup == null)
                    {
                        notebookRoot.AddSectionGroup(
                            new SectionGroup {
                                name = sectionGroupName
                            });
                        sectionGroup = notebookRoot.GetSectionGroup(sectionGroupName);
                        if (sectionGroup == null)
                        {
                            throw new InvalidOperationException("Cannot create SectionGroup in notebook schema");
                        }
                    }
                    sectionGroupRoot = sectionGroup;
                }
                else
                {
                    var sectionGroup = sectionGroupRoot.GetSectionGroup(sectionGroupName);
                    if (sectionGroup == null)
                    {
                        sectionGroupRoot.AddSectionGroup(
                            new SectionGroup {
                                name = sectionGroupName
                            });
                        sectionGroup = sectionGroupRoot.GetSectionGroup(sectionGroupName);
                        if (sectionGroup == null)
                        {
                            throw new InvalidOperationException("Cannot create SectionGroup in sectionGroup schema");
                        }
                    }
                    sectionGroupRoot = sectionGroup;
                }
            }
            if (sectionName != null)
            {
                if (sectionGroupRoot == null)
                {
                    var section = notebookRoot.GetSection(sectionName);
                    if (section == null)
                    {
                        notebookRoot.AddSection(
                            new Section {
                                name = sectionName
                            });
                    }
                }
                else
                {
                    var section = sectionGroupRoot.GetSection(sectionName);
                    if (section == null)
                    {
                        sectionGroupRoot.AddSection(
                            new Section {
                                name = sectionName
                            }
                        );
                    }
                }
            }
        }

        private readonly SectionGroup[] _initialSectionGroups;
        private readonly Section[] _initialSections;
    }
}