﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using Pasca.OneNoteAddIn.EqualityComparers;

namespace Pasca.OneNoteAddIn.Adapters
{
    public class SectionAdapter
    {
        public SectionAdapter(string pageContent)
        {
            SessionSerializer = new XmlSerializer(typeof(Section));
            using (var sr = new StringReader(pageContent))
            {
                using (var reader = XmlReader.Create(sr))
                {
                    if (SessionSerializer.CanDeserialize(reader))
                    {
                        Section = SessionSerializer.Deserialize(reader) as Section;
                        if (Section == null)
                        {
                            throw new SerializationException("Deserialized page content is not a page");
                        }
                    }
                    else
                    {
                        throw new SerializationException("Can't deserialize page content");
                    }
                }
            }
            var n = Section.Page.Length;
            _initialItems = new Page[n];
            Array.Copy(Section.Page, _initialItems, n);
        }

        public IEnumerable<string> ObjectsToDelete => _initialItems
                .Except(Section.Page, PageEqualityComparer.Instance)
                .Select(q => q.ID);

        public Section Section { get; }

        public XmlSerializer SessionSerializer { get; }

        private readonly Page[] _initialItems;
    }
}