﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using Pasca.OneNoteAddIn.EqualityComparers;

namespace Pasca.OneNoteAddIn.Adapters
{
    public class PageAdapter
    {
        public PageAdapter(string pageContent)
        {
            PageSerializer = new XmlSerializer(typeof(Page));
            using (var sr = new StringReader(pageContent))
            {
                using (var reader = XmlReader.Create(sr))
                {
                    if (PageSerializer.CanDeserialize(reader))
                    {
                        Page = PageSerializer.Deserialize(reader) as Page;
                        if (Page == null)
                        {
                            throw new SerializationException("Deserialized page content is not a page (equals null)");
                        }
                    }
                    else
                    {
                        throw new SerializationException("Can't deserialize page content");
                    }
                }
            }
            if (Page.Items == null)
            {
                Page.Items = new PageObject[0];
            }
            var n = Page.Items.Length;
            _initialItems = new PageObject[n];
            Array.Copy(Page.Items, _initialItems, n);
        }

        public IEnumerable<string> ObjectsToDelete => _initialItems
                .Except(Page.Items, PageObjectEqualityComparer.Instance)
                .Select(q => q.objectID);

        public Page Page { get; }

        public XmlSerializer PageSerializer { get; }

        private readonly PageObject[] _initialItems;
    }
}