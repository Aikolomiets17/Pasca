﻿using System;
using System.Threading;
using Pasca.Common.Auth;

namespace Pasca.OneNoteAddIn.Auth
{
    public interface IAuthorizationRequestHandler
    {
        void RequestAuthorization(IAuth auth);
    }

    public class AuthorizationRequestHandler : IAuthorizationRequestHandler
    {
        public AuthorizationRequestHandler(Action<IAuth> action)
        {
            _action = action;
        }

        public void RequestAuthorization(IAuth auth)
        {
            if (Interlocked.CompareExchange(ref _lock, 1, 0) == 0)
            {
                _action(auth);
                _lock = 0;
            }
            else
            {
                SpinWait.SpinUntil(() => _lock == 0);
            }
        }

        private readonly Action<IAuth> _action;
        private int _lock = 0;
    }
}