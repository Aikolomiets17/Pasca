﻿namespace Pasca.OneNoteAddIn.Auth
{
    public static class PermissionRoles
    {
        public const string Owner = "Owner";
        public const string Contributor = "Contributor";
        public const string Reader = "Reader";
    }
}