﻿using System;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.OneNoteAddIn.Extensions;

namespace Pasca.OneNoteAddIn.Auth
{
    public class OneNoteAuth : BearerTokenAuthBase
    {
        public OneNoteAuth() {}

        [Obsolete]
        public OneNoteAuth(string accessToken)
        {
            AccessToken = accessToken;
        }

        public async Task<bool> SignOut()
        {
            var client = new HttpClient();
            var result = await client.GetAsync(SignOutUri).ConfigureAwait(false);
            return result.StatusCode == HttpStatusCode.OK;
        }

        #region Copy

        public async Task<dynamic> CopyPageToSection(string sectionId, string pageId, string renameAs = null)
        {
            dynamic query = new ExpandoObject();
            query.id = sectionId;
            if (renameAs != null)
            {
                query.renameAs = renameAs;
            }
            return (await Post($"{Endpoint}pages/{pageId}/copyToSection".ToUri(), ((object)query).ToJson()).ConfigureAwait(false)).ToDynamic();
        }

        #endregion

        private const string Endpoint = "https://www.onenote.com/api/beta/me/notes/";

        #region Authorization

        protected override string ClientId => "dfb8f4b1-1962-4e2b-bd83-8e8e09e024fb";
        protected override string AuthRedirectUrl => "https://localhost/";
        protected override string AuthTokenUrl => "https://login.microsoftonline.com/common/oauth2/token";
        protected override string RefreshTokenUrl => "https://login.microsoftonline.com/common/oauth2/token";

        private const string AuthRequestUrl = "https://login.microsoftonline.com/common/oauth2/authorize";

        private const string Permissions =
                @"wl.signin wl.offline_access notes.readwrite.all user.read user.readbasic.all office.onenote office.onenote_update office.onenote_create";

        private const string Resource = "https://onenote.com/";

        public override Uri SignInUri
            =>
                    new Uri(
                        AuthRequestUrl +
                        $"?response_type=code&client_id={ClientId}&redirect_uri={AuthRedirectUrl}&scope={Permissions}&resource={Resource}")
        ;

        public override Uri SignOutUri => new Uri("");
        //AuthLogoutUrl + $"?client_id={ClientId}&redirect_uri={AuthRedirectUrl}");

        #endregion

        #region Get People

        public async Task<dynamic> GetStudentsByNotebook(string notebookId)
        {
            return (await Get(
                        $"{Endpoint}classNotebooks/{notebookId}/students".ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> GetTeachersByNotebook(string notebookId)
        {
            return (await Get(
                        $"{Endpoint}classNotebooks/{notebookId}/teachers".ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        #endregion

        #region Get by name

        public async Task<dynamic> GetNotebookByName(string name)
        {
            return (await Get(
                        $"{Endpoint}classNotebooks?filter=name eq '{name}'"
                                .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        protected async Task<dynamic> GetOrCreate(Func<Task<dynamic>> getAction, Func<Task<dynamic>> createAction)
        {
            var response = await getAction().ConfigureAwait(false);
            if (response.value.Count == 0)
            {
                return await createAction().ConfigureAwait(false);
            }
            return response;
        }

        public async Task<dynamic> GetSectionGroupInNotebookByName(string rootId, string name)
        {
            return (await Get(
                        $"{Endpoint}notebooks/{rootId}/sectionGroups?$filter=name eq '{name}'"
                                .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> GetOrCreateSectionGroupInNotebookByName(string rootId, string name)
        {
            return await GetOrCreate(
                       () => GetSectionGroupInNotebookByName(rootId, name),
                       () => CreateSectionGroupInNotebook(rootId, name)).ConfigureAwait(false);
        }

        public async Task<dynamic> GetSectionGroupInSectionGroupByName(string rootId, string name)
        {
            return (await Get(
                        $"{Endpoint}sectionGroups/{rootId}/sectionGroups?$filter=name eq '{name}'"
                                .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> GetOrCreateSectionGroupInSectionGroupByName(string rootId, string name)
        {
            return await GetOrCreate(
                       () => GetSectionGroupInSectionGroupByName(rootId, name),
                       () => CreateSectionGroupInSectionGroup(rootId, name)).ConfigureAwait(false);
        }

        public async Task<dynamic> GetSectionInSectionGroupByName(string rootId, string name)
        {
            return (await Get(
                        $"{Endpoint}sectionGroups/{rootId}/sections?$filter=name eq '{name}'"
                                .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> GetOrCreateSectionInSectionGroupByName(string rootId, string name)
        {
            return await GetOrCreate(
                       () => GetSectionInSectionGroupByName(rootId, name),
                       () => CreateSectionInSectionGroup(rootId, name)).ConfigureAwait(false);
        }

        public async Task<dynamic> GetPageInSectionByName(string rootId, string name)
        {
            return (await Get(
                        $"{Endpoint}sections/{rootId}/pages?$filter=name eq '{name}'"
                                .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        #endregion

        #region Get all

        public async Task<dynamic> GetNotebooks()
        {
            return (await Get(
                        $"{Endpoint}classNotebooks"
                                .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> GetSectionGroupsInNotebook(string rootId)
        {
            return (await Get(
                        $"{Endpoint}notebooks/{rootId}/sectionGroups"
                                .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        // TODO: GetOrCreate
        public async Task<dynamic> GetSectionGroupsInSectionGroup(string rootId)
        {
            return (await Get(
                        $"{Endpoint}sectionGroups/{rootId}/sectionGroups"
                                .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> GetSectionsInSectionGroup(string rootId)
        {
            return (await Get(
                        $"{Endpoint}sectionGroups/{rootId}/sections"
                                .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> GetPagesInSection(string rootId)
        {
            return (await Get(
                        $"{Endpoint}sections/{rootId}/pages"
                                .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        #endregion

        #region Add/Remove People

        public async Task<dynamic> AddStudent(string notebookId, string studentId)
        {
            dynamic query = new ExpandoObject();
            query.id = studentId;
            query.principalType = "Person";
            return
                    (await Post($"{Endpoint}classNotebooks/{notebookId}/students/".ToUri(), ((object)query).ToJson()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> AddTeacher(string notebookId, string teacherId)
        {
            dynamic query = new ExpandoObject();
            query.id = teacherId;
            query.principalType = "Person";
            return
                    (await Post($"{Endpoint}classNotebooks/{notebookId}/teachers/".ToUri(), ((object)query).ToJson()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> RemoveStudent(string notebookId, string studentId)
        {
            return (await Delete($"{Endpoint}classNotebooks/{notebookId}/students/{studentId}".ToUri()).ConfigureAwait(false)).ToDynamic();
        }

        public async Task<dynamic> RemoveTeacher(string notebookId, string teacherId)
        {
            return (await Delete($"{Endpoint}classNotebooks/{notebookId}/teachers/{teacherId}".ToUri()).ConfigureAwait(false)).ToDynamic();
        }

        #endregion

        #region Create Sections

        public async Task<dynamic> CreateSectionInSectionGroup(string rootId, string newName)
        {
            dynamic query = new ExpandoObject();
            query.name = newName;

            // make Create responds like Get
            dynamic returnObject = new ExpandoObject();
            var arr = new JArray {
                (await Post($"{Endpoint}sectionGroups/{rootId}/sections/".ToUri(), ((object)query).ToJson()).ConfigureAwait(false))
                .ToDynamic()
            };
            returnObject.value = arr;
            return returnObject;
        }

        public async Task<dynamic> CreateSectionGroupInSectionGroup(string rootId, string newName)
        {
            dynamic query = new ExpandoObject();
            query.name = newName;

            // make Create responds like Get
            dynamic returnObject = new ExpandoObject();
            var arr = new JArray {
                (await Post($"{Endpoint}sectionGroups/{rootId}/sectionGroups/".ToUri(), ((object)query).ToJson()).ConfigureAwait(false))
                .ToDynamic()
            };

            returnObject.value = arr;
            return returnObject;
        }

        public async Task<dynamic> CreateSectionGroupInNotebook(string rootId, string newName)
        {
            dynamic query = new ExpandoObject();
            query.name = newName;

            // make Create responds like Get
            dynamic returnObject = new ExpandoObject();
            var arr = new JArray {
                (await Post($"{Endpoint}notebooks/{rootId}/sectionGroups/".ToUri(), ((object)query).ToJson()).ConfigureAwait(false))
                .ToDynamic()
            };

            returnObject.value = arr;
            return returnObject;
        }

        public async Task<dynamic> CreateSectionInNotebook(string rootId, string newName)
        {
            dynamic query = new ExpandoObject();
            query.name = newName;

            // make Create responds like Get
            dynamic returnObject = new ExpandoObject();
            var arr = new JArray {
                (await Post($"{Endpoint}notebooks/{rootId}/sections/".ToUri(), ((object)query).ToJson()).ConfigureAwait(false)).ToDynamic()
            };
            returnObject.value = arr;
            return returnObject;
        }

        #endregion

        #region Permissions

        public async Task<dynamic> GetPermissionInNotebook(string rootId, string userId)
        {
            Assert.NotNull(rootId);
            Assert.NotNull(userId);

            return
                    (await Get(
                         $"{Endpoint}notebooks/{rootId}/permissions?filter=contains(tolower(userId),'{userId.ToLower()}')"
                                 .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> GetPermissionInSectionGroup(string rootId, string userId)
        {
            Assert.NotNull(rootId);
            Assert.NotNull(userId);

            return
                    (await Get(
                         $"{Endpoint}sectionGroups/{rootId}/permissions?filter=contains(tolower(userId),'{userId.ToLower()}')"
                                 .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> GetPermissionInSection(string rootId, string userId)
        {
            Assert.NotNull(rootId);
            Assert.NotNull(userId);

            return
                    (await Get(
                         $"{Endpoint}sections/{rootId}/permissions?filter=contains(tolower(userId),'{userId.ToLower()}')"
                                 .ToUri()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> AddPermissionInNotebook(string rootId, string userId, string role)
        {
            dynamic query = new ExpandoObject();
            query.userRole = role;
            query.userId = userId;
            return
                    (await Post($"{Endpoint}notebooks/{rootId}/permissions/".ToUri(), ((object)query).ToJson()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> RemovePermissionInNotebook(string rootId, string permissionId)
        {
            return (await Delete($"{Endpoint}notebooks/{rootId}/permissions/{permissionId}".ToUri()).ConfigureAwait(false)).ToDynamic();
        }

        public async Task<dynamic> AddPermissionInSectionGroup(string rootId, string userId, string role)
        {
            dynamic query = new ExpandoObject();
            query.userRole = role;
            query.userId = userId;
            return
                    (await Post($"{Endpoint}sectionGroups/{rootId}/permissions/".ToUri(), ((object)query).ToJson()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> RemovePermissionInSectionGroup(string rootId, string permissionId)
        {
            return (await Delete($"{Endpoint}sectionGroups/{rootId}/permissions/{permissionId}".ToUri()).ConfigureAwait(false)).ToDynamic();
        }

        public async Task<dynamic> AddPermissionInSection(string rootId, string userId, string role)
        {
            dynamic query = new ExpandoObject();
            query.userRole = role;
            query.userId = userId;
            return
                    (await Post($"{Endpoint}sections/{rootId}/permissions/".ToUri(), ((object)query).ToJson()).ConfigureAwait(false))
                    .ToDynamic();
        }

        public async Task<dynamic> RemovePermissionInSection(string rootId, string permissionId)
        {
            return (await Delete($"{Endpoint}sections/{rootId}/permissions/{permissionId}".ToUri()).ConfigureAwait(false)).ToDynamic();
        }

        public async Task<dynamic> AddPermissionInPage(string rootId, string userId, string role)
        {
            dynamic query = new ExpandoObject();
            query.userRole = role;
            query.userId = userId;
            return
                    (await Post($"{Endpoint}pages/{rootId}/permissions/".ToUri(), ((object)query).ToJson()).ConfigureAwait(false))
                    .ToDynamic();
        }

        #endregion
    }
}