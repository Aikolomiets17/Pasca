﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;
using Pasca.Common.Auth;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.Common.Trace;
using Pasca.OneNoteAddIn.Trace;

namespace Pasca.OneNoteAddIn.Auth
{
    public abstract class BearerTokenAuthBase : IAuth
    {
        protected BearerTokenAuthBase()
        {
            LoadTokens();
        }

        protected abstract string AuthTokenUrl { get; }
        protected abstract string AuthRedirectUrl { get; }
        protected abstract string RefreshTokenUrl { get; }
        protected abstract string ClientId { get; }

        protected string RefreshToken
        {
            get { return _refreshToken; }
            set
            {
                _refreshToken = value;
                SaveToken(value, RefreshStoragePath);
            }
        }

        protected ITracer Tracer => _tracer.Value;
        public abstract Uri SignInUri { get; }
        public abstract Uri SignOutUri { get; }

        public virtual bool IsAuth => AccessToken != null;

        public string AccessToken
        {
            get { return _accessToken; }
            protected set
            {
                _accessToken = value;
                SaveToken(value, AccessStoragePath);
            }
        }

        private void SaveToken(string token, string path)
        {
            var t = ProtectedData.Protect(
                Encoding.ASCII.GetBytes(token),
                Entropy,
                DataProtectionScope.CurrentUser);
            using (var storage = IsolatedStorageFile.GetUserStoreForAssembly())
            {
                using (var sw = storage.OpenFile(path, FileMode.Create, FileAccess.Write))
                {
                    sw.Write(t, 0, t.Length);
                }
            }
        }

        private void LoadTokens()
        {
            try
            {
                using (var storage = IsolatedStorageFile.GetUserStoreForAssembly())
                {
                    if (storage.FileExists(AccessStoragePath) && storage.FileExists(RefreshStoragePath))
                    {
                        using (var sr = storage.OpenFile(AccessStoragePath, FileMode.Open))
                        {
                            var bytes = new byte[10000];
                            sr.Read(bytes, 0, 10000);
                            _accessToken =
                                    Encoding.ASCII.GetString(
                                        ProtectedData.Unprotect(
                                            bytes,
                                            Entropy,
                                            DataProtectionScope.CurrentUser));
                        }
                        using (var sr = storage.OpenFile(RefreshStoragePath, FileMode.Open))
                        {
                            var bytes = new byte[10000];
                            sr.Read(bytes, 0, 10000);
                            _refreshToken =
                                    Encoding.ASCII.GetString(
                                        ProtectedData.Unprotect(
                                            bytes,
                                            Entropy,
                                            DataProtectionScope.CurrentUser));
                        }
                    }
                    else
                    {
                        ResetTokens();
                    }
                }
            }
            catch
            {
                ResetTokens();
            }
        }

        public bool DeleteTokens()
        {
            try
            {
                using (var storage = IsolatedStorageFile.GetUserStoreForAssembly())
                {
                    if (storage.FileExists(AccessStoragePath) && storage.FileExists(RefreshStoragePath))
                    {
                        storage.DeleteFile(AccessStoragePath);
                        storage.DeleteFile(RefreshStoragePath);
                    }
                }
                LoadTokens();
            }
            catch
            {

            }

            if (AccessToken != null)
            {
                return false;
            }
            return true;
        }

        private void ResetTokens()
        {
            _accessToken = null;
            _refreshToken = null;
        }

        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();
        private string _accessToken;

        private string _refreshToken;
        private const string AccessStoragePath = "OneNoteAddIn.Access";
        private const string RefreshStoragePath = "OneNoteAddIn.Refresh";

        private static readonly byte[] Entropy = {
            2,
            6,
            3,
            4
        };

        #region Auth

        public virtual async Task SubmitCode(Uri redirectUriWithCode)
        {
            var code = redirectUriWithCode.Query.GetParams()["code"];
            if (code == null)
            {
                throw new ArgumentException("Uri must contain code", nameof(redirectUriWithCode));
            }

            var client = new HttpClient();
            var response = await client.PostAsync(
                               AuthTokenUrl,
                               TokenContent(code)).ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var json = JObject.Parse(content);

            AccessToken = json["access_token"].ToString();
            RefreshToken = json["refresh_token"].ToString();
        }

        private async Task DoRefreshToken()
        {
            var client = new HttpClient();
            var response = await client.PostAsync(
                               RefreshTokenUrl,
                               RefreshTokenContent()).ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var json = JObject.Parse(content);

            AccessToken = json["access_token"].ToString();
            RefreshToken = json["refresh_token"].ToString();
        }

        protected virtual StringContent TokenContent(string code)
        {
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["grant_type"] = "authorization_code";
            query["client_id"] = ClientId;
            //query["client_secret"] = Secret;
            query["code"] = code;
            query["redirect_uri"] = AuthRedirectUrl;
            return new StringContent(query.ToString(), Encoding.Default, "application/x-www-form-urlencoded");
        }

        protected virtual StringContent RefreshTokenContent()
        {
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["grant_type"] = "refresh_token";
            query["client_id"] = ClientId;
            query["redirect_uri"] = AuthRedirectUrl;
            query["refresh_token"] = RefreshToken;
            // query["resource"] = Resource;
            return new StringContent(query.ToString(), Encoding.Default, "application/x-www-form-urlencoded");
        }

        #endregion

        #region Send

        protected async Task<string> Get(Uri uri)
        {
            try
            {
                return await GetInternal(uri).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                // todo: specify exception
                try
                {
                    if (IsAuth)
                    {
                        await DoRefreshToken().ConfigureAwait(false);
                    }
                    else
                    {
                        throw new AuthException(
                            $"Authorize {nameof(OneNoteAuth)} before API {uri} access ({nameof(IsAuth)}==false and {nameof(GetInternal)} threw {e.GetType()})",
                            e);
                    }
                }
                catch
                {
                    throw new AuthException(
                        $"Authorize {nameof(OneNoteAuth)} before API {uri} access ({nameof(IsAuth)}==false and {nameof(GetInternal)} threw {e.GetType()})",
                        e);
                }
                return await GetInternal(uri).ConfigureAwait(false);
            }
        }

        protected async Task<string> Delete(Uri uri)
        {
            try
            {
                return await DeleteInternal(uri).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                // todo: specify exception
                try
                {
                    if (IsAuth)
                    {
                        await DoRefreshToken().ConfigureAwait(false);
                    }
                    else
                    {
                        throw new AuthException(
                            $"Authorize {nameof(OneNoteAuth)} before API {uri} access ({nameof(IsAuth)}==false and {nameof(DeleteInternal)} threw {e.GetType()})",
                            e);
                    }
                }
                catch
                {
                    throw new AuthException(
                        $"Authorize {nameof(OneNoteAuth)} before API {uri} access ({nameof(IsAuth)}==false and {nameof(DeleteInternal)} threw {e.GetType()})",
                        e);
                }
                return await GetInternal(uri).ConfigureAwait(false);
            }
        }

        protected async Task<string> Post(Uri uri, string content)
        {
            try
            {
                return await PostInternal(uri, content).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                // todo: specify exception
                try
                {
                    if (IsAuth)
                    {
                        await DoRefreshToken().ConfigureAwait(false);
                    }
                    else
                    {
                        throw new AuthException(
                            $"Authorize {nameof(OneNoteAuth)} before API {uri} access ({nameof(IsAuth)}==false and {nameof(PostInternal)} threw {e.GetType()})",
                            e);
                    }
                }
                catch
                {
                    throw new AuthException(
                        $"Authorize {nameof(OneNoteAuth)} before API {uri} access ({nameof(IsAuth)}==false and {nameof(PostInternal)} threw {e.GetType()})",
                        e);
                }
                return await PostInternal(uri, content).ConfigureAwait(false);
            }
        }

        #endregion

        #region SendInternal

        private async Task<string> DeleteInternal(Uri uri)
        {
            var client = CreateDefaultClient(uri);
            return await ProcessResponse(
                       await client.DeleteAsync(uri).ConfigureAwait(false)).ConfigureAwait(false);
        }

        private async Task<string> GetInternal(Uri uri)
        {
            var client = CreateDefaultClient(uri);
            return await ProcessResponse(
                       await client.GetAsync(uri).ConfigureAwait(false)).ConfigureAwait(false);
        }

        private async Task<string> PostInternal(Uri uri, string content)
        {
            var client = CreateDefaultClient(uri);
            return await ProcessResponse(
                       await client.PostAsync(
                           uri,
                           new StringContent(content, Encoding.Default, "application/json")).ConfigureAwait(false)).ConfigureAwait(false);
        }

        private async Task<string> ProcessResponse(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                Tracer.WriteLine(
                    TracerTags.Http,
                    $"{response.RequestMessage.RequestUri}{Environment.NewLine}Response: 401 Unauthorized");
                throw new AuthException(
                    $"401 Unauthorized in {nameof(PostInternal)} for {response.RequestMessage.RequestUri}");
            }
            if (response.StatusCode == HttpStatusCode.NotImplemented)
            {
                Tracer.WriteLine(
                    TracerTags.Http,
                    $"{response.RequestMessage.RequestUri}{Environment.NewLine}Response: 501 NotImplemented");
            }
            var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

#if DEBUG
            Tracer.WriteLine(
                TracerTags.Http,
                $"Request: {response.RequestMessage.RequestUri}{Environment.NewLine}Response: {response}{Environment.NewLine}Content: {result}");
#endif

            return result;
        }

        private HttpClient CreateDefaultClient(Uri uri)
        {
            //var handler = new HttpClientHandler
            //{
            //    AllowAutoRedirect = true,
            //    UseCookies = true
            //};
            var client = new HttpClient {
                BaseAddress = uri
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        #endregion
    }
}