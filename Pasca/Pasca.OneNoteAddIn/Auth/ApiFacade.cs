﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pasca.Common.Auth;
using Pasca.Common.Exceptions;
using Pasca.Common.Students;
using Pasca.OneNoteAddIn.Localization;
using Pasca.OneNoteAddIn.Progress;
using Pasca.OneNoteAddIn.Settings;

namespace Pasca.OneNoteAddIn.Auth
{
    public class ApiFacade
    {
        // todo: make private
        private ApiFacade(
            GraphAuth graphAuth,
            OneNoteAuth oneNoteAuth,
            IAuthorizationRequestHandler authorizationRequest,
            OneNoteSetting sessionSetting,
            IProgressProvider progressProvider)
        {
            _graphAuth = graphAuth;
            _oneNoteAuth = oneNoteAuth;
            _authRequest = authorizationRequest;
            _setting = sessionSetting;
            _progressProvider = progressProvider;
        }

        public string NotebookName { get; set; }

        public bool Authorized => _oneNoteAuth.IsAuth;

        public IAuth GetAuth()
        {
            return _oneNoteAuth;
        }

        public bool Logout()
        {
            return _oneNoteAuth.DeleteTokens();
        }

        public async Task<string> GetNotebookId()
        {
            if (_notebookId == null && NotebookName != null)
            {
                return _notebookId = await GetNotebookIdByName(NotebookName);
            }

            return _notebookId;
        }

        [Obsolete]
        public async Task<PersonBuilder> GetPersonByFullName(string fullName)
        {
            var response = await AuthOnFail(_graphAuth, () => _graphAuth.GetUserInfoByFullName(fullName)).ConfigureAwait(false);
            return new PersonBuilder {
                FullName = response.displayName,
                Email = response.mail
            };
        }

        [Obsolete]
        public async Task<PersonBuilder> GetPersonByMail(string email)
        {
            var response = await AuthOnFail(_graphAuth, () => _graphAuth.GetUserInfoByEmail(email)).ConfigureAwait(false);
            return new PersonBuilder {
                FullName = response.displayName,
                Email = response.mail
            };
        }

        public async Task<string> GetNotebookIdByName(string notebookName)
        {
            return (await AuthOnFail(_oneNoteAuth, () => _oneNoteAuth.GetNotebookByName(NotebookName)).ConfigureAwait(false))
                    .value[0].id;
        }

        public async Task<IEnumerable<PersonBuilder>> GetClassStudents()
        {
            var response = (await AuthOnFail(_oneNoteAuth, async () => await _oneNoteAuth.GetStudentsByNotebook(await GetNotebookId())).ConfigureAwait(false)).value;
            return Enumerable.Select(
                response,
                (Func<dynamic, PersonBuilder>)(st => new PersonBuilder {
                    FullName = st.name.ToString(),
                    Email = st.email.ToString(),
                    Role = PersonRole.Student
                }));
        }

        public async Task<IEnumerable<PersonBuilder>> GetClassTeachers()
        {
            var response = (await AuthOnFail(_oneNoteAuth, async () => await _oneNoteAuth.GetStudentsByNotebook(await GetNotebookId())).ConfigureAwait(false)).value;
            return Enumerable.Select(
                response["teachers"],
                (Func<dynamic, PersonBuilder>)(st => new PersonBuilder {
                    FullName = st.name.ToString(),
                    Email = st.email.ToString(),
                    Role = PersonRole.Teacher
                }));
        }

        public async Task CreateStudentsDashboardSections(PascaSessionSetting session)
        {
            var students = session.SessionAuthors().Union(session.SessionReviewers()).ToArray();
            for (var i = 0; i < students.Length; i++)
            {
                _progressProvider.ReportProgress(
                    new ProgressReport(i / (double)students.Length, LocTags.CreatingDashboardSectionsViaWebApi));

                var student = students[i];

                var studentSectionGroup = await
                                                  AuthOnFail(
                                                      _oneNoteAuth,
                                                      async () =>
                                                          await _oneNoteAuth.GetOrCreateSectionGroupInNotebookByName(
                                                              await GetNotebookId(),
                                                              student.FullName)).ConfigureAwait(false);
                studentSectionGroup = studentSectionGroup.value[0];

                var dashboardSection = await AuthOnFail(
                                           _oneNoteAuth,
                                           () =>
                                               _oneNoteAuth.GetOrCreateSectionInSectionGroupByName(
                                                   studentSectionGroup.id.ToString(),
                                                   "Dashboard")).ConfigureAwait(false);
                dashboardSection = dashboardSection.value[0];
                var permissions = await AuthOnFail(
                                      _oneNoteAuth,
                                      () =>
                                          _oneNoteAuth.GetPermissionInSectionGroup(
                                              dashboardSection.id.ToString(),
                                              student.Email)).ConfigureAwait(false);
                permissions = permissions.value;

                foreach (var permission in permissions)
                {
                    if (permission.userRole != PermissionRoles.Reader)
                    {
                        await AuthOnFail(
                            _oneNoteAuth,
                            () =>
                                _oneNoteAuth.RemovePermissionInSection(
                                    dashboardSection.id.ToString(),
                                    permission.id.ToString())).ConfigureAwait(false);
                    }
                }
                await AuthOnFail(
                    _oneNoteAuth,
                    () =>
                        _oneNoteAuth.AddPermissionInSection(
                            dashboardSection.id.ToString(),
                            student.Email,
                            PermissionRoles.Reader)).ConfigureAwait(false);
            }
        }

        public async Task LockStudentsArtifactsSections(PascaSessionSetting session)
        {
            var students = session.SessionAuthors();
            for (var i = 0; i < students.Count; i++)
            {
                _progressProvider.ReportProgress(
                    new ProgressReport(i / (double)students.Count, LocTags.LockingArtifactsViaWebApi));

                var student = students[i];

                var studentSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              async () =>
                                                  await _oneNoteAuth.GetOrCreateSectionGroupInNotebookByName(
                                                      await GetNotebookId(),
                                                      student.FullName)).ConfigureAwait(false);
                studentSectionGroup = studentSectionGroup.value[0];

                var sessionSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              () =>
                                                  _oneNoteAuth.GetOrCreateSectionGroupInSectionGroupByName(
                                                      studentSectionGroup.id.ToString(),
                                                      session.SessionName())).ConfigureAwait(false);
                sessionSectionGroup = sessionSectionGroup.value[0];

                var artifactsSection = await AuthOnFail(
                                           _oneNoteAuth,
                                           () =>
                                               _oneNoteAuth.GetOrCreateSectionInSectionGroupByName(
                                                   sessionSectionGroup.id.ToString(),
                                                   session.SessionArtifactSectionName())).ConfigureAwait(false);
                artifactsSection = artifactsSection.value[0];

                var permissions = await AuthOnFail(
                                      _oneNoteAuth,
                                      () =>
                                          _oneNoteAuth.GetPermissionInSection(
                                              artifactsSection.id.ToString(),
                                              student.Email)).ConfigureAwait(false);
                permissions = permissions.value;

                foreach (var permission in permissions)
                {
                    if (permission.userRole != PermissionRoles.Reader)
                    {
                        await AuthOnFail(
                            _oneNoteAuth,
                            () =>
                                _oneNoteAuth.RemovePermissionInSection(
                                    artifactsSection.id.ToString(),
                                    permission.id.ToString())).ConfigureAwait(false);
                    }
                }

                await AuthOnFail(
                    _oneNoteAuth,
                    () =>
                        _oneNoteAuth.AddPermissionInSection(
                            artifactsSection.id.ToString(),
                            student.Email,
                            PermissionRoles.Reader)).ConfigureAwait(false);
            }
        }

        public async Task<bool> AreStudentArtifactsSectionsLocked(PascaSessionSetting session)
        {
            var students = session.SessionAuthors();
            for (var i = 0; i < students.Count; i++)
            {
                _progressProvider.ReportProgress(
                    new ProgressReport(i / (double)students.Count, LocTags.RequestingWebApi));

                var student = students[i];

                var studentSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              async () =>
                                                  await _oneNoteAuth.GetOrCreateSectionGroupInNotebookByName(
                                                      await GetNotebookId(),
                                                      student.FullName)).ConfigureAwait(false);
                studentSectionGroup = studentSectionGroup.value[0];

                var sessionSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              () =>
                                                  _oneNoteAuth.GetOrCreateSectionGroupInSectionGroupByName(
                                                      studentSectionGroup.id.ToString(),
                                                      session.SessionName())).ConfigureAwait(false);
                sessionSectionGroup = sessionSectionGroup.value[0];

                var artifactsSection = await AuthOnFail(
                                           _oneNoteAuth,
                                           () =>
                                               _oneNoteAuth.GetOrCreateSectionInSectionGroupByName(
                                                   sessionSectionGroup.id.ToString(),
                                                   session.SessionArtifactSectionName())).ConfigureAwait(false);
                artifactsSection = artifactsSection.value[0];

                var permissions = await AuthOnFail(
                                      _oneNoteAuth,
                                      () =>
                                          _oneNoteAuth.GetPermissionInSection(
                                              artifactsSection.id.ToString(),
                                              student.Email)).ConfigureAwait(false);
                permissions = permissions.value;

                foreach (var permission in permissions)
                {
                    if (permission.userRole != PermissionRoles.Reader)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public async Task LockStudentsReviewsSections(PascaSessionSetting session)
        {
            var students = session.SessionReviewers();
            for (var i = 0; i < students.Count; i++)
            {
                _progressProvider.ReportProgress(
                    new ProgressReport(i / (double)students.Count, LocTags.LockingReviewsViaWebApi));

                var student = students[i];

                var studentSectionGroup = await
                                                  AuthOnFail(
                                                      _oneNoteAuth,
                                                      async () =>
                                                          await _oneNoteAuth.GetOrCreateSectionGroupInNotebookByName(
                                                              await GetNotebookId(),
                                                              student.FullName)).ConfigureAwait(false);
                studentSectionGroup = studentSectionGroup.value[0];

                var sessionSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              () =>
                                                  _oneNoteAuth.GetOrCreateSectionGroupInSectionGroupByName(
                                                      studentSectionGroup.id.ToString(),
                                                      session.SessionName())).ConfigureAwait(false);
                sessionSectionGroup = sessionSectionGroup.value[0];

                var reviewsSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              () =>
                                                  _oneNoteAuth.GetOrCreateSectionGroupInSectionGroupByName(
                                                      sessionSectionGroup.id.ToString(),
                                                      session.SessionReviewArtifactsSectionGroupName())).ConfigureAwait(false);
                reviewsSectionGroup = reviewsSectionGroup.value[0];

                var permissions = await AuthOnFail(
                                      _oneNoteAuth,
                                      () =>
                                          _oneNoteAuth.GetPermissionInSection(
                                              reviewsSectionGroup.id.ToString(),
                                              student.Email)).ConfigureAwait(false);
                permissions = permissions.value;

                foreach (var permission in permissions)
                {
                    if (permission.userRole != PermissionRoles.Reader)
                    {
                        await AuthOnFail(
                            _oneNoteAuth,
                            () =>
                                _oneNoteAuth.RemovePermissionInSectionGroup(
                                    reviewsSectionGroup.id.ToString(),
                                    permission.id.ToString())).ConfigureAwait(false);
                    }
                }

                await AuthOnFail(
                    _oneNoteAuth,
                    () =>
                        _oneNoteAuth.AddPermissionInSectionGroup(
                            reviewsSectionGroup.id.ToString(),
                            student.Email,
                            PermissionRoles.Reader)).ConfigureAwait(false);
            }
        }

        public async Task<bool> AreStudentsReviewsSectionsLocked(PascaSessionSetting session)
        {
            var students = session.SessionReviewers();
            for (var i = 0; i < students.Count; i++)
            {
                _progressProvider.ReportProgress(
                    new ProgressReport(i / (double)students.Count, LocTags.RequestingWebApi));

                var student = students[i];

                var studentSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              async () =>
                                                  await _oneNoteAuth.GetOrCreateSectionGroupInNotebookByName(
                                                      await GetNotebookId(),
                                                      student.FullName)).ConfigureAwait(false);
                studentSectionGroup = studentSectionGroup.value[0];

                var sessionSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              () =>
                                                  _oneNoteAuth.GetOrCreateSectionGroupInSectionGroupByName(
                                                      studentSectionGroup.id.ToString(),
                                                      session.SessionName())).ConfigureAwait(false);
                sessionSectionGroup = sessionSectionGroup.value[0];

                var reviewsSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              () =>
                                                  _oneNoteAuth.GetOrCreateSectionGroupInSectionGroupByName(
                                                      sessionSectionGroup.id.ToString(),
                                                      session.SessionReviewArtifactsSectionGroupName())).ConfigureAwait(false);
                reviewsSectionGroup = reviewsSectionGroup.value[0];

                var permissions = await AuthOnFail(
                                      _oneNoteAuth,
                                      () =>
                                          _oneNoteAuth.GetPermissionInSection(
                                              reviewsSectionGroup.id.ToString(),
                                              student.Email)).ConfigureAwait(false);
                permissions = permissions.value;

                foreach (var permission in permissions)
                {
                    if (permission.userRole != PermissionRoles.Reader)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public async Task CreateAssignmentSections(PascaSessionSetting session)
        {
            var students = session.SessionAuthors().Union(session.SessionReviewers()).ToArray();
            for (var i = 0; i < students.Length; i++)
            {
                _progressProvider.ReportProgress(
                    new ProgressReport(i / (double)students.Length, LocTags.CreatingAssignmentSectionsViaWebApi));

                var student = students[i];

                var studentSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              async () =>
                                                  await _oneNoteAuth.GetOrCreateSectionGroupInNotebookByName(
                                                      await GetNotebookId(),
                                                      student.FullName)).ConfigureAwait(false);
                studentSectionGroup = studentSectionGroup.value[0];

                var sessionSectionGroup = await AuthOnFail(
                                              _oneNoteAuth,
                                              () =>
                                                  _oneNoteAuth.GetOrCreateSectionGroupInSectionGroupByName(
                                                      studentSectionGroup.id.ToString(),
                                                      session.SessionName())).ConfigureAwait(false);
                sessionSectionGroup = sessionSectionGroup.value[0];

                var assignmentSection = await AuthOnFail(
                                            _oneNoteAuth,
                                            () =>
                                                _oneNoteAuth.GetOrCreateSectionInSectionGroupByName(
                                                    sessionSectionGroup.id.ToString(),
                                                    session.SessionAssignmentSectionName())).ConfigureAwait(false);
                assignmentSection = assignmentSection.value[0];

                var permissions = await AuthOnFail(
                                      _oneNoteAuth,
                                      () =>
                                          _oneNoteAuth.GetPermissionInSection(
                                              assignmentSection.id.ToString(),
                                              student.Email)).ConfigureAwait(false);
                permissions = permissions.value;

                foreach (var permission in permissions)
                {
                    if (permission.userRole != PermissionRoles.Reader)
                    {
                        await AuthOnFail(
                            _oneNoteAuth,
                            () =>
                                _oneNoteAuth.RemovePermissionInSection(
                                    assignmentSection.id.ToString(),
                                    permission.id.ToString())).ConfigureAwait(false);
                    }
                }

                await AuthOnFail(
                    _oneNoteAuth,
                    () =>
                        _oneNoteAuth.AddPermissionInSection(
                            assignmentSection.id.ToString(),
                            student.Email,
                            PermissionRoles.Reader)).ConfigureAwait(false);
            }
        }

        [Obsolete]
        public async Task CreateStudentSectionGroupsInContentLibrarySectionGroup()
        {
            var collabSectionGroup =
                    await AuthOnFail(
                        _oneNoteAuth,
                        async () => await _oneNoteAuth.GetOrCreateSectionGroupInNotebookByName(await GetNotebookId(), ContentLibrary)).ConfigureAwait(false);
            collabSectionGroup = collabSectionGroup.value[0];
            var pascaSectionGroup =
                    await AuthOnFail(
                        _oneNoteAuth,
                        () =>
                            _oneNoteAuth.GetOrCreateSectionGroupInSectionGroupByName(
                                collabSectionGroup.id.ToString(),
                                Pasca)).ConfigureAwait(false);
            pascaSectionGroup = pascaSectionGroup.value[0];
            var students = await GetClassStudents().ConfigureAwait(false);
            foreach (var student in students)
            {
                var permissions = await AuthOnFail(
                                      _oneNoteAuth,
                                      () =>
                                          _oneNoteAuth.GetPermissionInSectionGroup(
                                              pascaSectionGroup.id.ToString(),
                                              student.Email)).ConfigureAwait(false);
                permissions = permissions.value;
                foreach (var permission in permissions)
                {
                    if (permission.userRole != PermissionRoles.Reader)
                    {
                        await AuthOnFail(
                            _oneNoteAuth,
                            () =>
                                _oneNoteAuth.RemovePermissionInSectionGroup(
                                    pascaSectionGroup.id.ToString(),
                                    permission.id.ToString())).ConfigureAwait(false);
                    }
                }

                await AuthOnFail(
                    _oneNoteAuth,
                    () =>
                        _oneNoteAuth.AddPermissionInSectionGroup(
                            pascaSectionGroup.id.ToString(),
                            student.Email,
                            PermissionRoles.Reader)).ConfigureAwait(false);

                var studentSectionGroup = await
                                                  AuthOnFail(
                                                      _oneNoteAuth,
                                                      () =>
                                                          _oneNoteAuth.GetOrCreateSectionGroupInSectionGroupByName(
                                                              pascaSectionGroup.id.ToString(),
                                                              student.FullName)).ConfigureAwait(false);
                studentSectionGroup = studentSectionGroup.value[0];
                await AuthOnFail(
                    _oneNoteAuth,
                    () =>
                        _oneNoteAuth.AddPermissionInSectionGroup(
                            studentSectionGroup.id.ToString(),
                            student.Email,
                            PermissionRoles.Contributor)).ConfigureAwait(false);
            }
        }

        private async Task<dynamic> AuthOnFail(IAuth auth, Func<Task<dynamic>> action)
        {
            try
            {
                return await action().ConfigureAwait(false);
            }
            catch (AuthException)
            {
                _authRequest.RequestAuthorization(auth);
                return await action().ConfigureAwait(false);
            }
        }

        private readonly IAuthorizationRequestHandler _authRequest;

        private readonly GraphAuth _graphAuth;
        private readonly OneNoteAuth _oneNoteAuth;

        private readonly IProgressProvider _progressProvider;
        private readonly OneNoteSetting _setting;
        private string _notebookId;

        public class Factory
        {
            public Factory(
                IAuthorizationRequestHandler authorizationRequest,
                IProgressProvider progressProvider)
            {
                Assert.NotNull(authorizationRequest);
                Assert.NotNull(progressProvider);
                
                _request = authorizationRequest;
                _progressProvider = progressProvider;
            }

            public ApiFacade Create(OneNoteSetting oneNoteSetting)
            {
                Assert.NotNull(oneNoteSetting);

                return new ApiFacade(
                    new GraphAuth(), 
                    new OneNoteAuth(), 
                    _request,
                    oneNoteSetting,
                    _progressProvider);
            }
            
            private readonly IProgressProvider _progressProvider;
            private readonly IAuthorizationRequestHandler _request;
        }

        #region Settings shortcuts

        //private string TeacherOnly => _setting.SessionTeacherOnlySectionGroupPostfix();

        [Obsolete]
        private string ContentLibrary => _setting.ContentLibrarySectionGroupPostfix();

        private string Pasca => _setting.PascaSectionName();

        #endregion
    }
}