﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;
using Pasca.Common.Auth;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.OneNoteAddIn.Extensions;

namespace Pasca.OneNoteAddIn.Auth
{
    [Obsolete]
    public class GraphAuth : IAuth
    {
        private string RefreshToken { get; set; }

        protected async Task<string> Get(Uri uri)
        {
            try
            {
                return await GetInternal(uri).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                // todo: specify exception
                if (IsAuth)
                {
                    await DoRefreshToken().ConfigureAwait(false);
                }
                else
                {
                    throw new AuthException(
                        $"Authorize {nameof(GraphAuth)} before API {uri} access ({nameof(IsAuth)}==false and GetInternal threw {e.GetType()})",
                        e);
                }
                return await GetInternal(uri).ConfigureAwait(false);
            }
        }

        public string AccessToken { get; private set; }

        public async Task<dynamic> GetUserInfoByFullName(string fullName)
        {
            return (await Get(UsersWhereDisplayNameUri(fullName)).ConfigureAwait(false)).ToDynamic();
        }

        public async Task<dynamic> GetUserInfoByEmail(string email)
        {
            return (await Get(UsersWhereMailUrl(email)).ConfigureAwait(false)).ToDynamic();
        }

        private async Task<string> GetInternal(Uri uri)
        {
            var client = new HttpClient {
                BaseAddress = uri
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
            var result = await client.GetAsync("/").ConfigureAwait(false);
            return await result.Content.ReadAsStringAsync().ConfigureAwait(false);
        }

        private const string ClientId = "dfb8f4b1-1962-4e2b-bd83-8e8e09e024fb";
        private const string Permissions = @"Contacts.Read Notes.ReadWrite.All User.Read User.ReadBasic.All";

        private const string AuthRequestUrl = "https://login.microsoftonline.com/common/oauth2/authorize";
        private const string AuthRedirectUrl = "urn:ietf:wg:oauth:2.0:oob";
        private const string AuthTokenUrl = "https://login.microsoftonline.com/common/oauth2/token";
        private const string LiveApiMeUrl = "https://graph.microsoft.com/v1.0/me";
        private const string UsersUrl = "https://graph.microsoft.com/beta/myOrganization/users";

        #region Authorization

        public Uri SignInUri
            =>
                    new Uri(
                        AuthRequestUrl +
                        $"?response_type=code&client_id={ClientId}&redirect_uri={AuthRedirectUrl}&scope={Permissions}&resource=https://graph.microsoft.com/")
        ;

        public Uri SignOutUri => new Uri("");
        //AuthLogoutUrl + $"?client_id={ClientId}&redirect_uri={AuthRedirectUrl}");

        private string TokenContent(string code)
        {
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["grant_type"] = "authorization_code";
            query["client_id"] = ClientId;
            //query["client_secret"] = Secret;
            query["code"] = code;
            query["redirect_uri"] = AuthRedirectUrl;
            return query.ToString();
        }

        private string RefreshTokenContent()
        {
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["grant_type"] = "refresh_token";
            query["client_id"] = ClientId;
            query["redirect_uri"] = AuthRedirectUrl;
            query["refresh_token"] = RefreshToken;
            // query["resource"] = Resource;
            return query.ToString();
        }

        public async Task SubmitCode(Uri redirectUriWithCode)
        {
            var code = redirectUriWithCode.Query.GetParams()["code"];
            if (code == null)
            {
                throw new ArgumentException("Uri must contain code", nameof(redirectUriWithCode));
            }

            var client = new HttpClient();
            var response = await client.PostAsync(
                               AuthTokenUrl,
                               new StringContent(
                                   TokenContent(code),
                                   Encoding.Default,
                                   "application/x-www-form-urlencoded")).ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var json = JObject.Parse(content);

            AccessToken = json["access_token"].ToString();
        }

        private async Task DoRefreshToken()
        {
            var client = new HttpClient();
            var response = await client.PostAsync(
                               AuthTokenUrl,
                               new StringContent(
                                   RefreshTokenContent(),
                                   Encoding.Default,
                                   "application/x-www-form-urlencoded")).ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var json = JObject.Parse(content);

            AccessToken = json["access_token"].ToString();
            RefreshToken = json["refresh_token"].ToString();
        }

        private Uri CreateLiveApiMeUri()
        {
            return new Uri(LiveApiMeUrl);
        }

        private Uri UsersWhereDisplayNameUri(string displayName)
        {
            var baseUri = new Uri(UsersUrl);
            var query = HttpUtility.ParseQueryString(baseUri.Query);
            query.Add("$filter", $"displayName eq '{displayName}'");
            return new Uri(baseUri.AbsolutePath + query);
        }

        private Uri UsersWhereMailUrl(string email)
        {
            var baseUri = new Uri(UsersUrl);
            var query = HttpUtility.ParseQueryString(baseUri.Query);
            query.Add("$filter", $"mail eq '{email}'");
            return new Uri(baseUri.AbsolutePath + query);
        }

        public async Task<bool> SignOut()
        {
            var client = new HttpClient();
            var result = await client.GetAsync(SignOutUri).ConfigureAwait(false);
            return result.StatusCode == HttpStatusCode.OK;
        }

        public bool IsAuth => AccessToken != null;

        #endregion
    }
}