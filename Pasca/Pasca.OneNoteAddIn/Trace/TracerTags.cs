﻿using Pasca.Common.Trace;

namespace Pasca.OneNoteAddIn.Trace
{
    public class TracerTags : CommonTracerTags
    {
        public const string Http = nameof(Http);
        public const string Error = nameof(Error);
        public const string Fail = nameof(Fail);
        public const string OneNote = nameof(OneNote);
        public const string Template = nameof(Template);
        public const string PrntHndl = nameof(PrntHndl);
    }
}