﻿using System;
using System.IO;
using Pasca.Common.Extensions;
using Pasca.Common.Trace;

namespace Pasca.OneNoteAddIn.Trace
{
    public class Tracer : ITracer
    {
        public Tracer()
        {
            CreationTime = DateTime.Now;
            CreationTimeString = CreationTime.ToLongTimeString().Replace(':', '.');
        }

        private static string Template { get; } = "{0}\t{1}\t{2}\n";

        private static string PathFolder { get; } =
            System.IO.Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "Pasca\\trace\\");

        private static string PathName { get; } = "PascaTrace_";
        private static string Ext { get; } = ".log";
        private DateTime CreationTime { get; }
        private string CreationTimeString { get; }
        private string Path => PathFolder + PathName + CreationTimeString + Ext;

        public void WriteLine(string tag, string message)
        {
            try
            {
                if (tag.Length > 7)
                {
                    tag = tag.Substring(0, 7);
                }
                lock (_lock)
                {
                    if (!Directory.Exists(PathFolder))
                    {
                        Directory.CreateDirectory(PathFolder);
                    }
                    using (var sw = new StreamWriter(Path, true))
                    {
                        sw.WriteLine(Template, tag, DateTime.Now.ToMsTimeString(), message);
                    }
                }
            }
            catch
            {
                // StreamWriter is inaccessible, skip trace
            }
        }

        public void Clean()
        {
            Directory.Delete(PathFolder, true);
        }
        public void Show()
        {
            System.Diagnostics.Process.Start("explorer.exe", PathFolder);
        }

        private readonly object _lock = new object();
    }
}