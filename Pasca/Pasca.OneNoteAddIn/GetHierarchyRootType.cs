﻿namespace Pasca.OneNoteAddIn
{
    public enum GetHierarchyRootType
    {
        Notebook,
        TeacherOnly
    }
}
