﻿namespace Pasca.OneNoteAddIn
{
    public static class Colors
    {
        public const string Blue = "#bdd7ee";
        public const string Green = "#c5e0b3";
        public const string Red = "#f5b7a6";
        public const string Orange = "#fee501";

        public const string Passed = Green;
        public const string Default = Blue;
        public const string Fail = Red;
        public const string Warning = Orange;
    }
}