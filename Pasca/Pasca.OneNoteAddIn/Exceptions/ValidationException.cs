﻿using System;

namespace Pasca.OneNoteAddIn.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException(string message)
            : base(message) {}
    }
}