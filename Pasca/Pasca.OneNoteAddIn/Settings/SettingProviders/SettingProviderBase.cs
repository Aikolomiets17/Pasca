﻿using System;
using Pasca.Common.Exceptions;
using Pasca.Common.Settings;
using Pasca.Common.Settings.DefaultSettingsRegistry;
using Pasca.Gui.Facade;

namespace Pasca.OneNoteAddIn.Settings.SettingProviders
{
    internal abstract class SettingProviderBase<T> : ISettingProvider<T>
        where T : Setting
    {
        protected SettingProviderBase(SettingsManager manager, IDefaultSettingsRegistry defaults)
        {
            Assert.NotNull(manager);
            Assert.NotNull(defaults);

            Manager = manager;
            Defaults = defaults;
        }

        protected SettingsManager Manager { get; }
        protected IDefaultSettingsRegistry Defaults { get; }

        public abstract void Subscribe(Action<T, T> settingChangedHandler);
        public abstract T GetSetting();
        public abstract bool IsEditable(Setting setting);

        public abstract void Commit(Setting newSettings);

        public virtual ISelectableValueProvider GetSelectableValueProvider(Setting setting)
        {
            return null;
        }
    }
}