﻿using System;
using System.Linq;
using Pasca.Common.Extensions;
using Pasca.Common.Settings;
using Pasca.Common.Settings.DefaultSettingsRegistry;

namespace Pasca.OneNoteAddIn.Settings.SettingProviders
{
    internal class SessionTimingSettingsProvider : SettingProviderBase<PascaSessionSetting>
    {
        public SessionTimingSettingsProvider(
            SettingsManager manager,
            IDefaultSettingsRegistry defaults,
            string sessionName)
            : base(manager, defaults)
        {
            _sessionName = sessionName;
        }

        public override void Subscribe(Action<PascaSessionSetting, PascaSessionSetting> settingChangedHandler)
        {
            Manager.Subscribe(CommonSettingName.Session, CreateSettingChangedHandler(settingChangedHandler));
        }

        public override PascaSessionSetting GetSetting()
        {
            return Select(Manager.GetSetting<PascaSessionSetting>(CommonSettingName.Session, _sessionName));
        }

        public override bool IsEditable(Setting setting)
        {
            switch (setting.Name as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.SessionName:
                return false;
            }

            return true;
        }

        private Action<Setting, Setting> CreateSettingChangedHandler(Action<PascaSessionSetting, PascaSessionSetting> handler)
        {
            return (oldSetting, newSetting) =>
            {
                var oldArraySetting = oldSetting as PascaSessionSetting;
                var newArraySetting = newSetting as PascaSessionSetting;
                if (oldArraySetting != null && newArraySetting != null &&
                    newArraySetting.SessionName().OrdinalEquals(_sessionName))
                {
                    handler(
                        Select(oldArraySetting),
                        Select(newArraySetting));
                }
            };
        }

        public override void Commit(Setting newSettings)
        {
            Manager.Update(newSettings);
        }

        private PascaSessionSetting Select(PascaSessionSetting sessionSetting)
        {
            return new PascaSessionSetting(ArraySetting.Update(
                CreateDefaultSetting(),
                new ArraySetting(
                    CommonSettingName.Session,
                    sessionSetting
                            .SubSettings
                            .Where(SessionSettingsSelector)
                            .ToList())));
        }

        private PascaSessionSetting CreateDefaultSetting()
        {
            return new PascaSessionSetting(
                CommonSettingName.Session,
                new[] {
                    CommonSettingName.SessionName,
                    CommonSettingName.SessionSubmissionBegin,
                    CommonSettingName.SessionSubmissionEnd,
                    CommonSettingName.SessionReviewBegin,
                    CommonSettingName.SessionReviewEnd
                }.Select(e => Defaults.GetDefault<Setting>(e)).ToList());
        }

        private bool SessionSettingsSelector(Setting setting)
        {
            switch (setting.Name as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.SessionName:
            case CommonSettingName.SessionSubmissionBegin:
            case CommonSettingName.SessionSubmissionEnd:
            case CommonSettingName.SessionReviewBegin:
            case CommonSettingName.SessionReviewEnd:
                return true;
            }

            return false;
        }

        private readonly string _sessionName;
    }
}