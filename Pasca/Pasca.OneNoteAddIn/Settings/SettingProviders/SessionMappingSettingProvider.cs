﻿using System;
using System.Linq;
using Pasca.Common.Algorithm;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.Common.Settings;
using Pasca.Common.Settings.DefaultSettingsRegistry;

namespace Pasca.OneNoteAddIn.Settings.SettingProviders
{
    internal class SessionMappingSettingProvider : SettingProviderBase<PascaSessionSetting>
    {
        public SessionMappingSettingProvider(
            SettingsManager manager,
            IDefaultSettingsRegistry defaults,
            string sessionName)
            : base(manager, defaults)
        {
            _sessionName = sessionName;
        }

        public override void Subscribe(Action<PascaSessionSetting, PascaSessionSetting> settingChangedHandler)
        {
            Manager.Subscribe(CommonSettingName.Session, CreateSettingChangedHandler(settingChangedHandler));
        }

        public override PascaSessionSetting GetSetting()
        {
            return Select(Manager.GetSetting<PascaSessionSetting>(CommonSettingName.Session, _sessionName));
        }

        public override bool IsEditable(Setting setting)
        {
            return false;
        }

        private Action<Setting, Setting> CreateSettingChangedHandler(Action<PascaSessionSetting, PascaSessionSetting> handler)
        {
            return (oldSetting, newSetting) =>
            {
                var oldArraySetting = oldSetting as PascaSessionSetting;
                var newArraySetting = newSetting as PascaSessionSetting;
                if (oldArraySetting != null && newArraySetting != null &&
                    newArraySetting.SessionName().OrdinalEquals(_sessionName))
                {
                    handler(
                        Select(oldArraySetting),
                        Select(newArraySetting));
                }
            };
        }

        public override void Commit(Setting newSettings)
        {
            var newSessionSettings = newSettings as PascaSessionSetting;
            Assert.NotNull(newSessionSettings);

            {
                var mapping = newSessionSettings[CommonSettingName.SessionMapping] as PersonMappingSetting;
                if (mapping != null)
                {
                    var mapper = new Mapper(
                        newSessionSettings.SessionAuthors(),
                        newSessionSettings.SessionReviewers(),
                        mapping.Value,
                        newSessionSettings.SessionReviewersRatio());

                    newSessionSettings[CommonSettingName.SessionMapping] = new IntMappingSetting(
                        CommonSettingName.SessionMapping,
                        mapper.Mapping);
                    newSessionSettings[CommonSettingName.SessionAuthors] = new AnonymizedPersonListSetting(
                        CommonSettingName.SessionAuthors,
                        mapper.Authors);
                    newSessionSettings[CommonSettingName.SessionReviewers] = new AnonymizedPersonListSetting(
                        CommonSettingName.SessionReviewers,
                        mapper.Reviewers);
                }
            }

            Manager.Update(newSettings);
        }

        private PascaSessionSetting Select(PascaSessionSetting sessionSetting)
        {
            return new PascaSessionSetting(ArraySetting.Update(
                CreateDefaultSetting(),
                new ArraySetting(
                    CommonSettingName.Session,
                    sessionSetting
                            .SubSettings
                            .Where(SessionSettingsSelector)
                            .ToList())));
        }

        private ArraySetting CreateDefaultSetting()
        {
            return new ArraySetting(
                CommonSettingName.Session,
                new[] {
                    CommonSettingName.SessionName,
                    CommonSettingName.SessionReviewersRatio,
                    CommonSettingName.SessionAuthors,
                    CommonSettingName.SessionReviewers,
                    CommonSettingName.SessionMapping,
                }.Select(e => Defaults.GetDefault<Setting>(e)).ToList());
        }

        private bool SessionSettingsSelector(Setting setting)
        {
            switch (setting.Name as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.SessionName:
            case CommonSettingName.SessionReviewersRatio:
            case CommonSettingName.SessionAuthors:
            case CommonSettingName.SessionReviewers:
            case CommonSettingName.SessionMapping:
                return true;
            }

            return false;
        }

        private readonly string _sessionName;
    }
}