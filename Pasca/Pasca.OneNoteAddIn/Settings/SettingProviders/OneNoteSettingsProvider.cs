﻿using System;
using System.Linq;
using Pasca.Common.Settings;
using Pasca.Common.Settings.DefaultSettingsRegistry;

namespace Pasca.OneNoteAddIn.Settings.SettingProviders
{
    internal class OneNoteSettingsProvider : SettingProviderBase<OneNoteSetting>
    {
        public OneNoteSettingsProvider(SettingsManager manager, IDefaultSettingsRegistry defaults)
            : base(manager, defaults) {}

        public override void Subscribe(Action<OneNoteSetting, OneNoteSetting> settingChangedHandler)
        {
            Manager.Subscribe(PascaSettingName.OneNote, CreateSettingsChangedHandler(settingChangedHandler));
        }

        public override OneNoteSetting GetSetting()
        {
            return Select(Manager.GetSetting<OneNoteSetting>(PascaSettingName.OneNote));
        }

        public override bool IsEditable(Setting setting)
        {
            return true;
        }

        public override void Commit(Setting newSettings)
        {
            Manager.Update(newSettings);
        }

        private Action<Setting, Setting> CreateSettingsChangedHandler(Action<OneNoteSetting, OneNoteSetting> handler)
        {
            return (oldSetting, newSetting) =>
            {
                var oldArraySetting = oldSetting as OneNoteSetting;
                var newArraySetting = newSetting as OneNoteSetting;
                if (oldArraySetting != null && newArraySetting != null)
                {
                    handler(Select(oldArraySetting), Select(newArraySetting));
                }
            };
        }

        private OneNoteSetting Select(OneNoteSetting sessionSetting)
        {
            return new OneNoteSetting(ArraySetting.Update(
                CreateDefaultSettings(),
                new ArraySetting(
                    PascaSettingName.OneNote,
                    sessionSetting.SubSettings.Where(SessionSettingsSelector)
                                  .ToList())));
        }

        private bool SessionSettingsSelector(Setting setting)
        {
            switch (setting.Name as PascaSettingName? ?? PascaSettingName.Invalid)
            {
            case PascaSettingName.TeacherOnlySectionGroupPostfix:
            case PascaSettingName.SettingsPagePostfix:
            case PascaSettingName.PascaSectionName:
            case PascaSettingName.DashboardSectionName:
                return true;
            default:
                return false;
            }
        }

        public OneNoteSetting CreateDefaultSettings()
        {
            return new OneNoteSetting(
                PascaSettingName.OneNote,
                new[] {
                            PascaSettingName.PascaSectionName,
                            PascaSettingName.SettingsPagePostfix,
                            PascaSettingName.TeacherOnlySectionGroupPostfix,
                            PascaSettingName.DashboardSectionName,
                        }
                        .Select(q => Defaults.GetDefault<Setting>(q)).ToList());
        }
    }
}