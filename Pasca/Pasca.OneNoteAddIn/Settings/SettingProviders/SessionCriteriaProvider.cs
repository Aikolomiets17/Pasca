﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pasca.Common.Algorithm.Criteria;
using Pasca.Common.Extensions;
using Pasca.Common.Settings;
using Pasca.Common.Settings.DefaultSettingsRegistry;

namespace Pasca.OneNoteAddIn.Settings.SettingProviders
{
    internal class SessionCriteriaProvider : SettingProviderBase<PascaSessionSetting>
    {
        public SessionCriteriaProvider(SettingsManager manager, IDefaultSettingsRegistry defaults, string sessionName)
            : base(manager, defaults)
        {
            _sessionName = sessionName;
        }

        public override void Subscribe(Action<PascaSessionSetting, PascaSessionSetting> settingChangedHandler)
        {
            Manager.Subscribe(CommonSettingName.Session, CreateSettingChangedHandler(settingChangedHandler));
        }

        private Action<Setting, Setting> CreateSettingChangedHandler(Action<PascaSessionSetting, PascaSessionSetting> handler)
        {
            return (oldSetting, newSetting) =>
            {
                var oldArraySetting = oldSetting as PascaSessionSetting;
                var newArraySetting = newSetting as PascaSessionSetting;
                if (oldArraySetting != null && newArraySetting != null &&
                    newArraySetting.SessionName().OrdinalEquals(_sessionName))
                {
                    handler(oldArraySetting, newArraySetting);
                }
            };
        }

        public override PascaSessionSetting GetSetting()
        {
            return Select(Manager.GetSetting<PascaSessionSetting>(CommonSettingName.Session, _sessionName));
        }

        public override bool IsEditable(Setting setting)
        {
            switch (setting.Name as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.SessionName:
                return false;
            default:
                return true;
            }
        }

        public override void Commit(Setting newSettings)
        {
            Manager.Update(newSettings);
        }

        private PascaSessionSetting Select(PascaSessionSetting sessionSetting)
        {
            return new PascaSessionSetting(ArraySetting.Update(
                CreateDefaultSetting(),
                new ArraySetting(
                    CommonSettingName.Session,
                    sessionSetting
                            .SubSettings
                            .Where(SessionSettingsSelector)
                            .ToList())));
        }

        private ArraySetting CreateDefaultSetting()
        {
            return new ArraySetting(
                CommonSettingName.Session,
                new List<Setting> {
                    new StringSetting(CommonSettingName.SessionName, string.Empty),
                    new AssessmentCriteriaSetting(CommonSettingName.SessionAssessmentCriteria, new AssessmentCriteria())
                });
        }

        private bool SessionSettingsSelector(Setting setting)
        {
            switch (setting.Name as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.SessionName:
            case CommonSettingName.SessionAssessmentCriteria:
                return true;
            default:
                return false;
            }
        }

        private readonly string _sessionName;
    }
}