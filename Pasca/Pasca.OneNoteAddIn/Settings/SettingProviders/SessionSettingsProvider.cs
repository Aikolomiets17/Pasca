﻿using System;
using System.Linq;
using Pasca.Common.Algorithm;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.Common.Settings;
using Pasca.Common.Settings.DefaultSettingsRegistry;
using Pasca.Common.Students;
using Pasca.Gui.Facade;
using Pasca.OneNoteAddIn.Model;

namespace Pasca.OneNoteAddIn.Settings.SettingProviders
{
    internal class SessionSettingsProvider : SettingProviderBase<PascaSessionSetting>
    {
        public SessionSettingsProvider(
            SettingsManager manager,
            IDefaultSettingsRegistry defaults,
            string sessionName,
            SelectableValueProvider<string>.SelectAction selectPageAction,
            Func<string, string> pageIdToName = null)
            : base(manager, defaults)
        {
            Assert.NotNull(sessionName);
            Assert.NotNull(selectPageAction);

            _sessionName = sessionName;
            _selectPageAction = selectPageAction;
            _pageIdToName = pageIdToName ?? (s => s);
        }

        public override void Subscribe(Action<PascaSessionSetting, PascaSessionSetting> settingChangedHandler)
        {
            Manager.Subscribe(CommonSettingName.Session, CreateSettingChangedHandler(settingChangedHandler));
        }

        public override PascaSessionSetting GetSetting()
        {
            return Select(Manager.GetSetting<PascaSessionSetting>(CommonSettingName.Session, _sessionName));
        }

        public override bool IsEditable(Setting setting)
        {
            switch (setting.Name as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.SessionName:
                return false;
            }

            return true;
        }

        public override ISelectableValueProvider GetSelectableValueProvider(Setting setting)
        {
            switch (setting.Name as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.SessionAssignment:
                return new SelectableValueProvider<string>(SelectPageAction, _pageIdToName);
            }

            return base.GetSelectableValueProvider(setting);
        }

        private string SelectPageAction()
        {
            var result = _selectPageAction();
            return _pageIdToName(result) == null ? null : result;
        }

        private Action<Setting, Setting> CreateSettingChangedHandler(Action<PascaSessionSetting, PascaSessionSetting> handler)
        {
            return (oldSetting, newSetting) =>
            {
                var oldArraySetting = oldSetting as PascaSessionSetting;
                var newArraySetting = newSetting as PascaSessionSetting;
                if (oldArraySetting != null && newArraySetting != null &&
                    newArraySetting.SessionName().OrdinalEquals(_sessionName))
                {
                    handler(
                        Select(oldArraySetting),
                        Select(newArraySetting));
                }
            };
        }

        public override void Commit(Setting newSessionSettings)
        {
            var newSettings = newSessionSettings as PascaSessionSetting;
            Assert.NotNull(newSettings);

            //var oldSettings = GetSetting();
            var newAuthors = newSettings.SessionAuthors();
            var newReviewers = newSettings.SessionReviewers();
            newAuthors.Sort(PersonComparer.Instance);
            newReviewers.Sort(PersonComparer.Instance);

            //if (!(PersonListComparer.Instance.Equals(oldSettings.SessionAuthors(), newAuthors)
            //      && PersonListComparer.Instance.Equals(oldSettings.SessionReviewers(), newReviewers)))
            {
                var mapper = new Mapper(
                    newAuthors,
                    newReviewers,
                    newSettings.SessionReviewersRatio());

                newSettings[CommonSettingName.SessionMapping] = new IntMappingSetting(
                    CommonSettingName.SessionMapping,
                    mapper.Mapping);
                newSettings[CommonSettingName.SessionAuthors] = new AnonymizedPersonListSetting(
                    CommonSettingName.SessionAuthors,
                    mapper.Authors);
                newSettings[CommonSettingName.SessionReviewers] = new AnonymizedPersonListSetting(
                    CommonSettingName.SessionReviewers,
                    mapper.Reviewers);
            }
            //else if (oldSettings.Find(CommonSettingName.SessionMapping) == null &&
            //         newSettings.Find(CommonSettingName.SessionMapping) == null)
            //{
            //    var mapper = new Mapper(
            //        newAuthors,
            //        newReviewers,
            //        newSettings.SessionReviewersRatio());

            //    newSettings[CommonSettingName.SessionMapping] = new IntMappingSetting(
            //        CommonSettingName.SessionMapping,
            //        mapper.Mapping);
            //}

            Manager.Update(newSettings);
        }

        private PascaSessionSetting Select(PascaSessionSetting sessionSetting)
        {
            return new PascaSessionSetting(ArraySetting.Update(
                CreateDefaultSettings(),
                new ArraySetting(
                    CommonSettingName.Session,
                    sessionSetting
                            .SubSettings
                            .Where(SessionSettingsSelector)
                            .ToList())));
        }

        private PascaSessionSetting CreateDefaultSettings()
        {
            return new PascaSessionSetting(
                CommonSettingName.Session,
                new[] {
                    CommonSettingName.SessionName,
                    CommonSettingName.SessionAssignment,
                    CommonSettingName.SessionAuthors,
                    CommonSettingName.SessionReviewers,
                    CommonSettingName.SessionReviewersRatio
                }.Select(e => Defaults.GetDefault<Setting>(e)).ToList());
        }

        private bool SessionSettingsSelector(Setting setting)
        {
            switch (setting.Name as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.SessionName:
            case CommonSettingName.SessionAssignment:
            case CommonSettingName.SessionAuthors:
            case CommonSettingName.SessionReviewers:
            case CommonSettingName.SessionReviewersRatio:
                return true;
            }

            return false;
        }

        private readonly Func<string, string> _pageIdToName;

        private readonly SelectableValueProvider<string>.SelectAction _selectPageAction;

        private readonly string _sessionName;
    }
}