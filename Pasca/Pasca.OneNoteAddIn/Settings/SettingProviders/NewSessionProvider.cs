﻿using System;
using System.Linq;
using Pasca.Common.Algorithm;
using Pasca.Common.Exceptions;
using Pasca.Common.Settings;
using Pasca.Common.Settings.DefaultSettingsRegistry;

namespace Pasca.OneNoteAddIn.Settings.SettingProviders
{
    internal class NewSessionProvider : SettingProviderBase<PascaSessionSetting>
    {
        public NewSessionProvider(SettingsManager manager, IDefaultSettingsRegistry defaults)
            : base(manager, defaults) {}

        public override void Subscribe(Action<PascaSessionSetting, PascaSessionSetting> settingChangedHandler)
        {
            // not supported
        }

        public override PascaSessionSetting GetSetting()
        {
            return new PascaSessionSetting(
                CommonSettingName.Session,
                new[] {
                    CommonSettingName.SessionName,
                    CommonSettingName.SessionAuthors,
                    CommonSettingName.SessionReviewers,
                    CommonSettingName.SessionReviewersRatio,
                    CommonSettingName.SessionSubmissionBegin,
                    CommonSettingName.SessionSubmissionEnd,
                    CommonSettingName.SessionReviewBegin,
                    CommonSettingName.SessionReviewEnd
                }.Select(e => Defaults.GetDefault<Setting>(e)).ToList()
            );
        }

        public override bool IsEditable(Setting setting)
        {
            return true;
        }

        public override void Commit(Setting newSessionSettings)
        {
            var newSettings = newSessionSettings as PascaSessionSetting;
            Assert.NotNull(newSessionSettings);

            var mapper = new Mapper(
                newSettings.SessionAuthors(),
                newSettings.SessionReviewers(),
                newSettings.SessionReviewersRatio());

            newSettings[CommonSettingName.SessionMapping] = new IntMappingSetting(
                CommonSettingName.SessionMapping,
                mapper.Mapping);
            newSettings[CommonSettingName.SessionAuthors] = new AnonymizedPersonListSetting(
                CommonSettingName.SessionAuthors,
                mapper.Authors);
            newSettings[CommonSettingName.SessionReviewers] = new AnonymizedPersonListSetting(
                CommonSettingName.SessionReviewers,
                mapper.Reviewers);

            Manager.Update(newSettings);
        }
    }
}