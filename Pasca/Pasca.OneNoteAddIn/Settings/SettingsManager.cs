using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Settings;
using Pasca.Common.Settings.DefaultSettingsRegistry;
using Pasca.Common.Students;
using Pasca.Common.Trace;
using Pasca.OneNoteAddIn.Extensions;
using Pasca.OneNoteAddIn.Serialization;
using Pasca.OneNoteAddIn.Settings.SettingProviders;

namespace Pasca.OneNoteAddIn.Settings
{
    public class SettingsManager : ISettingsManager
    {
        public SettingsManager(
            Func<IOneNoteSession> getSession,
            SettingsChangedSubscription settingsChangedSubscription)
        {
            _getSession = getSession;

            settingsChangedSubscription.SettingsChanged += OnSettingsChanged;
        }

        private IOneNoteSession OneNoteSession => _getSession();

        public string CurrentSessionName => GetSetting<StringSetting>(PascaSettingName.CurrentSessionName).Value;
        public PascaSessionSetting CurrentSession => GetSetting<PascaSessionSetting>(CommonSettingName.Session, CurrentSessionName);

        private ITracer Tracer => _tracer.Value;

        public event Action SessionsChanged = () => { };

        public void Subscribe(Enum name, Action<Setting, Setting> settingChangedHandler)
        {
            IList<Action<Setting, Setting>> handlers;
            if (!_settingChangedHandlers.TryGetValue(name, out handlers))
            {
                handlers = _settingChangedHandlers[name] = new List<Action<Setting, Setting>>();
            }

            handlers.Add(settingChangedHandler);
        }

        private void OnSettingsChanged(Setting oldSettings, Setting newSettings)
        {
            // todo: invoke every required handler
        }

        public T GetSetting<T>(Enum settingName, object param = null) where T : Setting
        {
            switch (settingName as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.Students:
                throw new InvalidOperationException($"GetSetting: {CommonSettingName.Students} should be requested async");
            case CommonSettingName.Sessions:
                Assert.Inherits(typeof(SessionListSetting), typeof(T));
                return new SessionListSetting(
                           CommonSettingName.Sessions,
                           OneNoteSession.GetSessionsList().Select(CreateSession).ToList()) as T;
            case CommonSettingName.Session:
                Assert.Inherits(typeof(PascaSessionSetting), typeof(T));
                Assert.NotNull(param);
                // ReSharper disable once PossibleNullReferenceException
                Assert.Type(typeof(string), param.GetType());
                return LoadSessionSettings((string)param) as T;
            }
            switch (settingName as PascaSettingName? ?? PascaSettingName.Invalid)
            {
            case PascaSettingName.OneNote:
                Assert.Inherits(typeof(OneNoteSetting), typeof(T));
                return (T)GetOneNoteSettings().Clone();
            case PascaSettingName.CurrentSessionName:
                Assert.Inherits(typeof(StringSetting), typeof(T));
                return GetOneNoteSettings().FindOrDefault<StringSetting>(PascaSettingName.CurrentSessionName) as T;
            }

            throw new ArgumentException($"SettingsManager cannot get setting {settingName}", nameof(settingName));
        }

        public async Task<T> GetSettingAsync<T>(Enum settingName, object param = null) where T : Setting
        {
            switch (settingName as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.Students:
                Assert.Inherits(typeof(PersonListSetting), typeof(T));
                return
                        new PersonListSetting(
                            CommonSettingName.Students,
                            (await OneNoteSession.GetStudentsList()).Select(CreatePerson).ToList()) as T;
            case CommonSettingName.Teachers:
                Assert.Inherits(typeof(PersonListSetting), typeof(T));
                return
                        new PersonListSetting(
                            CommonSettingName.Teachers,
                            (await OneNoteSession.GetTeachersList()).Select(CreatePerson).ToList()) as T;
            }

            return GetSetting<T>(settingName, param);
        }

        private OneNoteSetting GetOneNoteSettings()
        {
            var current = AppSettings.Default.SerializedValue;

            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.Write(current);
                sw.Flush();

                ms.Seek(0, SeekOrigin.Begin);
                using (var sr = new StreamReader(ms))
                {
                    try
                    {
                        return DR.Get<ISerializer<OneNoteSetting>>().Deserialize(sr);
                    }
                    catch
                    {
                        UpdateOneNoteSettings(
                            new OneNoteSettingsProvider(this, DR.Get<IDefaultSettingsRegistry>())
                            .CreateDefaultSettings());
                    }
                }
                GC.KeepAlive(sw);
            }

            // return default
            return new OneNoteSetting(PascaSettingName.OneNote, new List<Setting>());
        }

        private Session CreateSession(string name)
        {
            return new Session {
                SessionName = name
            };
        }

        private Person CreatePerson(PersonBuilder personBuilder)
        {
            return personBuilder.ToPerson();
        }

        private PascaSessionSetting LoadSessionSettings(string sessionName)
        {
            return OneNoteSession.GetSettings(sessionName);
        }

        private void UpdateSessionSettings(PascaSessionSetting settings)
        {
            var sessionName = settings.SessionName();
            var oldSettings = LoadSessionSettings(sessionName);
            var newSettings = new PascaSessionSetting(ArraySetting.Update(oldSettings, settings));

            var session = OneNoteSession;
            session.UpdateOrCreateSession(newSettings).Wait();
            SessionsChanged();

            // update session timing
            //var submissionBegin = newSettings[CommonSettings.SessionSubmissio÷nBegin];
            //var submissionEnd = newSettings[CommonSettings.SessionSubmissionEnd];
            //var reviewBegin = newSettings[CommonSettings.SessionReviewBegin];
            //var reviewEnd = newSettings[CommonSettings.SessionReviewEnd];
            //if (submissionBegin != null && submissionBegin != oldSettings[CommonSettings.SessionSubmissionBegin])
            //{
            //    session.
            //}
        }

        private void UpdateOneNoteSettings(OneNoteSetting settings)
        {
            AppSettings.Default.SerializedValue = DR.Get<ISerializer<OneNoteSetting>>().Serialize(settings);
            AppSettings.Default.Save();
        }

        public void Update(Setting setting)
        {
            Assert.NotNull(setting);

            switch (setting.Name as CommonSettingName? ?? CommonSettingName.Invalid)
            {
            case CommonSettingName.Session:
                Assert.Inherits(setting.GetType(), typeof(PascaSessionSetting));
                UpdateSessionSettings((PascaSessionSetting)setting);
                return;
            }
            switch (setting.Name as PascaSettingName? ?? PascaSettingName.Invalid)
            {
            case PascaSettingName.OneNote:
                Assert.Inherits(setting.GetType(), typeof(OneNoteSetting));
                UpdateOneNoteSettings((OneNoteSetting)setting);
                return;
            case PascaSettingName.CurrentSessionName:
                Assert.Inherits(setting.GetType(), typeof(StringSetting));
                UpdateOneNoteSettings(
                    new OneNoteSetting(
                    ArraySetting.Update(
                        GetOneNoteSettings(),
                        new ArraySetting(
                            PascaSettingName.OneNote,
                            new List<Setting> {
                                setting
                            }))));
                SessionsChanged();
                return;
            }
            Tracer.WriteLine(CommonTracerTags.SettingManager, $"Update: incompatible settingName={setting.Name}");
        }

        private readonly Func<IOneNoteSession> _getSession;

        private readonly Dictionary<Enum, IList<Action<Setting, Setting>>> _settingChangedHandlers =
                new Dictionary<Enum, IList<Action<Setting, Setting>>>();

        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();
    }
}