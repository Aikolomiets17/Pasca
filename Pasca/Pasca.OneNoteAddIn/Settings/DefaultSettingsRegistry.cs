﻿using System;
using Pasca.Common.Exceptions;
using Pasca.Common.Settings;

#pragma warning disable 1522

namespace Pasca.OneNoteAddIn.Settings
{
    internal sealed class DefaultSettingsRegistry : Common.Settings.DefaultSettingsRegistry.DefaultSettingsRegistry
    {
        public override T GetDefault<T>(Enum setting)
        {
            var settingName = setting as CommonSettingName?;
            switch (settingName)
            {
            case CommonSettingName.SessionSubmissionBegin:
                Assert.Inherits(typeof(DateTimeSetting), typeof(T));
                return new DateTimeSetting(CommonSettingName.SessionSubmissionBegin, DateTime.Today) as T;
            case CommonSettingName.SessionSubmissionEnd:
                Assert.Inherits(typeof(DateTimeSetting), typeof(T));
                return new DateTimeSetting(CommonSettingName.SessionSubmissionEnd, DateTime.Today.AddDays(7)) as T;
            case CommonSettingName.SessionReviewBegin:
                Assert.Inherits(typeof(DateTimeSetting), typeof(T));
                return new DateTimeSetting(CommonSettingName.SessionReviewBegin, DateTime.Today.AddDays(8)) as T;
            case CommonSettingName.SessionReviewEnd:
                Assert.Inherits(typeof(DateTimeSetting), typeof(T));
                return new DateTimeSetting(CommonSettingName.SessionReviewEnd, DateTime.Today.AddDays(15)) as T;
            }

            T pascaSettingVal;
            if (TryGetDefaultValueBasedOnDescAttribute<T, PascaSettingName>(setting, out pascaSettingVal))
            {
                return pascaSettingVal;
            }

            return base.GetDefault<T>(settingName);
        }

        public T GetDefault<T>(PascaSettingName settingName)
            where T : Setting
        {
            return GetDefault<T>((Enum)settingName);
        }
    }
}