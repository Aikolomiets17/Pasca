﻿using System;
using System.Collections.Generic;
using Pasca.Common.Exceptions;
using Pasca.Common.Settings;

namespace Pasca.OneNoteAddIn.Settings
{
    public sealed class OneNoteSetting : ArraySetting
    {
        public OneNoteSetting(Enum name) : base(name)
        {
            Assert.AreEqual(name, PascaSettingName.OneNote);
        }

        public OneNoteSetting(Enum name, List<Setting> values) : base(name, values)
        {
            Assert.AreEqual(name, PascaSettingName.OneNote);
        }

        public OneNoteSetting(ArraySetting @base)
            : base(@base.Name, @base.SubSettings)
        {
            Assert.AreEqual(@base.Name, PascaSettingName.OneNote);
        }

        private OneNoteSetting()
            :base(PascaSettingName.OneNote)
        {
        }

        public string SessionTeacherOnlySectionGroupPostfix()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.TeacherOnlySectionGroupPostfix).Value;
        }

        public string SettingsPagePostfix()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.SettingsPagePostfix).Value;
        }

        public string CurrentSessionName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.CurrentSessionName).Value;
        }

        public string PascaSectionName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.PascaSectionName).Value;
        }

        public string DashboardSectionName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.DashboardSectionName).Value;
        }

        [Obsolete]
        public string ContentLibrarySectionGroupPostfix()
        {
            //return FindOrDefault<StringSetting>(CommonSettings.SessionContentLibrarySectionGroupPostfix).Value;
            throw new NotSupportedException("Content Library is not used in this version of Pasca");
        }

        public override Setting Clone()
        {
            return new OneNoteSetting(Name, Clone(SubSettings));
        }
    }
}
