﻿using Pasca.Common.Settings;

#pragma warning disable CS3016 // Arrays as attribute arguments is not CLS-compliant

namespace Pasca.OneNoteAddIn.Settings
{
    // todo: extension which provides<T> default values
    internal enum PascaSettingName
    {
        Invalid = 0,

        #region General

        [Desc(typeof(ArraySetting))]
        OneNote,

        #endregion

        #region OneNote

        [Desc(typeof(StringSetting), "_Teacher Only")]
        TeacherOnlySectionGroupPostfix,
        [Desc(typeof(StringSetting), "_settings")]
        SettingsPagePostfix,
        [Desc(typeof(StringSetting), "Pasca")]
        PascaSectionName,
        [Desc(typeof(StringSetting), "Pasca Dashboard")]
        DashboardSectionName,
        [Desc(typeof(StringSetting))]
        CurrentSessionName,

        #endregion

        #region Session
        [Desc(typeof(StringSetting), "Assignment")]
        SessionAssignmentSectionName,
        [Desc(typeof(StringSetting), "Artifacts")]
        SessionArtifactSectionName,
        [Desc(typeof(StringSetting), "Reviews")]
        SessionReviewArtifactsSectionGroupName,
        [Desc(typeof(StringSetting), "Review Template")]
        SessionReviewTemplatePageName,
        [Desc(typeof(StringSetting), "Artifacts Report")]
        SessionArtifactsReportPageName,
        [Desc(typeof(StringSetting), "Authors Reviews Report")]
        SessionAuthorsReviewsReportPageName,
        [Desc(typeof(StringSetting), "Reviewers Reviews Report")]
        SessionReviewersReviewsReportPageName,
        [Desc(typeof(StringSetting), "Grades Report")]
        SessionGradesReportPageName,

        #endregion

    }
}