﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Pasca.Common.Providers;
using Pasca.Common.Settings;
using Pasca.Common.Students;

namespace Pasca.OneNoteAddIn.Settings
{
    public class AvailablePersonProvider : IAvailableStudentsProvider, IAvailableTeachersProvider
    {
        public AvailablePersonProvider(SettingsManager manager)
        {
            _manager = manager;
        }

        public async Task<IEnumerable<Person>> GetAvailableStudents()
        {
            return _studentsCache
                   ?? (_studentsCache = (await _manager.GetSettingAsync<PersonListSetting>(CommonSettingName.Students)).Value);
        }

        public async Task<IEnumerable<Person>> GetAvailableTeachers()
        {
            return _teachersCache
                   ?? (_teachersCache = (await _manager.GetSettingAsync<PersonListSetting>(CommonSettingName.Teachers)).Value);
        }

        public void Reset()
        {
            _studentsCache = null;
            _teachersCache = null;
        }

        private readonly SettingsManager _manager;

        private IEnumerable<Person> _studentsCache;
        private IEnumerable<Person> _teachersCache;
    }
}