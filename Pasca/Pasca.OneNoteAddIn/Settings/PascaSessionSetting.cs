﻿using System;
using System.Collections.Generic;
using Pasca.Common.Settings;

namespace Pasca.OneNoteAddIn.Settings
{
    public sealed class PascaSessionSetting : SessionSettings
    {
        public PascaSessionSetting(Enum name)
            : base(name) { }

        public PascaSessionSetting(Enum name, List<Setting> values)
            : base(name, values) { }

        public PascaSessionSetting(ArraySetting @base)
            : base(@base.Name, @base.SubSettings) { }

        private PascaSessionSetting() { }
        
        public string SessionReviewArtifactsSectionGroupName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.SessionReviewArtifactsSectionGroupName).Value;
        }

        public string SessionReviewTemplatePageName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.SessionReviewTemplatePageName).Value;
        }

        public string SessionArtifactsReportPageName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.SessionArtifactsReportPageName).Value;
        }

        public string SessionAuthorsReviewsReportPageName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.SessionAuthorsReviewsReportPageName).Value;
        }

        public string SessionReviewersReviewsReportPageName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.SessionReviewersReviewsReportPageName).Value;
        }

        public string SessionGradesReportPageName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.SessionGradesReportPageName).Value;
        }

        public string SessionArtifactSectionName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.SessionArtifactSectionName).Value;
        }

        public string SessionAssignmentSectionName()
        {
            return FindOrDefault<StringSetting>(PascaSettingName.SessionAssignmentSectionName).Value;
        }

        public override Setting Clone()
        {
            return new PascaSessionSetting(Name, Clone(SubSettings));
        }
    }
}
