﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Office.Interop.OneNote;
using Pasca.Common.Students;
using Pasca.Common.Types;
using Pasca.OneNoteAddIn.ReviewValidation;
using Pasca.OneNoteAddIn.Settings;

namespace Pasca.OneNoteAddIn
{
    public interface IOneNoteSession
    {
        string GetNotebookId();
        PascaSessionSetting GetSettings(string sessionName);
        Task UpdateOrCreateSession(PascaSessionSetting session);
        Task DeleteSession(string sessionName);
        Task<IEnumerable<PersonBuilder>> GetStudentsList();
        Task<IEnumerable<PersonBuilder>> GetTeachersList();
        IEnumerable<string> GetSessionsList();
        Task UpdateDashboards(PascaSessionSetting session);

        //todo: remove assignmentPage from signature, use session settings. also remove receiverSectionGroups
        Task<IEnumerable<string>> DistributeAssignment(
            IEnumerable<string> assignmentPage,
            PascaSessionSetting session);

        Task CollectArtifacts(PascaSessionSetting session);
        string GetCurrentPageId();
        string GetElementName(string id, HierarchyElement element = HierarchyElement.heNone);

        // todo: remove templatePage from signature, use session settings.
        void DistributeArtifactsAndTemplates(PascaSessionSetting session);
        Task CollectReviews(PascaSessionSetting session);
        ReviewValidationVerdictArgs CheckReviewPageValid(PascaSessionSetting session, string reviewPage);

        Task LockArtifacts(PascaSessionSetting session);
        Task<bool> AreArtifactsLocked(PascaSessionSetting session);
        Task LockReviews(PascaSessionSetting session);
        Task<bool> AreReviewsLocked(PascaSessionSetting session);

        void NavigateToSessionSectionGroup(PascaSessionSetting session);
        void PlaceReports(PascaSessionSetting session);
        double GetGrade(PascaSessionSetting session, AnonymizedPerson author);

        IEnumerable<ITreeItem<string>> GetHierarchy(GetHierarchyRootType rootType, bool includeDeleted = false);
        string CreateAssignmentSectionUrl(PascaSessionSetting session);
        string CreateArtifactsSectionUrl(PascaSessionSetting session);
    }
}