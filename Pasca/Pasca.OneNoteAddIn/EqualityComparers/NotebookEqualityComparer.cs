﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Pasca.Common.Extensions;
using Pasca.Common.Types;

namespace Pasca.OneNoteAddIn.EqualityComparers
{
    [SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
    public class NotebookEqualityComparer : IEqualityComparer<Notebook>
    {
        protected NotebookEqualityComparer() {}

        public static NotebookEqualityComparer Instance => Singleton.Instance;

        public bool Equals(Notebook x, Notebook y)
        {
            return StringExtensions.OrdinalEquals(x?.ID, y?.ID);
        }

        public int GetHashCode(Notebook obj)
        {
            return obj?.ID?.GetHashCode() ?? 0;
        }

        private static readonly SingletonComponent<NotebookEqualityComparer> Singleton =
                new SingletonComponent<NotebookEqualityComparer>(() => new NotebookEqualityComparer());
    }
}