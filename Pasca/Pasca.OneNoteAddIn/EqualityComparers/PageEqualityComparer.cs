﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Pasca.Common.Extensions;
using Pasca.Common.Types;

namespace Pasca.OneNoteAddIn.EqualityComparers
{
    [SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
    public class PageEqualityComparer : IEqualityComparer<Page>
    {
        protected PageEqualityComparer() {}
        public static PageEqualityComparer Instance => Singleton.Instance;

        public bool Equals(Page x, Page y)
        {
            return StringExtensions.OrdinalEquals(x?.ID, y?.ID);
        }

        public int GetHashCode(Page obj)
        {
            return obj?.ID?.GetHashCode() ?? 0;
        }

        private static readonly SingletonComponent<PageEqualityComparer> Singleton =
                new SingletonComponent<PageEqualityComparer>(() => new PageEqualityComparer());
    }
}