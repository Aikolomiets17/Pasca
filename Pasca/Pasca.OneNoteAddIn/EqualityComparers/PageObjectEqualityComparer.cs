﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Pasca.Common.Extensions;
using Pasca.Common.Types;

namespace Pasca.OneNoteAddIn.EqualityComparers
{
    [SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
    public class PageObjectEqualityComparer : IEqualityComparer<PageObject>
    {
        protected PageObjectEqualityComparer() {}
        public static PageObjectEqualityComparer Instance => Singleton.Instance;

        public bool Equals(PageObject x, PageObject y)
        {
            return StringExtensions.OrdinalEquals(x?.objectID, y?.objectID);
        }

        public int GetHashCode(PageObject obj)
        {
            return obj?.objectID?.GetHashCode() ?? 0;
        }

        private static readonly SingletonComponent<PageObjectEqualityComparer> Singleton =
                new SingletonComponent<PageObjectEqualityComparer>(() => new PageObjectEqualityComparer());
    }
}