﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Pasca.Common.Extensions;
using Pasca.Common.Types;

namespace Pasca.OneNoteAddIn.EqualityComparers
{
    [SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
    public class SectionEqualityComparer : IEqualityComparer<Section>
    {
        protected SectionEqualityComparer() {}
        public static SectionEqualityComparer Instance => Singleton.Instance;

        public bool Equals(Section x, Section y)
        {
            return StringExtensions.OrdinalEquals(x?.ID, y?.ID);
        }

        public int GetHashCode(Section obj)
        {
            return obj?.ID?.GetHashCode() ?? 0;
        }

        private static readonly SingletonComponent<SectionEqualityComparer> Singleton =
                new SingletonComponent<SectionEqualityComparer>(() => new SectionEqualityComparer());
    }
}