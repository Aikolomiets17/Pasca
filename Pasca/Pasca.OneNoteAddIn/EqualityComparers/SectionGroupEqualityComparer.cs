﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Pasca.Common.Extensions;
using Pasca.Common.Types;

namespace Pasca.OneNoteAddIn.EqualityComparers
{
    [SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
    public class SectionGroupEqualityComparer : IEqualityComparer<SectionGroup>
    {
        protected SectionGroupEqualityComparer() {}
        public static SectionGroupEqualityComparer Instance => Singleton.Instance;

        public bool Equals(SectionGroup x, SectionGroup y)
        {
            return StringExtensions.OrdinalEquals(x?.ID, y?.ID);
        }

        public int GetHashCode(SectionGroup obj)
        {
            return obj?.ID?.GetHashCode() ?? 0;
        }

        private static readonly SingletonComponent<SectionGroupEqualityComparer> Singleton =
                new SingletonComponent<SectionGroupEqualityComparer>(() => new SectionGroupEqualityComparer());
    }
}