﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Pasca.Common.Localization;
using Pasca.OneNoteAddIn.Properties;

namespace Pasca.OneNoteAddIn.Localization
{
    public class RibbonProvider : IRibbonProvider
    {
        public RibbonProvider(ILoc loc)
        {
            _loc = loc;
            _ribbon = new Lazy<string>(CreateRibbon);
        }

        public string GetRibbon()
        {
            return _ribbon.Value;
        }

        private string CreateRibbon()
        {
            var regex = new Regex(@"(label=""|screentip="")([a-zA-Z ]+)("")");
            var s = regex.Replace(Resources.ribbon, Replace);
            return s;
        }

        private string Replace(Match match)
        {
            return string.Join(
                string.Empty,
                match
                        .Groups
                        .OfType<Group>()
                        .Skip(1)
                        .Select(
                            (g, i) => i == 1 // Skip!
                                          ? _loc.GetLocalizedString(LocTags.Ribbon, g.Value)
                                          : g.Value));
        }

        private readonly ILoc _loc;
        private readonly Lazy<string> _ribbon;
    }
}