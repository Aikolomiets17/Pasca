﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Pasca.Common.DI;
using Pasca.Common.Localization;
using Pasca.OneNoteAddIn.Properties;

namespace Pasca.OneNoteAddIn.Localization
{
    public class OneNoteTemplateProvider
    {
        public string ReviewTemplate => _reviewTemplate.Value;
        public string Row2 => _row2.Value;
        public string Row3 => _row3.Value;
        public string Row4 => _row4.Value;
        public string Row5 => _row5.Value;
        public string CriteriaGroupHeaderRow => _criteriaGroupHeaderRow.Value;
        public string ShorDashboard => _shortDashboard.Value;
        public string Dashboard => _dashboard.Value;
        public string ArtifactsReport => _artifactsReport.Value;
        public string AuthorsReviewsReport => _authorsReviewsReport.Value;
        public string ReviewersReviewsReport => _reviewersReviewsReport.Value;
        public string GradesReport => _gradesReport.Value;

        // todo: tick and cross
        public string Yes
            =>
                    DR.Get<ILoc>()
                      .GetLocalizedString(
                          Common.Localization.LocTags.Common.Tag,
                          Common.Localization.LocTags.Common.Yes);

        public string No
            =>
                    DR.Get<ILoc>()
                      .GetLocalizedString(
                          Common.Localization.LocTags.Common.Tag,
                          Common.Localization.LocTags.Common.No);

        public string ShadingTemplate => "shadingColor=\"{0}\"";

        private static string Loc(string txt)
        {
            return Regex.Value.Replace(txt, Replace);
        }

        private static string Replace(Match match)
        {
            return string.Join(
                string.Empty,
                match
                        .Groups
                        .OfType<Group>()
                        .Skip(1)
                        .Select(
                            (g, i) => DR.Get<ILoc>().GetLocalizedString(LocTags.Xml, g.Value)));
        }

        private readonly Lazy<string> _artifactsReport = new Lazy<string>(
            () => Loc(Resources.ArtifactsReport));

        private readonly Lazy<string> _authorsReviewsReport = new Lazy<string>(
            () => Loc(Resources.AuthorsReviewsReport));

        private readonly Lazy<string> _criteriaGroupHeaderRow = new Lazy<string>(
            () => Loc(Resources.CriteriaGroupHeaderRow));

        private readonly Lazy<string> _shortDashboard = new Lazy<string>(
            () => Loc(Resources.ShortDashboard));

        private readonly Lazy<string> _dashboard = new Lazy<string>(
            () => Loc(Resources.Dashboard));

        private readonly Lazy<string> _gradesReport = new Lazy<string>(
            () => Loc(Resources.GradesReport));

        private readonly Lazy<string> _reviewersReviewsReport = new Lazy<string>(
            () => Loc(Resources.ReviewersReviewsReport));

        private readonly Lazy<string> _reviewTemplate = new Lazy<string>(
            () => Loc(Resources.ReviewTemplate));

        private readonly Lazy<string> _row2 = new Lazy<string>(
            () => Loc(Resources.Row2));

        private readonly Lazy<string> _row3 = new Lazy<string>(
            () => Loc(Resources.Row3));

        private readonly Lazy<string> _row4 = new Lazy<string>(
            () => Loc(Resources.Row4));

        private readonly Lazy<string> _row5 = new Lazy<string>(
            () => Loc(Resources.Row5));

        private static readonly Lazy<Regex> Regex = new Lazy<Regex>(
            () => new Regex("{{([a-zA-Z ]+)}}"));
    }
}