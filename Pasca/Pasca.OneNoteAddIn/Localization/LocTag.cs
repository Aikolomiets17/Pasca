﻿namespace Pasca.OneNoteAddIn.Localization
{
    public class LocTags : Common.Localization.LocTags
    {
        #region Ribbon

        public const string Ribbon = nameof(Ribbon);

        #endregion

        #region Xml

        public const string Xml = nameof(Xml);

        #endregion

        #region Progress

        public const string Progress = nameof(Progress);

        // Default
        public const string ReadyLabel = nameof(ReadyLabel);
        public const string LoadingLabel = nameof(LoadingLabel);

        // OneNoteSession General

        public const string SyncingNotebookHierarchy = nameof(SyncingNotebookHierarchy);
        public const string Navigating = nameof(Navigating);

        // UpdateOrCreateSession
        public const string CreatingSessionSettingsPage = nameof(CreatingSessionSettingsPage);
        //"Creating session setting page"

        public const string CreatingStudentsSectionsViaWebApi = nameof(CreatingStudentsSectionsViaWebApi);
        // "Creating students sections in content library via web api"

        public const string ApiRequestedWaitingForSync = nameof(ApiRequestedWaitingForSync);
        //"Api requested, waiting for notebook synchronization"

        public const string CreatingArtifactsAndReviewsSections = nameof(CreatingArtifactsAndReviewsSections);
        //"Creating artifacts and reviews sections"

        // UpdateDashboards
        public const string PlacingDashboards = nameof(PlacingDashboards);

        // DistributeAssignment
        public const string CopyingAssignments = nameof(CopyingAssignments);
        public const string PlacingAssignments = nameof(PlacingAssignments);

        // CollectingArtifacts
        public const string CollectingArtifacts = nameof(CollectingArtifacts);

        // DistributeArtifactsAndTemplates
        public const string PlacingArtifactsAndTemplates = nameof(PlacingArtifactsAndTemplates);

        // CollectingReviews
        public const string CollectingReviews = nameof(CollectingReviews);

        // DeleteSession

        public const string DeletingArtifactsInTeacherOnly = nameof(DeletingArtifactsInTeacherOnly);
        public const string DeletingArtifactsInContentLibrary = nameof(DeletingArtifactsInContentLibrary);
        public const string DeletingSessionSettings = nameof(DeletingSessionSettings);

        // PlaceReports

        public const string PlacingArtifactsReport = nameof(PlacingArtifactsReport);
        public const string PlacingAuthorsReviewsReport = nameof(PlacingAuthorsReviewsReport);
        public const string PlacingReviewersReviewsReport = nameof(PlacingReviewersReviewsReport);
        public const string PlacingGradesReport = nameof(PlacingGradesReport);

        // CheckReviewPageValid
        public const string ValidatingReview = nameof(ValidatingReview);

        // LocArtifacts

        public const string LockingArtifactsViaWebApi = nameof(LockingArtifactsViaWebApi);

        // LockReviews

        public const string LockingReviewsViaWebApi = nameof(LockingReviewsViaWebApi);

        // ApiFacade General

        public const string RequestingWebApi = nameof(RequestingWebApi);

        // CreateStudentsDashboardSections

        public const string CreatingDashboardSectionsViaWebApi = nameof(CreatingDashboardSectionsViaWebApi);

        // CreateStudentsDashboardSections

        public const string CreatingAssignmentSectionsViaWebApi = nameof(CreatingAssignmentSectionsViaWebApi);

        #endregion

        #region Results

        public const string Results = nameof(Results);
        public const string Ok = nameof(Ok);
        public const string TimeOut = nameof(TimeOut);
        public const string GradesProviderError = nameof(GradesProviderError);

        #endregion

        #region MessageBox

        public const string MessageBox = nameof(MessageBox);
        
        public const string PascaBusy = nameof(PascaBusy);
        public const string CouldNotLogout = nameof(CouldNotLogout);
        
        public const string AssignmentSectionNotFound = nameof(AssignmentSectionNotFound);
        public const string ArtifactsSectionNotFound = nameof(ArtifactsSectionNotFound);

        public const string DeleteAllSessions = nameof(DeleteAllSessions);

        public const string ReviewIsValid = nameof(ReviewIsValid);
        public const string ReviewPageNotFound = nameof(ReviewPageNotFound);
        public const string ReviewTableNotFound = nameof(ReviewTableNotFound);
        public const string UnknownTableError = nameof(UnknownTableError);
        public const string TableError = nameof(TableError);
        public const string UnknwonReviewValidationError = nameof(UnknwonReviewValidationError);

        public const string SubmissionDeadlineNotPassedYetLockArtifacts =
                nameof(SubmissionDeadlineNotPassedYetLockArtifacts);

        public const string ReviewDeadlineNotPassedYetLockReviews = nameof(ReviewDeadlineNotPassedYetLockReviews);

        public const string SubmissionDeadlineNotPassedYetCollectArtifacts =
                nameof(SubmissionDeadlineNotPassedYetCollectArtifacts);

        public const string ReviewDeadlineNotPassedYetCollectReviews = nameof(ReviewDeadlineNotPassedYetCollectReviews);
        public const string ArtifactsNotLockedYetCollectArtifacts = nameof(ArtifactsNotLockedYetCollectArtifacts);
        public const string ReviewsNotLockedYetCollectReviews = nameof(ReviewsNotLockedYetCollectReviews);
        
        public const string FewAuthors = nameof(FewAuthors);
        public const string FewReviewers = nameof(FewReviewers);

        #endregion
    }
}