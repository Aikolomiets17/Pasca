﻿namespace Pasca.OneNoteAddIn.Localization
{
    public interface IRibbonProvider
    {
        string GetRibbon();
    }
}