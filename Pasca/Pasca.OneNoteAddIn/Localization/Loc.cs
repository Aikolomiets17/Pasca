﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Pasca.Common.DI;
using Pasca.Common.Localization;
using Pasca.Common.Trace;
using Pasca.Common.Types;
using Pasca.OneNoteAddIn.Properties;

namespace Pasca.OneNoteAddIn.Localization
{
    public class Loc : ILoc
    {
        static Loc()
        {
            //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ru-RU");
            Singleton = new SingletonComponent<Loc>(
            () => new Loc(Resources.Language));
        }

        protected Loc(string lang)
        {
            string[] paths;
            if (!_paths.TryGetValue(lang, out paths))
            {
                paths = _paths[LangDefault];
            }

            foreach (var path in paths)
            {
                LoadCache(path);
            }
        }

        public static Loc Instance => Singleton.Instance;

        private ITracer Tracer => _tracer.Value;

        public string GetLocalizedString(string tag, string value, params object[] parameters)
        {
            if (tag == null || value == null)
            {
                return GetLocalizedString(value);
            }

            return string.Format(GetLocalizedString($"{tag}{PrefixSeparator}{value}"), parameters);
        }

        public string GetLocalizedString(string value)
        {
            if (value == null)
            {
                return Null;
            }

            if (_cache.ContainsKey(value))
            {
                return _cache[value];
            }

            Tracer.WriteLine(CommonTracerTags.Loc, $"{value} not found in cache");
            return value;
        }

        private void LoadCache(string path)
        {
            using (var sr = new StringReader(path))
            {
                while (true)
                {
                    var line = sr.ReadLine();
                    if (line == null)
                    {
                        break;
                    }
                    try
                    {
                        if (string.IsNullOrWhiteSpace(line))
                        {
                            continue;
                        }

                        line = Escape(line.Trim());
                        if (line == null)
                        {
                            continue;
                        }

                        var split = line.Split(Separator);
                        _cache.Add(split[0], split[1]);
                    }
                    // ReSharper disable once UnusedVariable
                    catch (ArgumentException keyAlreadyExists)
                    {
                        Tracer.WriteLine(CommonTracerTags.Loc, "Key already exists in line:" + line);
                    }
                    catch
                    {
                        Tracer.WriteLine(CommonTracerTags.Loc, "Could not split line:" + line);
                    }
                }
            }
        }

        protected string Escape(string value)
        {
            if (string.IsNullOrWhiteSpace(value) || value[0] == CommentSeparator)
            {
                return null;
            }
            return value
                    .Replace(SeparatorMask, Separator.ToString())
                    .Replace(PrefixSeparatorMask, PrefixSeparator.ToString())
                    .Replace(CommentSeparatorMask, CommentSeparator.ToString())
                    .Replace(SpaceMask, SpaceChar);
        }

        private readonly Dictionary<string, string> _cache
                = new Dictionary<string, string>();

        private readonly Dictionary<string, string[]> _paths = new Dictionary<string, string[]> {
            {
                LangRu, new[] {
                    Resources.Loc_ru,
                    Resources.Loc_common_ru,
                }
            }, {
                LangEn, new[] {
                    Resources.Loc,
                    Resources.Loc_common,
                }
            }
        };

        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();

        public const char Separator = '=';
        public const string SeparatorMask = @"\=";
        public const char PrefixSeparator = ':';
        public const string PrefixSeparatorMask = @"\:";
        public const char CommentSeparator = ';';
        public const string CommentSeparatorMask = @"\;";
        public const string SpaceMask = @"\s";
        public const string SpaceChar = " ";

        public const string LangRu = "ru";
        public const string LangEn = "en";
        public const string LangDefault = LangEn;

        public const string Null = nameof(Null);

        private static readonly SingletonComponent<Loc> Singleton;
    }
}