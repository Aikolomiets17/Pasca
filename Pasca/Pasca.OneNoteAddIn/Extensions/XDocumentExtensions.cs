﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.Common.Types;

namespace Pasca.OneNoteAddIn.Extensions
{
    public static class XDocumentExtensions
    {
        public static XElement ToXElement(this string xml)
        {
            return XDocument.Parse(xml).Root;
        }

        public static string AttributeValue(this XElement element, string attribute)
        {
            return element.Attribute(attribute)?.Value;
        }

        public static string Id(this XElement element)
        {
            return element.AttributeValue("ID");
        }

        public static string Name(this XElement element)
        {
            return element.AttributeValue("name");
        }

        public static string LocalName(this XElement element)
        {
            return element.Name.LocalName;
        }

        public static bool IsDeleted(this XElement element)
        {
            return (element.AttributeValue("isInRecycleBin")?.OrdinalIgnoreCaseEquals("true") ?? false)
                   || (element.AttributeValue("isRecycleBin")?.OrdinalIgnoreCaseEquals("true") ?? false);
        }

        public static IEnumerable<XElement> WithValue(
            this IEnumerable<XElement> elements,
            string attribute,
            string value)
        {
            return elements.Where(q => q.AttributeValue(attribute)?.OrdinalEquals(value) ?? false);
        }

        public static IEnumerable<XElement> WithTagName(this IEnumerable<XElement> elements, string localName)
        {
            return elements.Where(q => q.LocalName().OrdinalEquals(localName));
        }

        public static IEnumerable<XElement> WithName(this IEnumerable<XElement> elements, string name)
        {
            return elements.Where(q => q.Name().OrdinalEquals(name));
        }

        public static IEnumerable<XElement> WithId(this IEnumerable<XElement> elements, string id)
        {
            return elements.Where(q => q.Id().OrdinalEquals(id));
        }

        public static XElement RemoveIds(this XElement element)
        {
            element.Attribute("objectID")?.Remove();
            element.Attribute("ID")?.Remove();
            foreach (var el in element.Descendants())
            {
                el.Attribute("objectID")?.Remove();
                el.Attribute("ID")?.Remove();
            }

            return element;
        }

        #region Table

        public static string GetCell(this XElement table, int x, int y)
        {
            return table
                    ?.Elements().WithTagName("Row").Skip(y).FirstOrDefault()
                    ?.Elements().WithTagName("Cell").Skip(x).FirstOrDefault()
                    ?.Elements().WithTagName("OEChildren").FirstOrDefault()
                    ?.Elements().WithTagName("OE").FirstOrDefault()
                    ?.Elements().WithTagName("T").FirstOrDefault()
                    ?.Value;
        }

        #endregion

        public static string CreatePageLink(
            this XElement notebook,
            string name,
            string pageId,
            string sectionId,
            params string[] path)
        {
            Assert.NotNull(pageId);
            return CreateLinkBase(notebook, name, pageId, sectionId, path);
        }

        public static string CreateSectionLink(
            this XElement notebook,
            string name,
            string sectionId,
            params string[] path)
        {
            return CreateLinkBase(notebook, name, null, sectionId, path);
        }

        public static string CreateOneNoteUrl(this XElement root)
        {
            var path = root.AttributeValue("path");
            if (path == null)
            {
                return null;
            }

            return "onenote:" + root.AttributeValue("path");
        }

        public static ITreeItem<string> ToTree(this XElement root, ITreeItem parent, bool includeDeleted)
        {
            var treeItem = new StringTreeItem(root.Id(), root.Name(), parent);
            foreach (var sectionGroup in root.SectionGroups())
            {
                if (!includeDeleted && sectionGroup.IsDeleted())
                {
                    continue;
                }
                treeItem.Children.Add(sectionGroup.ToTree(treeItem, includeDeleted));
            }
            foreach (var section in root.Sections(false))
            {
                if (!includeDeleted && section.IsDeleted())
                {
                    continue;
                }
                treeItem.Children.Add(section.ToTree(treeItem, includeDeleted));
            }
            foreach (var page in root.Pages())
            {
                if (!includeDeleted && page.IsDeleted())
                {
                    continue;
                }
                treeItem.Children.Add(page.ToTree(treeItem, includeDeleted));
            }

            return treeItem;
        }

        private static string CreateLinkBase(
            this XElement notebook,
            string name,
            string pageId,
            string sectionId,
            params string[] path)
        {
            Assert.NotNull(name);
            Assert.NotNull(sectionId);
            Assert.NotNull(path);
            Assert.True(path.Length >= 2);

            var href = new StringBuilder("onenote:");
            for (var i = 0; i < path.Length - 2; i++)
            {
                href.Append(path[i]);
                href.Append("\\");
            }
            if (pageId == null)
            {
                href
                        .Append(path[path.Length - 2])
                        .Append("\\")
                        .Append(path[path.Length - 1])
                        .Append(".one")
                        .Append("#section-id=")
                        .Append(sectionId)
                        .Append("&end&base-path=")
                        .Append(notebook.AttributeValue("path"));
            }
            else
            {
                href
                        .Append(path[path.Length - 2])
                        .Append(".one#")
                        .Append(path[path.Length - 1])
                        .Append("&page-id=")
                        .Append(pageId)
                        .Append("&section-id=")
                        .Append(sectionId)
                        .Append("&end&base-path=")
                        .Append(notebook.AttributeValue("path"));
            }
            return $"<a href=\"{href}\">{name}</a>";
        }

        #region GetParts

        public static XElement GetSectionGroup(this XContainer notebook, string sectionGroup, bool recursive = false)
        {
            return notebook.SectionGroups(recursive)
                           .WithName(sectionGroup)
                           .FirstOrDefault();
        }

        public static XElement GetSectionGroup(
            this XElement root,
            string sectionGroup1,
            string sectionGroup2,
            params string[] sectionGroups)
        {
            return GetSection(
                root,
                null,
                new[] {
                    sectionGroup1,
                    sectionGroup2
                }.Concat(sectionGroups).ToArray());
        }

        public static IEnumerable<XElement> SectionGroups(this XContainer notebook, bool recursive = false)
        {
            if (recursive)
            {
                return notebook
                        ?.Descendants()
                        .WithTagName("SectionGroup");
            }
            return notebook
                    ?.Elements()
                    .WithTagName("SectionGroup");
        }

        public static XElement GetSection(this XElement sectionGroup, string sectionName, bool recursive = false)
        {
            return sectionGroup
                    .Sections(recursive)
                    .WithName(sectionName)
                    .FirstOrDefault();
        }

        public static XElement GetSection(this XElement root, string sectionName = null, params string[] sectionGroups)
        {
            foreach (var sectionGroupName in sectionGroups)
            {
                root = root
                        ?.GetSectionGroup(sectionGroupName);
            }

            return sectionName == null
                       ? root
                       : root?.GetSection(sectionName);
        }

        public static IEnumerable<XElement> Sections(this XElement sectionGroup, bool recursive)
        {
            if (recursive)
            {
                return sectionGroup
                        ?.Descendants()
                        .WithTagName("Section");
            }
            return sectionGroup
                    ?.Elements()
                    .WithTagName("Section");
        }

        public static XElement GetPage(this XElement section, string pageName, bool recursive = false)
        {
            return GetPages(section, pageName, recursive)
                    .FirstOrDefault();
        }

        public static IEnumerable<XElement> GetPages(this XElement section, string pageName, bool recursive = false)
        {
            return section
                    ?.Pages(recursive)
                    .WithName(pageName);
        }

        public static IEnumerable<XElement> Pages(this XContainer section, bool recursive = false)
        {
            if (recursive)
            {
                return section
                        ?.Descendants()
                        .WithTagName("Page");
            }
            return section
                    ?.Elements()
                    .WithTagName("Page");
        }

        #endregion
    }
}