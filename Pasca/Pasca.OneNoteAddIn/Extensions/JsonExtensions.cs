﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pasca.Common.DI;
using Pasca.Common.Trace;

namespace Pasca.OneNoteAddIn.Extensions
{
    public static class JsonExtensions
    {
        public static dynamic ToDynamic(this string content)
        {
            DR.Get<ITracer>().WriteLine("ToDynamic", content);
            if (string.IsNullOrWhiteSpace(content))
            {
                return new object();
            }
            return JObject.Parse(content);
        }

        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}