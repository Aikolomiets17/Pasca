﻿using System.IO;
using Pasca.OneNoteAddIn.Serialization;

namespace Pasca.OneNoteAddIn.Extensions
{
    public static class SerializerExtensions
    {
        public static string Serialize<T>(this ISerializer<T> serializer, T o)
        {
            using (var sw = new StringWriter())
            {
                serializer.Serialize(sw, o);
                sw.Flush();
                return sw.ToString();
            }
        }

        public static T Deserialize<T>(this ISerializer<T> serializer, TextReader sr)
        {
            return serializer.Deserialize(sr);
        }

        public static T Deserialize<T>(this ISerializer<T> serializer, string s)
        {
            using (var sr = new StringReader(s))
            {
                return Deserialize(serializer, sr);
            }
        }
    }
}