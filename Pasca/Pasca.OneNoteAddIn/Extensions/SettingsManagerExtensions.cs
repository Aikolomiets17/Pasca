﻿using Pasca.Common.Settings;
using Pasca.OneNoteAddIn.Settings;

namespace Pasca.OneNoteAddIn.Extensions
{
    internal static class SettingsManagerExtensions
    {
        public static SessionListSetting GetSessions(this SettingsManager settingsManager)
        {
            return settingsManager.GetSetting<SessionListSetting>(CommonSettingName.Sessions);
        }
    }
}