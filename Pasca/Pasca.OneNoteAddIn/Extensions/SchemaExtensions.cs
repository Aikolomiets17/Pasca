﻿using System.Linq;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;

namespace Pasca.OneNoteAddIn.Extensions
{
    public static class SchemaExtensions
    {
        public static SectionGroup GetSectionGroup(this Notebook notebook, string sectionGroupName)
        {
            return notebook?.SectionGroup?.FirstOrDefault(q => q.name.OrdinalEquals(sectionGroupName));
        }

        public static SectionGroup GetSectionGroup(this SectionGroup sectionGroup, string sectionGroupName)
        {
            return sectionGroup?.SectionGroup1?.FirstOrDefault(q => q.name.OrdinalEquals(sectionGroupName));
        }

        public static Section GetSection(this Notebook notebook, string sectionName)
        {
            return notebook?.Section?.FirstOrDefault(q => q.name.OrdinalEquals(sectionName));
        }

        public static Section GetSection(this SectionGroup sectionGroup, string sectionName)
        {
            return sectionGroup?.Section?.FirstOrDefault(q => q.name.OrdinalEquals(sectionName));
        }

        public static Section GetSection(this Notebook notebook, string sectionName, params string[] sectionGroups)
        {
            Assert.NotNull(sectionName);

            SectionGroup root = null;
            foreach (var sectionGroup in sectionGroups)
            {
                root = root == null
                           ? notebook?.SectionGroup?.FirstOrDefault(sg => sg.name.OrdinalEquals(sectionGroup))
                           : root.SectionGroup1?.FirstOrDefault(sg => sg.name.OrdinalEquals(sectionGroup));
            }

            return root == null
                       ? notebook?.Section?.FirstOrDefault(s => s.name.OrdinalEquals(sectionName))
                       : root.Section?.FirstOrDefault(s => s.name.OrdinalEquals(sectionName));
        }

        public static SectionGroup GetSectionGroup(this Notebook notebook, params string[] sectionGroups)
        {
            SectionGroup root = null;
            foreach (var sectionGroup in sectionGroups)
            {
                root = root == null
                           ? notebook?.SectionGroup?.FirstOrDefault(sg => sg.name.OrdinalEquals(sectionGroup))
                           : root.SectionGroup1?.FirstOrDefault(sg => sg.name.OrdinalEquals(sectionGroup));
            }

            return root;
        }

        public static void AddSection(this Notebook notebook, params Section[] newSections)
        {
            if (notebook.Section == null)
            {
                notebook.Section = new Section[] {};
            }
            notebook.Section = notebook.Section?.Concat(newSections).ToArray();
        }

        public static void AddSectionGroup(this Notebook notebook, params SectionGroup[] newSectionGroups)
        {
            if (notebook.SectionGroup == null)
            {
                notebook.SectionGroup = new SectionGroup[] {};
            }
            notebook.SectionGroup = notebook.SectionGroup.Concat(newSectionGroups).ToArray();
        }

        public static void AddSectionGroup(this SectionGroup notebook, params SectionGroup[] newSectionGroups)
        {
            if (notebook.SectionGroup1 == null)
            {
                notebook.SectionGroup1 = new SectionGroup[] {};
            }
            notebook.SectionGroup1 = notebook.SectionGroup1.Concat(newSectionGroups).ToArray();
        }

        public static void AddSection(this SectionGroup sectionGroup, params Section[] newSections)
        {
            if (sectionGroup.Section == null)
            {
                sectionGroup.Section = new Section[] {};
            }
            sectionGroup.Section = sectionGroup.Section.Concat(newSections).ToArray();
        }

        public static void AddPage(this Section section, params Page[] newPages)
        {
            if (section.Page == null)
            {
                section.Page = new Page[] {};
            }
            section.Page = section.Page.Concat(newPages).ToArray();
        }
    }
}