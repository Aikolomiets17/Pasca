﻿using System.IO;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Office.Interop.OneNote;
using Pasca.Common.DI;
using Pasca.Common.Trace;
using Pasca.OneNoteAddIn.Adapters;
using Pasca.OneNoteAddIn.Trace;

namespace Pasca.OneNoteAddIn.Extensions
{
    public static class ApplicationExtensions
    {
        public static string ClonePage(
            this IApplication app,
            XElement notebook,
            XElement input,
            string newName,
            string outputSectionName,
            params string[] outputSectionGroups)
        {
            //DR.Get<ITracer>().WriteLine(TracerTags.OneNote, "ClonePage(ext): " + input.ToString());
            var sectionId = notebook.GetSection(outputSectionName, outputSectionGroups).Id();
            var newPageId = app.CreatePage(sectionId);
            var newPage = new PageAdapter(input.RemoveIds().ToString());
            newPage.Page.ID = newPageId;
            app.UpdateContent(newPage, sectionId);
            if (newName != null)
            {
                app.RenamePage(newPageId, newName);
            }

            return newPageId;
        }

        public static void RenamePage(this IApplication app, string pageId, string newName)
        {
            string pageXml;
            app.GetPageContent(pageId, out pageXml);

            var doc = XDocument.Parse(pageXml);
            doc.Descendants().WithTagName("T").First().Value = newName;
            doc.Descendants().WithTagName("Outline").FirstOrDefault()?.Remove();

            try
            {
                app.UpdatePageContent("<?xml version=\"1.0\"?>\r\n" + doc);
            }
            catch
            {
                DR.Get<ITracer>().WriteLine("XML", doc.ToString());
                throw;
            }
            app.SyncHierarchy(pageId);
        }

        private static string AddNamespace(this string xml)
        {
            xml = xml
                    .Replace("<", "<one:")
                    .Replace(@"<one:/", @"</one:")
                    .Replace(@"<one:?", "<?")
                    .Replace(
                        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"",
                        "xmlns:one = \"http://schemas.microsoft.com/office/onenote/2013/onenote\"")
                    .Replace("xmlns=\"http://schemas.microsoft.com/office/onenote/2013/onenote\"", "");
            return xml;
        }

        #region GetHierarchy

        public static string GetHierarchy(
            this IApplication app,
            string root = "",
            HierarchyScope scope = HierarchyScope.hsPages)
        {
            string hierarchy;
            app.GetHierarchy(root, scope, out hierarchy); // структура тетрадки
            return hierarchy;
        }

        public static string GetPageContent(this IApplication app, string pageId, PageInfo level = PageInfo.piAll)
        {
            string pageXml;
            app.GetPageContent(pageId, out pageXml, level);
            return pageXml;
        }

        public static string GetParent(this IApplication app, string pageId)
        {
            string parent;
            try
            {
                app.GetHierarchyParent(pageId, out parent);
            }
            catch
            {
                return null;
            }
            return parent;
        }

        #endregion

        #region Create

        public static void CreateSection(
            this IApplication app,
            string notebookId,
            string sectionName = null,
            params string[] sectionGroups)
        {
            var notebookAdapter = new NotebookAdapter(app.GetHierarchy(notebookId, HierarchyScope.hsSelf));
            notebookAdapter.CreateSection(sectionName, sectionGroups);

            app.UpdateHierarchy(notebookAdapter, notebookId);
        }

        public static string CreatePage(this IApplication app, string sectionId)
        {
            string pageId;
            app.CreateNewPage(sectionId, out pageId);
            app.SyncHierarchy(sectionId);
            return pageId;
        }

        #endregion

        #region UpdateContent

        public static void UpdateHierarchy(this IApplication app, NotebookAdapter adapter, string rootId)
        {
            using (var ms = new MemoryStream())
            {
                adapter.PageSerializer.Serialize(ms, adapter.Notebook);
                foreach (var objectId in adapter.ObjectsToDelete)
                {
                    app.DeleteHierarchy(objectId);
                }
                ms.Flush();
                ms.Position = 0;
                using (var msReader = new StreamReader(ms))
                {
                    app.UpdateHierarchy(
                        msReader.ReadToEnd()
                                .AddNamespace());
                }
            }
            app.SyncHierarchy(rootId);
        }

        public static void UpdateContent(this IApplication app, PageAdapter adapter, string rootId)
        {
            using (var ms = new MemoryStream())
            {
                adapter.PageSerializer.Serialize(ms, adapter.Page);
                foreach (var objectId in adapter.ObjectsToDelete)
                {
                    app.DeletePageContent(adapter.Page.ID, objectId);
                }
                ms.Flush();
                ms.Position = 0;
                using (var msReader = new StreamReader(ms))
                {
                    app.UpdatePageContent
                    (
                        msReader.ReadToEnd()
                                .AddNamespace());
                }
            }
            app.SyncHierarchy(rootId);
        }

        #endregion

        #region Delete

        public static void Delete(this IApplication app, string deleteId, string rootId)
        {
            app.DeleteHierarchy(deleteId);

            if (rootId != null)
            {
                app.SyncHierarchy(rootId);
            }
            else
            {
                DR.Get<ITracer>()
                  .WriteLine(TracerTags.Error, "ApplicationExtensions.Delete: rootId==null");
            }
        }

        public static void DeletePagesWithSameName(this IApplication app, XElement section, string name)
        {
            if (section == null)
            {
                return;
            }

            foreach (var oldPage in section.GetPages(name))
            {
                app.Delete(oldPage.Id(), section.Id());
            }
        }

        #endregion
    }
}