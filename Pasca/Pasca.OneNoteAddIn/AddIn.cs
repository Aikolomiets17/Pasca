﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using System.Threading.Tasks;
using Extensibility;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.OneNote;
using Pasca.Common.Auth;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.Common.Localization;
using Pasca.Common.Providers;
using Pasca.Common.Settings;
using Pasca.Common.Settings.DefaultSettingsRegistry;
using Pasca.Common.Students;
using Pasca.Common.Trace;
using Pasca.Common.Types;
using Pasca.Gui.Facade;
using Pasca.Gui.Facade.Message;
using Pasca.Gui.ViewModel;
using Pasca.OneNoteAddIn.Adapters;
using Pasca.OneNoteAddIn.Auth;
using Pasca.OneNoteAddIn.Localization;
using Pasca.OneNoteAddIn.Model;
using Pasca.OneNoteAddIn.Progress;
using Pasca.OneNoteAddIn.Properties;
using Pasca.OneNoteAddIn.ReviewValidation;
using Pasca.OneNoteAddIn.Serialization;
using Pasca.OneNoteAddIn.Settings;
using Pasca.OneNoteAddIn.Settings.SettingProviders;
using Pasca.OneNoteAddIn.Trace;
using DefaultSettingsRegistry = Pasca.OneNoteAddIn.Settings.DefaultSettingsRegistry;
using LocTags = Pasca.OneNoteAddIn.Localization.LocTags;
using OneNoteApplication = Microsoft.Office.Interop.OneNote.Application; // Conflicts with System.Windows.Forms

#pragma warning disable 1998

#pragma warning disable CS3003 // Type is not CLS-compliant

namespace Pasca.OneNoteAddIn
{
    [ComVisible(true)]
    [Guid("9C2708E0-9089-46FF-A2D8-C7FC2F509CA4")]
    [ProgId("OneNoteAddIn")]
    public sealed class AddIn : IDTExtensibility2, IRibbonExtensibility
    {
        private IOneNoteSession Session => SessionFactory.Create(
            SettingsManager.GetSetting<OneNoteSetting>(PascaSettingName.OneNote));

        private ApiFacade ApiFacade => ApiFacadeFactory.Create(
            SettingsManager.GetSetting<OneNoteSetting>(PascaSettingName.OneNote));

        private OneNoteApplication App { get; set; }

        private OneNoteSession.Factory SessionFactory { get; set; }
        private ApiFacade.Factory ApiFacadeFactory { get; set; }

        private GradesProvider.Factory GradesProviderFactory { get; set; }
        private Bootstrapper Facade { get; set; }
        private SettingsManager SettingsManager { get; set; }
        private Wall Wall { get; set; }

        private ITracer Tracer => _tracer?.Value;
        private ILoc Loc => Localization.Loc.Instance;

        private SessionListSetting Sessions =>
            _sessions ??
            (_sessions = SettingsManager.GetSetting<SessionListSetting>(CommonSettingName.Sessions));

        // todo: OnBeginShutdownEvent
        public event Action OnDisconnectionEvent;

        #region DI

        private void RegisterDependencies(DR resolver)
        {
            _tracer = new Lazy<ITracer>(() => new Tracer());
            resolver.Register<ITracer>(_tracer.Value, true);
            resolver.Register<ILoc>(Loc);
            resolver.Register<IDefaultSettingsRegistry>(new DefaultSettingsRegistry());

            SetOneNoteSessionFactory();
            DoOnStartupComplete(
                () => resolver.Register<IParentWindowProvider>(
                    new ParentWindowProvider(GetCurrentWindowHandle(App))));

            SettingsManager = new SettingsManager(
                () => Session,
                new SettingsChangedSubscription()); // todo: event on settings changed
            SettingsManager.SessionsChanged += Invalidate;

            resolver.Register<ISettingsManager>(SettingsManager);

            var availablePersonProvider = new AvailablePersonProvider(SettingsManager);
            resolver.Register<IAvailableStudentsProvider>(availablePersonProvider);
            resolver.Register<IAvailableTeachersProvider>(availablePersonProvider);
            SettingsManager.SessionsChanged += availablePersonProvider.Reset;

            resolver.Register<ISerializer<OneNoteSetting>>(new XmlSettingsSerializer<OneNoteSetting>());
            resolver.Register<ISerializer<PascaSessionSetting>>(new XmlSettingsSerializer<PascaSessionSetting>());
        }

        #endregion

        private Lazy<ITracer> _tracer;

        private IRibbonUI _ribbon;

        #region Ribbon invalidatioon

        private static string[] InteractiveControls = {
            "PascaGeneral",
            "PascaSession",
            "CurrentSession",
            "DeleteSession",
            "DeleteAllSessions",
        };

        private static string[] StatusControls = {
            "StatusReady_1",
            "StatusReady_2",
            "StatusLoading_1",
            "StatusLoading_2",
            "StatusLabel_1",
            "StatusLabel_2",
        };

        private static string[] DeleteControls = {
           "DeleteGuard",
           "DeleteSession",
           "DeleteAllSessions",
        };

        private static string[] AuthControls = {
            "Auth",
        };

        private SessionListSetting _sessions;

        public void Invalidate(IRibbonControl control)
        {
            Invalidate();
        }

        private void InvalidateControls(params string[] names)
        {
            foreach (var name in names)
            {
                _ribbon?.InvalidateControl(name);
            }
        }

        private void Invalidate()
        {
            InvalidateControls(InteractiveControls);
            InvalidateDeleteControls(false);
            InvalidateStatusControls(false);
            _sessions = null;
            _ribbon.Invalidate();
        }

        private void InvalidateStatusControls(bool invalidateRibbon = true)
        {
            InvalidateControls(StatusControls);
            if (invalidateRibbon)
            {
                _ribbon.Invalidate();
            }
        }

        private void InvalidateDeleteControls(bool invalidateRibbon = true)
        {
            InvalidateControls(DeleteControls);
            if (invalidateRibbon)
            {
                _ribbon.Invalidate();
            }
        }

        private void InvalidateAuthControls()
        {
            InvalidateControls(AuthControls);
        }

        private void ShowBusyMessageBox()
        {
            Facade.ShowMessage(Loc.GetLocalizedString(LocTags.MessageBox, LocTags.PascaBusy));
        }

        private void ShowResultMessageBox(string result)
        {
            if (!result.OrdinalEquals(Results.Ok))
            {
                Facade.ShowMessage(Loc.GetLocalizedString(LocTags.Results, result));
            }
        }


        #endregion

        #region Startup

        public void OnAddInsUpdate(ref Array custom) { }

        public void OnStartupComplete(ref Array custom)
        {
            _onStartupComplete();
            _onStartupComplete = () => { };
        }

        private void DoOnStartupComplete(Action action)
        {
            _onStartupComplete += action;
        }

        private Action _onStartupComplete = () => { };

        /// <summary>
        ///     Called upon startup.
        ///     Keeps a reference to the current OneNote application object.
        /// </summary>
        /// <param name="application"></param>
        /// <param name="connectMode"></param>
        /// <param name="addInInst"></param>
        /// <param name="custom"></param>
        public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
        {
            AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            SetOneNoteApplication((OneNoteApplication)application);

            Facade = new Bootstrapper(RegisterDependencies, SubscribeShutdown);
        }

        public void OnRibbonLoad(IRibbonUI ribbon)
        {
            _ribbon = ribbon;
        }

        #endregion

        #region Shutdown

        /// <summary>
        /// 
        /// </summary>
        /// <param name="custom"></param>
        public void OnBeginShutdown(ref Array custom) { }

        /// <summary>
        ///     Cleanup
        /// </summary>
        /// <param name="removeMode"></param>
        /// <param name="custom"></param>
        public void OnDisconnection(ext_DisconnectMode removeMode, ref Array custom)
        {
            OnDisconnectionEvent?.Invoke();
            CleanUp();
        }

        private void CleanUp(bool killProcess = true)
        {
            AppDomain.CurrentDomain.FirstChanceException -= CurrentDomain_FirstChanceException;
            AppDomain.CurrentDomain.UnhandledException -= CurrentDomain_UnhandledException;
            App = null;
            SessionFactory = null;
            GradesProviderFactory = null;
            SettingsManager = null;
            Facade?.Shutdown(killProcess);
            Facade = null;
            if (_ribbon != null)
            {
                Marshal.ReleaseComObject(_ribbon);
                _ribbon = null;
            }
            GC.Collect(2, GCCollectionMode.Forced);
            GC.WaitForPendingFinalizers();
        }

        ~AddIn()
        {
            CleanUp();
        }

        private void SubscribeShutdown(Action onShutdownAction)
        {
            OnDisconnectionEvent += onShutdownAction;
        }

        #endregion

        #region CustomUI Extra

        /// <summary>
        ///     Returns the XML in Ribbon.xml so OneNote knows how to render our ribbon
        /// </summary>
        /// <param name="ribbonId"></param>
        /// <returns></returns>
        public string GetCustomUI(string ribbonId)
        {
            return new RibbonProvider(Loc).GetRibbon();
        }

        /// <summary>
        ///     Specified in Ribbon.xml, this method returns the image to display on the ribbon button
        /// </summary>
        /// <param name="imageName"></param>
        /// <returns></returns>
        public IStream GetImage(string imageName)
        {
            //Tracer.WriteLine(TracerTags.OneNote, $"GetImage({imageName})");
            var s = new MemoryStream();
            switch (imageName)
            {
            case "Auth.png":
                Resources.Auth.Save(s, ImageFormat.Png);
                break;
            case "CollectArtifacts.png":
                Resources.CollectArtifacts.Save(s, ImageFormat.Png);
                break;
            case "CollectReviews.png":
                Resources.CollectReviews.Save(s, ImageFormat.Png);
                break;
            case "DeleteGuard.png":
                Resources.DeleteGuard.Save(s, ImageFormat.Png);
                break;
            case "DeleteSession.png":
                Resources.DeleteSession.Save(s, ImageFormat.Png);
                break;
            case "DeleteAllSessions.png":
                Resources.DeleteAllSessions.Save(s, ImageFormat.Png);
                break;
            case "DeleteTraces.png":
                Resources.DeleteTraces.Save(s, ImageFormat.Png);
                break;
            case "DistributeArtifactsAndTemplates.png":
                Resources.DistributeArtifactsAndTemplates.Save(s, ImageFormat.Png);
                break;
            case "DistributeAssignment.png":
                Resources.DistributeAssignment.Save(s, ImageFormat.Png);
                break;
            case "EditPascaSettings.png":
                Resources.EditPascaSettings.Save(s, ImageFormat.Png);
                break;
            case "EditSessionSettings.png":
                Resources.EditSessionSettings.Save(s, ImageFormat.Png);
                break;
            case "EditSessionTemplate.png":
                Resources.EditSessionTemplate.Save(s, ImageFormat.Png);
                break;
            case "EditSessionTiming.png":
                Resources.EditSessionTiming.Save(s, ImageFormat.Png);
                break;
            case "LockArtifacts.png":
                Resources.LockArtifacts.Save(s, ImageFormat.Png);
                break;
            case "LockReviews.png":
                Resources.LockReviews.Save(s, ImageFormat.Png);
                break;
            case "Logout.png":
                Resources.Logout.Save(s, ImageFormat.Png);
                break;
            case "NewSession.png":
                Resources.NewSession.Save(s, ImageFormat.Png);
                break;
            case "PlaceReports.png":
                Resources.PlaceReports.Save(s, ImageFormat.Png);
                break;
            case "ShowDistributionChart.png":
                Resources.ShowDistributionChart.Save(s, ImageFormat.Png);
                break;
            case "ShowPieChart.png":
                Resources.ShowPieChart.Save(s, ImageFormat.Png);
                break;
            case "ShowSeriesChart.png":
                Resources.ShowSeriesChart.Save(s, ImageFormat.Png);
                break;
            case "ShowTraces.png":
                Resources.ShowTraces.Save(s, ImageFormat.Png);
                break;
            case "StatusLoading.png":
                Resources.StatusLoading.Save(s, ImageFormat.Png);
                break;
            case "StatusReady.png":
                Resources.StatusReady.Save(s, ImageFormat.Png);
                break;
            case "UpdateDashboards.png":
                Resources.UpdateDashboards.Save(s, ImageFormat.Png);
                break;
            case "ValidateReview.png":
                Resources.ValidateReview.Save(s, ImageFormat.Png);
                break;
            case "ViewArtifacts.png":
                Resources.ViewArtifacts.Save(s, ImageFormat.Png);
                break;
            case "ViewAssignment.png":
                Resources.ViewAssignment.Save(s, ImageFormat.Png);
                break;
            case "ViewSessionMapping.png":
                Resources.ViewSessionMapping.Save(s, ImageFormat.Png);
                break;
            default:
                Resources.Icon_png.Save(s, ImageFormat.Png);
                break;
            }
            return new CcomStreamWrapper(s);
        }

        #endregion

        #region Event Handlers

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Tracer?.WriteLine(TracerTags.Fail, "Exception unhandled. Pasca disposal");
            Tracer?.WriteLine(TracerTags.Fail, _lastFirstChanceException.Message);
            Tracer?.WriteLine(TracerTags.Fail, Environment.StackTrace);
            Tracer?.WriteLine(TracerTags.Fail, _lastFirstChanceException.StackTrace);
            Facade.ShowMessage("Exception unhandled. Pasca disposal");
            CleanUp(false);
            AppDomain.Unload(AppDomain.CurrentDomain);
        }

        private Exception _lastFirstChanceException;

        private int _processFirstChance = 1;

        [SuppressMessage("ReSharper", "EmptyGeneralCatchClause")]
        private void CurrentDomain_FirstChanceException(
            object sender,
            FirstChanceExceptionEventArgs e)
        {
            if (Interlocked.CompareExchange(ref _processFirstChance, 0, 1) == 1)
            {
                try
                {
                    if (ShouldTraceException(e.Exception))
                    {
                        var stackTrace = Environment.StackTrace;
                        Tracer?.WriteLine(TracerTags.Error, e.Exception.Message);
                        Tracer?.WriteLine(TracerTags.Error, stackTrace /*.Since("OneNoteAddIn")*/);
                    }
                    _lastFirstChanceException = e.Exception;
                }
                catch { }
                finally
                {
                    Interlocked.CompareExchange(ref _processFirstChance, 1, 0);
                }
            }
        }

        private static bool ShouldTraceException(Exception e)
        {
            return !e.Message.Contains("\"Newtonsoft.Json.Linq.JObject\"")
                && !e.Message.Contains(": frameworkName");
        }

        private void OnAuthorizationRequest(IAuth auth)
        {
            Facade.ShowSignInWindow(auth);
            InvalidateAuthControls();
        }

        #endregion

        #region Helpers

        private void SetOneNoteApplication(OneNoteApplication application)
        {
            App = new ApplicationAdapter(application);
        }

        private static ulong? GetCurrentWindowHandle(OneNoteApplication app)
        {
            var windows = app.Windows;
            ulong? handle = 0;
            if (windows?.Count > 0)
            {
                var currentWindow = windows[0];
                handle = currentWindow?.WindowHandle;

                if (currentWindow != null)
                {
                    Marshal.ReleaseComObject(currentWindow);
                    currentWindow = null;
                }
                GC.KeepAlive(currentWindow);
            }
            if (windows != null)
            {
                Marshal.ReleaseComObject(windows);
                windows = null;
            }
            GC.KeepAlive(windows);
            return handle;
        }

        private void SetOneNoteSessionFactory()
        {
            Assert.NotNull(App);

            Wall = new Wall(() => { }, ShowBusyMessageBox, ShowResultMessageBox);
            Wall.ProgressChanged += OnProgressChanged;

            var authReqHandler = new AuthorizationRequestHandler(OnAuthorizationRequest);
            ApiFacadeFactory = new ApiFacade.Factory(authReqHandler, Wall);
            SessionFactory = new OneNoteSession.Factory(App, ApiFacadeFactory, Wall);
            GradesProviderFactory = new GradesProvider.Factory();
        }

        #endregion

        #region Ribbon Controls

        #region Tab PascaGeneral

        #region Group PascaSettings

        public async Task EditPascaSettings(IRibbonControl control)
        {
            // todo: onenote is locked after window is closed but some work still doing
            await Wall.Wrap(
                () =>
                    Facade.ShowSettingsWindow(
                        new OneNoteSettingsProvider(SettingsManager, DR.Get<IDefaultSettingsRegistry>()))).ConfigureAwait(false);
        }

        public async Task ShowTraces(IRibbonControl control)
        {
            if (Tracer == null)
            {
                return;
            }
            await Wall.Wrap((Action)Tracer.Show).ConfigureAwait(false);
        }

        public async Task DeleteTraces(IRibbonControl control)
        {
            if (Tracer == null)
            {
                return;
            }
            await Wall.Wrap((Action)Tracer.Clean).ConfigureAwait(false);
        }

        #endregion

        #region Group SessionsManagement

        public async Task NewSession(IRibbonControl control)
        {
            await Wall.Wrap(
                () =>
                    Facade.ShowNewSessionWindow(
                        new NewSessionProvider(SettingsManager, DR.Get<IDefaultSettingsRegistry>()))).ConfigureAwait(false);
        }

        // todo: does not invalidate
        public int CurrentSessionGetItemCount(IRibbonControl control)
        {
            return Sessions.Value.Count;
        }

        public string CurrentSessionGetItemLabel(IRibbonControl control, int index)
        {
            return Sessions.Value[index].SessionName;
        }

        public string CurrentSessionGetText(IRibbonControl control)
        {
            return SettingsManager.CurrentSessionName;
        }

        public async Task CurrentSessionOnChange(IRibbonControl control, string chosen)
        {
            await Wall.Wrap(
                () =>
                {
                    SettingsManager.Update(new StringSetting(PascaSettingName.CurrentSessionName, chosen));
                    Invalidate();
                }).ConfigureAwait(false);
        }

        private bool _isDeleteVisible;

        public async Task DeleteGuard(IRibbonControl control)
        {
            _isDeleteVisible = true;
            InvalidateDeleteControls();
        }

        public bool DeleteGuardGetVisible(IRibbonControl control)
        {
            return !_isDeleteVisible;
        }

        public async Task DeleteSession(IRibbonControl control)
        {
            await Wall.Wrap(
                async () =>
                {
                    using (Wall.MakeStep(0.5))
                    {
                        await Session.DeleteSession(SettingsManager.CurrentSessionName);
                    }
                    SettingsManager.Update(
                        new StringSetting(
                            PascaSettingName.CurrentSessionName,
                            string.Empty));
                    Invalidate();
                }).ConfigureAwait(false);
        }

        public bool DeleteSessionGetVisible(IRibbonControl control)
        {
            return _isDeleteVisible
                && !string.IsNullOrEmpty(SettingsManager.CurrentSessionName)
                && SettingsManager.CurrentSession != null;
        }

        public async Task DeleteAllSessions(IRibbonControl control)
        {
            var result = Facade.ShowMessage(new MessageInfo {
                Body = Loc.GetLocalizedString(LocTags.MessageBox, LocTags.DeleteAllSessions),
                Buttons = MessageButtons.OkCancel
            });
            if (result == MessageResult.Ok)
            {
                await Wall.Wrap(
                    async () =>
                    {
                        foreach (var sessionName in Session.GetSessionsList())
                        {
                            await Session.DeleteSession(sessionName);
                        }
                        SettingsManager.Update(
                            new StringSetting(
                                PascaSettingName.CurrentSessionName,
                                string.Empty));
                        Invalidate();
                    }).ConfigureAwait(false);
            }
        }

        public bool DeleteAllSessionsGetVisible(IRibbonControl control)
        {
            return _isDeleteVisible
                && (Session.GetSessionsList()?.Any() ?? false);
        }

        public async Task InvalidateControls(IRibbonControl control)
        {
            await Wall.Wrap((Action)Invalidate).ConfigureAwait(false);
        }

        public bool InvalidateControlsGetVisible(IRibbonControl control)
        {
#if DEBUG
            return true;
#else
            return false;
#endif
        }

        public async Task Auth(IRibbonControl control)
        {
            await Wall.Wrap(() => Facade.ShowSignInWindow(ApiFacade.GetAuth())).ConfigureAwait(false);
        }

        public async Task Logout(IRibbonControl control)
        {
            await Wall.Wrap(
                () =>
                {
                    if (!ApiFacade.Logout())
                    {
                        Facade.ShowMessage(Loc.GetLocalizedString(LocTags.MessageBox, LocTags.CouldNotLogout));
                    }
                }).ConfigureAwait(false);
        }

        public bool AuthGetVisible(IRibbonControl control)
        {
            return !ApiFacade.Authorized;
        }

        public bool LogoutGetVisible(IRibbonControl control)
        {
            return ApiFacade.Authorized;
        }

        private void OnProgressChanged(ProgressReport progress)
        {
            Tracer?.WriteLine(TracerTags.OneNote, "OnProgressChanged");
            if (progress == Wall.Ready || progress == Wall.Started)
            {
                InvalidateStatusControls();
            }
            else
            {
                _progressChangedThrottler.Add(() => InvalidateStatusControls());
            }

        }

        private readonly TaskThrottler _progressChangedThrottler = new TaskThrottler(TimeSpan.FromSeconds(1));

        #endregion

        #region Group Status

        public bool StatusReadyGetVisible(IRibbonControl control)
        {
            return !Wall.IsBusy();
        }

        public bool StatusLoadingGetVisible(IRibbonControl control)
        {
            return Wall.IsBusy();
        }

        public string StatusReadyGetLabel(IRibbonControl control)
        {
            return Loc.GetLocalizedString(LocTags.Progress, Wall.GetProgress().Status);
        }

        public string StatusLoadingGetLabel(IRibbonControl control)
        {
            return
                    $"({Wall.GetProgress().ProgressPercent,3}%) {Loc.GetLocalizedString(LocTags.Progress, Wall.GetProgress().Status)}";
        }

        #endregion

        #endregion

        #region Tab PascaSession

        #region Group SessionSettings

        public async Task EditSessionSettings(IRibbonControl control)
        {
            await Wall.Wrap(
                () => Facade.ShowSettingsWindow(
                    new SessionSettingsProvider(
                        SettingsManager,
                        DR.Get<IDefaultSettingsRegistry>(),
                        SettingsManager.CurrentSessionName,
                        SelectPage,
                        GetPageName
                    ))).ConfigureAwait(false);
        }

        private string SelectPage()
        {
            return Facade.ShowSelectWindow(Session.GetHierarchy(GetHierarchyRootType.TeacherOnly))?.Value;
        }

        private string GetPageName(string id)
        {
            return Session.GetElementName(id, HierarchyElement.hePages);
        }

        public async Task EditSessionTemplate(IRibbonControl control)
        {
            await Wall.Wrap(
                () => Facade.ShowTemplateEditWindow(
                    new SessionCriteriaProvider(
                        SettingsManager,
                        DR.Get<IDefaultSettingsRegistry>(),
                        SettingsManager.CurrentSessionName))).ConfigureAwait(false);
        }

        public async Task EditSessionTiming(IRibbonControl control)
        {
            await Wall.Wrap(
                () =>
                    Facade.ShowSettingsWindow(
                        new SessionTimingSettingsProvider(
                            SettingsManager,
                            DR.Get<IDefaultSettingsRegistry>(),
                            SettingsManager.CurrentSessionName))).ConfigureAwait(false);
        }

        public async Task ViewSessionMapping(IRibbonControl control)
        {
            await Wall.Wrap(
                () =>
                {
                    if (!CheckAuthorsAndReviewersCount())
                    {
                        return;
                    }
                    Facade.ShowSessionMappingWindow(
                        new SessionMappingSettingProvider(
                            SettingsManager,
                            DR.Get<IDefaultSettingsRegistry>(),
                            SettingsManager.CurrentSessionName));
                }).ConfigureAwait(false);
        }

        public async Task ViewAssignment(IRibbonControl control)
        {
            await Wall.Wrap(
                () =>
                {
                    var sectionUrl = Session.CreateAssignmentSectionUrl(SettingsManager.CurrentSession);
                    if (sectionUrl == null)
                    {
                        Facade.ShowMessage(Loc.GetLocalizedString(LocTags.MessageBox, LocTags.AssignmentSectionNotFound));
                        return;
                    }

                    App.NavigateToUrl(sectionUrl);
                }).ConfigureAwait(false);
        }

        public async Task ViewArtifacts(IRibbonControl control)
        {
            await Wall.Wrap(
                () =>
                {
                    var sectionUrl = Session.CreateArtifactsSectionUrl(SettingsManager.CurrentSession);
                    if (sectionUrl == null)
                    {
                        Facade.ShowMessage(Loc.GetLocalizedString(LocTags.MessageBox, LocTags.ArtifactsSectionNotFound));
                        return;
                    }

                    App.NavigateToUrl(Session.CreateArtifactsSectionUrl(SettingsManager.CurrentSession));
                }).ConfigureAwait(false);
        }

        #endregion

        #region Group AssignmentManagement

        public bool IsPascaSessionVisible(IRibbonControl control)
        {
            return SettingsManager.GetSetting<SessionListSetting>(CommonSettingName.Sessions).Value?.Contains(
                new Session {
                    SessionName = SettingsManager.CurrentSessionName
                }) ?? false;
        }

        public async Task DistributeAssignment(IRibbonControl control)
        {
            await Wall.Wrap(
                async () =>
                {
                    if (!CheckAuthorsAndReviewersCount())
                    {
                        return;
                    }
                    var session = SettingsManager.CurrentSession;
                    using (Wall.MakeStep(0.5))
                    {
                        session[CommonSettingName.SessionAssignment] = new StringSetting(
                            CommonSettingName.SessionAssignment,
                            (await Session.DistributeAssignment(
                                       new[] {
                                           session.SessionAssignment()
                                       },
                                       session))
                                   .FirstOrDefault());
                    }
                    SettingsManager.Update(session);

                    //Session.DistributeDashboards(SettingsManager.CurrentSession);
                }).ConfigureAwait(false);
        }

        public async Task LockArtifacts(IRibbonControl control)
        {
            await Wall.Wrap(
                async () =>
                {
                    var sessionSetting = SettingsManager.CurrentSession;

                    var sessionSubmissionEnd = sessionSetting.SessionSubmissionEnd();
                    if (!sessionSubmissionEnd.Passed())
                    {
                        var result = Facade.ShowMessage(new MessageInfo {
                            Body = Loc.GetLocalizedString(
                                        LocTags.MessageBox,
                                        LocTags.SubmissionDeadlineNotPassedYetLockArtifacts,
                                        sessionSubmissionEnd),
                            Buttons = MessageButtons.OkCancel
                        });
                        if (result == MessageResult.Cancel)
                        {
                            return;
                        }
                    }

                    await Session.LockArtifacts(sessionSetting);
                }).ConfigureAwait(false);
        }

        public async Task CollectArtifacts(IRibbonControl control)
        {
            await Wall.Wrap(
                async () =>
                {
                    var sessionSetting = SettingsManager.CurrentSession;

                    using (Wall.MakeStep(0.2))
                    {
                        if (!await Session.AreArtifactsLocked(sessionSetting))
                        {
                            var result = Facade.ShowMessage(
                                new MessageInfo {
                                    Body =
                                            Loc.GetLocalizedString(
                                                LocTags.MessageBox,
                                                LocTags.ArtifactsNotLockedYetCollectArtifacts),
                                    Buttons = MessageButtons.OkCancel
                                });
                            if (result == MessageResult.Cancel)
                            {
                                return;
                            }
                        }
                    }

                    var sessionSubmissionEnd = sessionSetting.SessionSubmissionEnd();
                    if (!sessionSubmissionEnd.Passed())
                    {
                        var result = Facade.ShowMessage(
                            new MessageInfo {
                                Body = Loc.GetLocalizedString(
                                    LocTags.MessageBox,
                                    LocTags.SubmissionDeadlineNotPassedYetCollectArtifacts,
                                    sessionSubmissionEnd),
                                Buttons = MessageButtons.OkCancel
                            });
                        if (result == MessageResult.Cancel)
                        {
                            return;
                        }
                    }

                    await Session.CollectArtifacts(sessionSetting);
                }).ConfigureAwait(false);
        }

        #endregion

        #region Group ReviewManagement

        public async Task DistributeArtifactsAndTemplates(IRibbonControl control)
        {
            await Wall.Wrap(
                () => { Session.DistributeArtifactsAndTemplates(SettingsManager.CurrentSession); }).ConfigureAwait(false);
        }

        public async Task LockReviews(IRibbonControl control)
        {
            await Wall.Wrap(
                async () =>
                {
                    var sessionSetting = SettingsManager.CurrentSession;

                    var sessionReviewEnd = sessionSetting.SessionReviewEnd();
                    if (!sessionReviewEnd.Passed())
                    {
                        var result = Facade.ShowMessage(new MessageInfo {
                            Body = Loc.GetLocalizedString(
                                        LocTags.MessageBox,
                                        LocTags.ReviewDeadlineNotPassedYetLockReviews,
                                        sessionReviewEnd),
                            Buttons = MessageButtons.OkCancel
                        });
                        if (result == MessageResult.Cancel)
                        {
                            return;
                        }
                    }

                    await Session.LockReviews(sessionSetting);
                }).ConfigureAwait(false);
        }

        public async Task CollectReviews(IRibbonControl control)
        {
            await Wall.Wrap(
                async () =>
                {
                    var sessionSetting = SettingsManager.CurrentSession;

                    using (Wall.MakeStep(0.2))
                    {
                        if (!await Session.AreReviewsLocked(sessionSetting))
                        {
                            var result = Facade.ShowMessage(
                                new MessageInfo {
                                    Body =
                                            Loc.GetLocalizedString(
                                                LocTags.MessageBox,
                                                LocTags.ArtifactsNotLockedYetCollectArtifacts),
                                    Buttons = MessageButtons.OkCancel
                                });
                            if (result == MessageResult.Cancel)
                            {
                                return;
                            }
                        }
                    }

                    var sessionReviewEnd = sessionSetting.SessionReviewEnd();
                    if (!sessionReviewEnd.Passed())
                    {
                        var result = Facade.ShowMessage(new MessageInfo {
                            Body = Loc.GetLocalizedString(
                                LocTags.MessageBox,
                                LocTags.SubmissionDeadlineNotPassedYetCollectArtifacts,
                                sessionReviewEnd),
                            Buttons = MessageButtons.OkCancel
                        });
                        if (result == MessageResult.Cancel)
                        {
                            return;
                        }
                    }

                    await Session.CollectReviews(sessionSetting);
                }).ConfigureAwait(false);
        }

        public async Task ValidateReview(IRibbonControl control)
        {
            await Wall.Wrap(
                () =>
                {
                    var verdict = Session.CheckReviewPageValid(
                        SettingsManager.CurrentSession,
                        Session.GetCurrentPageId());
                    if (verdict.Verdict == ReviewValidationVerdict.Ok)
                    {
                        Facade.ShowMessage(DR.Get<ILoc>().GetLocalizedString(LocTags.MessageBox, LocTags.ReviewIsValid));
                    }
                    else if (verdict.Verdict == ReviewValidationVerdict.PageNotFound)
                    {
                        Facade.ShowMessage(DR.Get<ILoc>().GetLocalizedString(LocTags.MessageBox, LocTags.ReviewPageNotFound));
                    }
                    else if (verdict.Verdict == ReviewValidationVerdict.TableNotFound)
                    {
                        Facade.ShowMessage(DR.Get<ILoc>().GetLocalizedString(LocTags.MessageBox, LocTags.ReviewTableNotFound));
                    }
                    else if (verdict.Verdict == ReviewValidationVerdict.TableError)
                    {
                        var tableVerdict = verdict as TableErrorVerdictArgs;
                        if (tableVerdict == null)
                        {
                            Facade.ShowMessage(DR.Get<ILoc>().GetLocalizedString(LocTags.MessageBox, LocTags.UnknownTableError));
                        }
                        else
                        {
                            Facade.ShowMessage(DR.Get<ILoc>().GetLocalizedString(
                                    LocTags.MessageBox,
                                    LocTags.TableError,
                                    tableVerdict.X + 1,
                                    tableVerdict.Y + 1
                                    ));
                        }
                    }
                    else
                    {
                        Facade.ShowMessage(DR.Get<ILoc>().GetLocalizedString(LocTags.MessageBox, LocTags.UnknwonReviewValidationError));
                    }
                }).ConfigureAwait(false);
        }

        #endregion

        #region Group DashboardManagement

        public async Task UpdateDashboards(IRibbonControl control)
        {
            await Wall.Wrap(
                          async () =>
                          {
                              await Session.UpdateDashboards(SettingsManager.CurrentSession);
                          }).ConfigureAwait(false);
        }

        #endregion

        #region Group Reports

        public async Task PlaceReports(IRibbonControl control)
        {
            await Wall.Wrap(() => { Session.PlaceReports(SettingsManager.CurrentSession); }).ConfigureAwait(false);
        }

        #endregion

        #region Group Charts

        public async Task ShowSeriesChart(IRibbonControl control)
        {
            await Wall.Wrap(
                () => Facade.ShowFinalGradesSeriesChart(
                    GradesProviderFactory.Create(
                        SettingsManager.CurrentSession,
                        Session))).ConfigureAwait(false);
        }

        public async Task ShowPieChart(IRibbonControl control)
        {
            await Wall.Wrap(
                () => Facade.ShowFinalGradesPieChart(
                    GradesProviderFactory.Create(
                        SettingsManager.CurrentSession,
                        Session))).ConfigureAwait(false);
        }

        public async Task ShowDistributionChart(IRibbonControl control)
        {
            await Wall.Wrap(
                () => Facade.ShowFinalGradesDistributionChart(
                    GradesProviderFactory.Create(
                        SettingsManager.CurrentSession,
                        Session))).ConfigureAwait(false);
        }

        #endregion

        #endregion

        #endregion

        #region Validators

        private bool CheckAuthorsAndReviewersCount()
        {
            var authorsCount = SettingsManager.GetSetting<PersonListSetting>(CommonSettingName.SessionAuthors)
                                 .Value.Count;
            if (authorsCount < 2)
            {
                if (Facade.ShowMessage(
                        new MessageInfo {
                            Body = Loc.GetLocalizedString(LocTags.MessageBox, LocTags.FewAuthors, authorsCount),
                            Buttons = MessageButtons.OkCancel
                        }) != MessageResult.Ok)
                {
                    return false;
                }
            }
            var reviewersCount = SettingsManager.GetSetting<PersonListSetting>(CommonSettingName.SessionReviewers)
                                   .Value.Count;
            if (reviewersCount < 2)
            {
                if (Facade.ShowMessage(
                        new MessageInfo {
                            Body = Loc.GetLocalizedString(LocTags.MessageBox, LocTags.FewReviewers, authorsCount),
                            Buttons = MessageButtons.OkCancel
                        }) != MessageResult.Ok)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion
    }
}