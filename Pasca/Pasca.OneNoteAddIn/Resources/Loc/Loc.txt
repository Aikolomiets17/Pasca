; Settings
; General
Settings:OneNote=PASCA settings

; OneNote
Settings:TeacherOnlySectionGroupPostfix="Teacher-Only" section group name
Settings:SettingsPagePostfix=Settings page postfix
Settings:PascaSectionName=Pasca-related section groups name
Settings:DashboardSectionName=Dashboard section name
Settings:CurrentSessionName=Current session name

; Session
Settings:SessionAssignmentSectionName=Assignment section name
Settings:SessionArtifactSectionName=Artifacts section name
Settings:SessionReviewArtifactsSectionGroupName=Reviews section group name
Settings:SessionReviewTemplatePageName=Review template page name
Settings:SessionArtifactsReportPageName=Artifacts report page name
Settings:SessionAuthorsReviewsReportPageName=Authors reviews report page name
Settings:SessionReviewersReviewsReportPageName=Reviewers reviews report page name
Settings:SessionGradesReportPageName=Grades report page name

; Settings:SessionContentLibrarySectionGroupPostfix="Content Library" section group name




; Progress
Progress:ReadyLabel=Ready
Progress:LoadingLabel=Loading...

Progress:SyncingNotebookHierarchy=Syncing Notebook Hierarchy
Progress:Navigating=Navigating

Progress:CreatingSessionSettingsPage=Creating session setting page
Progress:CreatingStudentsSectionsViaWebApi=Creating students sections in content library via web api
Progress:ApiRequestedWaitingForSync=Api requested, waiting for notebook synchronization
Progress:CreatingArtifactsAndReviewsSections=Creating artifacts and reviews sections

Progress:PlacingDashboards=Placing dashboards

Progress:CopyingAssignments=Copying assignments to session section
Progress:PlacingAssignments=Placing assignments

Progress:CollectingArtifacts=Collecting artifacts

Progress:PlacingArtifactsAndTemplates=Placing artifacts and templates

Progress:CollectingReviews=Collecting reviews

Progress:ValidatingReview=Validating review

Progress:DeletingArtifactsInTeacherOnly=Deleting artifacts in TeacherOnly
Progress:DeletingArtifactsInContentLibrary=Deleting artifacts in Content Library
Progress:DeletingSessionSettings=Deleting session settings

Progress:LockingArtifactsViaWebApi=Locking artifacts via web api
Progress:LockingReviewsViaWebApi=Locing reviews via web api
Progress:RequestingWebApi=Requesting web api
Progress:CreatingDashboardSectionsViaWebApi=Creating dashboard sections via web api
Progress:CreatingAssignmentSectionsViaWebApi=Creating assignment sections via web api


; ribbon.xml
Ribbon:PASCA General=PASCA General
Ribbon:Status=Status
Ribbon:PASCA Settings=PASCA Settings
Ribbon:Show Traces=Show Logs
Ribbon:Delete Guard=\s
Ribbon:Delete Guard Tooltip=Delete sessions...
Ribbon:Delete Traces=Delete Logs
Ribbon:Sessions=Sessions
Ribbon:Sessions Management=Sessions Management
Ribbon:Create New Session=Create New Session
Ribbon:Current Session=Current Session
Ribbon:Delete Session=Delete Session
Ribbon:Delete All Sessions=Delete All Sessions
Ribbon:Invalidate Controls=Invalidate Controls
Ribbon:Auth=Authorize
Ribbon:Logout=Logout
Ribbon:PASCA Session=PASCA Session
Ribbon:Session Settings=Session Settings
Ribbon:Edit Session Settings=Edit Session Settings
Ribbon:Edit Session Template=Edit Session Template
Ribbon:Edit Session Timing=Edit Session Schedule
Ribbon:View Mapping=View Mapping
Ribbon:View Assignment=View Assignment
Ribbon:View Artifacts=View Session Assignment and Artifacts
Ribbon:Assignment Management=Assignment Management
Ribbon:Distribute Assignment=Distribute Assignment
Ribbon:Lock Artifacts=Lock Artifacts
Ribbon:Collect Artifacts=Collect Artifacts
Ribbon:Review Management=Review Management
Ribbon:Distribute Artifacts and Templates=Distribute Artifacts and Templates
Ribbon:Collect Reviews=Collect Reviews
Ribbon:Validate Review=Validate Review
Ribbon:Lock Reviews=Lock Reviews
Ribbon:Dashboard Management=Dashboard Management
Ribbon:Update Dashboards=Update Dashboards
Ribbon:Reports=Reports
Ribbon:Place Reports=Place Reports
Ribbon:Charts=Charts
Ribbon:Show Series Chart=Grades Chart
Ribbon:Show Pie Chart=Grades Pie Chart
Ribbon:Show Distribution Chart=Grades Distribution Chart

; MessageBox
MessageBox:PascaBusy=Another operation is currently in progress. Please, wait until previous task is complete.
MessageBox:CouldNotLogout=Could not log out. Please, restart OneNote and try again. If the problem continues, delete files OneNoteAddIn.Access and OneNoteAddIn.Refresh located in IsolatedStorage.

MessageBox:AssignmentSectionNotFound=Could not locate assignment, sorry
MessageBox:ArtifactsSectionNotFound=Could not locate artifacts section group, sorry

MessageBox:DeleteAllSessions=Do you really want to delete all sessions relevant data from current notebook?
MessageBox:ReviewIsValid=Review page is valid!

MessageBox:ReviewPageNotFound=Review page is not found
MessageBox:ReviewTableNotFound=Table is not found in the review page
MessageBox:UnknownTableError=Unknown table error encountered in the review page. Please, recreate the table according to the template.
MessageBox:TableError=Table is malformed at position ({0}\;{1})
MessageBox:UnknwonReviewValidationError=Could not validate review page
MessageBox:SubmissionDeadlineNotPassedYetLockArtifacts=Submission deadline of {0} has not been excedeed yet. Perhaps, authors should still have time to modify their artifacts. Are you sure to lock the artifacts?
MessageBox:ReviewDeadlineNotPassedYetLockReviews=Review deadline of {0} has not been excedeed yet. Perhaps, reviewers should still have time to modify their reviews. Are you sure to lock the reviews?
MessageBox:SubmissionDeadlineNotPassedYetCollectArtifacts=Submission deadline of {0} has not been excedeed yet. Perhaps, authors should still have time to modify their artifacts. Are you sure to collect the artifacts?
MessageBox:ReviewDeadlineNotPassedYetCollectReviews=Review deadline of {0} has not been excedeed yet. Perhaps, reviewers should still have time to modify their reviews. Are you sure to collect the reviews?
MessageBox:ArtifactsNotLockedYetCollectArtifacts=Session artifacts are still editable by authors. Perhaps, you should first lock the artifacts before collecting them. Are you sure to collect the artifacts?
MessageBox:ReviewsNotLockedYetCollectReviews=Session reviews are still editable by reviewers. Perhaps, you should first lock the reviews before collecting them. Are you sure to collect the reviews?

MessageBox:FewAuthors=There are only {0} authors registered in session. Are you sure you want to continue?
MessageBox:FewReviewers=There are only {0} reviewers registered in session. Are you sure you want to continue?

; Results
Results:Ok=OK
Results:UnknownError=Operation could not be performed due to internal error. Please, send log files to pasca_hse@outlook.com
Results:TimeOut=Timeout elapsed. Pasca failed to perform the task.
Results:GradesProviderError=In order to show data, based on students' grades, at least one author in session must have received a grade.
Results:AuthorizeOnly=In order to perform the operation, you need to sign in to Office 365

; Gui
Gui:Cancel=Cancel
Gui:Ok=OK
Gui:UnknownError=Operation could not be performed due to internal error. Please, send log files to pasca_hse@outlook.com
Gui:Edit=Edit
Gui:Warning=Warning
Gui:SettingsChanged=Settings have changed
Gui:Loading=Loading...
Gui:Student=Student

; Gui Settings
GuiSettings:AggregateExpression=Aggregate expression
GuiSettings:CriteriaGroupName=Criteria group name
GuiSettings:AddNewCriterion=Add new criterion
GuiSettings:CriterionDescription=Criterion description

GuiSettings:CriteriaGroupAggregateExpressionPlaceholder=[0]
GuiSettings:CriteriaAggregateExpressionPlaceholder=[1] + 2 * [2] - 0.5 * case([3], '+', 1, '-', 0)

GuiSettings:NoAuthors=No authors registered in session.

GuiSettings:FloatCriterionViewModel=Float Criterion
GuiSettings:NumericCriterionViewModel=Numeric Criterion
GuiSettings:SetCriterionViewModel=Set Criterion

GuiSettings:FloatCriterion=Float Criterion
GuiSettings:NumericCriterion=Numeric Criterion
GuiSettings:SetCriterion=Set Criterion

GuiSettings:Max=Max
GuiSettings:Min=Min
GuiSettings:Set=Set
GuiSettings:NewSetItem=Add new set item
GuiSettings:Add=Add
GuiSettings:Remove=Remove
GuiSettings:Edit=Edit

GuiSettings:AddButton=Add > 
GuiSettings:RemoveButton= < Remove
GuiSettings:NextButton=Next
GuiSettings:PreviousButton=Previous
GuiSettings:SelectButton=>

; Gui Artifacts
GuiArtifacts:ArtifactPage=Artifact Page

; Xml
; Dashboard.xml
Xml:SubmissionBegin=Artifacts submission begins at
Xml:SubmissionEnd=Artifacts submission ends at
Xml:ReviewBegin=Review begins at
Xml:ReviewEnd=Review ends at
Xml:ArtifactDone=Assignment artifact done
Xml:ReviewsDone=Peer reviews done
Xml:ReviewsCollected=Peer reviews collected
; ReviewTemplate.xml
Xml:Criterion Group N=Criterion Group #
Xml:Criterion N=Criterion #
Xml:Criterion=Criterion
Xml:Range=Range
Xml:Value=Value
Xml:CriteriaGroup=Criteria group
; ArtifactsTemplate.xml
Xml:Author=Author
Xml:Artifacts=Artifacts
; AuthorsReviewsReport.xml
Xml:Reviewer=Reviewer
Xml:Review=Review
Xml:Grade=Grade
; GradesReport.xml
Xml:Aggregate Grade=Aggregate Grade