﻿using System;
using Pasca.Common.DI;
using Pasca.Common.Trace;
using Pasca.Gui.Facade;
using Pasca.OneNoteAddIn.Trace;

namespace Pasca.OneNoteAddIn
{
    public class ParentWindowProvider : IParentWindowProvider
    {
        public ParentWindowProvider(ulong? handle)
        {
            _handle = handle.HasValue ? (IntPtr?)handle.Value : null;
        }

        private ITracer Tracer => _tracer.Value;

        public IntPtr? GetParentWindowHandle()
        {
            Tracer.WriteLine(TracerTags.PrntHndl, "parent handle: " + _handle);
            return _handle;
        }

        private readonly IntPtr? _handle;
        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();
    }
}