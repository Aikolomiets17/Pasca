﻿using System;

namespace Pasca.OneNoteAddIn.Progress
{
    public interface IProgressProvider
    {
        event Action<ProgressReport> ProgressChanged;

        ProgressReport GetProgress();
        void ReportProgress(ProgressReport progress);

        IDisposable MakeStep(double till);

        T MakeStepAndReturn<T>(Func<T> func, double till);
    }
}