﻿using System;

namespace Pasca.OneNoteAddIn.Progress
{
    public class ProgressReport
    {
        public ProgressReport()
        {
            Status = string.Empty;
        }

        public ProgressReport(string status)
        {
            Status = status;
        }

        public ProgressReport(double progress)
        {
            Progress = progress;
        }

        [Obsolete]
        public ProgressReport(int progressPercent)
        {
            ProgressPercent = progressPercent;
        }

        public ProgressReport(double progress, string status)
        {
            Progress = progress;
            Status = status;
        }

        [Obsolete]
        public ProgressReport(int progressPercent, string status)
        {
            ProgressPercent = progressPercent;
            Status = status;
        }

        public string Status { get; set; }

        public double Progress
        {
            get { return _progress; }
            set { _progress = Math.Max(Math.Min(value, 1), 0); }
        }

        public int ProgressPercent
        {
            get { return (int)(_progress * 100); }
            set { _progress = Math.Max(Math.Min(value, 100), 0) / 100.0; }
        }

        private double _progress;
    }
}