﻿using System;
using System.Threading.Tasks;

namespace Pasca.OneNoteAddIn.Progress
{
    public interface IWall
    {
        Task Wrap(Action action);
        Task Wrap(Func<Task> action);
        Task Wrap(Func<string> action);
        Task Wrap(Func<Task<string>> action);
        bool IsBusy();
    }
}