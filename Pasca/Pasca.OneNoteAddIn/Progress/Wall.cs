﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Trace;
using Pasca.Gui.ViewModel;
using Pasca.OneNoteAddIn.Localization;

namespace Pasca.OneNoteAddIn.Progress
{
    public class Wall : IWall, IProgressProvider
    {
        private Wall()
        {
            _progressReport = Ready;
        }

        // todo: remove invalidateAction
        public Wall(Action invalidateAction, Action busyAction, Action<string> resultAction)
            : this()
        {
            Assert.NotNull(invalidateAction);
            Assert.NotNull(busyAction);
            Assert.NotNull(resultAction);

            _invalidateAction = invalidateAction;
            _busyAction = busyAction;
            _resultAction = resultAction;
        }

        private readonly Action _busyAction;
        private readonly Action _invalidateAction;
        private readonly Action<string> _resultAction;

        private readonly List<ProgressStep> _steps = new List<ProgressStep>();
        private int _busy;

        private ProgressReport _progressReport;
        public ProgressReport Ready { get; } = new ProgressReport(1.0, LocTags.ReadyLabel);
        public ProgressReport Started { get; } = new ProgressReport(0.0, LocTags.LoadingLabel);

        private sealed class ProgressStep : IDisposable
        {
            public ProgressStep(Wall wall, double till)
            {
                Assert.NotNull(wall);

                _wall = wall;
                _initialProgress = _wall._progressReport.Progress;

                var lastStep = _wall._steps.LastOrDefault();
                _till = lastStep == null
                    ? till
                    : lastStep._initialProgress + till * (lastStep._till - lastStep._initialProgress);

                Assert.True(_till >= _initialProgress);

                _wall._steps.Add(this);
            }

            public void Dispose()
            {
                _wall._steps.Remove(this);
            }

            public ProgressReport AdjustProgress(ProgressReport progressReport)
            {
                return new ProgressReport(
                    _initialProgress + progressReport.Progress * (_till - _initialProgress),
                    progressReport.Status);
            }

            private readonly double _initialProgress;
            private readonly double _till;
            private readonly Wall _wall;
        }

        #region IProgressProvider

        public ProgressReport GetProgress()
        {
            return _progressReport;
        }

        public void ReportProgress(ProgressReport progressReport)
        {
            Tracer.WriteLine(CommonTracerTags.General, $"ReportProgress({progressReport.ProgressPercent})");
            _progressReport = _steps.Aggregate(progressReport, (acc, step) => step.AdjustProgress(acc));

            Tracer.WriteLine(CommonTracerTags.General, $"ReportProgressEnd({progressReport.ProgressPercent})");
            ProgressChanged(progressReport);
        }

        public IDisposable MakeStep(double till)
        {
            return new ProgressStep(this, till);
        }

        public T MakeStepAndReturn<T>(Func<T> func, double till)
        {
            Assert.NotNull(func);

            using (MakeStep(till))
            {
                return func();
            }
        }

        public event Action<ProgressReport> ProgressChanged = _ => { };

        #endregion

        #region IWall
        
        public async Task Wrap(Action action)
        {
            if (Interlocked.CompareExchange(ref _busy, 1, 0) == 0)
            {
                ReportProgress(Started);
                _invalidateAction();
                try
                {
                    await Task.Factory.StartNew(action).ContinueWith(Callback).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    ExceptionCallback(e);
                }
            }
            else
            {
                _busyAction();
            }
        }

        public async Task Wrap(Func<Task> action)
        {
            if (Interlocked.CompareExchange(ref _busy, 1, 0) == 0)
            {
                ReportProgress(Started);
                _invalidateAction();
                try
                {
                    var task = action().ContinueWith(Callback);
                    await task.ConfigureAwait(false);
                    if (task.IsFaulted)
                    {
                        ExceptionCallback(task.Exception);
                    }
                }
                catch (Exception e)
                {
                    ExceptionCallback(e);
                }
            }
            else
            {
                _busyAction();
            }
        }

        public async Task Wrap(Func<string> action)
        {
            if (Interlocked.CompareExchange(ref _busy, 1, 0) == 0)
            {
                ReportProgress(Started);
                _invalidateAction();
                try
                {
                    await Task.Factory.StartNew(action).ContinueWith(ResultCallback).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    ExceptionCallback(e);
                }
            }
            else
            {
                _busyAction();
            }
        }

        public async Task Wrap(Func<Task<string>> action)
        {
            if (Interlocked.CompareExchange(ref _busy, 1, 0) == 0)
            {
                ReportProgress(Started);
                _invalidateAction();
                try
                {
                    var task = action().ContinueWith(ResultCallback);
                    await task.ConfigureAwait(false);
                    if (task.IsFaulted)
                    {
                        ExceptionCallback(task.Exception);
                    }
                }
                catch (Exception e)
                {
                    ExceptionCallback(e);
                }
            }
            else
            {
                _busyAction();
            }
        }

        public bool IsBusy()
        {
            return _busy == 1;
        }

        private void Callback(Task task)
        {
            try
            {
                ReportProgress(Ready);
            }
            finally
            {
                Interlocked.Decrement(ref _busy);
                _invalidateAction();
            }
        }

        private void ResultCallback(Task<string> task)
        {
            var result = task.Result;
            _resultAction(result);

            Callback(task);
        }

        private void ExceptionCallback(Exception e)
        {
            _resultAction(Results.UnknownError);
            Callback(null);
        }

        #endregion

        private ITracer Tracer => _tracer.Value;
        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();
    }
}