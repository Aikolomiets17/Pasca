﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using STATSTG = System.Runtime.InteropServices.ComTypes.STATSTG;

namespace Pasca.OneNoteAddIn
{
    internal class CcomStreamWrapper : IStream
    {
        public CcomStreamWrapper(Stream streamWrap)
        {
            _mStream = streamWrap;
        }

        public void Clone(out IStream ppstm)
        {
            ppstm = new CcomStreamWrapper(_mStream);
        }

        public void Commit(int grfCommitFlags)
        {
            _mStream.Flush();
        }

        public void CopyTo(IStream pstm, long cb, IntPtr pcbRead, IntPtr pcbWritten) {}

        public void LockRegion(long libOffset, long cb, int dwLockType)
        {
            throw new NotImplementedException();
        }

        public void Read(byte[] pv, int cb, IntPtr pcbRead)
        {
            Marshal.WriteInt64(pcbRead, _mStream.Read(pv, 0, cb));
        }

        public void Revert()
        {
            throw new NotImplementedException();
        }

        public void Seek(long dlibMove, int dwOrigin, IntPtr plibNewPosition)
        {
            long posMoveTo;
            Marshal.WriteInt64(plibNewPosition, _mStream.Position);
            switch (dwOrigin)
            {
            case (int)SeekType.Set:
            {
                /* STREAM_SEEK_SET */
                posMoveTo = dlibMove;
            }
                break;
            case (int)SeekType.Cur:
            {
                /* STREAM_SEEK_CUR */
                posMoveTo = _mStream.Position + dlibMove;
            }
                break;
            case (int)SeekType.End:
            {
                /* STREAM_SEEK_END */
                posMoveTo = _mStream.Length + dlibMove;
            }
                break;
            default:
                return;
            }
            if (posMoveTo >= 0 && posMoveTo < _mStream.Length)
            {
                _mStream.Position = posMoveTo;
                Marshal.WriteInt64(plibNewPosition, _mStream.Position);
            }
        }

        public void SetSize(long libNewSize)
        {
            _mStream.SetLength(libNewSize);
        }

        public void Stat(out STATSTG pstatstg, int grfStatFlag)
        {
            pstatstg = new STATSTG {
                cbSize = _mStream.Length
            };
            if ((grfStatFlag & 0x0001 /* STATFLAG_NONAME */) != 0)
            {
                return;
            }
            pstatstg.pwcsName = _mStream.ToString();
        }

        public void UnlockRegion(long libOffset, long cb, int dwLockType)
        {
            throw new NotImplementedException();
        }

        public void Write(byte[] pv, int cb, IntPtr pcbWritten)
        {
            Marshal.WriteInt64(pcbWritten, 0);
            _mStream.Write(pv, 0, cb);
            Marshal.WriteInt64(pcbWritten, cb);
        }

        ~CcomStreamWrapper()
        {
            _mStream.Dispose();
        }

        private readonly Stream _mStream;

        private enum SeekType
        {
            Set,
            Cur,
            End
        }
    }
}