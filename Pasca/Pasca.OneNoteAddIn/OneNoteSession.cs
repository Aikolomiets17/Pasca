﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Office.Interop.OneNote;
using Pasca.Common.DI;
using Pasca.Common.Exceptions;
using Pasca.Common.Extensions;
using Pasca.Common.Students;
using Pasca.Common.Trace;
using Pasca.Common.Types;
using Pasca.OneNoteAddIn.Adapters;
using Pasca.OneNoteAddIn.Auth;
using Pasca.OneNoteAddIn.Exceptions;
using Pasca.OneNoteAddIn.Extensions;
using Pasca.OneNoteAddIn.Localization;
using Pasca.OneNoteAddIn.Progress;
using Pasca.OneNoteAddIn.ReviewValidation;
using Pasca.OneNoteAddIn.Serialization;
using Pasca.OneNoteAddIn.Settings;
using Pasca.OneNoteAddIn.Trace;
using OneNoteApplication = Microsoft.Office.Interop.OneNote.Application;

namespace Pasca.OneNoteAddIn
{
    public class OneNoteSession : IOneNoteSession
    {
        private OneNoteSession(
            OneNoteApplication app,
            ApiFacade api,
            OneNoteSetting oneNoteSetting,
            IProgressProvider progressProvider,
            OneNoteTemplateProvider template)
        {
            App = app;
            _api = api;
            _oneNoteSetting = oneNoteSetting;
            _progressProvider = progressProvider;
            _template = template;
        }

        private OneNoteApplication App { get; set; }
        private IOneNoteSession IThis => this;

        private ApiFacade Api
        {
            get
            {
                _api.NotebookName = Notebook.Name();
                return _api;
            }
        }

        protected string NotebookId => _notebookId ?? (_notebookId = Notebook.Id());

        protected XElement Notebook
        {
            get
            {
                if (_notebookId == null)
                {
                    var xNotebooks = App.GetHierarchy(scope: HierarchyScope.hsNotebooks).ToXElement();
                    // структура тетрадки
                    var xNotebook = xNotebooks
                            .Elements()
                            .WithTagName("Notebook")
                            .WithValue("isCurrentlyViewed", "true")
                            ?.FirstOrDefault();

                    _notebookId = xNotebook?.Id();
                }

                return App.GetHierarchy(_notebookId).ToXElement();
            }
        }

        private ITracer Tracer => _tracer.Value;

        string IOneNoteSession.GetNotebookId()
        {
            return NotebookId;
        }

        PascaSessionSetting IOneNoteSession.GetSettings(string sessionName)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.GetSettings)} Start");

            Sync();

            var path = GetSettingsElement(sessionName)
                    ?.AttributeValue("pathCache");
            if (path == null)
            {
                return null;
            }
            using (var sr = new StreamReader(path))
            {
                return DR.Get<ISerializer<PascaSessionSetting>>().Deserialize(sr);
            }
        }

        async Task IOneNoteSession.UpdateOrCreateSession(PascaSessionSetting session)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.UpdateOrCreateSession)} Start");

            _progressProvider.ReportProgress(new ProgressReport(0.1, LocTags.SyncingNotebookHierarchy));
            Sync();

            var sessionName = session.SessionName();

            // create setting page

            _progressProvider.ReportProgress(new ProgressReport(0.2, LocTags.CreatingSessionSettingsPage));
            var dump = DR.Get<ISerializer<PascaSessionSetting>>().Serialize(session);

            var settings = GetSettingsElement(sessionName);
            if (settings != null)
            {
                var settingsPage1 = GetSettingsPage(sessionName);
                App.Delete(settingsPage1.Id(), NotebookId);
            }
            CreateSettingPage(sessionName, dump);
            App.CreateSection(
                NotebookId,
                null,
                TeacherOnly,
                Pasca,
                session.SessionName());

            _progressProvider.ReportProgress(new ProgressReport(0.5, LocTags.CreatingArtifactsAndReviewsSections));

            foreach (var student in session.SessionAnonymizedAuthors())
            {
                App.CreateSection(
                    NotebookId,
                    session.SessionArtifactSectionName(),
                    student.FullName,
                    session.SessionName());
            }

            using (_progressProvider.MakeStep(0.95))
            {
                await IThis.UpdateDashboards(session);
            }
        }

        async Task IOneNoteSession.DeleteSession(string sessionName)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.DeleteSession)} Start");

            _progressProvider.ReportProgress(new ProgressReport(0.1, LocTags.SyncingNotebookHierarchy));
            Sync();

            _progressProvider.ReportProgress(new ProgressReport(0.2, LocTags.DeletingArtifactsInTeacherOnly));

            var id = Notebook.GetSection(
                                 sessionName,
                                 TeacherOnly,
                                 Pasca)
                             ?.Id();

            if (id != null)
            {
                App.Delete(
                    id,
                    Notebook.GetSectionGroup(
                                TeacherOnly,
                                Pasca)
                            ?.Id());
            }

            id = Notebook.GetSectionGroup(
                                 TeacherOnly,
                                 Pasca,
                                 sessionName)
                             ?.Id();
            if (id != null)
            {
                App.Delete(
                    id,
                    Notebook.GetSectionGroup(
                                TeacherOnly,
                                Pasca)
                            ?.Id());
            }

            _progressProvider.ReportProgress(new ProgressReport(0.4, LocTags.DeletingArtifactsInContentLibrary));

            var list = await IThis.GetStudentsList();
            foreach (var student in list)
            {
                id = Notebook.GetSectionGroup(
                                 student.FullName,
                                 sessionName)
                             ?.Id();
                if (id != null)
                {
                    App.Delete(
                        id,
                        Notebook.GetSectionGroup(
                                    student.FullName)
                                ?.Id());
                }

                App.DeletePagesWithSameName(
                    Notebook.GetSection(
                        student.FullName,
                        Dashboard),
                    sessionName);
            }

            _progressProvider.ReportProgress(new ProgressReport(0.8, LocTags.DeletingSessionSettings));

            id = Notebook.GetSection(
                             sessionName,
                             TeacherOnly,
                             Pasca)
                         ?.Id();
            if (id != null)
            {
                App.Delete(
                    id,
                    Notebook.GetSection(
                                TeacherOnly,
                                Pasca)
                            ?.Id());
            }
        }

        async Task<IEnumerable<PersonBuilder>> IOneNoteSession.GetStudentsList()
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.GetStudentsList)} Start");

            return await Api.GetClassStudents();
        }

        async Task<IEnumerable<PersonBuilder>> IOneNoteSession.GetTeachersList()
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.GetTeachersList)} Start");

            return await Api.GetClassTeachers();
        }

        IEnumerable<string> IOneNoteSession.GetSessionsList()
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.GetSessionsList)} Start");

            Sync();

            return Notebook
                    .GetSectionGroup(TeacherOnly)
                    ?.GetSectionGroup(Pasca)
                    ?.Sections(false)
                    .Select(q => q.Name())
                    ?? Enumerable.Empty<string>();
        }

        async Task IOneNoteSession.UpdateDashboards(PascaSessionSetting session)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.UpdateDashboards)} Start");

            _progressProvider.ReportProgress(new ProgressReport(0.1, LocTags.SyncingNotebookHierarchy));
            Sync();

            //_progressProvider.ReportProgress(new ProgressReport(0.2, LocTags.CreatingStudentsSectionsViaWebApi));
            using (_progressProvider.MakeStep(0.4))
            {
                await Api.CreateStudentsDashboardSections(session);
            }
            Tracer.WriteLine(TracerTags.OneNote, "api done, syncing");
            _progressProvider.ReportProgress(new ProgressReport(0.45, LocTags.ApiRequestedWaitingForSync));

            Sync();
            if (!new SleepWait(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(15), TimeSpan.FromMinutes(5))
                        .Wait(
                            () =>
                            {
                                Sync();
                                return session.SessionAnonymizedAuthors().Union(session.SessionAnonymizedReviewers())
                                              .All(
                                                  student => Notebook.GetSection(
                                                                 Dashboard,
                                                                 student.FullName) != null);
                            }))
            {
                throw new TimeoutException("Could not locate created via OneNote REST Api sectionGroups");
            }
            Tracer.WriteLine(TracerTags.OneNote, "api done, synced");
            _progressProvider.ReportProgress(new ProgressReport(0.7, LocTags.PlacingDashboards));

            var receivers = session.SessionAnonymizedAuthors().Union(session.SessionAnonymizedReviewers()).ToArray();
            for (var i = 0; i < receivers.Length; i++)
            {
                using (_progressProvider.MakeStep(0.7 + 0.3 * i / receivers.Length))
                {
                    DistributeDashboard(session, receivers[i]);
                }
            }
        }

        async Task<IEnumerable<string>> IOneNoteSession.DistributeAssignment(
            IEnumerable<string> assignmentPageIds,
            PascaSessionSetting session)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.DistributeAssignment)} Start");

            _progressProvider.ReportProgress(new ProgressReport(0.1, LocTags.SyncingNotebookHierarchy));
            Sync();

            using (_progressProvider.MakeStep(0.3))
            {
                await Api.CreateAssignmentSections(session);
            }
            Tracer.WriteLine(TracerTags.OneNote, "api done, syncing");
            _progressProvider.ReportProgress(new ProgressReport(0.3, LocTags.ApiRequestedWaitingForSync));

            Sync();
            if (!new SleepWait(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(15), TimeSpan.FromMinutes(5))
                        .Wait(
                            () =>
                            {
                                Sync();
                                return session.SessionAnonymizedAuthors().Union(session.SessionAnonymizedReviewers())
                                              .All(
                                                  student => Notebook.GetSection(
                                                                 session.SessionAssignmentSectionName(),
                                                                 student.FullName,
                                                                 session.SessionName()) != null);
                            }))
            {
                throw new TimeoutException("Could not locate created via OneNote REST Api sectionGroups");
            }

            _progressProvider.ReportProgress(new ProgressReport(0.5, LocTags.CopyingAssignments));
            Tracer.WriteLine(TracerTags.OneNote, "api done, copying assignment to session section");

            App.CreateSection(
                NotebookId,
                session.SessionAssignmentSectionName(),
                TeacherOnly,
                Pasca,
                session.SessionName());
            var sessionSection = Notebook.GetSection(
                session.SessionAssignmentSectionName(),
                TeacherOnly,
                Pasca,
                session.SessionName());

            var pageIds = assignmentPageIds.ToArray();
            var newPageIds = new List<string>();
            foreach (var assignment in pageIds)
            {
                try
                {
                    var page = App.GetPageContent(assignment).ToXElement();
                    App.DeletePagesWithSameName(sessionSection, page.Name());
                    App.CreateSection(
                        NotebookId,
                        session.SessionAssignmentSectionName(),
                        TeacherOnly,
                        Pasca,
                        session.SessionName());
                    newPageIds.Add(
                        App.ClonePage(
                            Notebook,
                            page,
                            page.Name(),
                            session.SessionAssignmentSectionName(),
                            TeacherOnly,
                            Pasca,
                            session.SessionName()));
                }
                catch { }
            }

            var receivers = session.SessionAnonymizedAuthors().Union(session.SessionAnonymizedReviewers()).ToArray();
            for (var i = 0; i < receivers.Length; i++)
            {
                _progressProvider.ReportProgress(
                    new ProgressReport(0.6 + i / (double)receivers.Length, LocTags.PlacingAssignments));
                foreach (var assignmentPageId in pageIds)
                {
                    try
                    {
                        var page = App.GetPageContent(assignmentPageId).ToXElement();
                        App.CreateSection(
                            NotebookId,
                            session.SessionAssignmentSectionName(),
                            receivers[i].FullName,
                            session.SessionName());
                        var section = Notebook.GetSection(
                            session.SessionAssignmentSectionName(),
                            receivers[i].FullName,
                            session.SessionName()
                        );
                        App.DeletePagesWithSameName(section, page.Name());
                        App.ClonePage(
                            Notebook,
                            page,
                            page.Name(),
                            session.SessionAssignmentSectionName(),
                            receivers[i].FullName,
                            session.SessionName());
                    }
                    catch { }
                }
                App.CreateSection(
                    NotebookId,
                    session.SessionArtifactSectionName(),
                    receivers[i].FullName,
                    session.SessionName());
            }

            return newPageIds;
        }

        async Task IOneNoteSession.CollectArtifacts(PascaSessionSetting session)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.CollectArtifacts)} Start");

            _progressProvider.ReportProgress(new ProgressReport(0.1, LocTags.SyncingNotebookHierarchy));
            Sync();

            var authors = session.SessionAnonymizedAuthors();
            for (var i = 0; i < authors.Count; i++)
            {
                _progressProvider.ReportProgress(
                    new ProgressReport(0.1 + (i / (double)authors.Count) * 0.8, LocTags.CollectingArtifacts));

                var artifactPages = Notebook
                                            .GetSection(
                                                session.SessionArtifactSectionName(),
                                                authors[i].FullName,
                                                session.SessionName())
                                            ?.Pages()
                                            ?.Select(p => App.GetPageContent(p.Id()).ToXElement())
                                    ?? Enumerable.Empty<XElement>();
                App.CreateSection(
                    NotebookId,
                    session.SessionArtifactSectionName(),
                    TeacherOnly,
                    Pasca,
                    session.SessionName(),
                    authors[i].FullName);
                var section = Notebook.GetSection(
                    session.SessionArtifactSectionName(),
                    TeacherOnly,
                    Pasca,
                    session.SessionName(),
                    authors[i].FullName);
                foreach (var artifactPage in artifactPages)
                {
                    App.DeletePagesWithSameName(section, artifactPage.Name());
                    App.ClonePage(
                        Notebook,
                        artifactPage,
                        null,
                        session.SessionArtifactSectionName(),
                        TeacherOnly,
                        Pasca,
                        session.SessionName(),
                        authors[i].FullName);
                }
            }

            using (_progressProvider.MakeStep(1.0))
            {
                await IThis.UpdateDashboards(session);
            }
        }

        string IOneNoteSession.GetCurrentPageId()
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.GetCurrentPageId)} Start");

            Sync();

            return Notebook.Pages(true)
                           .WithValue("isCurrentlyViewed", "true")
                           .FirstOrDefault()
                           ?.Id();
        }

        string IOneNoteSession.GetElementName(string id, HierarchyElement elementKind)
        {
            switch (elementKind)
            {
            case HierarchyElement.heNotebooks:
                return Notebook.Id().OrdinalEquals(id) ? Notebook.Name() : null;
            case HierarchyElement.heSectionGroups:
                return Notebook.SectionGroups(true).WithId(id).FirstOrDefault()?.Name();
            case HierarchyElement.heSections:
                return Notebook.Sections(true).WithId(id).FirstOrDefault()?.Name();
            case HierarchyElement.hePages:
                return Notebook.Pages(true).WithId(id).FirstOrDefault()?.Name();
            default:
                return Notebook.Descendants().WithId(id).FirstOrDefault()?.Name();
            }
        }

        void IOneNoteSession.DistributeArtifactsAndTemplates(PascaSessionSetting session)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.DistributeArtifactsAndTemplates)} Start");

            _progressProvider.ReportProgress(new ProgressReport(0.1, LocTags.SyncingNotebookHierarchy));
            Sync();

            var authors = session.SessionAnonymizedAuthors();
            var reviewers = session.SessionAnonymizedReviewers();
            var mapping = session.SessionMapping();

            for (var i = 0; i < authors.Count; i++)
            {
                var author = authors[i];
                var artifactPages = Notebook
                                            .GetSection(
                                                session.SessionArtifactSectionName(),
                                                TeacherOnly,
                                                Pasca,
                                                session.SessionName(),
                                                author.FullName)
                                            ?.Pages()
                                            ?.Select(p => App.GetPageContent(p.Id()).ToXElement())
                                    ?? Enumerable.Empty<XElement>();

                List<int> revs;
                if (mapping.TryGetValue(author.Number, out revs))
                {
                    for (var j = 0; j < revs.Count; j++)
                    {
                        _progressProvider.ReportProgress(
                            new ProgressReport(
                                0.1 + 0.9 * (i / (double)authors.Count + 1.0 / authors.Count * j / revs.Count),
                                LocTags.PlacingArtifactsAndTemplates));

                        var reviewerNumber = revs[j];
                        var reviewer = reviewers.With(reviewerNumber);
                        if (reviewer == null)
                        {
                            Tracer.WriteLine(TracerTags.OneNote, $"Reviewer with number {reviewerNumber} does not exist in session reviewers");
                            continue;
                        }

                        App.CreateSection(
                            NotebookId,
                            author.ToAnonymizedString(),
                            reviewer.FullName,
                            session.SessionName(),
                            session.SessionReviewArtifactsSectionGroupName());
                        var section = Notebook.GetSection(
                            author.ToAnonymizedString(),
                            reviewer.FullName,
                            session.SessionName(),
                            session.SessionReviewArtifactsSectionGroupName());

                        foreach (var artifactPage in artifactPages)
                        {
                            App.DeletePagesWithSameName(section, artifactPage.Name());
                            App.ClonePage(
                                Notebook,
                                artifactPage,
                                null,
                                author.ToAnonymizedString(),
                                reviewer.FullName,
                                session.SessionName(),
                                session.SessionReviewArtifactsSectionGroupName());
                        }

                        App.DeletePagesWithSameName(section, session.SessionReviewTemplatePageName());
                        var templatePageId = App.CreatePage(section.Id());
                        App.UpdatePageContent(GetTemplateTemplate(session, templatePageId));
                        App.RenamePage(templatePageId, session.SessionReviewTemplatePageName());
                    }
                }
            }
        }

        async Task IOneNoteSession.CollectReviews(PascaSessionSetting session)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.CollectReviews)} Start");

            _progressProvider.ReportProgress(new ProgressReport(0.1, LocTags.SyncingNotebookHierarchy));
            Sync();

            var authors = session.SessionAnonymizedAuthors();
            var reviewers = session.SessionAnonymizedReviewers();
            var mapping = session.SessionMapping();

            for (var i = 0; i < authors.Count; i++)
            {
                var author = authors[i];

                List<int> revs;
                if (mapping.TryGetValue(author.Number, out revs))
                {
                    for (var j = 0; j < revs.Count; j++)
                    {
                        _progressProvider.ReportProgress(
                            new ProgressReport(
                                0.1 + 0.7 * (i / (double)authors.Count + 1.0 / authors.Count * j / revs.Count),
                                LocTags.CollectingReviews));

                        var reviewerNumber = revs[j];
                        var reviewer = reviewers.With(reviewerNumber);
                        if (reviewer == null)
                        {
                            Tracer.WriteLine(TracerTags.OneNote, $"Reviewer with number {reviewerNumber} does not exist in session reviewers");
                            continue;
                        }

                        var reviewPages = Notebook
                                                    .GetSection(
                                                        author.ToAnonymizedString(),
                                                        reviewer.FullName,
                                                        session.SessionName(),
                                                        session.SessionReviewArtifactsSectionGroupName())
                                                    ?.Pages()
                                                    ?.Select(p => App.GetPageContent(p.Id()).ToXElement())
                                            ?? Enumerable.Empty<XElement>();

                        App.CreateSection(
                            NotebookId,
                            reviewer.FullName,
                            TeacherOnly,
                            Pasca,
                            session.SessionName(),
                            author.FullName,
                            session.SessionReviewArtifactsSectionGroupName());
                        var section = Notebook.GetSection(
                            reviewer.FullName,
                            TeacherOnly,
                            Pasca,
                            session.SessionName(),
                            author.FullName,
                            session.SessionReviewArtifactsSectionGroupName());
                        foreach (var reviewPage in reviewPages)
                        {
                            App.DeletePagesWithSameName(section, reviewPage.Name());

                            App.ClonePage(
                                Notebook,
                                reviewPage,
                                null,
                                reviewer.FullName,
                                TeacherOnly,
                                Pasca,
                                session.SessionName(),
                                author.FullName,
                                session.SessionReviewArtifactsSectionGroupName());
                        }
                    }
                }
            }

            using (_progressProvider.MakeStep(1.0))
            {
                await IThis.UpdateDashboards(session);
            }
        }

        void IOneNoteSession.NavigateToSessionSectionGroup(PascaSessionSetting session)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.CollectReviews)} Start");

            _progressProvider.ReportProgress(new ProgressReport(0.3, LocTags.SyncingNotebookHierarchy));
            Sync();
            _progressProvider.ReportProgress(new ProgressReport(0.5, LocTags.Navigating));

            var navId = Notebook.GetSectionGroup(
                TeacherOnly,
                Pasca,
                session.SessionName())?.Id();
            if (navId != null)
            {
                App.NavigateTo(navId);
            }
        }

        IEnumerable<ITreeItem<string>> IOneNoteSession.GetHierarchy(GetHierarchyRootType rootType, bool includeDeleted)
        {
            return new[] {
                GetRoot(rootType)
                .ToTree(null, includeDeleted)
            };
        }

        private XElement GetRoot(GetHierarchyRootType rootType)
        {
            switch (rootType)
            {
            case GetHierarchyRootType.Notebook:
                return Notebook;
            case GetHierarchyRootType.TeacherOnly:
                return Notebook.GetSectionGroup(TeacherOnly);
            default:
                throw new EnumValueException(typeof(GetHierarchyRootType), rootType);
            }
        }

        string IOneNoteSession.CreateAssignmentSectionUrl(PascaSessionSetting session)
        {
            return App.GetPageContent(session.SessionAssignment()).ToXElement()
                            ?.CreateOneNoteUrl();
        }

        string IOneNoteSession.CreateArtifactsSectionUrl(PascaSessionSetting session)
        {
            return Notebook.GetSectionGroup(
                TeacherOnly,
                Pasca,
                session.SessionName())?.CreateOneNoteUrl();
        }

        void IOneNoteSession.PlaceReports(PascaSessionSetting session)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.CollectReviews)} Start");

            _progressProvider.ReportProgress(new ProgressReport(0.01, LocTags.SyncingNotebookHierarchy));
            Sync();

            _progressProvider.ReportProgress(new ProgressReport(0.05, LocTags.PlacingArtifactsReport));
            PlaceArtifactsReport(session);

            _progressProvider.ReportProgress(new ProgressReport(0.2, LocTags.PlacingAuthorsReviewsReport));
            PlaceAuthorsReviewsReport(session);

            _progressProvider.ReportProgress(new ProgressReport(0.5, LocTags.PlacingReviewersReviewsReport));
            PlaceReviewersReviewsReport(session);

            _progressProvider.ReportProgress(new ProgressReport(0.7, LocTags.PlacingGradesReport));
            PlaceGradesReport(session);
        }

        ReviewValidationVerdictArgs IOneNoteSession.CheckReviewPageValid(PascaSessionSetting session, string pageId)
        {
            Tracer.WriteLine(TracerTags.OneNote, $"{nameof(IOneNoteSession.CollectReviews)} Start");

            _progressProvider.ReportProgress(new ProgressReport(0.1, LocTags.SyncingNotebookHierarchy));
            Sync();

            if (pageId == null)
            {
                return new ReviewValidationVerdictArgs(ReviewValidationVerdict.PageNotFound);
            }

            var page = XElement.Parse(App.GetPageContent(pageId));
            var table = page.Descendants().WithTagName("Table").FirstOrDefault();
            if (table == null)
            {
                return new ReviewValidationVerdictArgs(ReviewValidationVerdict.TableNotFound);
            }
            var criteria = session.SessionAssessmentCriteria();

            _progressProvider.ReportProgress(new ProgressReport(0.25, LocTags.ValidatingReview));
            try
            {
                var idx = 1;
                foreach (var criteriaGroup in criteria.CriteriaGroups)
                {
                    idx++;
                    foreach (var criterion in criteriaGroup.Criteria)
                    {
                        object mark;
                        if (!criterion.Validate(table.GetCell(4, idx), out mark))
                        {
                            return new TableErrorVerdictArgs(4, idx);
                        }
                        idx++;
                    }
                }
            }
            catch
            {
                return new TableErrorVerdictArgs(0, 0);
            }
            return new ReviewValidationVerdictArgs(ReviewValidationVerdict.Ok);
        }

        public double GetGrade(PascaSessionSetting session, AnonymizedPerson author)
        {
            var criteria = session.SessionAssessmentCriteria();
            var reviewers = session.SessionAnonymizedReviewers();
            var reviewerNumbers = session.SessionMapping()[author.Number];
            var aggrMarks = new List<double>();
            foreach (var reviewerNumber in reviewerNumbers)
            {
                var reviewer = reviewers.With(reviewerNumber);
                var pageId = Notebook.GetSection(
                                         reviewer.FullName,
                                         TeacherOnly,
                                         Pasca,
                                         session.SessionName(),
                                         author.FullName,
                                         session.SessionReviewArtifactsSectionGroupName())
                                     ?.GetPage(session.SessionReviewTemplatePageName(), false)
                                     ?.Id();
                if (pageId == null)
                {
                    Tracer.WriteLine(
                        TracerTags.OneNote,
                        $"Review page id is null. Skip (author={author},reviewer={reviewer}).");
                    continue;
                }
                var page = XElement.Parse(App.GetPageContent(pageId));
                var table = page.Descendants().WithTagName("Table").FirstOrDefault();
                if (table == null)
                {
                    Tracer.WriteLine(
                        TracerTags.OneNote,
                        $"Table not found in review page. Skip (author={author},reviewer={reviewer}).");
                    continue;
                }
                try
                {
                    var reviewerMarks = new double[criteria.CriteriaGroups.Count];
                    var idx = 1;
                    for (var i = 0; i < criteria.CriteriaGroups.Count; i++)
                    {
                        idx++;
                        var criteriaGroup = criteria.CriteriaGroups[i];
                        var marks = new object[criteriaGroup.Criteria.Count];
                        for (var j = 0; j < criteriaGroup.Criteria.Count; j++)
                        {
                            var criterion = criteriaGroup.Criteria[j];
                            object mark;
                            if (!criterion.Validate(table.GetCell(4, idx), out mark))
                            {
                                Tracer.WriteLine(
                                    TracerTags.OneNote,
                                    $"Criterion {criterion.RangeDescription} does not validate {mark}");
                                throw new ValidationException(
                                    $"Criterion {criterion.RangeDescription} does not validate {mark}");
                            }
                            marks[j] = mark;
                            idx++;
                        }
                        reviewerMarks[i] = criteriaGroup.Aggregate(marks);
                    }
                    aggrMarks.Add(criteria.Aggregate(reviewerMarks));
                }
                catch
                {
                    Tracer.WriteLine(
                        TracerTags.OneNote,
                        $"Table does not meet assessment criteria. Skip (author={author},reviewer={reviewer}).");
                    //continue;
                }
            }
            return aggrMarks.Average(0);
        }

        public double GetGrade(PascaSessionSetting session, AnonymizedPerson author, AnonymizedPerson reviewer)
        {
            var criteria = session.SessionAssessmentCriteria();
            var pageId = Notebook.GetSection(
                                         reviewer.FullName,
                                         TeacherOnly,
                                         Pasca,
                                         session.SessionName(),
                                         author.FullName,
                                         session.SessionReviewArtifactsSectionGroupName())
                                     ?.GetPage(session.SessionReviewTemplatePageName(), false)
                                     ?.Id();
            if (pageId == null)
            {
                Tracer.WriteLine(
                    TracerTags.OneNote,
                    $"Review page id is null. Skip (author={author},reviewer={reviewer}).");
                return double.NaN;
            }
            var page = XElement.Parse(App.GetPageContent(pageId));
            var table = page.Descendants().WithTagName("Table").FirstOrDefault();
            if (table == null)
            {
                Tracer.WriteLine(
                    TracerTags.OneNote,
                    $"Table not found in review page. Skip (author={author},reviewer={reviewer}).");
                return double.NaN;
            }
            try
            {
                var reviewerMarks = new double[criteria.CriteriaGroups.Count];
                var idx = 1;
                for (var i = 0; i < criteria.CriteriaGroups.Count; i++)
                {
                    idx++;
                    var criteriaGroup = criteria.CriteriaGroups[i];
                    var marks = new object[criteriaGroup.Criteria.Count];
                    for (var j = 0; j < criteriaGroup.Criteria.Count; j++)
                    {
                        var criterion = criteriaGroup.Criteria[j];
                        object mark;
                        if (!criterion.Validate(table.GetCell(4, idx), out mark))
                        {
                            Tracer.WriteLine(
                                TracerTags.OneNote,
                                $"Criterion {criterion.RangeDescription} does not validate {mark}");
                            throw new ValidationException(
                                $"Criterion {criterion.RangeDescription} does not validate {mark}");
                        }
                        marks[j] = mark;
                        idx++;
                    }
                    reviewerMarks[i] = criteriaGroup.Aggregate(marks);
                }
                return criteria.Aggregate(reviewerMarks);
            }
            catch
            {
                Tracer.WriteLine(
                    TracerTags.OneNote,
                    $"Table does not meet assessment criteria. Skip (author={author},reviewer={reviewer}).");
                return double.NaN;
            }
        }

        ~OneNoteSession()
        {
            App = null;
        }

        private void PlaceArtifactsReport(PascaSessionSetting session)
        {
            var section = Notebook.GetSection(
                session.SessionName(),
                TeacherOnly,
                Pasca);
            if (section == null)
            {
                App.CreateSection(
                    NotebookId,
                    session.SessionName(),
                    TeacherOnly,
                    Pasca);
                section = Notebook.GetSection(
                    session.SessionName(),
                    TeacherOnly,
                    Pasca);
            }
            App.DeletePagesWithSameName(section, session.SessionArtifactsReportPageName());

            var reportPageId = App.CreatePage(section.Id());
            App.UpdatePageContent(GetArtifactsReportPageContent(reportPageId, session));
        }

        private void PlaceAuthorsReviewsReport(PascaSessionSetting session)
        {
            var section = Notebook.GetSection(
                session.SessionName(),
                TeacherOnly,
                Pasca);
            if (section == null)
            {
                App.CreateSection(
                    NotebookId,
                    session.SessionName(),
                    TeacherOnly,
                    Pasca);
                section = Notebook.GetSection(
                    session.SessionName(),
                    TeacherOnly,
                    Pasca);
            }
            App.DeletePagesWithSameName(section, session.SessionAuthorsReviewsReportPageName());

            var reportPageId = App.CreatePage(section.Id());
            App.UpdatePageContent(GetAuthorsReviewsReportPageContent(reportPageId, session));
        }

        private void PlaceReviewersReviewsReport(PascaSessionSetting session)
        {
            var section = Notebook.GetSection(
                session.SessionName(),
                TeacherOnly,
                Pasca);
            if (section == null)
            {
                App.CreateSection(
                    NotebookId,
                    session.SessionName(),
                    TeacherOnly,
                    Pasca);
                section = Notebook.GetSection(
                    session.SessionName(),
                    TeacherOnly,
                    Pasca);
            }
            App.DeletePagesWithSameName(section, session.SessionReviewersReviewsReportPageName());

            var reportPageId = App.CreatePage(section.Id());
            App.UpdatePageContent(GetReviewersReviewsReportPageContent(reportPageId, session));
        }

        private void PlaceGradesReport(PascaSessionSetting session)
        {
            var section = Notebook.GetSection(
                session.SessionName(),
                TeacherOnly,
                Pasca);
            if (section == null)
            {
                App.CreateSection(
                    NotebookId,
                    session.SessionName(),
                    TeacherOnly,
                    Pasca);
                section = Notebook.GetSection(
                    session.SessionName(),
                    TeacherOnly,
                    Pasca);
            }
            App.DeletePagesWithSameName(section, session.SessionGradesReportPageName());

            var reportPageId = App.CreatePage(section.Id());
            App.UpdatePageContent(GetGradesReportPageContent(reportPageId, session));
        }

        private void Sync()
        {
            App.SyncHierarchy(Notebook.Id());
        }


        /*
         * OneNote-specific xml manipulations
         */

        public XElement GetSettingsPage(string sessionName)
        {
            var settingPageId = Notebook
                    .GetSectionGroup(TeacherOnly)
                    ?.GetSectionGroup(Pasca)
                    ?.GetSection(sessionName)
                    ?.GetPage(CreateSettingsPageName(sessionName))
                    ?.Id();

            if (settingPageId == null)
            {
                return null;
            }

            return App.GetPageContent(settingPageId).ToXElement();
        }

        public XElement GetSettingsElement(string sessionName)
        {
            return GetSettingsPage(sessionName)
                    ?.Descendants()
                    .WithTagName("InsertedFile")
                    .FirstOrDefault();
        }

        private string CreateSettingsPageName(string sessionName)
        {
            return sessionName + SettingsPostfix;
        }

        private void PlaceSettings(string sessionName, string dump)
        {
            var tempFile = Path.GetTempFileName();
            using (var sw = new StreamWriter(tempFile))
            {
                sw.Write(dump);
            }

            var settingsPage = GetSettingsPage(sessionName);

            var pageAdapter = new PageAdapter(settingsPage.ToString());
            var outline = new Outline {
                OEChildren = new[] {
                    new OEChildren {
                        Items = new object[] {
                            new OE {
                                Items = new object[] {
                                    new InsertedFile {
                                        pathSource = tempFile,
                                        preferredName = sessionName + ".txt"
                                    }
                                }
                            }
                        }
                    }
                }
            };
            pageAdapter.Page.Items = new PageObject[] {
                outline
            };

            App.UpdateContent(pageAdapter, NotebookId);
        }

        public void CreateSettingPage(string sessionName, string dump)
        {
            var sessionSection = Notebook.GetSection(sessionName, TeacherOnly, Pasca);
            if (sessionSection == null)
            {
                App.CreateSection(NotebookId, sessionName, TeacherOnly, Pasca);
                sessionSection = Notebook.GetSection(sessionName, TeacherOnly, Pasca);
                if (sessionSection == null)
                {
                    // something bad happened
                    throw new InvalidOperationException("Session section is null even after tried to create it");
                }
            }
            var pageId = App.CreatePage(sessionSection.Id());
            App.RenamePage(pageId, CreateSettingsPageName(sessionName));

            PlaceSettings(sessionName, dump);
        }

        #region ReviewTemplate

        private string GetTemplateTemplate(PascaSessionSetting setting, string pageId)
        {
            var s = _template.ReviewTemplate
                             .Replace("{PageId}", pageId)
                             .Replace("{Review Title}", setting.SessionReviewTemplatePageName());
            var rows = string.Empty;
            var assessmentCriteria = setting.SessionAssessmentCriteria();
            var criteriaGroupN = 0;
            foreach (var criteriaGroup in assessmentCriteria.CriteriaGroups)
            {
                rows += _template.CriteriaGroupHeaderRow
                                 .Replace("{Number}", (++criteriaGroupN).ToString())
                                 .Replace("{GroupName}", criteriaGroup.GroupName);
                var criterionN = 0;
                foreach (var criterion in criteriaGroup.Criteria)
                {
                    rows += _template.Row5
                                     .Replace("{Content1}", string.Empty)
                                     .Replace("{Content2}", (++criterionN).ToString())
                                     .Replace("{Content3}", criterion.Description)
                                     .Replace("{Content4}", criterion.RangeDescription)
                                     .Replace("{Content5}", string.Empty);
                }
            }
            s = s.Replace("{Rows}", rows);
            return s;
        }

        #endregion

        private readonly ApiFacade _api;
        private readonly IProgressProvider _progressProvider;
        private readonly OneNoteSetting _oneNoteSetting;
        private readonly OneNoteTemplateProvider _template;
        private readonly Lazy<ITracer> _tracer = DR.GetLazy<ITracer>();

        private string _notebookId;

        public class Factory
        {
            public Factory(
                OneNoteApplication app,
                ApiFacade.Factory apiFactory,
                IProgressProvider progressProvider)
            {
                Assert.NotNull(app);
                Assert.NotNull(apiFactory);
                Assert.NotNull(progressProvider);

                _app = app;
                _apiFactory = apiFactory;
                _progressProvider = progressProvider;

                _templateProvider = new OneNoteTemplateProvider();
            }

            ~Factory()
            {
                _app = null;
            }

            public OneNoteSession Create(OneNoteSetting oneNoteSetting)
            {
                Assert.NotNull(oneNoteSetting);

                return new OneNoteSession(
                    _app,
                    _apiFactory.Create(oneNoteSetting),
                    oneNoteSetting,
                    _progressProvider,
                    _templateProvider);
            }

            private readonly ApiFacade.Factory _apiFactory;
            private readonly IProgressProvider _progressProvider;

            private readonly OneNoteTemplateProvider _templateProvider;
            private OneNoteApplication _app;
        }

        #region Lock

        public async Task LockArtifacts(PascaSessionSetting session)
        {
            _progressProvider.ReportProgress(new ProgressReport(0.1, LocTags.LockingArtifactsViaWebApi));
            using (_progressProvider.MakeStep(1.0))
            {
                await Api.LockStudentsArtifactsSections(session);
            }
        }

        public async Task<bool> AreArtifactsLocked(PascaSessionSetting session)
        {
            return await Api.AreStudentArtifactsSectionsLocked(session);
        }

        public async Task LockReviews(PascaSessionSetting session)
        {
            _progressProvider.ReportProgress(new ProgressReport(0.1, LocTags.LockingReviewsViaWebApi));
            using (_progressProvider.MakeStep(1.0))
            {
                await Api.LockStudentsReviewsSections(session);
            }
        }

        public async Task<bool> AreReviewsLocked(PascaSessionSetting session)
        {
            return await Api.AreStudentsReviewsSectionsLocked(session);
        }

        #endregion

        #region Settings shortcuts

        private string TeacherOnly => _oneNoteSetting.SessionTeacherOnlySectionGroupPostfix();
        //private string LibraryContent => _setting.SessionContentLibrarySectionGroupPostfix();
        private string Pasca => _oneNoteSetting.PascaSectionName();
        private string SettingsPostfix => _oneNoteSetting.SettingsPagePostfix();
        private string Dashboard => _oneNoteSetting.DashboardSectionName();

        #endregion

        #region Dashboard

        private void DistributeDashboard(PascaSessionSetting session, AnonymizedPerson receiver)
        {
            App.CreateSection(
                NotebookId,
                Dashboard,
                receiver.FullName);

            var dashboardSection = Notebook.GetSection(
                Dashboard,
                receiver.FullName);
            var pageId = dashboardSection
                    .GetPage(session.SessionName())
                    ?.Id();
            if (pageId != null)
            {
                App.Delete(
                    pageId,
                    dashboardSection.Id());
            }

            App.ClonePage(
                Notebook,
                _progressProvider.MakeStepAndReturn(() => GetDashboardTemplate(session, receiver, pageId).ToXElement(), 1.0),
                session.SessionName(),
                Dashboard,
                receiver.FullName);
        }

        private string CreateSubmissionBeginShading(PascaSessionSetting session)
        {
            return string.Format(
                _template.ShadingTemplate,
                session.SessionSubmissionBegin().Passed()
                    ? Colors.Passed
                    : Colors.Default);
        }

        private string CreateSubmissionEndShading(PascaSessionSetting session, Person student)
        {
            if (Notebook.GetSection(
                    session.SessionArtifactSectionName(),
                    TeacherOnly,
                    Pasca,
                    session.SessionName(),
                    student.FullName) != null)
            {
                return string.Format(_template.ShadingTemplate, Colors.Passed);
            }
            if (!session.SessionSubmissionBegin().Passed())
            {
                return string.Format(_template.ShadingTemplate, Colors.Default);
            }
            return string.Format(
                _template.ShadingTemplate,
                session.SessionSubmissionEnd().Passed()
                    ? Colors.Fail
                    : Colors.Warning);
        }

        private string CreateReviewBeginShading(PascaSessionSetting session)
        {
            return string.Format(
                _template.ShadingTemplate,
                session.SessionReviewBegin().Passed()
                    ? Colors.Passed
                    : Colors.Default);
        }

        private string CreateReviewEndShading(PascaSessionSetting session, AnonymizedPerson student)
        {
            if (GetReviewsDoneCount(session, student) == GetReviewsNeedCount(session, student))
            {
                return string.Format(_template.ShadingTemplate, Colors.Passed);
            }
            if (!session.SessionReviewBegin().Passed())
            {
                return string.Format(_template.ShadingTemplate, Colors.Default);
            }
            return string.Format(
                _template.ShadingTemplate,
                session.SessionReviewEnd().Passed()
                    ? Colors.Fail
                    : Colors.Warning);
        }

        private string CreateShadingReviewsNeedCollected(PascaSessionSetting session, AnonymizedPerson student)
        {
            return string.Format(
                _template.ShadingTemplate,
                GetReviewsCollectedCount(session, student) >= GetReviewsCollectedNeedCount(session, student)
                    ? Colors.Passed
                    : Colors.Default);
        }

        private string GetArtifactDone(PascaSessionSetting session, Person student)
        {
            return
                    Notebook.GetSection(
                        session.SessionArtifactSectionName(),
                        TeacherOnly,
                        Pasca,
                        session.SessionName(),
                        student.FullName) != null
                        ? _template.Yes
                        : _template.No;
        }

        private string CreateArtifactDoneShading(PascaSessionSetting session, Person student)
        {
            if (Notebook.GetSection(
                session.SessionArtifactSectionName(),
                student.FullName,
                session.SessionName())?.Pages()?.Any() ?? false)
            {
                return string.Format(_template.ShadingTemplate, Colors.Passed);
            }
            if (session.SessionSubmissionEnd().Passed())
            {
                return string.Format(_template.ShadingTemplate, Colors.Fail);
            }
            return string.Format(
                _template.ShadingTemplate,
                session.SessionSubmissionBegin().Passed()
                    ? Colors.Warning
                    : Colors.Default);
        }

        private int GetReviewsDoneCount(PascaSessionSetting session, AnonymizedPerson student)
        {
            var authors = session.SessionAnonymizedAuthors();

            var count = 0;
            var mapping = session.SessionMapping().ToList();
            for (int i = 0; i < mapping.Count; i++)
            {
                var authorReviewersNumbers = mapping[i];
                var author = authors.With(authorReviewersNumbers.Key);
                if (author == null)
                {
                    Tracer.WriteLine(TracerTags.OneNote, $"Reviewer with number {authorReviewersNumbers.Key} does not exist in session reviewers");
                    count++;
                    continue;
                }

                if (authorReviewersNumbers.Value.Contains(student.Number))
                {
                    var reviewId = Notebook.GetSection(
                        student.FullName,
                        TeacherOnly,
                        Pasca,
                        session.SessionName(),
                        author.FullName,
                        session.SessionReviewArtifactsSectionGroupName())
                        ?.GetPage(
                            session.SessionReviewTemplatePageName())
                            ?.Id();

                    using (_progressProvider.MakeStep(i / (double)mapping.Count))
                    {
                        if (reviewId != null &&
                            IThis.CheckReviewPageValid(session, reviewId).Verdict ==
                            ReviewValidationVerdict.Ok)
                        {
                            count++;
                        }
                    }
                }
            }
            return count;
        }

        private int GetReviewsNeedCount(PascaSessionSetting session, AnonymizedPerson student)
        {
            return session.SessionMapping().Values.Count(v => v.Contains(student.Number));
        }

        private int GetReviewsCollectedCount(PascaSessionSetting session, AnonymizedPerson student)
        {
            var reviewers = session.SessionAnonymizedReviewers();

            var count = 0;
            List<int> reviewerNumbers;
            if (!session.SessionMapping().TryGetValue(student.Number, out reviewerNumbers))
            {
                return 0;
            }
            for (int i = 0; i < reviewerNumbers.Count; i++)
            {
                var reviewer = reviewers.With(reviewerNumbers[i]);
                if (reviewer == null)
                {
                    Tracer.WriteLine(TracerTags.OneNote, $"Reviewer with number {reviewerNumbers[i]} does not exist in session reviewers");
                    count++;
                    continue;
                }

                var reviewId = Notebook.GetSection(
                    reviewer.FullName,
                    TeacherOnly,
                    Pasca,
                    session.SessionName(),
                    student.FullName,
                    session.SessionReviewArtifactsSectionGroupName())
                    ?.GetPage(
                        session.SessionReviewTemplatePageName())
                        ?.Id();

                using (_progressProvider.MakeStep(i / (double)reviewerNumbers.Count))
                {
                    if (reviewId != null &&
                        IThis.CheckReviewPageValid(session, reviewId).Verdict ==
                        ReviewValidationVerdict.Ok)
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        private int GetReviewsCollectedNeedCount(PascaSessionSetting session, AnonymizedPerson student)
        {
            return session.SessionMapping()[student.Number].Count;
        }

        private string GetDashboardTemplate(PascaSessionSetting session, AnonymizedPerson receiver, string pageId)
        {
            var s = _template.Dashboard
                             .Replace("{PageId}", pageId)
                             .Replace("{SessionName}", session.SessionName())
                             .Replace("{SubmissionBegin}", session.SessionSubmissionBegin().ToShortDateString())
                             .Replace("{SubmissionEnd}", session.SessionSubmissionEnd().ToShortDateString())
                             .Replace("{ReviewBegin}", session.SessionReviewBegin().ToShortDateString())
                             .Replace("{ReviewEnd}", session.SessionReviewEnd().ToShortDateString())
                             .Replace("{ArtifactDone}", GetArtifactDone(session, receiver))
                             .Replace("{ReviewsDone}", _progressProvider.MakeStepAndReturn(
                                 () => GetReviewsDoneCount(session, receiver).ToString(), 0.2))
                             .Replace("{ReviewsNeed}", GetReviewsNeedCount(session, receiver).ToString())
                             .Replace("{ReviewsCollected}", _progressProvider.MakeStepAndReturn(
                                 () => GetReviewsCollectedCount(session, receiver).ToString(), 0.4))
                             .Replace("{ReviewsNeedCollected}", GetReviewsCollectedNeedCount(session, receiver).ToString())
                             .Replace("{ShadingSubmissionBegin}", CreateSubmissionBeginShading(session))
                             .Replace("{ShadingSubmissionEnd}", CreateSubmissionEndShading(session, receiver))
                             .Replace("{ShadingReviewBegin}", CreateReviewBeginShading(session))
                             .Replace("{ShadingReviewEnd}", _progressProvider.MakeStepAndReturn(
                                 () => CreateReviewEndShading(session, receiver), 0.6))
                             .Replace("{ShadingArtifactDone}", CreateArtifactDoneShading(session, receiver))
                             .Replace("{ShadingReviewsDone}", _progressProvider.MakeStepAndReturn(
                                 () => CreateReviewEndShading(session, receiver), 0.8))
                             .Replace("{ShadingReviewsNeedCollected}", _progressProvider.MakeStepAndReturn(
                                 () => CreateShadingReviewsNeedCollected(session, receiver), 1.0));
            return s;
        }

        #endregion

        #region Reports

        private string GetArtifactsReportPageContent(string pageId, PascaSessionSetting session)
        {
            var s = _template.ArtifactsReport;
            var rows = string.Empty;
            foreach (var author in session.SessionAnonymizedAuthors())
            {
                var section = Notebook.GetSection(
                    session.SessionArtifactSectionName(),
                    TeacherOnly,
                    Pasca,
                    session.SessionName(),
                    author.FullName);
                rows += _template.Row2
                                 .Replace("{Content1}", author.FullName)
                                 .Replace(
                                     "{Content2}",
                                     section == null
                                         ? "Not done"
                                         : Notebook.CreateSectionLink(
                                             author.FullName,
                                             section.Id(),
                                             TeacherOnly,
                                             Pasca,
                                             session.SessionName(),
                                             author.FullName,
                                             session.SessionArtifactSectionName()));
            }
            return s
                    .Replace("{Title}", session.SessionArtifactsReportPageName())
                    .Replace("{Rows}", rows)
                    .Replace("{PageId}", pageId);
        }

        private string GetAuthorsReviewsReportPageContent(string pageId, PascaSessionSetting session)
        {
            var reviewers = session.SessionAnonymizedReviewers();

            var s = _template.AuthorsReviewsReport;
            var rows = string.Empty;
            foreach (var author in session.SessionAnonymizedAuthors())
            {
                foreach (var reviewerNumber in session.SessionMapping()[author.Number])
                {
                    var reviewer = reviewers.With(reviewerNumber);
                    if (reviewer == null)
                    {
                        Tracer.WriteLine(TracerTags.OneNote, $"Reviewer with number {reviewerNumber} does not exist in session reviewers");
                        continue;
                    }

                    var section = Notebook.GetSection(
                        reviewer.FullName,
                        TeacherOnly,
                        Pasca,
                        session.SessionName(),
                        author.FullName,
                        session.SessionReviewArtifactsSectionGroupName());
                    var reviewPageId = section?.GetPage(session.SessionReviewTemplatePageName());
                    if (section == null || reviewPageId == null)
                    {
                        // todo: separate templates for exceptional cases
                        rows += _template.Row4
                                         .Replace("{Title}", session.SessionAuthorsReviewsReportPageName())
                                         .Replace("{Content1}", author.FullName)
                                         .Replace("{Content2}", reviewer.FullName)
                                         .Replace("{Content3}", "Not Done")
                                         .Replace("{Content4}", string.Empty);
                    }
                    else
                    {
                        rows += _template.Row4
                                         .Replace("{Content1}", author.FullName)
                                         .Replace("{Content2}", reviewer.FullName)
                                         .Replace(
                                             "{Content3}",
                                             Notebook.CreateSectionLink(
                                                 reviewer.FullName,
                                                 section.Id(),
                                                 TeacherOnly,
                                                 Pasca,
                                                 session.SessionName(),
                                                 author.FullName,
                                                 session.SessionReviewArtifactsSectionGroupName(),
                                                 reviewer.FullName))
                                         .Replace("{Content4}", GetGrade(session, author, reviewer).ToString());
                    }
                }
            }
            return s
                    .Replace("{Title}", session.SessionAuthorsReviewsReportPageName())
                    .Replace("{Rows}", rows)
                    .Replace("{PageId}", pageId);
        }

        private string GetReviewersReviewsReportPageContent(string pageId, PascaSessionSetting session)
        {
            var authors = session.SessionAnonymizedAuthors();

            var s = _template.ReviewersReviewsReport;
            var rows = string.Empty;
            foreach (var reviewer in session.SessionAnonymizedAuthors())
            {
                foreach (var authorNumber in session.SessionMapping()
                                              .Where(kv => kv.Value.Contains(reviewer.Number))
                                              .Select(kv => kv.Key))
                {
                    var author = authors.With(authorNumber);
                    if (reviewer == null)
                    {
                        Tracer.WriteLine(TracerTags.OneNote, $"Reviewer with number {authorNumber} does not exist in session reviewers");
                        continue;
                    }

                    var section = Notebook.GetSection(
                        reviewer.FullName,
                        TeacherOnly,
                        Pasca,
                        session.SessionName(),
                        author.FullName,
                        session.SessionReviewArtifactsSectionGroupName());
                    var reviewPageId = section?.GetPage(session.SessionReviewTemplatePageName());
                    if (section == null || reviewPageId == null)
                    {
                        // todo: separate templates for exceptional cases
                        rows += _template.Row4
                                         .Replace("{Content1}", reviewer.FullName)
                                         .Replace("{Content2}", author.FullName)
                                         .Replace("{Content3}", "Not Done")
                                         .Replace("{Content4}", string.Empty);
                    }
                    else
                    {
                        rows += _template.Row4
                                         .Replace("{Content1}", reviewer.FullName)
                                         .Replace("{Content2}", author.FullName)
                                         .Replace(
                                             "{Content3}",
                                             Notebook.CreateSectionLink(
                                                 reviewer.FullName,
                                                 section.Id(),
                                                 TeacherOnly,
                                                 Pasca,
                                                 session.SessionName(),
                                                 author.FullName,
                                                 session.SessionReviewArtifactsSectionGroupName(),
                                                 reviewer.FullName))
                                         .Replace("{Content4}", GetGrade(session, author, reviewer).ToString());
                    }
                }
            }
            return s
                    .Replace("{Title}", session.SessionReviewersReviewsReportPageName())
                    .Replace("{Rows}", rows)
                    .Replace("{PageId}", pageId);
        }

        private string GetGradesReportPageContent(string pageId, PascaSessionSetting session)
        {
            var s = _template.GradesReport;
            var rows = string.Empty;
            foreach (var author in session.SessionAnonymizedAuthors())
            {
                rows += _template.Row2
                                 .Replace("{Content1}", author.FullName)
                                 .Replace("{Content2}", GetGrade(session, author).ToString());
            }
            return s
                    .Replace("{Title}", session.SessionGradesReportPageName())
                    .Replace("{Rows}", rows)
                    .Replace("{PageId}", pageId);
        }

        #endregion
    }
}