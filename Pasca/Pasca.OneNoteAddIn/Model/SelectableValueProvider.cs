﻿using System;
using System.Threading.Tasks;
using Pasca.Common.Exceptions;
using Pasca.Gui.Facade;

namespace Pasca.OneNoteAddIn.Model
{
    public class SelectableValueProvider<T> : ISelectableValueProvider<T>
    {
        public delegate T SelectAction();

        public SelectableValueProvider(SelectAction selectAction, Func<T, T> getVisibleFunc = null)
        {
            Assert.NotNull(selectAction);

            _selectAction = selectAction;
            _getVisibleFunc = getVisibleFunc ?? (s => s);
        }

        public T TrySelect()
        {
            return _selectAction();
        }

        public T GetVisibleData(T data)
        {
            return _getVisibleFunc(data);
        }

        private readonly Func<T, T> _getVisibleFunc;

        private readonly SelectAction _selectAction;
    }
}