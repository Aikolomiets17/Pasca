﻿using System;
using System.Collections.Generic;
using Pasca.Common.Exceptions;
using Pasca.Common.Providers;
using Pasca.Common.Students;
using Pasca.OneNoteAddIn.Settings;

namespace Pasca.OneNoteAddIn.Model
{
    public sealed class GradesProvider : IGradesProvider
    {
        private GradesProvider(Func<Dictionary<Person, double>> getter)
        {
            Assert.NotNull(getter);

            _getter = getter;
        }

        public Dictionary<Person, double> GetFinalGrades()
        {
            return _getter();
        }

        private Func<Dictionary<Person, double>> _getter;

        public sealed class Factory
        {
            public GradesProvider Create(PascaSessionSetting sessionSetting, IOneNoteSession session)
            {
                return new GradesProvider(() => CreateDictionary(sessionSetting, session));
            }

            private Dictionary<Person, double> CreateDictionary(
                PascaSessionSetting sessionSetting,
                IOneNoteSession session)
            {
                var dict = new Dictionary<Person, double>();
                foreach (var author in sessionSetting.SessionAnonymizedAuthors())
                {
                    dict.Add(author, session.GetGrade(sessionSetting, author));
                }
                return dict;
            }
        }
    }
}