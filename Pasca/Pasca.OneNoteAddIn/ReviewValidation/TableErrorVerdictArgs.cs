namespace Pasca.OneNoteAddIn.ReviewValidation
{
    public class TableErrorVerdictArgs : ReviewValidationVerdictArgs
    {
        public TableErrorVerdictArgs(int x, int y)
            : base(ReviewValidationVerdict.TableError)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }
    }
}