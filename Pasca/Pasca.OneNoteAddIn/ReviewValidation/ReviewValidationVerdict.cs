﻿namespace Pasca.OneNoteAddIn.ReviewValidation
{
    public enum ReviewValidationVerdict
    {
        Ok,
        PageNotFound,
        TableNotFound,
        TableError
    }
}