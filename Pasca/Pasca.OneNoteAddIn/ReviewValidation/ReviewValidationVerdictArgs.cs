﻿namespace Pasca.OneNoteAddIn.ReviewValidation
{
    public class ReviewValidationVerdictArgs
    {
        public ReviewValidationVerdictArgs(ReviewValidationVerdict verdict)
        {
            Verdict = verdict;
        }

        public ReviewValidationVerdict Verdict { get; }
    }
}