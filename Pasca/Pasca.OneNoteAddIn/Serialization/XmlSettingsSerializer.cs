﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using Pasca.Common.Algorithm.Criteria;
using Pasca.Common.Settings;
using Pasca.Common.Students;
using Pasca.Common.Types;

namespace Pasca.OneNoteAddIn.Serialization
{
    public class XmlSettingsSerializer<T> : ISerializer<T>
        where T:Setting
    {
        static XmlSettingsSerializer()
        {
            // ReSharper disable RedundantNameQualifier
            _assemblies = new[] {
                typeof(Pasca.Common.Types.ITreeItem).Assembly,
                typeof(Pasca.Gui.Extensions.ServiceProviderExtensions).Assembly,
                typeof(Pasca.OneNoteAddIn.Settings.PascaSettingName).Assembly
            };
            // ReSharper restore RedundantNameQualifier

            _settingTypes = new[] {
                typeof(Setting),
                typeof(Criterion),
                typeof(Person),
            }.SelectMany(
                t => _assemblies.SelectMany(
                    a => a.GetTypes().Where(q => t.IsAssignableFrom(q) && !q.IsAbstract && !q.ContainsGenericParameters)))
                    .ToArray();
        }
        
        public static XmlSettingsSerializer<T> Instance => Singleton.Instance;

        public void Serialize(TextWriter sw, T o)
        {
            _serializer.Serialize(sw, o);
        }

        public T Deserialize(TextReader sr)
        {
            return (T)_serializer.Deserialize(sr);
        }

        private readonly XmlSerializer _serializer = new XmlSerializer(typeof(T), _settingTypes);

        private static readonly SingletonComponent<XmlSettingsSerializer<T>> Singleton =
                new SingletonComponent<XmlSettingsSerializer<T>>(() => new XmlSettingsSerializer<T>());

        //private static readonly Type[] SettingTypes =  {
        //    typeof(Setting),
        //    typeof(ArraySetting),
        //    typeof(BooleanSetting),
        //    //typeof(ListSetting),
        //    typeof(NumericSetting),
        //    typeof(NumericWithRangeSetting),
        //    typeof(RangeSetting),
        //    typeof(SessionListSetting),
        //    typeof(StringSetting),
        //    typeof(AnonymizedPersonListSetting),
        //    typeof(PersonListSetting),
        //    typeof(IntegerListSetting),
        //    typeof(DateTimeSetting),
        //    typeof(StudentMappingSetting),
        //    typeof(AssessmentCriteriaSetting),
        //    typeof(NumericCriterion),
        //    typeof(FloatCriterion),
        //    typeof(SetCriterion),
        //    typeof(AnonymizedPerson)
        //};

        private static readonly Assembly[] _assemblies;
        private static readonly Type[] _settingTypes;
    }
}