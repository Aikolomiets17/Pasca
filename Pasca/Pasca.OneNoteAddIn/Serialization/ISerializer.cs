﻿using System.IO;

namespace Pasca.OneNoteAddIn.Serialization
{
    public interface ISerializer<T>
    {
        void Serialize(TextWriter sw, T o);
        T Deserialize(TextReader sr);
    }
}