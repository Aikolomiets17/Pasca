﻿using System;
using Pasca.Common.Settings;

namespace Pasca.OneNoteAddIn
{
    // todo: use it
    public class SettingsChangedSubscription
    {
        public event Action<Setting, Setting> SettingsChanged = delegate { };
    }
}